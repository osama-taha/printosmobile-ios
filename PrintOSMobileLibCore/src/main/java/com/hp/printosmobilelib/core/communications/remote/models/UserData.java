package com.hp.printosmobilelib.core.communications.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A model representing the Response of the {@link LoginService#login(String, UserCredentials)} endpoint.
 *
 * @author Osama Taha
 */
public class UserData {

    @JsonProperty("context")
    private Context context;
    @JsonProperty("type")
    private String type;
    @JsonProperty("user")
    private User user;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The context
     */
    @JsonProperty("context")
    public Context getContext() {
        return context;
    }

    /**
     * @param context The context
     */
    @JsonProperty("context")
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserData{");
        sb.append("context=").append(context);
        sb.append(", type='").append(type).append('\'');
        sb.append(", user=").append(user);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class User implements Serializable {

        @JsonProperty("primaryPhone")
        private String primaryPhone;
        @JsonProperty("lastName")
        private String lastName;
        @JsonProperty("invalidAttempts")
        private Integer invalidAttempts;
        @JsonProperty("createdTime")
        private Long createdTime;
        @JsonProperty("userId")
        private String userId;
        @JsonProperty("locked")
        private Boolean locked;
        @JsonProperty("displayName")
        private String displayName;
        @JsonProperty("firstName")
        private String firstName;
        @JsonProperty("primaryEmail")
        private String primaryEmail;
        @JsonProperty("expireTime")
        private String expireTime;
        @JsonProperty("lastLoggedInTime")
        private Long lastLoggedInTime;
        @JsonProperty("hpidId")
        private String hpidId;
        @JsonProperty("primaryAddress")
        private Address address;
        @JsonProperty("eulaVersion")
        private String eulaVersion;
        @JsonProperty("needToAcceptEula")
        private boolean needToAcceptEula;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("eulaVersion")
        public String getEulaVersion() {
            return eulaVersion;
        }

        @JsonProperty("eulaVersion")
        public void setEulaVersion(String eulaVersion) {
            this.eulaVersion = eulaVersion;
        }

        @JsonProperty("needToAcceptEula")
        public boolean isNeedToAcceptEula() {
            return needToAcceptEula;
        }

        @JsonProperty("needToAcceptEula")
        public void setNeedToAcceptEula(boolean needToAcceptEula) {
            this.needToAcceptEula = needToAcceptEula;
        }

        /**
         * @return The primaryPhone
         */
        @JsonProperty(value = "primaryPhone", required = false)
        public String getPrimaryPhone() {
            return primaryPhone;
        }

        /**
         * @param primaryPhone The expireTime
         */
        @JsonProperty(value = "primaryPhone", required = false)
        public void setPrimaryPhone(String primaryPhone) {
            this.primaryPhone = primaryPhone;
        }

        @JsonProperty("primaryEmail")
        public String getPrimaryEmail() {
            return primaryEmail;
        }

        @JsonProperty("primaryEmail")
        public void setPrimaryEmail(String primaryEmail) {
            this.primaryEmail = primaryEmail;
        }

        @JsonProperty("hpidId")
        public void setHpidId(String hpidId) {
            this.hpidId = hpidId;
        }

        @JsonProperty("hpidId")
        public String getHpidId() {
            return hpidId;
        }

        @JsonProperty("lastLoggedInTime")
        public void setLastLoggedInTime(Long lastLoggedInTime) {
            this.lastLoggedInTime = lastLoggedInTime;
        }

        @JsonProperty("lastLoggedInTime")
        public Long getLastLoggedInTime() {
            return lastLoggedInTime;
        }

        /**
         * @return The expireTime
         */
        @JsonProperty("expireTime")
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * @param expireTime The expireTime
         */
        @JsonProperty("expireTime")
        public void setExpireTime(String expireTime) {
            this.expireTime = expireTime;
        }

        /**
         * @return The lastName
         */
        @JsonProperty("lastName")
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        @JsonProperty("lastName")
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The invalidAttempts
         */
        @JsonProperty("invalidAttempts")
        public Integer getInvalidAttempts() {
            return invalidAttempts;
        }

        /**
         * @param invalidAttempts The invalidAttempts
         */
        @JsonProperty("invalidAttempts")
        public void setInvalidAttempts(Integer invalidAttempts) {
            this.invalidAttempts = invalidAttempts;
        }

        /**
         * @return The createdTime
         */
        @JsonProperty("createdTime")
        public Long getCreatedTime() {
            return createdTime;
        }

        /**
         * @param createdTime The createdTime
         */
        @JsonProperty("createdTime")
        public void setCreatedTime(Long createdTime) {
            this.createdTime = createdTime;
        }

        /**
         * @return The userId
         */
        @JsonProperty("userId")
        public String getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        @JsonProperty("userId")
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         * @return The locked
         */
        @JsonProperty("locked")
        public Boolean getLocked() {
            return locked;
        }

        /**
         * @param locked The locked
         */
        @JsonProperty("locked")
        public void setLocked(Boolean locked) {
            this.locked = locked;
        }

        /**
         * @return The displayName
         */
        @JsonProperty("displayName")
        public String getDisplayName() {
            return displayName;
        }

        /**
         * @param displayName The displayName
         */
        @JsonProperty("displayName")
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        /**
         * @return The firstName
         */
        @JsonProperty("firstName")
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        @JsonProperty("firstName")
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonProperty("primaryAddress")
        public Address getAddress() {
            return address;
        }

        @JsonProperty("primaryAddress")
        public void setAddress(Address address) {
            this.address = address;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("User{");
            sb.append("expireTime='").append(expireTime).append('\'');
            sb.append(", lastName='").append(lastName).append('\'');
            sb.append(", invalidAttempts=").append(invalidAttempts);
            sb.append(", createdTime=").append(createdTime);
            sb.append(", userId='").append(userId).append('\'');
            sb.append(", locked=").append(locked);
            sb.append(", displayName='").append(displayName).append('\'');
            sb.append(", firstName='").append(firstName).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Address implements Serializable {

        @JsonProperty("addressLine1")
        private String addressLine1;
        @JsonProperty("addressLine2")
        private String addressLine2;
        @JsonProperty("locality")
        private String locality;
        @JsonProperty("region")
        private String region;
        @JsonProperty("country")
        private String country;
        @JsonProperty("postalCode")
        private String postalCode;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("addressLine1")
        public String getAddressLine1() {
            return addressLine1;
        }

        @JsonProperty("addressLine1")
        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        @JsonProperty("addressLine2")
        public String getAddressLine2() {
            return addressLine2;
        }

        @JsonProperty("addressLine2")
        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        @JsonProperty("locality")
        public String getLocality() {
            return locality;
        }

        @JsonProperty("locality")
        public void setLocality(String locality) {
            this.locality = locality;
        }

        @JsonProperty("region")
        public String getRegion() {
            return region;
        }

        @JsonProperty("region")
        public void setRegion(String region) {
            this.region = region;
        }

        @JsonProperty("postalCode")
        public String getPostalCode() {
            return postalCode;
        }

        @JsonProperty("postalCode")
        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        @JsonProperty("country")
        public String getCountry() {
            return country;
        }

        @JsonProperty("country")
        public void setCountry(String country) {
            this.country = country;
        }


        public void setAdditionalProperties(Map<String, Object> additionalProperties) {
            this.additionalProperties = additionalProperties;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "type",
            "id",
            "name"
    })
    public static class Context implements Serializable {

        @JsonProperty("type")
        private String type;
        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The type
         */
        @JsonProperty("type")
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The id
         */
        @JsonProperty("id")
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Context{");
            sb.append("type='").append(type).append('\'');
            sb.append(", id='").append(id).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }

    }
}