package com.hp.printosmobilelib.core.communications.remote;

import android.content.Context;
import android.content.SharedPreferences;

import com.hp.printosmobilelib.core.communications.remote.models.UserData;

/*
 * A Singleton preferences class to manage storing/retrieving the data.
 *
 * @author Osama Taha
 */
public class Preferences {

    /**
     * Desired preferences file name.
     */
    private static final String FILE_NAME_EXTENSION = "_preferences";
    public static final String KEY_ORGANIZATION = "LoggedInUserOrganizationId";
    public static final String KEY_ORGANIZATION_NAME = "LoggedInUserOrganizationName";
    public static final String KEY_ORGANIZATION_TYPE = "LoggedInUserOrganizationType";
    private static final String KEY_USER_ID = "LoggedInUserIdentifier";
    private static final String KEY_USER_TYPE = "LoggedInUserType";

    private static Preferences sSharedPrefs;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Preferences(Context context) {
        preferences = context.getSharedPreferences(context.getPackageName() + FILE_NAME_EXTENSION, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static Preferences getInstance(Context context) {

        if (sSharedPrefs == null) {
            sSharedPrefs = new Preferences(context.getApplicationContext());
        }

        return sSharedPrefs;
    }

    public static String getFileName(Context context) {
        return context.getPackageName() + FILE_NAME_EXTENSION;
    }

    /**
     * A method for storing String value.
     * <p/>
     *
     * @param key The name of the preference to store.
     * @param val The new value for the preference.
     */
    public void put(String key, String val) {
        editor.putString(key, val);
    }

    /**
     * A method for storing int value.
     * <p/>
     *
     * @param key The name of the preference to store.
     * @param val The new value for the preference.
     */
    public void put(String key, int val) {
        editor.putInt(key, val);
    }

    /**
     * A method for storing boolean value.
     * <p/>
     *
     * @param key The name of the preference to store.
     * @param val The new value for the preference.
     */
    public void put(String key, boolean val) {
        editor.putBoolean(key, val);
    }

    /**
     * A method for storing float value.
     * <p/>
     *
     * @param key The name of the preference to store.
     * @param val The new value for the preference.
     */
    public void put(String key, float val) {
        editor.putFloat(key, val);
    }

    /**
     * A method for storing long value.
     * <p/>
     *
     * @param key The name of the preference to store.
     * @param val The new value for the preference.
     */
    public void put(String key, long val) {
        editor.putLong(key, val);
    }

    /**
     * Retrieve a String value from the preferences.
     * <p/>
     *
     * @param key          The name of the preference to retrieve.
     * @param defaultValue Value to return if this preference does not exist.
     */
    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    public int getInt(String key) {
        return preferences.getInt(key, 0);
    }

    /**
     * Retrieve an integer value from the preferences.
     * <p/>
     *
     * @param key          The name of the preference to retrieve.
     * @param defaultValue Value to return if this preference does not exist.
     */
    public int getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }

    public long getLong(String key) {
        return preferences.getLong(key, 0);
    }

    /**
     * Retrieve a long value from the preferences.
     * <p/>
     *
     * @param key          The name of the preference to retrieve.
     * @param defaultValue Value to return if this preference does not exist.
     */
    public long getLong(String key, long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }

    public float getFloat(String key) {
        return preferences.getFloat(key, 0);
    }

    /**
     * Retrieve a float value from the preferences.
     * <p/>
     *
     * @param key          The name of the preference to retrieve.
     * @param defaultValue Value to return if this preference does not exist.
     */
    public float getFloat(String key, float defaultValue) {
        return preferences.getFloat(key, defaultValue);
    }

    /**
     * Retrieve a boolean value from the preferences.
     * <p/>
     *
     * @param key          The name of the preference to retrieve.
     * @param defaultValue Value to return if this preference does not exist.
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    /**
     * Remove keys from SharedPreferences.
     *
     * @param keys The name of the key(s) to be removed.
     */
    public void remove(String... keys) {
        for (String key : keys) {
            editor.remove(key);
        }
        commit();
    }

    /**
     * Remove all keys from SharedPreferences.
     */
    public void clear() {
        editor.clear();
        commit();
    }

    /**
     * commit all changes
     */
    public void commit() {
        editor.commit();
    }

    public void saveOrganization(UserData.Context organization) {

        put(KEY_ORGANIZATION, organization.getId());
        put(KEY_ORGANIZATION_NAME, organization.getName());
        put(KEY_ORGANIZATION_TYPE, organization.getType());
        commit();

    }

    public void clearOrganization() {
        put(KEY_ORGANIZATION, "");
        put(KEY_ORGANIZATION_NAME, "");
        put(KEY_ORGANIZATION_TYPE, "");
        commit();
    }

    public void saveUserType(String userType) {
        put(KEY_USER_TYPE, userType);
        commit();
    }

    public String getUserType() {
        return getString(KEY_USER_TYPE, "");
    }

    public String getSavedOrganizationId() {
        return getString(Preferences.KEY_ORGANIZATION, "");
    }

    public UserData.Context getSelectedOrganization() {

        UserData.Context organization = new UserData.Context();
        organization.setId(getString(Preferences.KEY_ORGANIZATION, ""));
        organization.setName(getString(Preferences.KEY_ORGANIZATION_NAME, ""));
        organization.setType(getString(Preferences.KEY_ORGANIZATION_TYPE, ""));

        return organization;
    }

    public boolean containsKey(String key) {
        return preferences.contains(key);
    }

    public void saveUserId(String userId) {
        put(KEY_USER_ID, userId);
        commit();
    }

    public String getUserId() {
        return getString(KEY_USER_ID, "");
    }

}