package com.hp.printosmobile.presentation.modules.shared;

/**
 * Created by Osama on 3/18/18.
 */
public enum LanguageEnum {

    EN("en-US", "en"),
    FR("fr-FR", "fr"),
    IT("it-IT", "it"),
    DE("de-DE", "ge"),
    JP("ja-JP", "jp"),
    ZH("zh-HANS", "ch"),
    KO("ko-KO", "kor"),
    RU("ru-RU", "russ"),
    PT("pt-PT", "por"),
    ES("es-ES", "sp");

    private final String appLangCode;
    private final String kzLangCode;

    LanguageEnum(String appLangCode, String kzLangCode) {
        this.appLangCode = appLangCode;
        this.kzLangCode = kzLangCode;
    }

    public String getAppLangCode() {
        return appLangCode;
    }

    public String getKzLanguageCode() {
        return kzLangCode;
    }

    public static LanguageEnum from(String appLangCode) {

        if (appLangCode == null) {
            return EN;
        }

        for (LanguageEnum language : LanguageEnum.values()) {
            if (language.getAppLangCode().equalsIgnoreCase(appLangCode))
                return language;
        }

        return EN;
    }

}
