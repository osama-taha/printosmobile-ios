package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah 5/30/2018
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RankBreakdownData {

    @JsonProperty("ranks")
    List<RankData> rankDataList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("ranks")
    public List<RankData> getRankDataList() {
        return rankDataList;
    }

    @JsonProperty("ranks")
    public void setRankDataList(List<RankData> rankDataList) {
        this.rankDataList = rankDataList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RankBreakdownData{");
        sb.append("rankDataList=").append(rankDataList);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class RankData {

        @JsonProperty("type")
        private String type;
        @JsonProperty("name")
        private String name;
        @JsonProperty("history")
        private List<History> historyList;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("type")
        public String getType() {
            return type;
        }

        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("history")
        public List<History> getHistoryList() {
            return historyList;
        }

        @JsonProperty("history")
        public void setHistoryList(List<History> historyList) {
            this.historyList = historyList;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("RankData{");
            sb.append("type='").append(type).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", historyList=").append(historyList);
            sb.append('}');
            return sb.toString();
        }
    }


    public static class History {

        @JsonProperty("isLowerThanThreshold")
        private boolean isLowerThanThreshold;
        @JsonProperty("isLowerThanTotalThreshold")
        private boolean isLowerThanTotalThreshold;
        @JsonProperty("isTop")
        private boolean isTop;
        @JsonProperty("lowerThirdValue")
        private int lowerThirdValue;
        @JsonProperty("name")
        private String name;
        @JsonProperty("offset")
        private Integer offset;
        @JsonProperty("position")
        private Integer position;
        @JsonProperty("total")
        private int total;
        @JsonProperty("week")
        private int week;
        @JsonProperty("year")
        private int year;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("isLowerThanThreshold")
        public boolean isLowerThanThreshold() {
            return isLowerThanThreshold;
        }

        @JsonProperty("isLowerThanThreshold")
        public void setLowerThanThreshold(boolean lowerThanThreshold) {
            isLowerThanThreshold = lowerThanThreshold;
        }

        @JsonProperty("isLowerThanTotalThreshold")
        public boolean isLowerThanTotalThreshold() {
            return isLowerThanTotalThreshold;
        }

        @JsonProperty("isLowerThanTotalThreshold")
        public void setLowerThanTotalThreshold(boolean lowerThanTotalThreshold) {
            isLowerThanTotalThreshold = lowerThanTotalThreshold;
        }

        @JsonProperty("isTop")
        public boolean isTop() {
            return isTop;
        }

        @JsonProperty("isTop")
        public void setTop(boolean top) {
            isTop = top;
        }

        @JsonProperty("lowerThirdValue")
        public int getLowerThirdValue() {
            return lowerThirdValue;
        }

        @JsonProperty("lowerThirdValue")
        public void setLowerThirdValue(int lowerThirdValue) {
            this.lowerThirdValue = lowerThirdValue;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("offset")
        public Integer getOffset() {
            return offset;
        }

        @JsonProperty("offset")
        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        @JsonProperty("position")
        public Integer getPosition() {
            return position;
        }

        @JsonProperty("position")
        public void setPosition(Integer position) {
            this.position = position;
        }

        @JsonProperty("total")
        public int getTotal() {
            return total;
        }

        @JsonProperty("total")
        public void setTotal(int total) {
            this.total = total;
        }

        @JsonProperty("week")
        public int getWeek() {
            return week;
        }

        @JsonProperty("week")
        public void setWeek(int week) {
            this.week = week;
        }

        @JsonProperty("year")
        public int getYear() {
            return year;
        }

        @JsonProperty("year")
        public void setYear(int year) {
            this.year = year;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("History{");
            sb.append("isLowerThanThreshold=").append(isLowerThanThreshold);
            sb.append(", isLowerThanTotalThreshold=").append(isLowerThanTotalThreshold);
            sb.append(", isTop=").append(isTop);
            sb.append(", lowerThirdValue=").append(lowerThirdValue);
            sb.append(", name='").append(name).append('\'');
            sb.append(", offset=").append(offset);
            sb.append(", position=").append(position);
            sb.append(", total=").append(total);
            sb.append(", week=").append(week);
            sb.append(", year=").append(year);
            sb.append('}');
            return sb.toString();
        }
    }
}

