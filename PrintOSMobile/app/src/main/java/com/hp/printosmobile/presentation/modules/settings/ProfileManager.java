package com.hp.printosmobile.presentation.modules.settings;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.ProfileImageInfo;
import com.hp.printosmobile.data.remote.services.ProfileService;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 10/8/2017.
 */

public class ProfileManager {

    private static final String IMAGE_FILE_TAG = "file";
    private static final String IMAGE_FILE_ATTACHMENT_NAME = "blob";
    private static final String MIME_IMAGE = "image/JPEG";

    private ProfileManager() {

    }


    public static Observable<String> getProfileImageInfo() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ProfileService profileService = serviceProvider.getProfileService();

        return profileService.getProfileInfo().map(new Func1<Response<ProfileImageInfo>, String>() {
            @Override
            public String call(Response<ProfileImageInfo> profileImageInfoResponse) {
                if (profileImageInfoResponse != null && profileImageInfoResponse.isSuccessful()) {
                    ProfileImageInfo profileImageInfo = profileImageInfoResponse.body();
                    if (profileImageInfo != null && profileImageInfo.getLinks() != null
                            && profileImageInfo.getLinks().getImage() != null) {
                        return profileImageInfo.getLinks().getImage().getImageUrl();
                    }
                }
                return null;
            }
        });
    }

    public static Observable<ResponseBody> uploadProfileImage(Context context, Uri profileImageUri) {

        String filePath = getRealPathFromURIPath(profileImageUri, context);
        File file = new File(filePath);

        RequestBody mFile = RequestBody.create(MediaType.parse(MIME_IMAGE), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData(IMAGE_FILE_TAG, IMAGE_FILE_ATTACHMENT_NAME, mFile);

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ProfileService profileService = serviceProvider.getProfileService();

        return profileService.uploadProfileImage(fileToUpload).map(new Func1<Response<ResponseBody>, ResponseBody>() {
            @Override
            public ResponseBody call(Response<ResponseBody> responseBodyResponse) {
                HPLogger.enableFileLogging(true);
                if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                    return responseBodyResponse.body();
                }
                return null;
            }
        });
    }

    public static Observable<ResponseBody> deleteProfile() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ProfileService profileService = serviceProvider.getProfileService();

        return profileService.deleteProfileImage().map(new Func1<Response<ResponseBody>, ResponseBody>() {
            @Override
            public ResponseBody call(Response<ResponseBody> responseBodyResponse) {
                HPLogger.enableFileLogging(true);
                if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                    return responseBodyResponse.body();
                }
                return null;
            }
        });
    }

    private static String getRealPathFromURIPath(Uri profileImageUri, Context context) {
        Cursor cursor = context.getContentResolver().query(profileImageUri, null, null, null, null);
        if (cursor == null) {
            return profileImageUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            cursor.close();
            return cursor.getString(idx);
        }
    }
}
