package com.hp.printosmobile.utils;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.rate.RateDialog;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

/**
 * Created by Anwar Asbah on 4/10/2017.
 */

public class RateUtils {

    private static final String TAG = RateUtils.class.getName();

    private static final String PLAY_STORE_OLD_NAME = "de.androidpit.app";
    private static final String PLAY_STORE_NEW_NAME = "com.android.vending";

    public enum RateStatusEnum {

        PENDING("pending"),
        REMIND_ME_LATER("later"),
        SUPPRESS("suppressed"),
        FEEDBACK_SENT("feedback_sent");

        String key;

        RateStatusEnum(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        public static RateStatusEnum getRateStatus(String key) {
            if (key != null) {
                for (RateStatusEnum status : RateStatusEnum.values()) {
                    if (key.equals(status.key)) {
                        return status;
                    }
                }
            }
            return PENDING;
        }
    }

    private static final int MIN_NUMBER_OF_AUTHENTICATED_SESSIONS = 2;

    private static boolean isShowing = false;

    public static boolean displayRatingDialog(Activity context) {

        HPLogger.d(TAG, "Should display rating dialog");

        if (!googlePlayStoreInstalled(context) || isShowing) {
            return false;
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(context);

        String currentVersion = BuildConfig.VERSION_NAME.trim();
        String prevVersion = printOSPreferences.getAppVersion();

        HPLogger.d(TAG, "currentVersion: " + currentVersion + " previous: " + prevVersion);

        String statusKey = printOSPreferences.getRateStatus();
        RateStatusEnum rateStatusEnum = RateStatusEnum.getRateStatus(statusKey);

        if (currentVersion.equals(prevVersion)) {

            HPLogger.d(TAG, "Rating status: " + rateStatusEnum.getKey());

            if (rateStatusEnum != RateStatusEnum.SUPPRESS) {

                int numberOfDays = printOSPreferences.getRateDialogNumberOfDaysSinceLastShown();
                HPLogger.d(TAG, "number of days since last shown: " + numberOfDays);

                int recurringPeriod = printOSPreferences.getRateDialogRecurringPeriod();
                if (numberOfDays >= recurringPeriod) {
                    PrintOSPreferences.getInstance(context).setRateDialogDateShown();
                    showDialog(context);
                    return true;
                }
            }

        } else {

            HPLogger.d(TAG, "First launch for this app version.");
            //new launch reset param
            printOSPreferences.setAppVersion(currentVersion);
        }
        return false;
    }

    private static void showDialog(final Activity context) {

        HPLogger.d(TAG, "show Rating dialog");
        Analytics.sendEvent(Analytics.EVENT_RATE_RATE_DIALOG_SHOWN);

        try {
            RateDialog.getInstance(new RateDialog.RateDialogCallback() {
                @Override
                public void onDismiss() {
                    isShowing = false;

                    if (context instanceof MainActivity) {
                        ((MainActivity) context).onDialogDismissed();
                    }
                }
            }).show(context.getFragmentManager(), RateDialog.TAG);
            isShowing = true;
        } catch (Exception e) {
            isShowing = false;
        }

    }

    public static boolean googlePlayStoreInstalled(Activity activity) {

        PackageManager packageManager = activity.getApplication().getPackageManager();
        List<PackageInfo> packages = packageManager.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);

        for (PackageInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(PLAY_STORE_OLD_NAME)
                    || packageInfo.packageName.equals(PLAY_STORE_NEW_NAME)) {
                return PrintOSApplication.isGooglePlayServicesEnabled();
            }
        }

        return false;
    }
}
