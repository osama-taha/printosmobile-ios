package com.hp.printosmobile.presentation.modules.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.modules.eula.EULAViewModel;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.presentation.modules.hiddensettings.HiddenSettingsActivity;
import com.hp.printosmobile.presentation.modules.hpidpopup.HPIDPopup;
import com.hp.printosmobile.presentation.modules.resetpassword.ResetPasswordActivity;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.AAAError;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.EncryptionUtils;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * Login Screen activity
 *
 * @author Osama Taha
 */
public class LoginActivity extends BaseActivity implements LoginView {

    public static final String TAG = LoginActivity.class.getName();
    private static final String SETTINGS = "settings";

    @Bind(R.id.login_layout)
    View loginLayout;
    @Bind(R.id.edit_text_username)
    EditText usernameEditText;
    @Bind(R.id.edit_text_password)
    EditText passwordEditText;
    @Bind(R.id.button_login)
    TextView loginButton;
    @Bind(R.id.progress_loading)
    ProgressBar loadingView;
    @Bind(R.id.server_info_layout)
    View serverInfoLayout;
    @Bind(R.id.server_name_text)
    TextView serverNameTextView;
    @Bind(R.id.register_to_printos_button)
    TextView registerButton;
    @Bind(R.id.forgot_password_button)
    TextView forgotPasswordButton;
    @Bind(R.id.button_demo)
    TextView demoButton;
    @Bind(R.id.hpid_tooltip_layout)
    View hpidLayout;

    private LoginPresenter presenter;
    private EULAViewModel eulaModel;
    private boolean demoButtonClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        demoButtonClicked = false;
        presenter = new LoginPresenter();
        presenter.attachView(this);

        IntercomSdk.getInstance(this).logout();
        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_LOGIN_EVENT);

        init();

        loginLayout.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        loginLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                        requestNeededPermissions();
                    }
                });

        PrintOSPreferences.getInstance(this).setVersionCheckingTime(0);
    }

    private void init() {

        String serverUrl = PrintOSPreferences.getInstance(this).getServerUrl();
        String dailyUrl = getString(R.string.daily_server);
        String pressQAUrl = getString(R.string.pressqa_server);
        String productionServer = getString(R.string.production_server);

        UserCredentials userCredentials = SessionManager.getInstance(this).getTempCredentials();
        SessionManager.getInstance(this).clearTempCredentials();

        if (TextUtils.isEmpty(userCredentials.getPassword()) ||
                TextUtils.isEmpty(userCredentials.getLogin())) {
            if (serverUrl != null) {
                if (serverUrl.equals(dailyUrl)) {
                    usernameEditText.setText(getString(R.string.daily_user));
                    passwordEditText.setText(getString(R.string.daily_password));
                } else if (serverUrl.equals(pressQAUrl)) {
                    usernameEditText.setText(getString(R.string.pressqa_user));
                    passwordEditText.setText(getString(R.string.pressqa_password));
                } else if (serverUrl.equals(productionServer)) {
                    HPUIUtils.setVisibility(true, demoButton);
                }
            }
        } else {
            usernameEditText.setText(userCredentials.getLogin());
            passwordEditText.setText(userCredentials.getPassword());
            onLoginButtonClicked();
        }

        setServerInfo();

        loginLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftKeyboard();
                }
            }
        });

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onLoginButtonClicked();
                    return true;
                }
                return false;
            }
        });
    }

    private void setServerInfo() {

        PrintOSPreferences userPreferences = PrintOSPreferences.getInstance(this);

        if (!userPreferences.isProductionStack()) {
            String serverName = userPreferences.getServerName();
            serverInfoLayout.setVisibility(View.VISIBLE);
            serverNameTextView.setText(serverName);
            HPUIUtils.setVisibility(false, demoButton);
        } else {
            HPUIUtils.setVisibility(true, demoButton);
            serverInfoLayout.setVisibility(View.GONE);
        }
    }

    private void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this, usernameEditText, passwordEditText);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void setLoginInButtonEnabled(boolean enabled) {
        loginButton.setEnabled(enabled);
    }

    @Override
    public void onLoginSuccessful(UserData userData) {

        PrintOSApplication.setStartTimeMilliSeconds(false);

        if (demoButtonClicked) {
            PrintOSPreferences.getInstance(this).setDemoModeFlag(true);
            PrintOSPreferences.getInstance(this).setDemoAccountTimestamp(System.currentTimeMillis());
            Analytics.sendEvent(Analytics.DEMO_MODE_EVENT);
        }

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);

        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.COMING_FROM_LOGIN, true);
        Navigator.openMainActivity(this, bundle);

        startVersionCheckingService();

        finish();

    }

    @Override
    public void showGeneralLoginError(APIException error) {

        Analytics.sendEvent(Analytics.LOGIN_FAILED_ACTION);

        AnswersSdk.logLogin(AnswersSdk.LOGIN_METHOD_BUTTON, false);

        AppUtils.sendLoginErrorEvent(error, Analytics.LOGIN_FAILED_ACTION);

        if (error.getAAAError() != null) {

            AAAError.SmsError smsError = error.getAAAError().getSmsError();
            AAAError.ErrorSubCode errorSubCode = smsError.getSubCode();

            if (error.getKind() == APIException.Kind.UNAUTHORIZED) {
                if (errorSubCode == AAAError.ErrorSubCode.INVALID_USER_NAME_PASSWORD) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_unauthorized), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.ACCOUNT_LOCKED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_account_is_locked), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.EULA_NOT_ACCEPTED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_eula_not_accepted), Snackbar.LENGTH_SHORT);
                    openEULAActivity();
                } else if (errorSubCode == AAAError.ErrorSubCode.PASSWORD_EXPIRED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_password_has_expired), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.ERROR_CODE_1026) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_account_not_verified), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.ERROR_CODE_1027) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_social_media), Snackbar.LENGTH_SHORT);
                } else {
                    HPUIUtils.displaySnackbar(this, loginLayout, smsError.getMessage(), Snackbar.LENGTH_SHORT);
                }
            } else if (error.getKind() == APIException.Kind.URL_NOT_FOUND_ERROR) {
                if (errorSubCode == AAAError.ErrorSubCode.ERROR_CODE_1023) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_expired_link), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.ERROR_CODE_1024) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_id_account_not_registered), Snackbar.LENGTH_SHORT);
                }
            } else if (error.getKind() == APIException.Kind.INTERNAL_SERVER_ERROR && errorSubCode == AAAError.ErrorSubCode.ERROR_CODE_1026) {
                HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_no_context), Snackbar.LENGTH_SHORT);
            }

        } else {

            HPUIUtils.displayToastException(this, error, LoginActivity.TAG, false);
        }
    }

    @Override
    public void showErrorMsg(String msg) {
        HPUIUtils.displaySnackbar(this, loginLayout, msg, Snackbar.LENGTH_SHORT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        HPLogger.d(TAG, "Activity result " + resultCode + " and code " + requestCode);

        if (requestCode == Constants.IntentExtras.EULA_ACTIVITY_REQUEST_CODE
                && resultCode == Constants.IntentExtras.EULA_ACTIVITY_RESULT_CODE) {

            boolean didAcceptEula = data.getBooleanExtra(Constants.IntentExtras.EULA_ACTIVITY_ACCEPTED_RESULT, false);

            if (didAcceptEula) {

                this.eulaModel = (EULAViewModel) data.getSerializableExtra(Constants.IntentExtras.EULA_ACTIVITY_RESULT_OBJECT);
                onLoginButtonClicked();

            } else {
                HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_eula_not_accepted), Snackbar.LENGTH_SHORT);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        versionDataGetUpdated();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.button_login)
    public void onLoginButtonClicked() {
        HPUIUtils.hideSoftKeyboard(this, usernameEditText, passwordEditText);

        String userName = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        if (!userName.isEmpty() && !password.isEmpty()) {

            Analytics.sendEvent(Analytics.EVENT_CLICK_LOGIN);

            if (!hiddenSettingShouldAppear(userName)) {
                presenter.login(userName, password, null);
            }
        }
    }

    @OnClick(R.id.button_demo)
    public void onDemoButtonClicked() {
        HPUIUtils.hideSoftKeyboard(this, usernameEditText, passwordEditText);
        demoButtonClicked = true;
        presenter.login(EncryptionUtils.decrypt(getString(R.string.demo_key_1)), EncryptionUtils.decrypt(getString(R.string.demo_key_2)), eulaModel == null ? null : eulaModel.getEulaVersion());
    }

    @OnClick(R.id.register_to_printos_button)
    public void onRegisterButtonClicked() {

        HPLogger.d(TAG, "register to printos clicked");

        Analytics.sendEvent(Analytics.REGISTER_TO_PRINTOS_CLICKED_ACTION);

        AppUtils.startApplication(this, Uri.parse(Constants.REGISTRATION_LINK));
    }

    public boolean hiddenSettingShouldAppear(String userName) {
        if (userName.equalsIgnoreCase(SETTINGS)) {
            LoginActivity.this.finish();
            Intent intent = new Intent(this, HiddenSettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @OnClick(R.id.forgot_password_button)
    public void onForgotPasswordClicked() {

        HPLogger.d(TAG, "forgot password clicked");

        Analytics.sendEvent(Analytics.FORGOT_PASS_CLICKED_ACTION);

        if (!PrintOSPreferences.getInstance(this).isHPIDFeatureEnabled()) {
            Intent intent = new Intent(this, ResetPasswordActivity.class);
            startActivity(intent);
        } else {
            String serverUrl = PrintOSPreferences.getInstance(this).getServerUrl();
            AppUtils.startApplication(this, Uri.parse(serverUrl));
        }

    }

    @OnClick(R.id.hpid_tooltip_layout)
    public void onHPIDTooltipClicked() {

        HPIDPopup hpidPopup = HPIDPopup.getInstance(new HPIDPopup.HPIDPopupCallbacks() {
            @Override
            public void onDialogDismissed() {
                //not needed.
            }
        });

        getSupportFragmentManager().beginTransaction().add(hpidPopup, HPIDPopup.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    protected void versionDataGetUpdated() {
        super.versionDataGetUpdated();
        setServerInfo();

        boolean forgotPasswordEnabled = PrintOSPreferences.getInstance(this).isForgotPasswordEnabled();
        boolean hpidEnabled = PrintOSPreferences.getInstance(this).isHPIDFeatureEnabled();

        HPUIUtils.setVisibility(forgotPasswordEnabled || hpidEnabled, forgotPasswordButton);
        HPUIUtils.setVisibilityForViews(hpidEnabled, hpidLayout);

    }

    private void openEULAActivity() {

        Intent intent = new Intent(this, EulaActivity.class);
        intent.putExtra(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, true);
        startActivityForResult(intent, Constants.IntentExtras.EULA_ACTIVITY_REQUEST_CODE);

    }

    @Override
    public void onError(APIException exception, String tag) {
        //TODO
    }
}
