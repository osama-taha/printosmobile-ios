package com.hp.printosmobile.presentation.modules.shared;

import android.text.TextUtils;

import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;

/**
 * Created by Osama Taha on 6/14/16.
 */
public class UserViewModel {

    private String userId;
    private String firstName;
    private String lastName;
    private String displayName;
    private String email;
    private String countryCode;
    private String selfProvisionOrg;
    private String userRoles;
    private OrganizationViewModel userOrganization;
    private boolean needToAcceptEula;
    private String eulaVersion;

    public String getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(String userRoles) {
        this.userRoles = userRoles;
    }

    public String getSelfProvisionOrg() {
        return selfProvisionOrg;
    }

    public void setSelfProvisionOrg(String selfProvisionOrg) {
        this.selfProvisionOrg = selfProvisionOrg;
    }

    public String getUserId() {
        return userId;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {

        firstName = !TextUtils.isEmpty(firstName) ? firstName : "";
        lastName = !TextUtils.isEmpty(lastName) ? lastName : "";

        String name = String.format("%s %s", firstName, lastName).trim();
        if (TextUtils.isEmpty(name)) {
            return !TextUtils.isEmpty(displayName) ? displayName : "";
        }

        return name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setUserOrganization(OrganizationViewModel userOrganization) {
        this.userOrganization = userOrganization;
    }

    public OrganizationViewModel getUserOrganization() {
        return userOrganization;
    }

    public boolean isNeedToAcceptEula() {
        return needToAcceptEula;
    }

    public void setNeedToAcceptEula(boolean needToAcceptEula) {
        this.needToAcceptEula = needToAcceptEula;
    }

    public String getEulaVersion() {
        return eulaVersion;
    }

    public void setEulaVersion(String eulaVersion) {
        this.eulaVersion = eulaVersion;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserViewModel{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
