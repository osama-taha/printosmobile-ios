package com.hp.printosmobile.presentation.modules.invites;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 11/6/2017.
 */

public class InviteUserRoleViewModel implements Serializable {

    private String name;
    private String id;
    private String orgType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }
}
