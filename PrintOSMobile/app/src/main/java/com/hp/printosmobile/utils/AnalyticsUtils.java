package com.hp.printosmobile.utils;

import android.content.Context;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Anwar Asbah on 11/12/2017.
 */

public class AnalyticsUtils {


    private AnalyticsUtils() {
    }

    public static void sendShareEvents(Context context, String action) {
        HPLogger.d("Share", "onShareClicked: " + action);
        if (context == null || action == null) {
            return;
        }
        Analytics.sendEvent(String.format(Analytics.EVENT_ACTION_STRING_FORMAT, getKey(action)));
        AnswersSdk.logShare(action);
    }

    private static String getKey(String key) {
        if (key == null) {
            return "";
        }

        return key.replace("-", "").replace("  ", " ").replace(" ", "_").toUpperCase();
    }

    public static void sendEvents(Context context, String analyticsAction) {
        if (context == null) {
            return;
        }

        if (analyticsAction != null) {
            Analytics.sendEvent(getKey(analyticsAction));
        }
    }
}
