package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.BeatCoinData;
import com.hp.printosmobile.data.remote.models.CoinsStoreData;
import com.hp.printosmobile.data.remote.models.CollectCoinsRequestBody;
import com.hp.printosmobile.data.remote.models.LoginBeatCoinData;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Anwar Asbah 7/18/2017
 */
public interface BeatCoinService {

    @GET(ApiConstants.NOTIFICATIONS_API)
    Observable<Response<BeatCoinData>> getBeatCoinsDetails(@Path("id") String id);

    @GET(ApiConstants.INVITES_BEAT_COINS_API)
    Observable<Response<LoginBeatCoinData>> getInviteBeatCoinsOnLogin();

    @POST(ApiConstants.COINS_BASE_URL + "api/v1/points?csapikey=a05033dfa3354586ba02f7e47443a51c")
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    Observable<Response<List<CoinsStoreData>>> createCoinsUrl(@Body List<CoinsStoreData> body);

    @POST(Constants.BeatCoinsStore.PRINT_BEAT_STORE_INSTANT_API)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    Observable<Response<List<CoinsStoreData>>> collectCoins(@Body List<CoinsStoreData> requestBody);

}
