package com.hp.printosmobile.presentation.modules.tooltip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Osama Taha on 12/17/16.
 */
public class TodayInfoTooltip extends Tooltip<TodayInfoTooltipViewModel> {

    private static final String IMPRESSION_EXTENSION = " (%s)";

    private LinearLayout impressionsListLayout;
    private View separatorView;
    private LinearLayout printedListLayout;

    public TodayInfoTooltip(Context context) {
        super(context);
    }

    @Override
    protected void initContentView() {

        impressionsListLayout = contentView.findViewById(R.id.impressions_list_layout);
        separatorView = contentView.findViewById(R.id.separator_view);
        printedListLayout = contentView.findViewById(R.id.printed_list_layout);

    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.today_info_tooltip_layout;
    }

    @Override
    public void updateView(TodayInfoTooltipViewModel todayInfoTooltipViewModel) {

        int maxLabelWidth = getInfoItemNameMaxWidth(impressionsListLayout, todayInfoTooltipViewModel);

        buildInfoItemsList(impressionsListLayout, todayInfoTooltipViewModel.getImpressionsList(), maxLabelWidth);
        buildInfoItemsList(printedListLayout, todayInfoTooltipViewModel.getPrintedList(), maxLabelWidth);

        HPUIUtils.setVisibility((!todayInfoTooltipViewModel.getImpressionsList().isEmpty()
                && !todayInfoTooltipViewModel.getPrintedList().isEmpty()), separatorView);

    }


    private void buildInfoItemsList(LinearLayout containerLayout, List<TodayInfoTooltipViewModel.InfoItemViewModel> items, int maxLabelWidth) {

        if (items == null || items.isEmpty()) {
            return;
        }

        containerLayout.removeAllViews();

        for (TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel : items) {

            View tooltipItemLayout = LayoutInflater.from(mContext).inflate(R.layout.today_info_tooltip_item_layout, containerLayout, false);
            TextView nameTextView = tooltipItemLayout.findViewById(R.id.item_name_text);
            TextView valueTextView = tooltipItemLayout.findViewById(R.id.item_value_text);
            containerLayout.addView(tooltipItemLayout);

            String impressionExtension = "";
            if (infoItemViewModel.getImpressionType() != null) {
                impressionExtension = String.format(IMPRESSION_EXTENSION, infoItemViewModel.getImpressionType());
            }

            nameTextView.setText(infoItemViewModel.getInfoItemName());
            valueTextView.setText(infoItemViewModel.getInfoItemValue() + impressionExtension);
            valueTextView.setTextColor(infoItemViewModel.getTextColorResourceId());
            setInfoItemNameTextViewWidth(nameTextView, maxLabelWidth);

        }
    }

    private int getInfoItemNameMaxWidth(LinearLayout containerLayout, TodayInfoTooltipViewModel todayInfoTooltipViewModel) {

        int maxWidth = 0;

        View tooltipItemLayout = LayoutInflater.from(mContext).inflate(R.layout.today_info_tooltip_item_layout, containerLayout, false);
        TextView infoItemNameTextView = tooltipItemLayout.findViewById(R.id.item_name_text);

        List<List<TodayInfoTooltipViewModel.InfoItemViewModel>> infoLists
                = Arrays.asList(todayInfoTooltipViewModel.getImpressionsList(),
                todayInfoTooltipViewModel.getPrintedList());

        for (List<TodayInfoTooltipViewModel.InfoItemViewModel> infoList : infoLists) {
            if (infoList != null) {
                for (TodayInfoTooltipViewModel.InfoItemViewModel infoItem : infoList) {
                    infoItemNameTextView.setText(infoItem.getInfoItemName());
                    maxWidth = Math.max(maxWidth, getInfoItemNameTextViewWidth(infoItemNameTextView));
                }
            }
        }

        return maxWidth + HPUIUtils.dpToPx(mContext, 3); //must be bigger than the exact actual size
    }

    public int getInfoItemNameTextViewWidth(TextView textView) {
        return HPUIUtils.getTextWidth(textView, textView.getText().toString());
    }

    public void setInfoItemNameTextViewWidth(TextView textView, int width) {

        ViewGroup.LayoutParams params = textView.getLayoutParams();
        params.width = width;
        textView.setLayoutParams(params);

    }

}
