package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.text.TextUtils;
import android.util.Log;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingDataManager;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LeaderboardListPresenter extends Presenter<LeaderboardListView> {

    private static final String TAG = LeaderboardListPresenter.class.getSimpleName();
    public static final int PAGE_SIZE = 100;

    private String siteId;
    private String bu;
    private RankingViewModel.SiteRankViewModel rankTypeViewModel;
    private Subscription subscription;
    private RankingViewModel.SiteRankViewModel topRank;
    private boolean lastPage;

    public LeaderboardListPresenter() {

    }

    public void initLeaderboardList() {

        int from;
        final int to;

        HPLogger.i(TAG, "Lower third value " + rankTypeViewModel.getLowerThirdValue() + " site rank " + rankTypeViewModel.getActualRank());

        if (rankTypeViewModel.isLowerThird()) {

            from = Math.max(1, rankTypeViewModel.getLowerThirdValue() - PAGE_SIZE);
            to = rankTypeViewModel.getLowerThirdValue();

        } else {

            from = Math.max(1, rankTypeViewModel.getActualRank() - PAGE_SIZE);
            to = Math.min(rankTypeViewModel.getLowerThirdValue(), rankTypeViewModel.getActualRank() + PAGE_SIZE);
        }

        mView.showLoadingView();

        BusinessUnitViewModel businessUnitViewModel = HomePresenter.getSelectedBusinessUnit();

        if (businessUnitViewModel != null && businessUnitViewModel.getFiltersViewModel() != null
                && businessUnitViewModel.getFiltersViewModel().getSelectedSite() != null) {
            siteId = businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId();
            siteId = TextUtils.isEmpty(siteId) ? "-1" : siteId;
            bu = businessUnitViewModel.getBusinessUnit().getName();
        }

        subscription = RankingDataManager.getRankingLeaderboardList(siteId, rankTypeViewModel, bu, from, to)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<List<RankingViewModel.SiteRankViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Failed to get ranking leaderboard data " + e);
                        if (mView != null) {
                            mView.showErrorView();
                            mView.hideLoadingView();
                        }
                    }

                    @Override
                    public void onNext(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {

                        if (mView == null) {
                            return;
                        }

                        List<RankingViewModel.SiteRankViewModel> dummyItems = new ArrayList<>();

                        //TODO move to data manager.
                        topRank = siteRankViewModels.get(0);

                        for (int i = 1; i < topRank.getActualRank(); i++) {
                            RankingViewModel.SiteRankViewModel dummySiteViewModel = new RankingViewModel.SiteRankViewModel();
                            dummySiteViewModel.setName("loading...");
                            dummySiteViewModel.setRankAreaType(topRank.getRankAreaType());
                            dummySiteViewModel.setTrend(RankingViewModel.Trend.EQUALS);
                            dummySiteViewModel.setIncognito(false);
                            dummySiteViewModel.setActualRank(i);
                            dummySiteViewModel.setInTop(i <= 5);
                            dummyItems.add(dummySiteViewModel);
                        }

                        dummyItems.addAll(siteRankViewModels);

                        mView.hideErrorView();
                        mView.hideLoadingView();

                        setLastPage(to == rankTypeViewModel.getLowerThirdValue());

                        mView.onInitLeaderboardListCompleted(dummyItems);


                    }
                });

        addSubscriber(subscription);

    }

    public boolean getNextPage(int currentOffset) {

        if (mView == null) {
            return false;
        }

        if (currentOffset >= rankTypeViewModel.getLowerThirdValue()) {

            setLastPage(true);
            mView.addFooterView();
            mView.showNoResultsFooterView();

            return false;
        }

        int from = currentOffset + 1;
        int to = Math.min(rankTypeViewModel.getLowerThirdValue(), currentOffset + PAGE_SIZE + 1);

        mView.addFooterView();
        mView.showLoadingFooterView();

        subscription = RankingDataManager.getRankingLeaderboardList(siteId, rankTypeViewModel, bu, from, to)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<RankingViewModel.SiteRankViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(TAG, "Failed to get ranking leaderboard data " + e);

                        if (mView != null) {
                            mView.addFooterView();
                            mView.showErrorFooterView();
                        }
                    }

                    @Override
                    public void onNext(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {
                        if (mView != null) {
                            mView.removeFooterView();
                            mView.onNextPage(siteRankViewModels);
                        }
                    }
                });

        addSubscriber(subscription);

        return true;

    }

    public boolean getPreviousPage() {

        //Reached the top.
        if (topRank.getActualRank() == 1) {
            return false;
        }

        int from = Math.max(topRank.getActualRank() - PAGE_SIZE, 1);
        int to = topRank.getActualRank();

        subscription = RankingDataManager.getRankingLeaderboardList(siteId, rankTypeViewModel, bu, from, to)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<RankingViewModel.SiteRankViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            mView.onPrevPageLoaded(null);
                        }
                    }

                    @Override
                    public void onNext(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {

                        if (mView == null) {
                            return;
                        }

                        if (siteRankViewModels != null && siteRankViewModels.size() > 0) {
                            topRank = siteRankViewModels.get(0);
                        }

                        mView.onPrevPageLoaded(siteRankViewModels);

                    }
                });

        return true;

    }

    public int getCurrentTopRank() {
        return topRank.getActualRank();
    }

    public void setRankTypeViewModel(RankingViewModel.SiteRankViewModel rankTypeViewModel) {
        this.rankTypeViewModel = rankTypeViewModel;
    }

    public RankingViewModel.SiteRankViewModel getRankTypeViewModel() {
        return rankTypeViewModel;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}
