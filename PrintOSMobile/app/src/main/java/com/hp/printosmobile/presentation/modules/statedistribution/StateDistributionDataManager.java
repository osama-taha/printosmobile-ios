package com.hp.printosmobile.presentation.modules.statedistribution;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.StateDistributionData;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * created by Anwar Asbah 2/18/2018
 */
public class StateDistributionDataManager {

    private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Observable<StateDistributionViewModel> getPressStateDistribution(
            String deviceSerialNumber, BusinessUnitEnum businessUnitEnum, boolean isShiftSupport) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        String buName = businessUnitEnum.getName();

        return realtimeTrackingService.getPressStateDistribution(buName, deviceSerialNumber, isShiftSupport)
                .map(new Func1<Response<StateDistributionData>, StateDistributionViewModel>() {
                    @Override
                    public StateDistributionViewModel call(Response<StateDistributionData> stateDistributionDataResponse) {
                        if (stateDistributionDataResponse != null && stateDistributionDataResponse.isSuccessful()) {
                            return parseStateDistributionData(stateDistributionDataResponse.body());
                        }
                        return null;
                    }
                });
    }

    private static StateDistributionViewModel parseStateDistributionData(StateDistributionData data) {

        if (data == null) {
            return null;
        }

        Map<DeviceState, Float> aggregateMap = new LinkedHashMap<>();
        if (data.getStatesAggregation() != null) {
            List<Map.Entry<DeviceState, Float>> aggregateList = new LinkedList<>();
            Map<String, Float> dataMap = data.getStatesAggregation();
            for (String state : dataMap.keySet()) {
                Map.Entry<DeviceState, Float> entry = new HashMap.SimpleEntry<>(
                        DeviceState.from(state), dataMap.get(state));
                aggregateList.add(entry);
            }

            Collections.sort(aggregateList, new Comparator<Map.Entry<DeviceState, Float>>() {
                @Override
                public int compare(Map.Entry<DeviceState, Float> argL, Map.Entry<DeviceState, Float> argR) {
                    return -argL.getValue().compareTo(argR.getValue());
                }
            });

            for (Map.Entry<DeviceState, Float> entry : aggregateList) {
                aggregateMap.put(entry.getKey(), entry.getValue());
            }
        }

        List<StateDistributionViewModel.PressState> pressStateList = new LinkedList<>();
        if (data.getReportedStates() != null) {
            for (StateDistributionData.ReportedState state : data.getReportedStates()) {
                StateDistributionViewModel.PressState pressState = new StateDistributionViewModel.PressState();
                pressState.setStartDate(HPDateUtils.parseDate(state.getTimestamp(), DATE_FORMAT));
                pressState.setEndDate(HPDateUtils.parseDate(state.getEndTime(), DATE_FORMAT));
                pressState.setDeviceState(DeviceState.from(state.getState()));

                pressStateList.add(pressState);
            }
        }

        Collections.sort(pressStateList, StateDistributionViewModel.PRESS_STATE_DATE_COMPARATOR);
        for (int index = 0; index < pressStateList.size() - 1; index++) {
            pressStateList.get(index).setEndDate(pressStateList.get(index + 1).getStartDate());
        }

        StateDistributionViewModel viewModel = new StateDistributionViewModel();
        viewModel.setAggregateStateMap(aggregateMap);
        viewModel.setPressStates(pressStateList);

        return viewModel;
    }
}