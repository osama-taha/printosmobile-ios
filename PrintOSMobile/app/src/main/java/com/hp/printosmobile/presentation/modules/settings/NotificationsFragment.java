package com.hp.printosmobile.presentation.modules.settings;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.npspopup.NPSNotificationManager;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent.NotificationEventEnum;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.PermissionUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * created by anwar asbah 1/17/2017
 **/
public class NotificationsFragment extends HPFragment implements NotificationsSubMenuView {

    private static final String TAG = "NotificationSubscription";

    @Bind(R.id.content_layout)
    View contentLayout;
    @Bind(R.id.button_layout)
    View buttonLayout;
    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.notifications_service_list)
    RecyclerView notificationServiceList;
    @Bind(R.id.error_layout)
    View errorMsg;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.all_notifications_switch)
    SwitchButton allNotificationsSwitchButton;

    NotificationsAdapter notificationsAdapter;
    NotificationFragmentCallback callback;
    NotificationsPresenter presenter;

    public static NotificationsFragment getNewInstance(NotificationFragmentCallback callback) {
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        notificationsFragment.callback = callback;
        return notificationsFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntercomSdk.getInstance(getActivity()).logEvent(IntercomSdk.PBM_VIEW_NOTIFICATIONS_SETTINGS_EVENT);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_NOTIFICATION_SETTINGS);
        Analytics.sendEvent(Analytics.VIEW_NOTIFICATIONS_SETTINGS_ACTION);

        buttonLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        initPresenter();
    }

    private void initPresenter() {
        presenter = new NotificationsPresenter();
        presenter.attachView(this);
        presenter.getAvailableSubscriptions();
    }

    private void initView(List<String> supportedSubscriptions) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        notificationServiceList.setLayoutManager(layoutManager);

        List<SubscriptionEvent> events = new ArrayList<>();
        NotificationEventEnum[] eventEnums = NotificationEventEnum.values();
        for (int i = 0; i < eventEnums.length; i++) {
            if (eventEnums[i] != NotificationEventEnum.UNKNOWN) {
                if (eventEnums[i] == NotificationEventEnum.NPS_NOTIFICATIONS &&
                        !PrintOSPreferences.getInstance(getActivity()).isNPSFeatureEnabled()) {
                    continue;
                }

                if (eventEnums[i].isOptional() && (supportedSubscriptions == null
                        || !supportedSubscriptions.contains(eventEnums[i].getKey()))) {
                    continue;
                }

                SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
                subscriptionEvent.setNotificationEventEnum(eventEnums[i]);
                events.add(subscriptionEvent);
            }
        }
        notificationsAdapter = new NotificationsAdapter(events);
        notificationServiceList.setAdapter(notificationsAdapter);

        notificationServiceList.addItemDecoration(new DividerItemDecoration(getActivity(), 1, events.size(), false));

        notificationServiceList.setHasFixedSize(true);

        HPUIUtils.setVisibility(false, contentLayout, errorMsg);
        HPUIUtils.setVisibility(true, loadingView);

        allNotificationsSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allNotificationsSwitchButton.isChecked()) {
                    notificationsAdapter.subscribeAll(true);
                } else {
                    notificationsAdapter.subscribeAll(false);
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_notifications;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.menu_notifications_settings;
    }

    @Override
    public void setSupportedSubscriptions(List<String> events) {
        initView(events);
        presenter.getUserSubscriptions();
    }

    @Override
    public void setSubscriptions(List<SubscriptionEvent> events) {
        HPUIUtils.setVisibility(false, loadingView, errorMsg);
        HPUIUtils.setVisibility(true, contentLayout, buttonLayout);

        boolean allSubscribed = notificationsAdapter.setSubscriptions(events);
        allNotificationsSwitchButton.setChecked(allSubscribed);
    }

    @Override
    public void onFailedToGetSubscription(APIException apiExceptions) {

        if (getActivity() == null) {
            return;
        }

        errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(PrintOSApplication.getAppContext(), apiExceptions));

        HPUIUtils.setVisibility(false, loadingView, contentLayout, buttonLayout);
        HPUIUtils.setVisibility(true, errorMsg);
    }

    @Override
    public void onSubscribed(SubscriptionEvent subscriptionEvent) {
        notificationsAdapter.setSubscriptions(subscriptionEvent);
    }

    @OnClick(R.id.error_layout)
    public void tryAgain() {
        HPLogger.d(TAG, "tryAgain clicked");
        HPUIUtils.setVisibility(false, errorMsg, contentLayout);
        HPUIUtils.setVisibility(true, loadingView);

        presenter.getAvailableSubscriptions();
    }

    @OnClick(R.id.cancel_button)
    public void cancelClicked() {
        HPLogger.d(TAG, "cancel clicked");
        if (callback != null) {
            callback.closeNotificationsFragment();
        }
    }

    @OnClick(R.id.save_button)
    public void saveClicked() {
        HPLogger.d(TAG, "save clicked");
        notificationsAdapter.updateSubscription();


        if (callback != null) {
            callback.closeNotificationsFragment();
        }
    }

    public interface NotificationFragmentCallback {
        void closeNotificationsFragment();
    }

    public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder> {

        private List<SubscriptionEvent> notificationEvents;
        private boolean[] statesChange;

        public NotificationsAdapter(List<SubscriptionEvent> notificationEvents) {
            this.notificationEvents = notificationEvents;
            statesChange = new boolean[getItemCount()];
        }

        @Override
        public NotificationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_notifications_item, parent, false);
            return new NotificationsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final NotificationsViewHolder holder, final int position) {
            final SubscriptionEvent notificationEvent = notificationEvents.get(position);

            final NotificationEventEnum notificationEventEnum = notificationEvent
                    .getNotificationEventEnum();

            holder.notificationServiceName.setText(getString(notificationEventEnum.getNameID()));
            holder.notificationTypeHeader.setText(getString(notificationEventEnum.getSectionNameStringID()));
            holder.notificationHeaderLayout.setVisibility(isStartOfNewSegment(position) ?
                    View.VISIBLE : View.GONE);

            holder.notificationsSwitchButton.setChecked(notificationEvent.isMobile());

            holder.index = position;

            holder.notificationsSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    statesChange[position] = true;
                    notificationEvent.setMobile(isChecked);
                    allNotificationsSwitchButton.setChecked(areMobileNotificationsSubscribed());
                }
            });
        }

        @Override
        public int getItemCount() {
            return notificationEvents == null ? 0 : notificationEvents.size();
        }

        public boolean setSubscriptions(List<SubscriptionEvent> subscriptions) {
            if (subscriptions == null) {
                return false;
            }

            boolean mobileNotificationsSubscribed = false;
            for (SubscriptionEvent event : notificationEvents) {
                for (SubscriptionEvent subscriptionEvent : subscriptions) {
                    if (subscriptionEvent.getNotificationEventEnum() == event.getNotificationEventEnum()) {
                        event.setUserSubscriptionID(subscriptionEvent.getUserSubscriptionID());
                        event.setEmail(subscriptionEvent.isEmail());
                        event.setBrowser(subscriptionEvent.isBrowser());
                        event.setMobile(subscriptionEvent.isMobile());
                        break;
                    }
                }

                mobileNotificationsSubscribed = mobileNotificationsSubscribed || event.isMobile();
            }

            notifyDataSetChanged();

            return mobileNotificationsSubscribed;
        }

        public void setSubscriptions(SubscriptionEvent subscriptionEvent) {
            for (SubscriptionEvent event : notificationEvents) {
                if (subscriptionEvent.getNotificationEventEnum() == event.getNotificationEventEnum()) {
                    event.setUserSubscriptionID(subscriptionEvent.getUserSubscriptionID());
                }
            }

            notifyDataSetChanged();
        }

        public boolean areMobileNotificationsSubscribed() {
            boolean subscribed = false;
            for (SubscriptionEvent event : notificationEvents) {
                subscribed = subscribed || event.isMobile();
            }

            return subscribed;
        }

        public void subscribeAll(boolean enableSubscription) {
            HPLogger.d(TAG, "subscribe to All clicked, enable: " + enableSubscription);
            for (int i = 0; i < notificationEvents.size(); i++) {
                SubscriptionEvent event = notificationEvents.get(i);
                boolean isSubscribed = event.isMobile();
                if (enableSubscription) {
                    if (!isSubscribed) {
                        statesChange[i] = true;
                        event.setMobile(true);
                    }
                } else {
                    if (isSubscribed) {
                        statesChange[i] = true;
                        event.setMobile(false);
                    }
                }
            }

            notifyDataSetChanged();
        }

        public void updateSubscription() {
            if (presenter != null) {
                presenter.setWasToastShown(false);
            }

            for (int i = 0; i < statesChange.length; i++) {
                if (statesChange[i]) {

                    SubscriptionEvent event = notificationEvents.get(i);
                    NotificationEventEnum eventEnum = event.getNotificationEventEnum();

                    boolean isSubscribed = event.isMobile();

                    if (eventEnum == NotificationEventEnum.NPS_NOTIFICATIONS) {
                        NPSNotificationManager.enableNotifications(event.isMobile());
                    }

                    String descriptionKey = event.getNotificationEventEnum().getKey();

                    IntercomSdk.getInstance(getContext()).sendUserRegisterNotificationTypeEvent(isSubscribed, descriptionKey);
                    Analytics.sendEvent(isSubscribed ?
                            Analytics.SUBSCRIBE_NOTIFICATION_ACTION : Analytics.UNSUBSCRIBE_NOTIFICATION_ACTION, descriptionKey);

                    HPLogger.d(TAG, String.format("%s notification type %s",
                            isSubscribed ? "subscribing" : "unsubscribing", eventEnum.getKey()));

                    if (eventEnum.isSubscribable()) {
                        if (isSubscribed) {
                            presenter.subscribeEvent(event);
                        } else {
                            presenter.unsubscribeToEvent(event);
                        }
                    }
                }
            }

        }

        public boolean isStartOfNewSegment(int position) {
            if (position == 0) {
                return true;
            }

            NotificationEventEnum notificationEventEnumPrev = notificationEvents.get(position - 1)
                    .getNotificationEventEnum();
            NotificationEventEnum notificationEventEnumCurrent = notificationEvents.get(position)
                    .getNotificationEventEnum();

            return notificationEventEnumCurrent.getSectionNameStringID()
                    != notificationEventEnumPrev.getSectionNameStringID();

        }

        public class NotificationsViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.notification_type_header_text_view)
            TextView notificationTypeHeader;
            @Bind(R.id.header_layout)
            View notificationHeaderLayout;
            @Bind(R.id.notification_name_text_view)
            TextView notificationServiceName;
            @Bind(R.id.notification_switch_button)
            SwitchButton notificationsSwitchButton;

            int index;

            public NotificationsViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }


    protected boolean requestLocationPermissionIfNeeded() {

        return PermissionUtils.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION,
                new PermissionUtils.PermissionAskListener() {
                    @Override
                    public void onPermissionAsk() {
                        requestLocationPermission();
                    }

                    @Override
                    public void onPermissionPreviouslyDenied() {
                        requestLocationPermission();
                    }

                    @Override
                    public void onPermissionDisabled() {
                        HPLogger.d(TAG, "Request app settings page.");
                        showGoToAppSettingsDialog();
                    }

                    @Override
                    public void onPermissionGranted() {
                        HPLogger.d(TAG, "Location permission already granted.");
                    }
                });

    }

    private void showGoToAppSettingsDialog() {

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder
                .setMessage(R.string.location_based_notifications_dialog_message)
                .setPositiveButton(R.string.location_based_notifications_dialog_go_to_settings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HPLogger.d(TAG, "Open app settings page.");
                        AppUtils.openAppSettings(PrintOSApplication.getAppContext());
                        Analytics.sendEvent(Analytics.OPEN_APP_SETTINGS);
                    }
                })
                .setNegativeButton(R.string.location_based_notifications_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .create()
                .show();

    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                Constants.IntentExtras.ACCESS_LOCATION_PERMISSION_REQUEST_CODE
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == Constants.IntentExtras.ACCESS_LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Enable location service.
                HPLogger.d(TAG, "accept location services permission.");
                Analytics.sendEvent(Analytics.ACCEPT_LOCATION_SERVICES);
            } else {
                HPLogger.d(TAG, "deny location services permission.");
                Analytics.sendEvent(Analytics.DENY_LOCATION_SERVICES);
            }
        }
    }
}
