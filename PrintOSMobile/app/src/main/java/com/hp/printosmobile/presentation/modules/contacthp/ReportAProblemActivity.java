package com.hp.printosmobile.presentation.modules.contacthp;

import android.net.Uri;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.contacthp.shared.AttachImageView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpContactThroughView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created be Anwar Asbah 10/11/2016
 */
public class ReportAProblemActivity extends ContactHPBaseActivity {

    @Bind(R.id.attach_image_view)
    AttachImageView attachImageView;
    @Bind(R.id.subject_text_view)
    ContactHpTextField subjectTextView;
    @Bind(R.id.get_there_text_view)
    ContactHpTextField getThereTextView;
    @Bind(R.id.what_happened_text_view)
    ContactHpTextField whatHappenedTextView;
    @Bind(R.id.expected_text_view)
    ContactHpTextField expectedTextView;
    @Bind(R.id.contact_through_view)
    ContactHpContactThroughView contactThroughView;

    @Override
    protected int getLayoutResource() {
        return R.layout.contact_hp_report_a_problem;
    }

    @Override
    protected void init() {
        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_PROBLEM_EVENT);

        attachImageView.addAttachImageClickListener(this);
        whatHappenedTextView.setTextFieldRightDrawable(null);
        expectedTextView.setTextFieldRightDrawable(null);
    }

    @Override
    protected void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this,
                subjectTextView.getAutoCompleteTextView(),
                getThereTextView.getAutoCompleteTextView(),
                whatHappenedTextView.getAutoCompleteTextView(),
                expectedTextView.getAutoCompleteTextView());
    }

    @Override
    public int getTitleResourceID() {
        return R.string.hp_contact_report_problem_title;
    }

    @Override
    public void sendRequest() {
        List<Uri> uriList = attachImageView.getAttachedImagesUri();

        if (uriList == null || uriList.isEmpty()) {
            Analytics.sendEvent(Analytics.REPORT_A_PROBLEM_SEND_ACTION);
        } else {
            Analytics.sendEvent(Analytics.REPORT_A_PROBLEM_SEND_ACTION, Analytics.CONTACT_HP_SCREENSHOT_LABEL);
        }

        ContactHpContactThroughView.ContactMethod contactMethodType = contactThroughView.getContactOption();

        presenter.reportAProblem(this,
                subjectTextView.getText(),
                getThereTextView.getText(),
                whatHappenedTextView.getText(),
                expectedTextView.getText(),
                uriList,
                contactMethodType,
                contactMethodType != ContactHpContactThroughView.ContactMethod.DO_NOT_CONTACT);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_SEND_PROBLEM_EVENT);
    }

    @Override
    public void setHasPhone(boolean hasPhone) {
        contactThroughView.setHasPhone(hasPhone);
    }

    @Override
    public void setFormEnable(boolean isEnabled) {
        attachImageView.setEnabled(isEnabled);
        subjectTextView.setEnabled(isEnabled);
        getThereTextView.setEnabled(isEnabled);
        whatHappenedTextView.setEnabled(isEnabled);
        expectedTextView.setEnabled(isEnabled);
        contactThroughView.setEnabled(isEnabled);
    }

    @Override
    public boolean isFormFilled() {
        String subject = subjectTextView.getText();
        String getThere = getThereTextView.getText();
        String happened = whatHappenedTextView.getText();
        String expected = expectedTextView.getText();

        return displayWarningMessage(subject, R.string.hp_contact_fill_subject_warning)
                && displayWarningMessage(getThere, R.string.hp_contact_fill_get_here_warning)
                && displayWarningMessage(happened, R.string.hp_contact_fill_what_happened_warning)
                && displayWarningMessage(expected, R.string.hp_contact_fill_expected_warning);
    }

    @Override
    public void onUserRetrieved(UserData.User user, APIException apiException) {
        setHasPhone(user != null && user.getPrimaryPhone() != null);
    }

    @Override
    public void onCountriesRetrieved(Map<String, String> countryDataList, APIException apiException) {

    }

    @Override
    public void onUserUpdated() {

    }
}
