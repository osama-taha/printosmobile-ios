package com.hp.printosmobile.presentation.modules.home;

import android.content.Context;
import android.text.TextUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.cache.ApiCacheUtils;
import com.hp.printosmobile.data.remote.models.ActiveShiftsData;
import com.hp.printosmobile.data.remote.models.ActualVsTargetData;
import com.hp.printosmobile.data.remote.models.BeatCoinData;
import com.hp.printosmobile.data.remote.models.BusinessUnit;
import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.data.remote.models.DevicesData;
import com.hp.printosmobile.data.remote.models.DevicesStatisticsData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.GeneralData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.JobsData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.Notifications;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.PrinterData;
import com.hp.printosmobile.data.remote.models.LoginBeatCoinData;
import com.hp.printosmobile.data.remote.models.MergedTodayAndLastWeekData;
import com.hp.printosmobile.data.remote.models.PBNotificationData;
import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.PrintVolumeData;
import com.hp.printosmobile.data.remote.models.RealtimeActualVsTargetDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeActualVsTargetGraphData;
import com.hp.printosmobile.data.remote.models.RealtimeManyDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeTargetManyDataV2;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.data.remote.models.ServiceCallData;
import com.hp.printosmobile.data.remote.models.ShiftData;
import com.hp.printosmobile.data.remote.models.SiteRankingData;
import com.hp.printosmobile.data.remote.models.TargetData;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.data.remote.models.WeekData;
import com.hp.printosmobile.data.remote.services.BeatCoinService;
import com.hp.printosmobile.data.remote.services.ConfigService;
import com.hp.printosmobile.data.remote.services.MetaDataService;
import com.hp.printosmobile.data.remote.services.PBNotificationService;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;
import com.hp.printosmobile.data.remote.services.PersonalAdvisorService;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.data.remote.services.RankingService;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;
import com.hp.printosmobile.data.remote.services.ReportDataV2;
import com.hp.printosmobile.data.remote.services.ServiceCallService;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobile.data.remote.services.WeeklyDataV2;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.modules.PBNotificationDataViewModel;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.devicedetails.InkModel;
import com.hp.printosmobile.presentation.modules.devicedetails.JobModel;
import com.hp.printosmobile.presentation.modules.devicedetails.SubstrateModel;
import com.hp.printosmobile.presentation.modules.filters.DeviceFilterViewModel;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.filters.FiltersViewModel;
import com.hp.printosmobile.presentation.modules.filters.SiteViewModel;
import com.hp.printosmobile.presentation.modules.insights.InsightsPresenter;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreTrendEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIValueHandleEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.ReportChartTypeEnum;
import com.hp.printosmobile.presentation.modules.main.ReportKpiEnum;
import com.hp.printosmobile.presentation.modules.main.ReportTypeEnum;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingDataManager;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceExtraData;
import com.hp.printosmobile.presentation.modules.shared.DeviceLastUpdateType;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.MaintenanceState;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.today.TodayGraphUtils;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltipViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel.WeekPressStatus.WeeklyPressStatusEnum;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationEnum;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationUtils;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func5;

/**
 * Created by Osama Taha on 5/21/16.
 */
public class HomeDataManager {

    private static final String TAG = HomeDataManager.class.getName();

    private static final String CONFIG_DEVICE_CACHE_PREF_KEY = "CONFIG_DEVICE_CACHE_PREF_KEY";
    private static final String ORGANIZATION_DEVICE_CACHE_PREF_KEY = "ORGANIZATION_DEVICE_CACHE_PREF_KEY";
    private static final String BU_PREFERENCE_CACHE_PREF_KEY = "BU_PREFERENCE_CACHE_PREF_KEY";
    private static final String META_DATA_DEVICE_CACHE_PREF_KEY = "META_DATA_DEVICE_CACHE_PREF_KEY";
    private static final String SERVICE_CALL_CASH_ID_KEY = "SERVICE_CALL_CASH_ID_KEY";
    private static final String SERVICE_CALL_CASH_KEY = "SERVICE_CALL_CASH_KEY";
    private static final String WEEK_CACHE_ID_KEY = "WEEK_CACHE_ID_KEY";
    private static final String RANKING_CACHE_ID_KEY = "RANKING_CACHE_ID_KEY";
    private static final String WEEK_CACHE_KEY = "WEEK_CACHE_KEY";
    private static final String RANKING_CACHE_KEY = "RANKING_CACHE_KEY";

    private static final String DATA_FORMAT_ACTUAL_VS_TARGET = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String SELECTED_PRESS_ALL = "selected";
    private static final String PRINT_VOLUME_ITEM = "Print Volume";
    private static final String PRINTED_IMPRESSIONS_UNIT = "PrintedImpressions";
    private static final String DEFAULT_DATE_FORMAT_NO_TIME = "yyyy-MM-dd";
    private static final int NUMBER_OF_WEEKS = 4;
    private static final String LAST_UPDATED_TIME_DATE_FORMAT_V2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String SERVICE_CALL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone("UTC");
    private static final String DATA_FORMAT_LATEX_JOB_COMPLETION = "yyyy-MM-dd'T'HH:mm:ssZZZ";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final int CLOSED_SERVICE_CALLS_DAYS_DEFAULT = 3;
    private static final int MAX_DECIMAL_PLACE = 2;

    private static final int DAYS_IN_MILLS = 24 * 60 * 60 * 1000;

    public static final int[] SHIFTS_COLORS;
    private static final String PRINT_VOLUME_KPI_NAME = "Print volume";
    private static final String UNIT_BY = "Impressions";

    static {
        SHIFTS_COLORS = new int[]{
                R.color.shift_color_1,
                R.color.shift_color_2,
                R.color.shift_color_3,
                R.color.shift_color_4,
                R.color.shift_color_5,
                R.color.shift_color_6,
                R.color.shift_color_7
        };
    }

    private HomeDataManager() {
    }

    public static Observable<List<DeviceViewModel>> getDevicesDataForBusinessUnit(final BusinessUnitViewModel businessUnitViewModel, boolean perSite, boolean isShiftSupport, boolean isPolling) {

        if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {

            return getPWPDevices(businessUnitViewModel, perSite);

        } else {

            Observable observable = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER ?
                    getLatexDashboardViewModel(businessUnitViewModel, perSite, isShiftSupport, isPolling)
                    : getPrintBeatViewModel(businessUnitViewModel, perSite, isShiftSupport, isPolling);

            return observable.map(new Func1<PrintBeatDashboardViewModel, List<DeviceViewModel>>() {
                @Override
                public List<DeviceViewModel> call(PrintBeatDashboardViewModel printBeatDashboardViewModel) {
                    return printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels();
                }
            });
        }

    }


    public static Observable<List<DeviceViewModel>> getPWPDevices(final BusinessUnitViewModel businessUnitViewModel, boolean perSite) {

        return Observable.just(perSite ? businessUnitViewModel.getFiltersViewModel().getSelectedSite().getDevices()
                : businessUnitViewModel.getFiltersViewModel().getSelectedDevices())
                .map(new Func1<List<FilterItem>, List<DeviceViewModel>>() {
                    @Override
                    public List<DeviceViewModel> call(List<FilterItem> filterItems) {

                        List<DeviceViewModel> deviceViewModels = new ArrayList<>();

                        for (FilterItem filterItem : filterItems) {

                            DeviceFilterViewModel deviceFilterItem = (DeviceFilterViewModel) filterItem;

                            DeviceViewModel deviceViewModel = new DeviceViewModel();

                            deviceViewModel.setSerialNumber(deviceViewModel.getSerialNumber());
                            deviceViewModel.setSerialNumberDisplay(deviceFilterItem.getSerialNumberDisplay());
                            deviceViewModel.setName(deviceFilterItem.getDeviceName());
                            deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());
                            deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(BusinessUnitEnum.IHPS_PRESS, deviceFilterItem.getPressModel()));
                            deviceViewModel.setDeviceState(DeviceState.UNW);

                            deviceViewModels.add(deviceViewModel);

                        }

                        return deviceViewModels;
                    }
                });
    }

    public static Observable<ActiveShiftsViewModel> getActiveShifts(final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        return realtimeTrackingService.getActiveShifts(businessUnitViewModel.getBusinessUnit().getName(), serialNumbers).map(new Func1<Response<ActiveShiftsData>, ActiveShiftsViewModel>() {
            @Override
            public ActiveShiftsViewModel call(Response<ActiveShiftsData> activeShiftsDataResponse) {
                return mapShiftsData(activeShiftsDataResponse.body());
            }
        });

    }

    private static ActiveShiftsViewModel mapShiftsData(ActiveShiftsData activeShiftsData) {

        ActiveShiftsViewModel activeShiftsViewModel = new ActiveShiftsViewModel();

        if (activeShiftsData == null) {

            activeShiftsViewModel.setSiteHasShifts(false);

        } else {

            activeShiftsViewModel.setSiteHasShifts(activeShiftsData.getSiteContainShifts());

            if (activeShiftsViewModel.isSiteHasShifts()) {

                List<ShiftViewModel> shiftViewModels = new ArrayList<>();

                shiftViewModels.add(parseShiftData(activeShiftsData.getDayShift()));
                shiftViewModels.add(parseShiftData(activeShiftsData.getCurrShift()));

                activeShiftsViewModel.setShiftViewModels(shiftViewModels);
            }
        }

        return activeShiftsViewModel;
    }

    private static ShiftViewModel parseShiftData(ShiftData shiftData) {

        if (shiftData == null) {
            return null;
        }

        ShiftViewModel shiftViewModel = new ShiftViewModel();
        shiftViewModel.setShiftId(shiftData.getShiftMetaDataId());
        shiftViewModel.setShiftName(shiftData.getShiftName());
        shiftViewModel.setShiftType(ShiftViewModel.ShiftType.fromString(shiftData.getShiftMetaDataId()));
        shiftViewModel.setStartDay(shiftData.getStartDay());
        shiftViewModel.setStartTime(shiftData.getStartTime());
        shiftViewModel.setStartHour(shiftData.getStartHour());
        shiftViewModel.setEndDay(shiftData.getEndDay());
        shiftViewModel.setEndHour(shiftData.getEndHour());
        shiftViewModel.setPresentedDay(shiftData.getPresentedDay());

        return shiftViewModel;
    }

    /**
     * Returns an observable object responsible for combining the response of PrintVolume & targetData APIs.
     */
    public static Observable<PrintBeatDashboardViewModel> getPrintBeatViewModel(final BusinessUnitViewModel businessUnitViewModel, final boolean perSite, boolean isShiftSupport, boolean isPolling) {

        //TODO: Check with the server team if they can combine these APIs in one API to make things simple.

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        List<String> apiSerialNumbers;
        final List<String> serialNumbers = perSite ?
                businessUnitViewModel.getFiltersViewModel().getSiteSerialNumbers() :
                businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RT_SUPPORTED);

        apiSerialNumbers = serialNumbers;
        if (perSite && (apiSerialNumbers == null || apiSerialNumbers.isEmpty())) {
            apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        }

        final SiteViewModel siteViewModel = businessUnitViewModel.getFiltersViewModel().getSelectedSite();
        final List<FilterItem> selectedDevices;
        if (perSite) {
            if (siteViewModel == null) {
                selectedDevices = new ArrayList<>();
            } else {
                selectedDevices = businessUnitViewModel.getFiltersViewModel().getSelectedSite().getDevices();
            }
        } else {
            selectedDevices = businessUnitViewModel.getFiltersViewModel().getSelectedDevices();
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (!perSite && (serialNumbers == null || serialNumbers.isEmpty())) {
            return Observable.just(mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, null, null, selectedDevices));
        }

        if (!printOSPreferences.supportV2()) {

            return Observable.combineLatest(realtimeTrackingService.getTargetData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, isPolling),
                    realtimeTrackingService.getPrintVolumeData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, isPolling),
                    new Func2<Response<List<TargetData>>, Response<List<PrintVolumeData>>, PrintBeatDashboardViewModel>() {
                        @Override
                        public PrintBeatDashboardViewModel call(Response<List<TargetData>> targetDataResponse, Response<List<PrintVolumeData>> printVolumeResponse) {

                            PrintBeatDashboardViewModel viewModel = null;
                            if (targetDataResponse.isSuccessful() && printVolumeResponse.isSuccessful() && !serialNumbers.isEmpty()) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, targetDataResponse.body(), printVolumeResponse.body(), selectedDevices);
                            } else if (serialNumbers == null || serialNumbers.isEmpty()) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, null, null, selectedDevices);
                            }
                            return viewModel;
                        }
                    }
            );

        } else {

            String unitSystem = printOSPreferences.getUnitSystem().name();

            return Observable.combineLatest(realtimeTrackingService.getTargetDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, unitSystem, isPolling),
                    realtimeTrackingService.getPrintVolumeDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, unitSystem, isPolling),
                    new Func2<Response<RealtimeTargetManyDataV2>, Response<RealtimeManyDataV2>, PrintBeatDashboardViewModel>() {
                        @Override
                        public PrintBeatDashboardViewModel call(Response<RealtimeTargetManyDataV2> realtimeTargetManyDataV2Response, Response<RealtimeManyDataV2> realtimeManyDataV2Response) {

                            PrintBeatDashboardViewModel viewModel = null;
                            if (realtimeTargetManyDataV2Response.isSuccessful() && realtimeManyDataV2Response.isSuccessful() && !serialNumbers.isEmpty()) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, realtimeTargetManyDataV2Response.body().getData(), realtimeManyDataV2Response.body().getData(), selectedDevices);
                            } else if (serialNumbers == null || serialNumbers.isEmpty()) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, null, null, selectedDevices);
                            }
                            return viewModel;
                        }
                    }
            );

        }

    }

    public static Observable<TodayViewModel> getRealtimeTodayVsLastWeekData(final TodayViewModel todayViewModel,
                                                                            final BusinessUnitViewModel businessUnitViewModel, boolean perSite, boolean isShiftSupport) {

        final PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = apiServicesProvider.getRealtimeTrackingService();

        Calendar now = Calendar.getInstance();
        String nowShortString = now.get(Calendar.DAY_OF_MONTH) + "/" + now.get(Calendar.HOUR_OF_DAY);
        String unitString = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUnitSystem().name();
        String selectedOrganization = printOSPreferences.getSavedOrganizationId();

        List<String> apiSerialNumbers;
        final List<String> serialNumbers = perSite ? businessUnitViewModel.getFiltersViewModel().getSiteSerialNumbers() :
                businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RT_SUPPORTED);

        apiSerialNumbers = serialNumbers;
        if (apiSerialNumbers == null || apiSerialNumbers.isEmpty()) {
            apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        }

        Collections.sort(apiSerialNumbers);

        String serialNumbersString = "";
        for (int i = 0; i < apiSerialNumbers.size(); i++) {
            serialNumbersString += apiSerialNumbers.get(i);
        }

        String identifier = nowShortString + unitString + selectedOrganization + isShiftSupport + serialNumbersString;
        String oldIdentifier = printOSPreferences.getTodayGraphIdentifier();

        final MergedTodayAndLastWeekData cachedData = printOSPreferences.getTodayGraphCache();

        if (identifier.equals(oldIdentifier) && cachedData != null) {
            return Observable.fromCallable(new Callable<TodayViewModel>() {
                @Override
                public TodayViewModel call() throws Exception {
                    return TodayGraphUtils.parseTodayLastWeekData(todayViewModel, cachedData);

                }
            });
        }

        printOSPreferences.setTodayGraphCache(null);
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setTodayGraphIdentifier(identifier);
        return realtimeTrackingService.getRealtimeTodayVsWeekData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers,
                true, isShiftSupport, unitString).map(new Func1<Response<MergedTodayAndLastWeekData>, TodayViewModel>() {

            @Override
            public TodayViewModel call(Response<MergedTodayAndLastWeekData> mergedTodayAndLastWeekDataResponse) {
                if (mergedTodayAndLastWeekDataResponse != null && mergedTodayAndLastWeekDataResponse.isSuccessful()
                        && mergedTodayAndLastWeekDataResponse.body() != null) {
                    printOSPreferences.setTodayGraphCache(mergedTodayAndLastWeekDataResponse.body());
                    return TodayGraphUtils.parseTodayLastWeekData(todayViewModel, mergedTodayAndLastWeekDataResponse.body());
                }
                return null;
            }
        });
    }

    public static Observable<TodayHistogramViewModel> getActualVsTarget(Context context, final BusinessUnitViewModel businessUnitViewModel, final boolean isShiftSupport, boolean isPolling) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        boolean isHistogramGraphEnabled = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .isHistogramGraphEnabled();

        String days = String.valueOf(isShiftSupport && isHistogramGraphEnabled ?
                PrintOSPreferences.getInstance(context).getHistogramNumberOfShifts() :
                1 + context.getResources().getInteger(R.integer.today_panel_histogram_span));

        List<String> apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RT_SUPPORTED);
        if (apiSerialNumbers == null || apiSerialNumbers.isEmpty()) {
            apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (printOSPreferences.supportV2()) {
            return realtimeTrackingService.getActualVsTargetDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, days, isShiftSupport, printOSPreferences.getUnitSystem().name(), isPolling).map(new Func1<Response<RealtimeActualVsTargetDataV2>, TodayHistogramViewModel>() {
                @Override
                public TodayHistogramViewModel call(Response<RealtimeActualVsTargetDataV2> realtimeActualVsTargetDataV2Response) {

                    ActualVsTargetData actualVsTargetData = realtimeActualVsTargetDataV2Response == null ? null : realtimeActualVsTargetDataV2Response.body().getData();
                    return parseActualVsTargetData(businessUnitViewModel, actualVsTargetData);
                }
            });

        } else {

            return realtimeTrackingService.getActualVsTargetData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, days, isShiftSupport, isPolling).map(new Func1<Response<ActualVsTargetData>, TodayHistogramViewModel>() {
                @Override
                public TodayHistogramViewModel call(Response<ActualVsTargetData> actualVsTargetDataResponse) {
                    return parseActualVsTargetData(businessUnitViewModel, actualVsTargetDataResponse.body());
                }
            });

        }
    }

    public static Observable<TodayHistogramViewModel> getActualVsTargetGraph(final BusinessUnitViewModel businessUnitViewModel) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        Calendar today = Calendar.getInstance();
        Calendar lastMonth = Calendar.getInstance();
        lastMonth.add(Calendar.MONTH, -1);

        String days = String.valueOf((int) ((today.getTimeInMillis() - lastMonth.getTimeInMillis()) / DAYS_IN_MILLS));

        List<String> apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RT_SUPPORTED);
        if (apiSerialNumbers == null || apiSerialNumbers.isEmpty()) {
            return Observable.just(parseActualVsTargetData(businessUnitViewModel, (RealtimeActualVsTargetGraphData) null));
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        return realtimeTrackingService.getActualVsTargetGraphData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, days, printOSPreferences.getUnitSystem().name()).map(new Func1<Response<RealtimeActualVsTargetGraphData>, TodayHistogramViewModel>() {
            @Override
            public TodayHistogramViewModel call(Response<RealtimeActualVsTargetGraphData> realtimeActualVsTargetGraphDataResponse) {
                RealtimeActualVsTargetGraphData graphData = realtimeActualVsTargetGraphDataResponse == null ? null :
                        realtimeActualVsTargetGraphDataResponse.body();
                return parseActualVsTargetData(businessUnitViewModel, graphData);
            }
        });
    }


    private static TodayHistogramViewModel parseActualVsTargetData(BusinessUnitViewModel businessUnitViewModel, ActualVsTargetData actualVsTargetData) {

        TodayHistogramViewModel viewModel = new TodayHistogramViewModel();
        viewModel.setBusinessUnit(businessUnitViewModel.getBusinessUnit());

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);
        tomorrow.set(Calendar.SECOND, 0);
        tomorrow.set(Calendar.MILLISECOND, 0);

        if (actualVsTargetData != null) {

            int lowerThresholds = 0;
            int higherThresholds = 0;
            int shiftsColorIndex = 0;
            if (actualVsTargetData.getThresholds() != null) {
                higherThresholds = actualVsTargetData.getThresholds().getSucceedPercent();
                lowerThresholds = actualVsTargetData.getThresholds().getFailPercent();
            }

            viewModel.setLowerThreshold(lowerThresholds);
            viewModel.setHigherThreshold(higherThresholds);

            List<TodayHistogramViewModel.TodayHistogramItem> histogramData = new ArrayList<>();

            Map<String, TodayHistogramViewModel.HistogramShiftLegend> histogramLegends = new HashMap<>();

            if (actualVsTargetData.getDays() != null && !actualVsTargetData.getDays().isEmpty()) {

                for (int index = 0; index < actualVsTargetData.getDays().size(); index++) {

                    ActualVsTargetData.Day day = actualVsTargetData.getDays().get(index);

                    TodayHistogramViewModel.TodayHistogramItem histogramItem = new TodayHistogramViewModel.TodayHistogramItem();

                    histogramItem.setActual(day.getActual());
                    histogramItem.setTarget(day.getTarget());
                    histogramItem.setStatus(TodayHistogramViewModel.TodayHistogramItem.TodayHistogramItemStatus.getStatus(
                            day.getActual(), day.getTarget(), lowerThresholds, higherThresholds));
                    histogramItem.setDayOrdinal(day.getDay());

                    if (day.getData() != null) {

                        if (day.getData().getShiftData() != null && day.getData().getShiftData().getPresentedDate() != null) {
                            histogramItem.setDate(HPDateUtils.parseDate(day.getData().getShiftData().getPresentedDate(), DATA_FORMAT_ACTUAL_VS_TARGET));
                            if (histogramItem.getDate() != null && tomorrow.getTime().compareTo(histogramItem.getDate()) > 0) {
                                histogramData.add(histogramItem);
                            }
                        }

                        float errorValue = TodayHistogramViewModel.TodayHistogramItem.ERROR_VALUE;
                        histogramItem.setSheets(getFloat(day.getData().getSheets(), errorValue));
                        histogramItem.setMeters(getFloat(day.getData().getMeters(), errorValue));
                        histogramItem.setMetersSquare(getFloat(day.getData().getSquareMeters(), errorValue));

                        if (viewModel.getImpressionType() == null) {
                            viewModel.setImpressionType(day.getData().getImpressionType());
                        }
                    }

                    ShiftViewModel shiftViewModel = parseShiftData(day.getShiftData());

                    if (shiftViewModel != null) {

                        viewModel.setShiftSupport(shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT);

                        if (!histogramLegends.containsKey(shiftViewModel.getShiftId())) {

                            TodayHistogramViewModel.HistogramShiftLegend histogramShiftLegend = new TodayHistogramViewModel.HistogramShiftLegend();
                            histogramShiftLegend.setLegendName(shiftViewModel.getShiftName());
                            histogramShiftLegend.setLegendColor(SHIFTS_COLORS[shiftsColorIndex++]);
                            histogramLegends.put(shiftViewModel.getShiftId(), histogramShiftLegend);

                        }
                    }

                    histogramItem.setShift(shiftViewModel);

                }

            }

            Collections.sort(histogramData, TodayHistogramViewModel.TodayHistogramItem.COMPARATOR);

            viewModel.setData(histogramData);
            viewModel.setShiftLegends(histogramLegends);

            return viewModel;
        }

        return null;
    }

    private static TodayHistogramViewModel parseActualVsTargetData(BusinessUnitViewModel businessUnitViewModel, RealtimeActualVsTargetGraphData graphData) {

        TodayHistogramViewModel viewModel = new TodayHistogramViewModel();
        viewModel.setBusinessUnit(businessUnitViewModel.getBusinessUnit());

        if (graphData != null) {
            MeasureTypeEnum measureTypeEnum = MeasureTypeEnum.from(graphData.getMeasureType());

            List<TodayHistogramViewModel.TodayHistogramItem> histogramData = new ArrayList<>();

            if (graphData.getLast30PvData() != null) {
                for (int index = 0; index < graphData.getLast30PvData().size(); index++) {

                    RealtimeActualVsTargetGraphData.HistogramItem histogramDataItem = graphData.getLast30PvData().get(index);
                    RealtimeActualVsTargetGraphData.DayItem dayItem = histogramDataItem.getPvData();
                    RealtimeActualVsTargetGraphData.DayItem lastWeekItem = histogramDataItem.getLastWeekPvData();

                    if (dayItem != null && dayItem.getDate() != null) {

                        TodayHistogramViewModel.TodayHistogramItem histogramItem = new TodayHistogramViewModel.TodayHistogramItem();

                        histogramItem.setDate(HPDateUtils.parseDate(dayItem.getDate(), DEFAULT_DATE_FORMAT_NO_TIME));
                        if (dayItem.getValue() != null) {
                            histogramItem.setActual(dayItem.getValue().intValue());
                        }
                        if ((measureTypeEnum == MeasureTypeEnum.MIXED ||
                                measureTypeEnum == MeasureTypeEnum.SHEETS)
                                && dayItem.getSheets() != null) {
                            histogramItem.setSheets(dayItem.getSheets().intValue());
                        }
                        if ((measureTypeEnum == MeasureTypeEnum.MIXED ||
                                measureTypeEnum == MeasureTypeEnum.LENGTH)
                                && dayItem.getMeters() != null) {
                            histogramItem.setMeters(dayItem.getMeters().intValue());
                        }
                        if ((measureTypeEnum == MeasureTypeEnum.MIXED ||
                                measureTypeEnum == MeasureTypeEnum.LENGTH)
                                && dayItem.getSqm() != null) {
                            histogramItem.setMetersSquare(dayItem.getSqm().intValue());
                        }
                        if (lastWeekItem != null) {
                            if (lastWeekItem.getValue() != null) {
                                histogramItem.setLastWeekValue(lastWeekItem.getValue().intValue());
                            }
                            if (lastWeekItem.getSheets() != null) {
                                histogramItem.setLastWeekSheets(lastWeekItem.getSheets().intValue());
                            }
                            if (lastWeekItem.getSqm() != null) {
                                histogramItem.setLastWeekMetersSquare(lastWeekItem.getSqm().intValue());
                            }
                            if (lastWeekItem.getMeters() != null) {
                                histogramItem.setLastWeekMeters(lastWeekItem.getMeters().intValue());
                            }
                        }
                        histogramData.add(histogramItem);
                    }
                }
            }

            Collections.sort(histogramData, TodayHistogramViewModel.TodayHistogramItem.COMPARATOR);
            viewModel.setData(histogramData);
            viewModel.setImpressionType(graphData.getImpressionsType());
            viewModel.setMeasureTypeEnum(measureTypeEnum);

            return viewModel;
        }

        return null;
    }

    private static float getFloat(Double value, float defaultValue) {
        if (value != null) {
            return value.floatValue();
        }
        return defaultValue;
    }

    public static Observable<PrintBeatDashboardViewModel> getLatexDashboardViewModel(final BusinessUnitViewModel businessUnitViewModel, boolean perSite, boolean isShiftSupport, boolean isPolling) {

        return Observable.combineLatest(getPrintBeatViewModel(businessUnitViewModel, perSite, isShiftSupport, isPolling), getLatexDevicesStatisticsData(businessUnitViewModel), new Func2<PrintBeatDashboardViewModel, List<DeviceExtraData>, PrintBeatDashboardViewModel>() {
            @Override
            public PrintBeatDashboardViewModel call(PrintBeatDashboardViewModel printBeatDashboardViewModel, List<DeviceExtraData> deviceExtraDatas) {

                List<DeviceViewModel> printBeatDevices = printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels();

                if (printBeatDevices != null && !printBeatDevices.isEmpty()
                        && deviceExtraDatas != null && !deviceExtraDatas.isEmpty()) {

                    for (DeviceViewModel deviceViewModel : printBeatDevices) {
                        for (DeviceExtraData deviceExtraData : deviceExtraDatas) {
                            if (deviceExtraData.getSerialNumber().equals(deviceViewModel.getSerialNumber())) {
                                deviceViewModel.setExtraData(deviceExtraData);
                                deviceViewModel.setHasExtraData(true);
                                //Get device last update time from device stats if it is null in PB.
                                if (deviceViewModel.getLastUpdateTime() == null && deviceExtraData.getLastUpdatedAt() != null) {
                                    deviceViewModel.setLastUpdateTime(deviceExtraData.getLastUpdatedAt());
                                    deviceViewModel.setDeviceLastUpdateType(DeviceLastUpdateType.from(deviceViewModel.getLastUpdateTime(),
                                            businessUnitViewModel.getBusinessUnit(), deviceViewModel.isWrongTZ()));
                                }
                                break;
                            }
                        }
                    }
                }

                printBeatDashboardViewModel.setProductionViewModel(parseLatexProductionSnapshotData(printBeatDevices, deviceExtraDatas));

                return printBeatDashboardViewModel;
            }
        });

    }

    private static ProductionViewModel parseLatexProductionSnapshotData(List<DeviceViewModel> printBeatDevices, List<DeviceExtraData> deviceExtraDatas) {

        if (deviceExtraDatas == null) {
            return null;
        }

        int queued = 0, printing = 0, printed = 0, failedPrinted = 0;

        for (DeviceExtraData model : deviceExtraDatas) {
            queued += model.getFutureJobs() == null ? 0 : model.getFutureJobs().size();
            printing += model.getPrintingJobs() == null ? 0 : model.getPrintingJobs().size();
        }

        //Get total number of printed jobs from PB as requested.
        if (printBeatDevices != null) {
            for (DeviceViewModel deviceViewModel : printBeatDevices) {
                printed += (deviceViewModel.getPrintedJobs() > 0 ? deviceViewModel.getPrintedJobs() : 0);
            }
        }

        ProductionViewModel viewModel = new ProductionViewModel();
        viewModel.setNumberOfPrintingJobs(printing);
        viewModel.setBusinessUnitEnum(BusinessUnitEnum.LATEX_PRINTER);
        viewModel.setJobsInQueue(queued);
        viewModel.setNumberOfTodayFailedPrintedJobs(failedPrinted);
        viewModel.setNumberOfTodayPrintedJobs(printed);

        return viewModel;
    }


    private static PrintBeatDashboardViewModel mapTodayData(Context context, BusinessUnitViewModel businessUnitViewModel, SiteViewModel siteViewModel, boolean perSite, List<TargetData> targetDataList, List<PrintVolumeData> printVolumeDataList, List<FilterItem> selectedDevices) {

        List<DeviceViewModel> pressViewModels = new ArrayList<>();

        TodayViewModel todayViewModel = new TodayViewModel();

        ProductionViewModel productionViewModel = new ProductionViewModel();
        productionViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

        //Getting the Indigo devices.
        Map<String, DeviceData> buDevices = businessUnitViewModel.getDevices();

        //Loops through the data and combines the print volume data and target data in one object for each press.
        if (buDevices != null && !buDevices.values().isEmpty() && printVolumeDataList != null && targetDataList != null) {

            for (PrintVolumeData printVolumeData : printVolumeDataList) {

                TargetData currentTargetData = null;
                for (TargetData targetData : targetDataList) {
                    if (targetData.getIntraDailyTarget().getDevice().equalsIgnoreCase(printVolumeData.getDevice())) {
                        currentTargetData = targetData;
                        break;
                    }
                }

                //selected device here is representing the total printing and target data of all presses.
                if (printVolumeData.getDevice().equalsIgnoreCase(SELECTED_PRESS_ALL)) {

                    todayViewModel.setPrintVolumeValue(printVolumeData.getValue());

                    if (currentTargetData != null) {
                        todayViewModel.setPrintVolumeShiftTargetValue(currentTargetData.getShiftTarget().getValue());
                        todayViewModel.setPrintVolumeIntraDailyTargetValue(currentTargetData.getIntraDailyTarget().getValue());
                    }

                    TodayInfoTooltipViewModel tooltipViewModel = new TodayInfoTooltipViewModel();
                    tooltipViewModel.setImpressionsList(new ArrayList<TodayInfoTooltipViewModel.InfoItemViewModel>());
                    tooltipViewModel.setPrintedList(new ArrayList<TodayInfoTooltipViewModel.InfoItemViewModel>());

                    todayViewModel.setInfoTooltipViewModel(tooltipViewModel);

                    int color = HPScoreUtils.getProgressColor(context,
                            todayViewModel.getBusinessUnitViewModel() == null ? BusinessUnitEnum.UNKNOWN : todayViewModel.getBusinessUnitViewModel().getBusinessUnit(),
                            todayViewModel.getPrintVolumeValue(), todayViewModel.getPrintVolumeIntraDailyTargetValue(), false);

                    List<TodayViewModel.Impression> impressions = new ArrayList<>();

                    for (PrintVolumeData.ImpressionValue impressionValue : printVolumeData.getValues()) {

                        impressions.add(new TodayViewModel.Impression(impressionValue.getName(), impressionValue.getValue()));

                        TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(impressionValue.getName(),
                                HPLocaleUtils.getLocalizedValue(impressionValue.getValue().intValue()), color);
                        tooltipViewModel.getImpressionsList().add(infoItemViewModel);

                    }

                    PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();

                    if (printVolumeData.getLitersConsumed() != null) {
                        String itemName = context.getString(Unit.LITERS.
                                getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                        TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, HPLocaleUtils.getDecimalString(printVolumeData.getLitersConsumed().floatValue(), true, MAX_DECIMAL_PLACE), color);
                        tooltipViewModel.getPrintedList().add(infoItemViewModel);
                    }

                    if (printVolumeData.getSheets() != null) {
                        String label = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? context.getString(R.string.sheets) : context.getString(R.string.sheets_printed);
                        String value = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? DevicesUtils.getPrintedValue(printVolumeData.getSheets()) : String.valueOf(printVolumeData.getSheets());
                        TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(label, value, color);
                        infoItemViewModel.setImpressionType(printVolumeData.getImpressionType());
                        tooltipViewModel.getPrintedList().add(infoItemViewModel);
                    }
                    todayViewModel.setSheets(printVolumeData.getSheets() == null ? -1 : printVolumeData.getSheets());


                    if (printVolumeData.getMeters() != null) {
                        String itemName = context.getString(Unit.METER.
                                getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                        TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, DevicesUtils.getPrintedValue(printVolumeData.getMeters().intValue()), color);
                        tooltipViewModel.getPrintedList().add(infoItemViewModel);
                    }
                    todayViewModel.setMeters(printVolumeData.getMeters() == null ? -1 : printVolumeData.getMeters().intValue());

                    if (printVolumeData.getSquareMeters() != null) {
                        String itemName = context.getString(Unit.SQM.
                                getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                        TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, DevicesUtils.getPrintedValue(printVolumeData.getSquareMeters().intValue()), color);
                        tooltipViewModel.getPrintedList().add(infoItemViewModel);
                    }
                    todayViewModel.setSquareMeters(printVolumeData.getSquareMeters() == null ? -1 : printVolumeData.getSquareMeters().intValue());

                    todayViewModel.setValueImpressions(impressions);

                    if (!tooltipViewModel.getImpressionsList().isEmpty()) {
                        Collections.sort(tooltipViewModel.getImpressionsList(), TodayInfoTooltipViewModel.NAME_COMPARATOR);
                    }

                    tooltipViewModel.setHasInfo(businessUnitViewModel.getBusinessUnit() != BusinessUnitEnum.LATEX_PRINTER
                            && (!tooltipViewModel.getImpressionsList().isEmpty() ||
                            !tooltipViewModel.getPrintedList().isEmpty()));

                    todayViewModel.setImpressionType(printVolumeData.getImpressionType());

                } else {


                    DeviceViewModel deviceViewModel = new DeviceViewModel();

                    DeviceData deviceData = buDevices.get(printVolumeData.getDevice());

                    List<DeviceViewModel.Job> queuedJobs = new ArrayList<>();
                    List<PrintVolumeData.Job> queuedJobsData = printVolumeData.getNextTenJobs();
                    if (queuedJobsData != null) {
                        for (PrintVolumeData.Job Job : queuedJobsData) {
                            DeviceViewModel.Job indigoJob = new DeviceViewModel.Job();
                            indigoJob.setIndex(Job.getIndex());
                            indigoJob.setName(Job.getJobName());
                            indigoJob.setDuration(Job.getTimeToComplete() == null ? 0 : Job.getTimeToComplete().getDuration());
                            try {
                                indigoJob.setTimeUnit((Job.getTimeToComplete() == null || Job.getTimeToComplete().getUnit() == null) ?
                                        TimeUnit.SECONDS : TimeUnit.valueOf(Job.getTimeToComplete().getUnit()));
                            } catch (Exception e) {
                                indigoJob.setTimeUnit(TimeUnit.SECONDS);
                            }
                            queuedJobs.add(indigoJob);
                        }
                    }

                    if (deviceData != null && deviceData.getMessage() != null && deviceData.getMessage().getLocalizedMsg() != null) {
                        DeviceViewModel.Message message = new DeviceViewModel.Message();
                        message.setMsg(deviceData.getMessage().getLocalizedMsg());
                        message.setMsgID(deviceData.getMessage().getMsgId());

                        deviceViewModel.setMessage(message);
                    }

                    if (printVolumeData.getOperatorInfo() != null) {
                        DeviceViewModel.OperatorInfo operatorInfo = new DeviceViewModel.OperatorInfo();
                        operatorInfo.setOperatorFirstName(printVolumeData.getOperatorInfo().getOperatorFirstName());
                        operatorInfo.setOperatorLastName(printVolumeData.getOperatorInfo().getOperatorLastName());
                        operatorInfo.setOperatorUserId(printVolumeData.getOperatorInfo().getOperatorUserId());
                        operatorInfo.setOperatorImgUrl(printVolumeData.getOperatorInfo().getOperatorImgUrl());
                        deviceViewModel.setOperatorInfo(operatorInfo);
                    }

                    boolean isInShift = printVolumeData.getInShift() == null ? true : printVolumeData.getInShift().booleanValue();
                    if (!isInShift) {
                        DeviceViewModel.Message message = new DeviceViewModel.Message();
                        message.setMsg(context.getString(businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER ?
                                R.string.printer_message_currently_not_in_active_shift : R.string.press_message_currently_not_in_active_shift));

                        deviceViewModel.setMessage(message);
                    }

                    List<DeviceViewModel.Job> completedJobs = new ArrayList<>();
                    List<PrintVolumeData.Job> completedJobsData = printVolumeData.getLastPrintedJobs();
                    if (completedJobsData != null) {
                        for (PrintVolumeData.Job Job : completedJobsData) {
                            DeviceViewModel.Job indigoJob = new DeviceViewModel.Job();
                            indigoJob.setName(Job.getJobName());
                            completedJobs.add(indigoJob);
                        }
                    }

                    deviceViewModel.setSerialNumber(deviceData.getSerialNumber());
                    deviceViewModel.setSerialNumberDisplay(deviceData.getSerialNumberDisplay() == null ?
                            deviceData.getSerialNumber() : deviceData.getSerialNumberDisplay());

                    Map<DeviceViewModel.JobType, List<DeviceViewModel.Job>> jobs = new HashMap<>();
                    jobs.put(DeviceViewModel.JobType.QUEUED, queuedJobs);
                    jobs.put(DeviceViewModel.JobType.COMPLETED, completedJobs);
                    deviceViewModel.setJobs(jobs);

                    String lastUpdateDateUTC = printVolumeData.getLastUpdateLfp();
                    if (lastUpdateDateUTC != null) {
                        Date utcDate = HPDateUtils.parseDate(lastUpdateDateUTC, SERVICE_CALL_DATE_FORMAT, UTC_TIME_ZONE);
                        String defaultDateFormat = HPDateUtils.formatDate(utcDate, DEFAULT_DATE_FORMAT, UTC_TIME_ZONE);
                        deviceViewModel.setLastUpdateTime(HPDateUtils.parseDate(defaultDateFormat, DEFAULT_DATE_FORMAT));
                    }

                    deviceViewModel.setDeviceLastUpdateType(DeviceLastUpdateType.from(deviceViewModel.getLastUpdateTime(), businessUnitViewModel.getBusinessUnit(),
                            printVolumeData.getTimeSynced() != null && !printVolumeData.getTimeSynced()));

                    PrintVolumeData.PressState pressState = printVolumeData.getPressState();
                    boolean isUnderWarranty = printVolumeData.isUnderWarranty() == null ?
                            true : printVolumeData.isUnderWarranty().booleanValue();

                    DeviceState deviceState;
                    if (pressState == null) {
                        if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER) {
                            deviceState = DeviceState.from(DeviceState.NULL.getState(), DeviceState.DeviceStateColor.NEUTRAL.getColorType());
                        } else {
                            deviceState = DeviceState.UNW;
                        }
                    } else {
                        deviceState = DeviceState.from(pressState.getState(), pressState.getColor());
                    }

                    deviceViewModel.setSortPosition(deviceData.getSortPosition());
                    deviceViewModel.setDeviceState(deviceState);
                    deviceViewModel.setDeviceStateDisplayName((pressState == null || TextUtils.isEmpty(pressState.getState())) ? "" : pressState.getState());
                    deviceViewModel.setMaintenanceState(MaintenanceState.from(printVolumeData.getMaintenanceState()));
                    deviceViewModel.setUnderWarranty(isUnderWarranty);
                    PrintVolumeData.TimeToDone timeToDone = printVolumeData.getTimeToDone();
                    deviceViewModel.setDurationToDone(timeToDone == null ? -1 : timeToDone.getDuration());
                    deviceViewModel.setDurationToDoneUnit(timeToDone == null ? null : TimeUnit.valueOf(timeToDone.getUnit()));
                    deviceViewModel.setPrintVolumeValue(printVolumeData.getValue());
                    deviceViewModel.setName(deviceData.getDeviceName());
                    deviceViewModel.setWrongTZ(printVolumeData.getTimeSynced() != null && !printVolumeData.getTimeSynced());
                    deviceViewModel.setModel(deviceData.getPressModel());
                    deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(businessUnitViewModel.getBusinessUnit(), deviceData.getPressModel()));
                    deviceViewModel.setTimeInStateMin(printVolumeData.getTimeInStateMin());
                    deviceViewModel.setImpressionType(printVolumeData.getImpressionType());

                    deviceViewModel.setPrintedJobs(printVolumeData.getPrintedJobs() == null ? -1 : (int) printVolumeData.getPrintedJobs().longValue());
                    deviceViewModel.setSheets(printVolumeData.getSheets() == null ? -1 : printVolumeData.getSheets());
                    deviceViewModel.setMeters(printVolumeData.getMeters() == null ? -1 : (int) printVolumeData.getMeters().doubleValue());
                    deviceViewModel.setMetersSquare(printVolumeData.getSquareMeters() == null ? -1 : (int) printVolumeData.getSquareMeters().doubleValue());
                    deviceViewModel.setLitersConsumed(printVolumeData.getLitersConsumed() == null ? -1 : printVolumeData.getLitersConsumed());
                    deviceViewModel.setCurrentJob(printVolumeData.getCurrentJob());
                    deviceViewModel.setCurrentJobProgress(printVolumeData.getCurrentJobProgress());

                    PrintVolumeData.TimeToDone currentJobTimeToDone = printVolumeData.getCurrentJobTimeToDone();
                    deviceViewModel.setCurrentJobTimeToDone(currentJobTimeToDone == null ? -1 : currentJobTimeToDone.getDuration());
                    deviceViewModel.setCurrentJobTimeToDoneUnit(currentJobTimeToDone == null ? null : TimeUnit.valueOf(currentJobTimeToDone.getUnit()));

                    deviceViewModel.setJobsInQueue(printVolumeData.getJobsInQueue() != null ? printVolumeData.getJobsInQueue() : 0);
                    deviceViewModel.setLastSeenMin(printVolumeData.getLastSeenMin() != null ? printVolumeData.getLastSeenMin() : 0);

                    deviceViewModel.setRTSupported(deviceData.isRtSupported());

                    if (deviceViewModel.getPrintedJobs() > -1) {
                        productionViewModel.setJobsCompleted(productionViewModel.getJobsCompleted() + deviceViewModel.getPrintedJobs());
                    }

                    productionViewModel.setJobsInQueue(productionViewModel.getJobsInQueue() + deviceViewModel.getJobsInQueue());

                    productionViewModel.setHasCompletedJobs(productionViewModel.isHasCompletedJobs() ||
                            (completedJobs != null && !completedJobs.isEmpty()));

                    //Match a print volume object with its corresponding object in the target data list.
                    if (currentTargetData != null) {
                        deviceViewModel.setPrintVolumeShiftTargetValue(currentTargetData.getShiftTarget().getValue());
                        deviceViewModel.setPrintVolumeIntraDailyTargetValue(currentTargetData.getIntraDailyTarget().getValue());
                    }

                    deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

                    pressViewModels.add(deviceViewModel);
                }

            }
        }

        //If perSite.. then all the non-RT devices are already included.
        if (!perSite)

        {
            for (FilterItem filterItem : selectedDevices) {
                if (filterItem instanceof DeviceFilterViewModel) {
                    if (!((DeviceFilterViewModel) filterItem).isRtSupported()) {
                        DeviceViewModel deviceViewModel = new DeviceViewModel();
                        deviceViewModel.setName(filterItem.getName());
                        deviceViewModel.setRTSupported(false);
                        deviceViewModel.setDeviceState(DeviceState.UNW);
                        deviceViewModel.setSerialNumber(((DeviceFilterViewModel) filterItem).getSerialNumber());
                        deviceViewModel.setSerialNumberDisplay(((DeviceFilterViewModel) filterItem).getSerialNumberDisplay());
                        deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(businessUnitViewModel.getBusinessUnit(), ((DeviceFilterViewModel) filterItem).getPressModel()));
                        deviceViewModel.setImpressionType(((DeviceFilterViewModel) filterItem).getImpressionType());
                        deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

                        DeviceData deviceData = buDevices.get(deviceViewModel.getSerialNumber());

                        if (deviceData != null) {
                            if (deviceData.getMessage() != null && deviceData.getMessage().getLocalizedMsg() != null) {
                                DeviceViewModel.Message message = new DeviceViewModel.Message();
                                message.setMsg(deviceData.getMessage().getLocalizedMsg());
                                message.setMsgID(deviceData.getMessage().getMsgId());

                                deviceViewModel.setMessage(message);
                            }
                            deviceViewModel.setSortPosition(deviceData.getSortPosition());
                        }

                        pressViewModels.add(deviceViewModel);
                    }
                }
            }
        }


        todayViewModel.setDeviceViewModels(pressViewModels);

        PrintBeatDashboardViewModel dashboardViewModel = new PrintBeatDashboardViewModel();
        todayViewModel.setSiteViewModel(siteViewModel);
        todayViewModel.setBusinessUnitViewModel(businessUnitViewModel);
        dashboardViewModel.setTodayViewModel(todayViewModel);
        dashboardViewModel.setProductionViewModel(productionViewModel);

        return dashboardViewModel;

    }

    public static Observable<List<DeviceExtraData>> getLatexDevicesStatisticsData(
            final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ConfigService configService = serviceProvider.getConfigService();

        List<String> devicesIds = businessUnitViewModel.getFiltersViewModel().getSiteDevicesIds();

        return configService.getDevicesStatisticsData(devicesIds).map(new Func1<Response<DevicesStatisticsData>, List<DeviceExtraData>>() {
            @Override
            public List<DeviceExtraData> call(Response<DevicesStatisticsData> devicesDataResponse) {
                return mapDeviceStatisticsDataToDevices(devicesDataResponse.body(), businessUnitViewModel.getFiltersViewModel().getSelectedDevices());
            }
        });

    }

    private static List<DeviceExtraData> mapDeviceStatisticsDataToDevices
            (DevicesStatisticsData
                     devicesStatisticsData, List<FilterItem> filterItems) {

        List<DevicesStatisticsData.DeviceStatistics> devicesStatistics = devicesStatisticsData == null ? null : devicesStatisticsData.getDeviceStatistics();

        List<DeviceExtraData> printersViewModel = new ArrayList<>();

        for (FilterItem filterItem : filterItems) {

            DeviceFilterViewModel deviceViewModel = (DeviceFilterViewModel) filterItem;

            DeviceExtraData deviceExtraData = new DeviceExtraData();

            deviceExtraData.setSerialNumber(deviceViewModel.getSerialNumber());
            deviceExtraData.setName(deviceViewModel.getDeviceName());
            deviceExtraData.setState(DeviceState.UNW);

            printersViewModel.add(deviceExtraData);

            if (devicesStatistics != null) {

                for (DevicesStatisticsData.DeviceStatistics deviceStatistics : devicesStatistics) {

                    if (deviceStatistics.getDeviceId().equals(deviceViewModel.getDeviceId())) {

                        DevicesStatisticsData.DeviceStatistics.Data data = deviceStatistics.getData();

                        List<InkModel> inkModels = new ArrayList<>();
                        List<SubstrateModel> substrateModels = new ArrayList<>();
                        List<JobModel> futureJobs = new ArrayList<>();
                        List<JobModel> printingJobs = new ArrayList<>();
                        List<JobModel> printedJobs = new ArrayList<>();

                        if (data != null) {

                            GeneralData generalData = data.getGeneralData();

                            if (generalData != null && generalData.getPrinterData() != null) {

                                PrinterData printerData = generalData.getPrinterData().getData();
                                PrinterData.PrinterInfo info = printerData == null ? null : printerData.getPrinterInfo();
                                Notifications notifications = printerData == null ? null : printerData.getNotifications();

                                if (info != null) {

                                    deviceExtraData.setStateDisplayName(TextUtils.isEmpty(info.getDeviceStatus()) ? "" : info.getDeviceStatus());
                                    deviceExtraData.setState(DeviceState.from(info.getDeviceStatus(),
                                            DeviceState.DeviceStateColor.NEUTRAL.getColorType()));
                                    deviceExtraData.setModel(info.getPrinterModel());
                                    deviceExtraData.setMessage(info.getStatusMessage());

                                    //different formats returned
                                    Date updateDate = HPDateUtils.parseDate(info.getLastUpdateAt(),
                                            DATA_FORMAT_LATEX_JOB_COMPLETION);
                                    updateDate = updateDate != null ? updateDate : HPDateUtils.parseDate(info.getLastUpdateAt(),
                                            LAST_UPDATED_TIME_DATE_FORMAT_V2);
                                    deviceExtraData.setLastUpdatedAt(updateDate);

                                    deviceExtraData.setFirmwareVersion(info.getPrinterFirmwareVersion());

                                }

                                if (notifications != null) {
                                    deviceExtraData.setFrontPanelMessage(notifications.getFrontPanelMessage());
                                }
                            }


                            DevicesStatisticsData.InkData inkData = data.getInkData();
                            List<DevicesStatisticsData.InkData.InkInfo> inkInfoList = inkData == null ? null : inkData.getInkInfo();
                            if (inkInfoList != null) {
                                for (DevicesStatisticsData.InkData.InkInfo inkInfo : inkInfoList) {
                                    InkModel inkModel = new InkModel();
                                    inkModel.setInkEnum(InkModel.InkEnum.from(inkInfo.getConsumableLabelCode()));
                                    inkModel.setInkState(InkModel.InkState.from(inkInfo.getMeasuredQuantityState()));
                                    inkModel.setInkApiName(inkInfo.getConsumableLabelCode());
                                    try {
                                        inkModel.setLevel((int) Double.parseDouble(inkInfo.getLevel()));
                                        inkModel.setCapacity((int) Double.parseDouble(inkInfo.getMaxCapacity()));
                                    } catch (Exception e) {
                                        HPLogger.d(TAG, " error while reading inks");
                                    }
                                    inkModel.setExpiryDate(inkInfo.getExpirationDate());
                                    inkModels.add(inkModel);
                                }
                            }

                            DevicesStatisticsData.SubstrateData substrateData = data.getSubstrateData();
                            List<DevicesStatisticsData.SubstrateData.Substrate> substrateList = substrateData == null ? null : substrateData.getSubstrates();
                            if (substrateList != null) {
                                for (DevicesStatisticsData.SubstrateData.Substrate substrate : substrateList) {
                                    SubstrateModel substrateModel = new SubstrateModel();

                                    substrateModel.setMediaName(substrate.getMediaName());
                                    substrateModel.setUnit(SubstrateModel.SubstrateUnit.from(substrate.getUnit()));

                                    try {
                                        substrateModel.setMediaCounter((float) Double.parseDouble(substrate.getSubstrateCounter()));
                                    } catch (Exception e) {
                                        substrateModel.setMediaCounter(-1f);
                                    }

                                    try {
                                        substrateModel.setMediaWidth((float) Double.parseDouble(substrate.getMediaWidth()));
                                    } catch (Exception e) {
                                        substrateModel.setMediaWidth(-1f);
                                    }

                                    substrateModel.setRemainingLength(substrate.getRemainingLength());

                                    substrateModels.add(substrateModel);
                                }
                            }

                            DevicesStatisticsData.JobData jobData = data.getJobData();
                            JobsData jobs = jobData == null ? null : jobData.getJobs();
                            List<JobsData.FutureJob> futureJobData = jobs == null ? null : jobs.getFuture();
                            List<JobsData.FutureJob> presentJobData = jobs == null ? null : jobs.getPrintingJobs();
                            List<JobsData.HistoryJob> historyJobData = jobs == null ? null : jobs.getHistory();

                            if (futureJobData != null) {
                                for (JobsData.FutureJob futureJob : futureJobData) {
                                    JobModel futureJobModel = new JobModel();
                                    futureJobModel.setJobName(futureJob.getFileName());
                                    futureJobModel.setJobImageUrl(futureJob.getThumbnailUrl());
                                    futureJobModel.setProgress(futureJob.getProgress());
                                    futureJobModel.setEndState(JobModel.EndState.from(futureJob.getJobState()));
                                    futureJobs.add(futureJobModel);
                                }
                            }
                            if (presentJobData != null) {
                                for (JobsData.FutureJob printingJob : presentJobData) {
                                    JobModel jobModel = new JobModel();
                                    jobModel.setJobName(printingJob.getFileName());
                                    jobModel.setJobImageUrl(printingJob.getThumbnailUrl());
                                    jobModel.setProgress(printingJob.getProgress());
                                    jobModel.setEndState(JobModel.EndState.from(printingJob.getJobState()));
                                    printingJobs.add(jobModel);
                                }
                            }
                            if (historyJobData != null) {
                                for (JobsData.HistoryJob historyJob : historyJobData) {
                                    JobModel jobModel = new JobModel();
                                    jobModel.setJobName(historyJob.getFileName());
                                    jobModel.setFinishTime(HPDateUtils.parseDate(historyJob.getCompletionTime(), DATA_FORMAT_LATEX_JOB_COMPLETION));
                                    jobModel.setEndState(JobModel.EndState.from(historyJob.getEndState()));
                                    printedJobs.add(jobModel);
                                }
                            }
                        }

                        Collections.sort(inkModels, InkModel.COMPARATOR);
                        deviceExtraData.setInkList(inkModels);
                        deviceExtraData.setSubstrateList(substrateModels);
                        deviceExtraData.setFutureJobs(futureJobs);
                        deviceExtraData.setPrintedJobs(printedJobs);
                        deviceExtraData.setPrintingJobs(printingJobs);

                        break;
                    }
                }
            }

        }

        return printersViewModel;
    }

    public static Observable<ServiceCallViewModel> getServiceCallData(
            final BusinessUnitViewModel businessUnitViewModel) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ServiceCallService serviceCallService = serviceProvider.getServiceCallService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        final int closedDaysBack = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).closedServiceCallsDays(CLOSED_SERVICE_CALLS_DAYS_DEFAULT);

        String cacheID = ApiCacheUtils.getListAsString(serialNumbers) + closedDaysBack;
        if (ApiCacheUtils.didCacheIDChange(SERVICE_CALL_CASH_ID_KEY, cacheID)) {
            ApiCacheUtils.clearApiCache(SERVICE_CALL_CASH_KEY);
        }

        return ApiCacheUtils.getCacheAndUpdate(serviceCallService.getServiceCalls(serialNumbers, closedDaysBack),
                SERVICE_CALL_CASH_KEY,
                new TypeReference<ServiceCallData>() {
                })
                .map(new Func1<Response<ServiceCallData>, ServiceCallViewModel>() {
                    @Override
                    public ServiceCallViewModel call(Response<ServiceCallData> serviceCallDataResponse) {
                        if (serviceCallDataResponse != null && serviceCallDataResponse.isSuccessful()) {
                            ServiceCallViewModel serviceCallViewModel = parseServiceCallData(serviceCallDataResponse.body(), businessUnitViewModel);
                            serviceCallViewModel.setClosedServiceCallsDays(closedDaysBack);
                            return serviceCallViewModel;
                        }
                        return null;
                    }
                });
    }

    private static ServiceCallViewModel parseServiceCallData(ServiceCallData
                                                                     serviceCallData, BusinessUnitViewModel businessUnitViewModel) {
        if (serviceCallData == null || serviceCallData.getCallsDataList() == null || businessUnitViewModel == null
                || businessUnitViewModel.getDevices() == null) {
            return null;
        }

        Map<String, DeviceData> deviceViewModels = businessUnitViewModel.getDevices();
        List<ServiceCallData.CallsData> callsData = serviceCallData.getCallsDataList();

        List<ServiceCallViewModel.CallViewModel> callViewModels = new LinkedList<>();
        int numberOfOpenCalls = 0;
        int numberOfClosedCalls = 0;
        ServiceCallViewModel serviceCallViewModel = new ServiceCallViewModel();

        for (int i = 0; i < callsData.size(); i++) {
            ServiceCallData.CallsData call = callsData.get(i);

            ServiceCallViewModel.CallViewModel callViewModel = new ServiceCallViewModel.CallViewModel();
            callViewModel.setState(ServiceCallViewModel.ServiceCallStateEnum.from(call.getStatus()));
            callViewModel.setOpenedBy(call.getCallerName());
            callViewModel.setId(call.getCallId());
            callViewModel.setType(call.getIssueType());
            callViewModel.setDescription(call.getDescription());

            if (call.getOpenDate() != null) {
                callViewModel.setDateOpened(HPDateUtils.parseDate(call.getOpenDate(), SERVICE_CALL_DATE_FORMAT));
            }

            if (call.getCloseDate() != null) {
                callViewModel.setDateClosed(HPDateUtils.parseDate(call.getCloseDate(), SERVICE_CALL_DATE_FORMAT));
            }

            if (call.getSerialNumber() != null) {
                callViewModel.setPressSerialNumber(call.getSerialNumber());
                if (deviceViewModels.containsKey(call.getSerialNumber())) {
                    DeviceData deviceData = deviceViewModels.get(call.getSerialNumber());
                    callViewModel.setPressName(deviceData.getDeviceName());
                    callViewModel.setImpressionType(deviceData.getImpressionType());
                }

            }

            callViewModels.add(callViewModel);
            switch (callViewModel.getState()) {
                case OPEN:
                    numberOfOpenCalls += 1;
                    break;
                case CANCELED:
                case CLOSED:
                    numberOfClosedCalls += 1;
                    break;
                default:
                    break;
            }
        }

        Collections.sort(callViewModels, ServiceCallViewModel.SERVICE_CALL_COMPARATOR);
        serviceCallViewModel.setCallViewModels(callViewModels);
        serviceCallViewModel.setNumberOfClosedCalls(numberOfClosedCalls);
        serviceCallViewModel.setNumberOfOpenCalls(numberOfOpenCalls);

        return serviceCallViewModel;
    }

    /**
     * Returns an observable object to get a list of week view models given a list of devices.
     */
    public static Observable<WeekCollectionViewModel> getMultipleWeekData(
            final Context context, final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();
        MetaDataService metaDataService = serviceProvider.getMetaDataService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();

        PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();

        String cacheID = businessUnitViewModel.getBusinessUnit().getName() +
                ApiCacheUtils.getListAsString(serialNumbers) +
                NUMBER_OF_WEEKS +
                unitSystem.name() +
                businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId() +
                PrintOSPreferences.getInstance(context).getLanguage();
        if (ApiCacheUtils.didCacheIDChange(WEEK_CACHE_ID_KEY, cacheID)) {
            ApiCacheUtils.clearApiCache(WEEK_CACHE_KEY);
        }

        return Observable.combineLatest(
                ApiCacheUtils.getCacheAndUpdate(metaDataService.getMetaData(),
                        META_DATA_DEVICE_CACHE_PREF_KEY,
                        new TypeReference<Map<String, BusinessUnit>>() {
                        }),
                ApiCacheUtils.getCacheAndUpdate(performanceTrackingService.getWeeklyReport(businessUnitViewModel.getBusinessUnit().getName(), serialNumbers, NUMBER_OF_WEEKS, null, unitSystem.name(), businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId(), PrintOSPreferences.getInstance(context).getLanguage()),
                        WEEK_CACHE_KEY,
                        new TypeReference<WeeklyDataV2>() {
                        }),
                new Func2<Response<Map<String, BusinessUnit>>, Response<WeeklyDataV2>, WeekCollectionViewModel>() {
                    @Override
                    public WeekCollectionViewModel call(Response<Map<String, BusinessUnit>> businessUnitsMapResponse, Response<WeeklyDataV2> weekDataListResponse) {
                        if (businessUnitsMapResponse.isSuccessful() && weekDataListResponse.isSuccessful()) {
                            if (weekDataListResponse.body() != null && weekDataListResponse.body().getData() != null) {
                                return parseWeekData(context, weekDataListResponse.body().getData(), businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                            }

                            return parseWeekData(context, new ArrayList<WeekData>(), businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                        }

                        return null;
                    }
                });

    }


    public static Observable<RankingViewModel> getSiteRankingData(final Context context, final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RankingService rankingService = serviceProvider.getRankingService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();

        String cacheID = businessUnitViewModel.getBusinessUnit().getName() +
                ApiCacheUtils.getListAsString(serialNumbers) +
                businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId() +
                PrintOSPreferences.getInstance(context).getLanguage();

        if (ApiCacheUtils.didCacheIDChange(RANKING_CACHE_ID_KEY, cacheID)) {
            ApiCacheUtils.clearApiCache(RANKING_CACHE_KEY);
        }

        return ApiCacheUtils.getCacheAndUpdate(rankingService.getSiteRankingData(businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId()
                , businessUnitViewModel.getBusinessUnit().getName(),
                PrintOSPreferences.getInstance(context).getLanguage()),
                RANKING_CACHE_KEY, new TypeReference<SiteRankingData>() {
                }).map(new Func1<Response<SiteRankingData>, RankingViewModel>() {
            @Override
            public RankingViewModel call(Response<SiteRankingData> siteRankingDataResponse) {

                if (siteRankingDataResponse != null && siteRankingDataResponse.isSuccessful()) {
                    return parseRankingData(context, siteRankingDataResponse.body(), businessUnitViewModel);
                }
                return null;
            }
        });

    }

    private static RankingViewModel parseRankingData(Context context, SiteRankingData siteRankingData, BusinessUnitViewModel businessUnitViewModel) {

        if (siteRankingData == null) {
            return null;
        }

        boolean isRankingEnabled;

        VersionsData versionsData = PrintOSPreferences.getInstance(context).getVersionsData();

        boolean shouldIgnoreFlags = PrintOSPreferences.getInstance(context).shouldIgnoreFlags();
        if (versionsData != null && versionsData.getConfiguration() != null &&
                versionsData.getConfiguration().getWeeklyPrintbeat() != null) {

            VersionsData.Configuration.WeeklyPrintbeat.RankingFeature rankingFeature
                    = versionsData.getConfiguration().getWeeklyPrintbeat().getRankingFeature();

            isRankingEnabled = (shouldIgnoreFlags || rankingFeature.isEnabled()) && rankingFeature.getSupportedEquipments().contains(businessUnitViewModel.getBusinessUnit().getName());
        } else {
            isRankingEnabled = false;
        }

        if (isRankingEnabled && businessUnitViewModel.getFiltersViewModel() != null) {

            boolean devicesBelongToGroup = businessUnitViewModel.getFiltersViewModel().isGroupSelected();

            isRankingEnabled = !devicesBelongToGroup;
        }

        if (isRankingEnabled) {

            boolean siteHasRankingData = siteRankingData.getRegion() != null
                    || siteRankingData.getWorldWide() != null
                    || siteRankingData.getSubRegion() != null;

            if (siteHasRankingData) {

                RankingViewModel rankingViewModel = new RankingViewModel();

                rankingViewModel.setWeek(siteRankingData.getWeek());
                rankingViewModel.setYear(siteRankingData.getYear());
                rankingViewModel.setSubRegionViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.SUB_REGION, siteRankingData.getSubRegion(), siteRankingData.getWeek(), siteRankingData.getYear(), siteRankingData.getCountryCode(),siteRankingData.getScore()));
                rankingViewModel.setHasSubRegionSection(rankingViewModel.getSubRegionViewModel() != null);

                rankingViewModel.setRegionViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.REGION, siteRankingData.getRegion(), siteRankingData.getWeek(), siteRankingData.getYear(), siteRankingData.getCountryCode(),siteRankingData.getScore()));
                rankingViewModel.setHasRegionSection(rankingViewModel.getRegionViewModel() != null);

                rankingViewModel.setWorldWideViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.WORLD_WIDE, siteRankingData.getWorldWide(), siteRankingData.getWeek(), siteRankingData.getYear(), siteRankingData.getCountryCode(),siteRankingData.getScore()));
                rankingViewModel.setHasWorldWideSection(rankingViewModel.getWorldWideViewModel() != null);

                boolean showTrophy = true;
                String topFiveMessage = null;
                if (rankingViewModel.hasWorldWideSection() && rankingViewModel.getWorldWideViewModel().getActualRank() <= 5) {
                    topFiveMessage = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_the_world);
                    rankingViewModel.setTopFiveWorld(true);
                } else if (rankingViewModel.hasRegionSection() && rankingViewModel.getRegionViewModel().getActualRank() <= 5) {
                    topFiveMessage = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_region, rankingViewModel.getRegionViewModel().getName());
                    rankingViewModel.setTopFiveRegion(true);
                } else if (rankingViewModel.hasSubRegionSection() && rankingViewModel.getSubRegionViewModel().getActualRank() <= 5) {
                    rankingViewModel.setTopFiveSubRegion(true);
                    topFiveMessage = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_sub_region, rankingViewModel.getSubRegionViewModel().getName());
                } else {
                    showTrophy = false;
                }

                if (rankingViewModel.hasWorldWideSection() && rankingViewModel.getWorldWideViewModel().getActualRank() <= 10) {
                    rankingViewModel.setTop10World(true);
                } else if (rankingViewModel.hasRegionSection() && rankingViewModel.getRegionViewModel().getActualRank() <= 10) {
                    rankingViewModel.setTop10Region(true);
                } else if (rankingViewModel.hasSubRegionSection() && rankingViewModel.getSubRegionViewModel().getActualRank() <= 10) {
                    rankingViewModel.setTop10SubRegion(true);
                } else if (rankingViewModel.hasWorldWideSection() && rankingViewModel.getWorldWideViewModel().getActualRank() <= 100) {
                    rankingViewModel.setTop100World(true);
                }

                rankingViewModel.setShowTrophy(showTrophy);
                rankingViewModel.setTopFiveMessage(topFiveMessage);

                return rankingViewModel;

            }

        }

        return null;
    }


    private static RankingViewModel.SiteRankViewModel getRankAreaViewModel(Context context, RankingViewModel.RankAreaType rankAreaType, SiteRankingData.Area region, Integer week, Integer year, String countryCode, Integer score) {

        if (region == null) {
            return null;
        }

        RankingViewModel.SiteRankViewModel siteRankViewModel = new RankingViewModel.SiteRankViewModel();
        siteRankViewModel.setTitle(context.getString(rankAreaType.getTitleResourceId()));
        siteRankViewModel.setActualRank(region.getPosition());
        siteRankViewModel.setTotal(region.getTotal());
        siteRankViewModel.setRankAreaType(RankingViewModel.RankAreaType.from(region.getRankType()));
        int offset = region.getOffset();

        RankingViewModel.Trend trend;
        if (offset > 0) {
            trend = RankingViewModel.Trend.UP;
        } else if (offset < 0) {
            trend = RankingViewModel.Trend.DOWN;
        } else {
            trend = RankingViewModel.Trend.EQUALS;
        }

        siteRankViewModel.setTrend(trend);

        siteRankViewModel.setOffset(offset);
        siteRankViewModel.setName(region.getName());
        siteRankViewModel.setMessage(region.isLowerThanThreshold() ? context.getString(R.string.weekly_printbeat_site_weekly_rank_lower_third) : null);
        siteRankViewModel.setWeek(week);
        siteRankViewModel.setYear(year);
        siteRankViewModel.setScore(score != null ? score : 0);
        siteRankViewModel.setLowerThird(region.isLowerThanThreshold());
        siteRankViewModel.setLowerThirdValue(region.getLowerTresholdValue());
        siteRankViewModel.setCountryFlagUrl(RankingDataManager.getCountryFlagUrl(countryCode));

        return siteRankViewModel;

    }

    /**
     * Converts a list of weekData to a WeekCollectionViewModel.
     */
    private static WeekCollectionViewModel parseWeekData(Context
                                                                 context, List<WeekData> weeks, BusinessUnitViewModel businessUnitViewModel, BusinessUnit
                                                                 metaData) {

        Map<String, BusinessUnit.Kpi> kpiMetaData = metaData.getKpiMetaDataMap();
        BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(metaData.getBusinessUnit());
        businessUnitViewModel.setKpiMaxScoreMap(new HashMap<String, Integer>());

        int index = 0;

        List<WeekViewModel> listWeekViewModel = new ArrayList<>();
        for (WeekData weekData : weeks) {
            WeekViewModel weekViewModel = new WeekViewModel();

            weekViewModel.setPerformance(KPIScoreStateEnum.from(weekData.getKpiScoreState()));
            weekViewModel.setRelativeIndexToLastWeek(index);
            index += 1;

            weekViewModel.setWeekScore(weekData.getWeeklyPoints());
            weekViewModel.setWeekMaxScore(100);
            weekViewModel.setFromDate(HPDateUtils.parseDate(weekData.getUiStartDate(), DEFAULT_DATE_FORMAT_NO_TIME));
            weekViewModel.setToDate(HPDateUtils.parseDate(weekData.getUiEndDate(), DEFAULT_DATE_FORMAT_NO_TIME));
            weekViewModel.setWeekNumber(HPDateUtils.getWeekOfYear(weekData.getUiStartDate(), DEFAULT_DATE_FORMAT_NO_TIME));

            List<KPIViewModel> kpis = new ArrayList<>();

            for (WeekData.Scorable kpi : weekData.getScorables()) {

                KPIViewModel kpiViewModel = new KPIViewModel();

                kpiViewModel.setName(kpi.getName());
                kpiViewModel.setLocalizedName(ReportKpiEnum.from(context, kpi.getName(), BusinessUnitEnum.from(metaData.getBusinessUnit()))
                        .getLocalizedNameString(context, kpi.getName()));
                kpiViewModel.setScore(kpi.getScore());
                kpiViewModel.setValue(kpi.getValue());
                kpiViewModel.setScoreTrend(KPIScoreTrendEnum.from(kpi.getKpiScoreTrend()));
                kpiViewModel.setScoreState(KPIScoreStateEnum.from(kpi.getKpiScoreState()));
                kpiViewModel.setCustomText(kpi.getCustomText());
                kpiViewModel.setKpiEnum(KpiExplanationEnum.from(context, kpi.getName(), BusinessUnitEnum.from(metaData.getBusinessUnit())));

                BusinessUnit.Kpi kpiData;

                if (kpiMetaData.containsKey(kpi.getName())) {

                    kpiData = kpiMetaData.get(kpi.getName());

                    if (kpiData != null) {
                        kpiViewModel.setUnit(KPIValueHandleEnum.from(businessUnitEnum == BusinessUnitEnum.SCITEX_PRESS
                                || businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER ? kpiData.getText() : kpiData.getValueHandle()));
                        kpiViewModel.setMaxScore(kpiData.getScoreWeight());
                        kpiViewModel.setKpiDrawable(ReportKpiEnum.from(context, kpiViewModel.getName(), businessUnitEnum).getWeekPanelKpiIcon());
                        businessUnitViewModel.getKpiMaxScoreMap().put(kpiViewModel.getName(), kpiViewModel.getMaxScore());
                    }
                }

                KpiExplanationViewModel kpiExplanationViewModel = null;
                if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                    kpiExplanationViewModel = KpiExplanationUtils.parseKpiExplanationItemForIndigo(kpi, context, kpiViewModel.getMaxScore());
                } else if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) {
                    kpiExplanationViewModel = KpiExplanationUtils.parseKpiExplanationItemForLatex(kpi, context, kpiViewModel.getMaxScore());
                }
                kpiViewModel.setKpiExplanationViewModels(kpiExplanationViewModel);

                kpis.add(kpiViewModel);
            }

            weekViewModel.setKpis(kpis);

            List<WeekViewModel.WeekPressStatus> weekPressStatuses = new ArrayList<>();
            Map<String, WeekData.PressStatus> pressStatusHashMap = weekData.getPressesStatus();
            Map<String, DeviceData> deviceViewModelMap = businessUnitViewModel.getDevices();
            if (pressStatusHashMap != null && businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS &&
                    PrintOSPreferences.getInstance(context).isWeekIncrementalFeatureEnabled()) {
                for (String serialNumber : pressStatusHashMap.keySet()) {
                    WeekData.PressStatus pressStatus = pressStatusHashMap.get(serialNumber);
                    WeekViewModel.WeekPressStatus weekPressStatus = new WeekViewModel.WeekPressStatus();
                    weekPressStatus.setSerialNumber(serialNumber);
                    if (deviceViewModelMap != null && deviceViewModelMap.containsKey(serialNumber)) {
                        weekPressStatus.setPressName(deviceViewModelMap.get(serialNumber).getDeviceName());
                        weekPressStatus.setPressType(deviceViewModelMap.get(serialNumber).getImpressionType());
                    }
                    weekPressStatus.setDateReady(HPDateUtils.parseDate(pressStatus.getReadyDate(),
                            DATA_FORMAT_ACTUAL_VS_TARGET));
                    weekPressStatus.setWeeklyPressStatusEnum(WeeklyPressStatusEnum.from(
                            pressStatus.getStatus()
                    ));
                    weekPressStatuses.add(weekPressStatus);
                }
            }
            if (weekPressStatuses != null && !weekPressStatuses.isEmpty()) {
                Collections.sort(weekPressStatuses, WeekViewModel.WeekPressStatus.weekPressStatusComparator);
            }
            weekViewModel.setWeekPressStatuses(weekPressStatuses);
            weekViewModel.setLowPrintVolume(weekData.isLowPrintVolume());
            listWeekViewModel.add(weekViewModel);
        }

        Collections.sort(listWeekViewModel, WeekViewModel.DATE_COMPARATOR);

        WeekCollectionViewModel weekCollectionViewModel = new WeekCollectionViewModel();
        weekCollectionViewModel.setWeeks(listWeekViewModel);
        weekCollectionViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

        return weekCollectionViewModel;
    }

    public static Observable<Map<BusinessUnitEnum, BusinessUnitViewModel>> initBusinessUnits(
            final Context context) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();

        ConfigService configService = serviceProvider.getConfigService();
        MetaDataService metaDataService = serviceProvider.getMetaDataService();
        PreferencesService preferencesService = serviceProvider.getPreferencesService();

        return Observable.combineLatest(
                ApiCacheUtils.getCacheAndUpdate(preferencesService.getPreferences(),
                        BU_PREFERENCE_CACHE_PREF_KEY, new TypeReference<PreferencesData>() {
                        }),
                ApiCacheUtils.getCacheAndUpdate(configService.getOrganizationsDevices(),
                        ORGANIZATION_DEVICE_CACHE_PREF_KEY, new TypeReference<DevicesData>() {
                        }),
                ApiCacheUtils.getCacheAndUpdate(configService.getConfigDevices(PrintOSPreferences.getInstance(context).getLanguageCode()),
                        CONFIG_DEVICE_CACHE_PREF_KEY, new TypeReference<List<DeviceData>>() {
                        }),
                ApiCacheUtils.getCacheAndUpdate(metaDataService.getMetaData(),
                        META_DATA_DEVICE_CACHE_PREF_KEY, new TypeReference<Map<String, BusinessUnit>>() {
                        }), PermissionsManager.getInstance().getPermissionsData(false),

                new Func5<Response<PreferencesData>, Response<DevicesData>, Response<List<DeviceData>>, Response<Map<String, BusinessUnit>>, Response<PermissionsData>, Map<BusinessUnitEnum, BusinessUnitViewModel>>() {
                    @Override
                    public Map<BusinessUnitEnum, BusinessUnitViewModel> call(Response<PreferencesData> preferencesDataResponse, Response<DevicesData> devicesDataResponse, Response<List<DeviceData>> listResponse, Response<Map<String, BusinessUnit>> businessUnitsMapResponse, Response<PermissionsData> permissionsDataResponse) {

                        if (preferencesDataResponse != null && preferencesDataResponse.body() != null) {
                            PreferencesData.General general = preferencesDataResponse.body().getGeneral();
                            if (general != null) {
                                PrintOSPreferences.saveUnitSystem(general.getUnitSystem());
                            }
                        }

                        Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitMap = mapDevicesToBusinessUnits(devicesDataResponse.body().getDevices(), listResponse.body());
                        mapKPIToBusinessUnits(context, businessUnitMap, businessUnitsMapResponse.body());
                        defineThresholds(context, businessUnitsMapResponse.body());

                        return businessUnitMap;
                    }
                });
    }

    private static void defineThresholds(Context context, Map<String, BusinessUnit> businessUnitMap) {
        if (businessUnitMap != null) {
            for (String unitName : businessUnitMap.keySet()) {
                BusinessUnit businessUnit = businessUnitMap.get(unitName);
                BusinessUnit.Thresholds thresholds = businessUnit.getThresholds();
                if (thresholds != null) {
                    BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(unitName);
                    PrintOSPreferences.getInstance(context).setThresholds(businessUnitEnum,
                            thresholds.getSucceedPercent(),
                            thresholds.getAveragePercent(),
                            thresholds.getFailPercent());
                }
            }
        }
    }


    private static void mapKPIToBusinessUnits(Context
                                                      context, Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitMap, Map<String, BusinessUnit> businessUnitResponse) {

        if (businessUnitResponse == null) {
            return;
        }

        for (String businessUnitKey : businessUnitResponse.keySet()) {
            BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(businessUnitKey);
            if (businessUnitMap.containsKey(businessUnitEnum)) {
                BusinessUnitViewModel businessUnitViewModel = businessUnitMap.get(businessUnitEnum);

                BusinessUnit businessUnit = businessUnitResponse.get(businessUnitKey);
                if (businessUnit != null) {
                    Map<String, BusinessUnit.Kpi> kpiMap = businessUnit.getKpiMetaDataMap();
                    List<BusinessUnitViewModel.BusinessUnitKpi> businessUnitKpiList = new ArrayList<>();
                    if (kpiMap != null) {
                        for (String kpiKey : kpiMap.keySet()) {
                            ReportTypeEnum reportTypeEnum = ReportTypeEnum.UNKNOWN;
                            ReportChartTypeEnum reportChartTypeEnum = ReportChartTypeEnum.UNKNOWN;
                            BusinessUnit.Kpi kpiData = kpiMap.get(kpiKey);
                            if (kpiData != null && kpiData.getDataField() != null) {
                                reportTypeEnum = ReportTypeEnum.from(kpiData.getDataField());
                                reportChartTypeEnum = ReportChartTypeEnum.from(context, kpiData.getDefaultChart(), kpiData.getName());
                            }
                            BusinessUnitViewModel.BusinessUnitKpi kpiUnit = new BusinessUnitViewModel.BusinessUnitKpi();
                            kpiUnit.setReportKpiEnum(kpiKey);
                            kpiUnit.setReportTypeEnum(reportTypeEnum);
                            kpiUnit.setReportChartTypeEnum(reportChartTypeEnum);
                            businessUnitKpiList.add(kpiUnit);
                        }
                    }
                    businessUnitViewModel.setBusinessUnitKpi(businessUnitKpiList);
                }
            }
        }
    }

    private static Map<BusinessUnitEnum, BusinessUnitViewModel> mapDevicesToBusinessUnits
            (List<DevicesData.DeviceData> organizationsDevices, List<DeviceData> devicesData) {

        Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsDevicesMap = new HashMap<>();

        for (int deviceIndex = 0; deviceIndex < devicesData.size(); deviceIndex++) {

            DeviceData deviceData = devicesData.get(deviceIndex);

            BusinessUnitEnum businessUnit = BusinessUnitEnum.from(deviceData.getBusinessUnit());

            // Work only with the supported business units.
            if (businessUnit == BusinessUnitEnum.UNKNOWN) {
                continue;
            }

            for (DevicesData.DeviceData organizationDevice : organizationsDevices) {

                String deviceSerialNumber = deviceData.getSerialNumber();
                deviceSerialNumber = deviceSerialNumber == null ? "" : deviceSerialNumber;

                String[] splitSerialNumber = deviceSerialNumber.split("!");
                String serialNumber = splitSerialNumber.length > 0 ? splitSerialNumber[0] : "";
                String productNumber = splitSerialNumber.length > 1 ? splitSerialNumber[1] : null;

                boolean sameSerialNumber = serialNumber.equals(organizationDevice.getSerialNumber());
                boolean sameProductNumber = productNumber == null || productNumber.equals(organizationDevice.getModel());

                if (sameSerialNumber && sameProductNumber) {
                    deviceData.setDeviceId(organizationDevice.getDeviceId());
                    //Server bug
                    break;
                }
            }

            BusinessUnitViewModel businessUnitViewModel;

            if (!businessUnitsDevicesMap.containsKey(businessUnit)) {

                businessUnitViewModel = new BusinessUnitViewModel();
                businessUnitViewModel.setBusinessUnit(businessUnit);
                businessUnitViewModel.setDevices(new LinkedHashMap<String, DeviceData>());
                businessUnitsDevicesMap.put(businessUnit, businessUnitViewModel);

            } else {
                businessUnitViewModel = businessUnitsDevicesMap.get(businessUnit);
            }

            businessUnitViewModel.getDevices().put(deviceData.getSerialNumber(), deviceData);

            List<Panel> panels;

            switch (businessUnitViewModel.getBusinessUnit()) {

                case INDIGO_PRESS:
                    panels = Arrays.asList(Panel.INDIGO_PRODUCTION, Panel.TODAY, Panel.HISTOGRAM, Panel.DAILY_SPOTLIGHT, Panel.RANKING, Panel.PERFORMANCE, Panel.PANEL_SERVICE_CALL, Panel.PERSONAL_ADVISOR);
                    break;
                case LATEX_PRINTER:
                    panels = Arrays.asList(Panel.LATEX_PRODUCTION, Panel.TODAY, Panel.HISTOGRAM, Panel.RANKING, Panel.PERFORMANCE);
                    break;
                case IHPS_PRESS:
                    panels = Arrays.asList(Panel.PRINTERS, Panel.RANKING, Panel.PERFORMANCE);
                    break;
                case SCITEX_PRESS:
                    panels = Arrays.asList(Panel.TODAY, Panel.HISTOGRAM, Panel.RANKING, Panel.PERFORMANCE);
                    break;
                default:
                    panels = new ArrayList<>();
                    break;
            }

            businessUnitViewModel.setPanels(panels);
        }

        for (BusinessUnitViewModel businessUnitViewModel : businessUnitsDevicesMap.values()) {

            List<DeviceData> deviceDataList = new LinkedList<>();
            for (String deviceKey : businessUnitViewModel.getDevices().keySet()) {
                deviceDataList.add(businessUnitViewModel.getDevices().get(deviceKey));
            }

            FiltersViewModel filtersViewModel = new FiltersViewModel();
            filtersViewModel.initFiltersViewModel(deviceDataList);
            businessUnitViewModel.setFiltersViewModel(filtersViewModel);
        }

        return businessUnitsDevicesMap;
    }

    public static Observable<ResponseBody> likeAdvice(int adviceId, String device,
                                                      boolean isLiked) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.likeAdvice(String.valueOf(adviceId), device, isLiked).asObservable();
    }

    public static Observable<ResponseBody> suppressAdvice(int adviceId, String device) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.suppressAdvice(String.valueOf(adviceId), device, true).asObservable();
    }

    public static Observable<ResponseBody> unhideAdvices(List<String> serialNumbers) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.unhideAllAdvices(serialNumbers, false).asObservable();
    }

    public static Observable<BeatCoinsViewModel> getBeatCoins(String id) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        BeatCoinService beatCoinService = serviceProvider.getBeatCoinService();
        return beatCoinService.getBeatCoinsDetails(id).map(new Func1<Response<BeatCoinData>, BeatCoinsViewModel>() {
            @Override
            public BeatCoinsViewModel call(Response<BeatCoinData> beatCoinDataResponse) {
                if (beatCoinDataResponse != null && beatCoinDataResponse.isSuccessful() && beatCoinDataResponse.body() != null) {
                    return parseBeatCoinViewModel(beatCoinDataResponse.body());
                }
                return null;
            }
        });
    }

    static Observable<PBNotificationDataViewModel> getPBNotificationData(String id) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PBNotificationService pbNotificationService = serviceProvider.getPBNotificationService();

        return pbNotificationService.getPBNotificationData(id).map(new Func1<Response<PBNotificationData>, PBNotificationDataViewModel>() {
            @Override
            public PBNotificationDataViewModel call(Response<PBNotificationData> pbNotificationDataResponse) {
                if (pbNotificationDataResponse.isSuccessful() && pbNotificationDataResponse.body().getSerialNumber() != null) {
                    PBNotificationDataViewModel viewModel = new PBNotificationDataViewModel();
                    viewModel.setSerialNumber(pbNotificationDataResponse.body().getSerialNumber());
                    return viewModel;
                }
                return null;
            }
        });
    }

    private static BeatCoinsViewModel parseBeatCoinViewModel(BeatCoinData beatCoinData) {
        if (beatCoinData.getCoins() <= 0 || beatCoinData.getCoinsURL() == null ||
                beatCoinData.getCoinsURLExpirationDate() == null) {
            HPLogger.d(TAG, "getBeatCoins returned corrupted response");
            return null;
        }
        BeatCoinsViewModel viewModel = new BeatCoinsViewModel();
        viewModel.setNumberOfBeatCoins(beatCoinData.getCoins());
        viewModel.setUrl(beatCoinData.getCoinsURL());
        if (beatCoinData.getCoinsURLExpirationDate() != null) {
            Date expiryDate = HPDateUtils.parseDate(beatCoinData.getCoinsURLExpirationDate(), SERVICE_CALL_DATE_FORMAT);
            if (expiryDate == null || HPDateUtils.isExpired(expiryDate)) {
                return null;
            }
            viewModel.setNumberOfDaysTillExpiry(HPDateUtils.getNumberOfDaysFromToday(expiryDate));
        }
        return viewModel;
    }

    public static Observable<BeatCoinsViewModel> getInviteBeatCoin() {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        BeatCoinService beatCoinService = serviceProvider.getBeatCoinService();
        return beatCoinService.getInviteBeatCoinsOnLogin().map(new Func1<Response<LoginBeatCoinData>, BeatCoinsViewModel>() {
            @Override
            public BeatCoinsViewModel call(Response<LoginBeatCoinData> loginBeatCoinDataResponse) {
                if (loginBeatCoinDataResponse != null && loginBeatCoinDataResponse.isSuccessful()) {
                    BeatCoinsViewModel viewModel = parseLoginBeatCoinViewModel(loginBeatCoinDataResponse.body());

                    if (viewModel != null) {
                        Analytics.sendEvent(Analytics.USER_LOGIN_AFTER_INVITE_ACTION);
                    }

                    return viewModel;
                }

                return null;
            }
        });
    }

    private static BeatCoinsViewModel parseLoginBeatCoinViewModel(LoginBeatCoinData
                                                                          beatCoinData) {
        if (beatCoinData.getCoins() == null || beatCoinData.getCoins() <= 0 || beatCoinData.getCoinsURL() == null) {
            return null;
        }
        BeatCoinsViewModel viewModel = new BeatCoinsViewModel();
        viewModel.setNumberOfBeatCoins(beatCoinData.getCoins());
        viewModel.setUrl(beatCoinData.getCoinsURL());
        viewModel.setType(BeatCoinsViewModel.BeatCoinType.INVITE);
        return viewModel;
    }

    public static Observable<UserData.User> getUserData() {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProvider();
        UserDataService userDataService = apiServicesProvider.getUserDataService();

        return userDataService.getUser().map(new Func1<Response<UserData.User>, UserData.User>() {
            @Override
            public UserData.User call(Response<UserData.User> userResponse) {
                if (userResponse.isSuccessful()) {
                    return userResponse.body();
                }
                return null;
            }
        });
    }

    public static Observable<TodayHistogramViewModel> getPrintVolumeDataPerResolution(HistogramResolution resolution) {

        PerformanceTrackingService performanceTrackingService = PrintOSApplication.getApiServicesProvider()
                .getPerformanceTrackingService();

        final BusinessUnitViewModel businessUnit = HomePresenter.getSelectedBusinessUnit();
        BusinessUnitEnum businessUnitEnum = businessUnit.getBusinessUnit();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        PreferencesData.UnitSystem unitSystem = printOSPreferences.getUnitSystem();


        return performanceTrackingService.getKpiReportV2(PRINT_VOLUME_KPI_NAME, businessUnitEnum.getName(), false, devices,
                resolution.getApiOffset(), resolution.getApiTag(),
                UNIT_BY, unitSystem.name()).map(new Func1<Response<ReportDataV2>, TodayHistogramViewModel>() {
            @Override
            public TodayHistogramViewModel call(Response<ReportDataV2> reportDataV2Response) {
                if (reportDataV2Response.isSuccessful() && reportDataV2Response.body() != null) {
                    TodayHistogramViewModel todayHistogramViewModel = parsePrintVolumePerResolutionData(
                            HomePresenter.getSelectedBusinessUnit(), reportDataV2Response.body());

                    return todayHistogramViewModel;
                }
                return null;
            }
        });

    }

    private static TodayHistogramViewModel parsePrintVolumePerResolutionData (BusinessUnitViewModel businessUnitViewModel,
                                                                              ReportDataV2 reportData) {
        TodayHistogramViewModel viewModel = new TodayHistogramViewModel();
        viewModel.setBusinessUnit(businessUnitViewModel.getBusinessUnit());
        viewModel.setShiftSupport(HomePresenter.isShiftSupport());

        if(reportData == null || reportData.getData() == null) {
            return null;
        }

        Map<String,Integer> impressionsList = new HashMap<>();

        //grouping values by date
        for (ReportData.UnitEvent unitEvent : reportData.getData().getUnitEvents()) {
            if (unitEvent.getEvents() != null) {
                for (ReportData.UnitEvent.Event event : unitEvent.getEvents()) {
                    String happenedOn = event.getHappenedOn();
                    if (happenedOn != null) {

                        int imp = 0;
                        if(impressionsList.containsKey(happenedOn)) {
                            imp = impressionsList.get(happenedOn);
                            impressionsList.remove(happenedOn);
                        }

                        imp += event.getImpressions();
                        impressionsList.put(happenedOn, imp);
                    }
                }
            }
        }

        List<TodayHistogramViewModel.TodayHistogramItem> histogramData = new ArrayList<>();
        for (String timeString : impressionsList.keySet()) {

            TodayHistogramViewModel.TodayHistogramItem histogramItem = new TodayHistogramViewModel.TodayHistogramItem();

            histogramItem.setDate(HPDateUtils.parseDate(timeString, DEFAULT_DATE_FORMAT_NO_TIME));
            histogramItem.setActual(impressionsList.get(timeString));

            histogramData.add(histogramItem);
        }

        Collections.sort(histogramData, TodayHistogramViewModel.TodayHistogramItem.COMPARATOR);

        for (int index = 1; index < histogramData.size(); index++) {
            TodayHistogramViewModel.TodayHistogramItem current = histogramData.get(index);
            TodayHistogramViewModel.TodayHistogramItem lastWeek = histogramData.get(index - 1);

            current.setLastWeekValue(lastWeek.getActual());
        }

        if (histogramData.size() > 1) {
            histogramData.remove(0);
        }

        viewModel.setData(histogramData);
        viewModel.setMeasureTypeEnum(MeasureTypeEnum.IMPRESSIONS);

        return viewModel;
    }


    public static void clearApiCache() {
        ApiCacheUtils.clearApiCache(CONFIG_DEVICE_CACHE_PREF_KEY,
                ORGANIZATION_DEVICE_CACHE_PREF_KEY,
                BU_PREFERENCE_CACHE_PREF_KEY,
                META_DATA_DEVICE_CACHE_PREF_KEY,
                InsightsPresenter.KZ_BU_CACHE_PREF_KEY);
    }

    public static void clearPrefCache() {
        ApiCacheUtils.clearApiCache(BU_PREFERENCE_CACHE_PREF_KEY);
    }

    public static int[] getShiftsColors() {
        return SHIFTS_COLORS;
    }


}