package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.InviteContactInputData;
import com.hp.printosmobile.data.remote.models.InviteSendUserIDData;
import com.hp.printosmobile.data.remote.models.InviteUserRoleData;
import com.hp.printosmobile.data.remote.models.InvitesListData;

import io.intercom.okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Anwar Asbah 11/5/2017
 */
public interface InvitesService {

    @GET(ApiConstants.INVITES_LIST_API)
    Observable<Response<InvitesListData>> getInvitesList();

    @GET(ApiConstants.INVITES_USER_ROLE_API)
    Observable<Response<InviteUserRoleData>> getInviteUserRoles();

    @POST(ApiConstants.INVITES_API)
    Observable<Response<InvitesListData.Invite>> inviteContact(@Body InviteContactInputData inviteContactInputData);

    @POST(ApiConstants.INVITES_SEND_USER_ID_API)
    Observable<Response<ResponseBody>> sendInviteUserID(@Body InviteSendUserIDData inviteSendUserIDData);

    @DELETE(ApiConstants.INVITES_REVOKE_API)
    Observable<Response<ResponseBody>> revokeInvite(@Path("id") String id);

    @POST(ApiConstants.INIVTES_RESEND_API)
    Observable<Response<InvitesListData.Invite>> resendInvite(@Body InviteContactInputData inviteContactInputData);
}
