package com.hp.printosmobile.aaa;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.data.remote.models.CurrentOrganizationData;
import com.hp.printosmobile.data.remote.models.OrganizationBody;
import com.hp.printosmobile.data.remote.models.UserRolesData;
import com.hp.printosmobile.data.remote.services.OrganizationService;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func3;
import rx.schedulers.Schedulers;

/**
 * Created Osama Taha
 */

public class AAAManager {

    private static final String TAG = AAAManager.class.getSimpleName();

    private AAAManager() {

    }

    public static Observable<UserData.User> validateToken() {

        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.checkCookieAsync().map(new Func1<Response<UserData>, UserData.User>() {
            @Override
            public UserData.User call(Response<UserData> userDataResponse) {
                if (userDataResponse != null && userDataResponse.isSuccessful()) {
                    return userDataResponse.body().getUser();
                }
                return null;
            }
        });

    }

    public static Observable<Response<UserData>> performAutoLogin(Context context) {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.login("", SessionManager.getInstance(context).getUserCredentials()).asObservable();
    }

    public static Observable<ResponseBody> changeContext(Context context) {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.changeOrganizationContextAsync(getOrganizationJsonBody(context)).asObservable();
    }

    public static OrganizationJsonBody getOrganizationJsonBody(Context context) {
        UserData.Context organizationContext = Preferences.getInstance(context).getSelectedOrganization();
        OrganizationJsonBody body = new OrganizationJsonBody();
        body.setId(organizationContext.getId());
        body.setName(organizationContext.getName());
        body.setType(organizationContext.getType());
        return body;
    }

    public static Observable<CurrentOrganizationData> getCurrentOrganizationData() {

        OrganizationService organizationService = PrintOSApplication.getApiServicesProvider().getOrganizationService();

        return organizationService.getOrganizationData()
                .map(new Func1<Response<CurrentOrganizationData>, CurrentOrganizationData>() {
                    @Override
                    public CurrentOrganizationData call(Response<CurrentOrganizationData> currentOrganizationDataResponse) {

                        if (currentOrganizationDataResponse.isSuccessful()) {
                            return currentOrganizationDataResponse.body();
                        }
                        return null;
                    }
                });
    }


    public static Observable<UserRolesData> getUserRules() {

        UserDataService userDataService = PrintOSApplication.getApiServicesProvider().getUserDataService();
        return userDataService.getUserRoles()
                .map(new Func1<Response<UserRolesData>, UserRolesData>() {
                    @Override
                    public UserRolesData call(Response<UserRolesData> userRolesResponse) {
                        if (userRolesResponse.isSuccessful()) {
                            return userRolesResponse.body();
                        }
                        return null;
                    }
                });

    }

    public static void initUser() {

        Observable.combineLatest(getUserOrganizations(), getUserRules(), getCurrentOrganizationData(),
                new Func3<List<OrganizationViewModel>, UserRolesData, CurrentOrganizationData, Void>() {
                    @Override
                    public Void call(List<OrganizationViewModel> organizationViewModels, UserRolesData userRolesData, CurrentOrganizationData currentOrganizationData) {

                        saveUserData(userRolesData, currentOrganizationData);

                        return null;

                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "initUser onError " + e);
            }

            @Override
            public void onNext(Void aVoid) {

            }
        });

    }

    private static void saveUserData(UserRolesData userRolesData, CurrentOrganizationData currentOrganizationData) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (currentOrganizationData != null) {

            preferences.saveSelfProvisionOrganization(String.valueOf(currentOrganizationData.getAnonymous()));
            if (currentOrganizationData.getInternalIDs() != null) {

                boolean channelSupportKz = false;
                for (CurrentOrganizationData.InternalID internalID : currentOrganizationData.getInternalIDs()) {

                    BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.fromGbu(internalID.getGbu());

                    if(businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                        preferences.setHasIndigoGBU(true);
                    }

                    if (internalID.getGbu() != null && businessUnitEnum.isChannelSupportKz()) {
                        channelSupportKz = true;
                        break;
                    }
                }

                preferences.setChannelSupportKz(preferences.isChannelOrg() && channelSupportKz);

            }

        }

        if (userRolesData != null && userRolesData.getUserRoles() != null) {

            String roles = "";
            List<UserRolesData.UserRole> rolesList = userRolesData.getUserRoles();

            for (int i = 0; i < rolesList.size(); i++) {

                UserRolesData.UserRole role = rolesList.get(i);

                if (role != null && role.getSystemRole() != null && role.getSystemRole().getName() != null) {
                    roles += role.getSystemRole().getName() + (i == userRolesData.getUserRoles().size() - 1 ? "" : "|");
                }
            }

            preferences.saveUserRoles(roles);

        }

        //Init Sdks
        PrintOSApplication.initSDKs();

    }

    public static Observable<OrganizationViewModel> changeContext(final OrganizationJsonBody organizationJsonBody) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();

        OrganizationService organizationService = serviceProvider.getOrganizationService();

        return organizationService.changeOrganizationContext(organizationJsonBody).map(new Func1<Response<UserData>, OrganizationViewModel>() {

            @Override
            public OrganizationViewModel call(Response<UserData> userDataResponse) {
                if (userDataResponse != null && userDataResponse.isSuccessful() &&
                        userDataResponse.body() != null && userDataResponse.body().getContext() != null) {
                    OrganizationViewModel viewModel = new OrganizationViewModel();
                    UserData.Context userContext = userDataResponse.body().getContext();
                    viewModel.setOrganizationId(userContext.getId());
                    viewModel.setOrganizationName(userContext.getName());
                    viewModel.setOrganizationTypeDisplayName(userContext.getType());
                    viewModel.setAccountType(AccountType.from(userContext.getType()));
                    return viewModel;
                }
                return null;
            }
        });
    }


    public static Observable<List<OrganizationViewModel>> getUserOrganizations() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        OrganizationService organizationService = serviceProvider.getOrganizationService();

        return organizationService.getOrganizations().map(new Func1<Response<OrganizationBody>, List<OrganizationViewModel>>() {
            @Override
            public List<OrganizationViewModel> call(Response<OrganizationBody> organizationResponse) {
                if (organizationResponse.isSuccessful()) {
                    return parseOrganizations(organizationResponse.body());
                }
                return null;
            }
        });

    }

    private static List<OrganizationViewModel> parseOrganizations(OrganizationBody organizationBody) {

        if (organizationBody == null || organizationBody.getContexts() == null) {
            return null;
        }

        List<OrganizationViewModel> models = new ArrayList<>();

        boolean isHPUser = false;
        boolean isChannelUser = false;
        OrganizationViewModel baseOrganization = null;

        for (OrganizationBody.Organization organization : organizationBody.getContexts()) {

            OrganizationViewModel viewModel = new OrganizationViewModel();
            viewModel.setOrganizationId(organization.getId());
            viewModel.setOrganizationName(organization.getName());
            viewModel.setOrganizationTypeDisplayName(organization.getType());

            String orgType = organization.getType();
            if (!TextUtils.isEmpty(orgType)) {
                if (orgType.toUpperCase().contains(AccountType.HP.name())) {
                    isHPUser = true;
                    baseOrganization = viewModel;
                } else if (orgType.toUpperCase().contains(AccountType.CHANNEL.name())) {
                    isChannelUser = true;
                    baseOrganization = viewModel;
                }
            }

            models.add(viewModel);
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (isHPUser) {
            printOSPreferences.saveUserType(AccountType.HP.name());
        } else if (isChannelUser) {
            printOSPreferences.saveUserType(AccountType.CHANNEL.name());
        } else {
            printOSPreferences.saveUserType(AccountType.PSP.name());
        }

        if (baseOrganization != null) {
            printOSPreferences.setBaseOrganization(baseOrganization.getOrganizationId());
        }

        return models;
    }

}
