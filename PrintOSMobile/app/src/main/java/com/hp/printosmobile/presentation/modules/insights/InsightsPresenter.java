package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.cache.ApiCacheUtils;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func3;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/20/2017.
 */
public class InsightsPresenter extends Presenter<InsightsView> {


    public static final String KZ_BU_CACHE_PREF_KEY = "KZ_BU_CACHE_KEY";

    public void getKzBusinessUnits(final Context context) {

        if (mView == null) {
            return;
        }

        mView.showLoadingView();

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();

        InsightsService insightsService = serviceProvider.getInsightsService();

        Subscription subscription = ApiCacheUtils.getCacheAndUpdate(insightsService.getKzBu(), KZ_BU_CACHE_PREF_KEY, new TypeReference<List<String>>() {
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<List<String>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mView == null) {
                            return;
                        }

                        mView.hideLoadingView();
                        mView.showErrorView();

                    }

                    @Override
                    public void onNext(Response<List<String>> buResponse) {

                        if (mView == null) {
                            return;
                        }

                        mView.hideLoadingView();

                        if (buResponse.isSuccessful()) {

                            if (buResponse.body() == null || buResponse.body().isEmpty()) {

                                mView.onEmptyBusinessUnits();

                            } else {

                                mView.hideErrorView();

                                List<BusinessUnitEnum> businessUnitEnums = new ArrayList<>();

                                for (String businessUnit : buResponse.body()) {

                                    BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.fromKzBu(businessUnit);

                                    if (PrintOSPreferences.getInstance(context).isInsightsEnabledForBu(businessUnitEnum)){
                                        businessUnitEnums.add(businessUnitEnum);
                                    }

                                }

                                Collections.sort(businessUnitEnums, new Comparator<BusinessUnitEnum>() {
                                    @Override
                                    public int compare(BusinessUnitEnum o1, BusinessUnitEnum o2) {
                                        return o1.getSortOrder() - o2.getSortOrder();
                                    }
                                });

                                mView.onBusinessUnitDataReceived(businessUnitEnums);
                            }

                        } else {

                            mView.onEmptyBusinessUnits();
                        }
                    }
                });

        addSubscriber(subscription);
    }

    public Observable<List<Object>> getInsightsData(BusinessUnitViewModel bu, String fromDate, String toDate, final boolean isDataSelected) {

        Observable observable;

        if (bu.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS && !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHPOrg() && !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isChannelOrg()) {
            observable = Observable.combineLatest(InsightsDataManager.getTop5Data(bu, InsightKpiEnum.JAM, fromDate, toDate),
                    InsightsDataManager.getTop5Data(bu, InsightKpiEnum.FAILURE, fromDate, toDate),
                    InsightsDataManager.getKZRecommendedContent(bu), new Func3<InsightsViewModel, InsightsViewModel, List<KZItem>, List<Object>>() {
                        @Override
                        public List<Object> call(InsightsViewModel jamsViewModel, InsightsViewModel failuresViewModel, List<KZItem> kzItems) {

                            return buildInsightsData(kzItems, jamsViewModel, failuresViewModel, new PersonalAdvisorViewModel());

                        }
                    });
        } else {
            observable = InsightsDataManager.getKZRecommendedContent(bu);
        }

        return observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

    }

    private List<Object> buildInsightsData(List<KZItem> kzItems, InsightsViewModel jamsViewModel, InsightsViewModel failuresViewModel, PersonalAdvisorViewModel personalAdvisorViewModel) {

        List<Object> insightsData = new ArrayList<>();
        boolean jamsAdded = false;
        boolean failuresAdded = false;
        boolean personalAdvisorAdded = false;

        boolean hasTop5FailureData = failuresViewModel != null && failuresViewModel.getInsightModels() != null && !failuresViewModel.getInsightModels().isEmpty();
        boolean hasTop5JamsData = jamsViewModel != null && jamsViewModel.getInsightModels() != null && !jamsViewModel.getInsightModels().isEmpty();

        if (kzItems != null) {
            for (int i = 0; i < kzItems.size(); i++) {

                insightsData.add(kzItems.get(i));

                if (i == 0) {

                    if (hasTop5FailureData) {
                        insightsData.add(failuresViewModel);
                        failuresAdded = true;
                    }

                } else if (i == 1) {

                    if (hasTop5JamsData) {
                        insightsData.add(jamsViewModel);
                        jamsAdded = true;
                    } else if (personalAdvisorViewModel != null) {
                        insightsData.add(personalAdvisorViewModel);
                        personalAdvisorAdded = true;
                    }
                } else if (i == 2) {
                    if (personalAdvisorViewModel != null && !personalAdvisorAdded) {
                        insightsData.add(personalAdvisorViewModel);
                        personalAdvisorAdded = true;
                    }
                }
            }
        }

        if (!failuresAdded && hasTop5FailureData) {
            insightsData.add(failuresViewModel);
        }

        if (!jamsAdded && hasTop5JamsData) {
            insightsData.add(jamsViewModel);
        }

        if (!personalAdvisorAdded) {
            insightsData.add(personalAdvisorViewModel);
        }

        return insightsData;
    }

    public void getTop5Data(BusinessUnitViewModel bu, final InsightKpiEnum kpi, String fromDate, String toDate, final boolean isDataSelected) {

        Subscription subscription = InsightsDataManager.getTop5Data(bu, kpi, fromDate, toDate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<InsightsViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(kpi.getTag(), "unable to fetch insights data " + e);
                        onRequestError(e, kpi.getTag());
                    }

                    @Override
                    public void onNext(InsightsViewModel insightsViewModel) {
                        mView.onInsightTop5DataRetrieved(insightsViewModel, kpi, isDataSelected);
                    }
                });

        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e, String tag) {

        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tag);
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}