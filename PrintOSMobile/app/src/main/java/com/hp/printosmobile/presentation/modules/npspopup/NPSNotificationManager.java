package com.hp.printosmobile.presentation.modules.npspopup;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.notification.NotificationUtils;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Calendar;

/**
 * Created by Anwar Asbah on 7/16/2017.
 */

public class NPSNotificationManager {

    private static final String TAG = NPSNotificationManager.class.getName();

    public static void displayNotification(Context context) {

        AccountType accountType = PrintOSPreferences.getInstance(context).getAccountType();
        boolean isRegularUser = accountType == AccountType.PSP;

        if (PrintOSPreferences.getInstance(context).getHasIndigoDivision() &&
                PrintOSPreferences.getInstance(context).isNPSFeatureEnabled() && isRegularUser) {
            String title = context.getString(R.string.printos_app_name);
            String message = context.getString(R.string.nps_notification_msg);

            fireNotification(context, title, message);
        }
    }

    private static void fireNotification(Context context, String title, String message) {

        HPLogger.v(TAG, "fireNotification");
        int requestCode = NotificationUtils.getUniqueRequestCode();

        Intent intent = getNotificationIntent(context);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, requestCode, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(
                    Constants.Notification.PRINTOS_NOTIFICATION_CHANNEL_ID, Constants.Notification.PRINTOS_NOTIFICATION_CHANNEL_NAME, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, Constants.Notification.PRINTOS_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(ResourcesCompat.getColor(context.getResources(), R.color.c62, null))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message).setBigContentTitle(title))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);

        int id = (int) System.currentTimeMillis();
        notificationManager.notify(id, notificationBuilder.build());

        HPLogger.d(TAG, "show nps notification");

        String notificationKey = SubscriptionEvent.NotificationEventEnum.NPS_NOTIFICATIONS.getKey();

        Analytics.sendNotificationArriveFirebaseEvent(notificationKey);
        Analytics.sendEvent(Analytics.SHOW_NOTIFICATION_ACTION, notificationKey);
    }

    private static Intent getNotificationIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.NPS_EXTRA, true);
        intent.putExtras(bundle);

        return intent;
    }

    public static void enableNotifications(boolean enabled) {
        HPLogger.v(TAG, "enableNotifications(" + enabled + ")");
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setNPSNotificationsEnabled(enabled);
        if (enabled) {
            setRecurringNSPAlarm();
        } else {
            removeRecurringNPSAlarm();
        }
    }

    private static void setRecurringNSPAlarm() {
        HPLogger.v(TAG, "setRecurringNSPAlarm");

        Calendar updateTime = Calendar.getInstance();
        updateTime.set(Calendar.HOUR_OF_DAY, 12);
        updateTime.set(Calendar.MINUTE, 0);

        Context context = PrintOSApplication.getAppContext();
        PendingIntent recurringNPSNotification = getNPSAlarmIntent();
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        long npsMinDisplayTime = DialogManager.getNPSMinDisplayTimeInMillSeconds(context);
        alarms.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + npsMinDisplayTime, npsMinDisplayTime, recurringNPSNotification);
    }

    private static void removeRecurringNPSAlarm() {
        AlarmManager alarms = (AlarmManager) PrintOSApplication.getAppContext().getSystemService(
                Context.ALARM_SERVICE);
        alarms.cancel(getNPSAlarmIntent());
    }

    private static PendingIntent getNPSAlarmIntent() {
        Intent nspIntent = new Intent(PrintOSApplication.getAppContext(), NPSBroadcastReceiver.class);
        nspIntent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        return PendingIntent.getBroadcast(PrintOSApplication.getAppContext(),
                0, nspIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private NPSNotificationManager() {
    }

}
