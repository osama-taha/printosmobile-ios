package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.URLUtil;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.kz.KZAssetResult;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anwar Asbah on 6/20/2017.
 */

public class InsightsUtils {

    private static final String YOUTUBE_URL_REGEX = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    private static final String KNOWLEDGE_ZONE_URL_PREFIX = "URL=";

    private InsightsUtils() {
    }

    public static String getPanelTitle(Context context, InsightsViewModel insightsViewModel) {

        if (insightsViewModel == null || insightsViewModel.getInsightModels() == null) {
            if (context == null) {
                return "";
            }
            return context.getString(R.string.fragment_insights_name);
        }

        return insightsViewModel.getInsightKpiEnum() == InsightKpiEnum.JAM ? context.getString(R.string.insights_top_jams_panel_title)
                : context.getString(R.string.insights_top_failures_panel_title);
    }

    public static String getTop5PanelText(Context context, InsightsViewModel insightsViewModel) {

        if (insightsViewModel == null || insightsViewModel.getInsightModels() == null) {
            if (context == null) {
                return "";
            }
            return context.getString(R.string.fragment_insights_name);
        }

        List<InsightsViewModel.InsightModel> models = new ArrayList<>();
        models.addAll(insightsViewModel.getInsightModels());
        Collections.sort(models, InsightsViewModel.COMPARATOR);

        return insightsViewModel.getInsightKpiEnum() == InsightKpiEnum.JAM ? getJamTitle(context, models)
                : getFailureTitle(context, models);
    }

    private static String getJamTitle(Context context, List<InsightsViewModel.InsightModel> models) {
        int sumOfTop1 = 0;
        int sumOfTop5 = 0;
        int sumOfTop3 = 0;
        int sumOfTop2 = 0;

        if (models != null) {
            for (int i = 0; i < models.size(); i++) {
                if (models.get(i) != null) {
                    sumOfTop1 += i < 1 ? models.get(i).getPercentage() : 0;
                    sumOfTop2 += i < 2 ? models.get(i).getPercentage() : 0;
                    sumOfTop3 += i < 3 ? models.get(i).getPercentage() : 0;
                    sumOfTop5 += i < 5 ? models.get(i).getPercentage() : 0;
                }
            }
        }

        if (sumOfTop1 >= 50) {
            return context.getString(R.string.insights_jam_title_1);
        } else if (sumOfTop1 >= 40) {
            return context.getString(R.string.insights_jam_title_2, String.valueOf(sumOfTop1));
        } else if (sumOfTop5 > 80) {
            return context.getString(R.string.insights_jam_title_3);
        } else if (sumOfTop5 < 50) {
            return context.getString(R.string.insights_jam_title_4);
        } else if (sumOfTop2 >= 50) {
            return context.getString(R.string.insights_jam_title_5, String.valueOf(sumOfTop2));
        } else if (sumOfTop5 >= 70) {
            return context.getString(R.string.insights_jam_title_6);
        } else if (sumOfTop5 < 55) {
            return context.getString(R.string.insights_jam_title_7);
        } else if (sumOfTop3 > 50) {
            return context.getString(R.string.insights_jam_title_8);
        } else {
            return context.getString(R.string.insights_jam_title_9);
        }
    }

    private static String getFailureTitle(Context context, List<InsightsViewModel.InsightModel> models) {
        int sumOfTop1 = 0;
        int sumOfTop5 = 0;
        int sumOfTop3 = 0;
        int sumOfTop2 = 0;

        if (models != null) {
            for (int i = 0; i < models.size(); i++) {
                if (models.get(i) != null) {
                    sumOfTop1 += i < 1 ? models.get(i).getPercentage() : 0;
                    sumOfTop2 += i < 2 ? models.get(i).getPercentage() : 0;
                    sumOfTop3 += i < 3 ? models.get(i).getPercentage() : 0;
                    sumOfTop5 += i < 5 ? models.get(i).getPercentage() : 0;
                }
            }
        }

        if (sumOfTop1 >= 50) {
            return context.getString(R.string.insights_failure_title_1);
        } else if (sumOfTop1 >= 35) {
            return context.getString(R.string.insights_failure_title_2, String.valueOf(sumOfTop1));
        } else if (sumOfTop5 > 70) {
            return context.getString(R.string.insights_failure_title_3);
        } else if (sumOfTop5 < 40) {
            return context.getString(R.string.insights_failure_title_4);
        } else if (sumOfTop2 >= 40) {
            return context.getString(R.string.insights_failure_title_5, String.valueOf(sumOfTop2));
        } else if (sumOfTop5 >= 60) {
            return context.getString(R.string.insights_failure_title_6);
        } else if (sumOfTop5 < 50) {
            return context.getString(R.string.insights_failure_title_7);
        } else if (sumOfTop3 > 40) {
            return context.getString(R.string.insights_failure_title_8);
        } else {
            return context.getString(R.string.insights_failure_title_9);
        }
    }

    public static String getKzPDFUrl(KZAssetResult kzAssetResult) {

        String assetName = !TextUtils.isEmpty(kzAssetResult.getName()) ? kzAssetResult.getName() : String.valueOf(System.currentTimeMillis());
        assetName = assetName.indexOf(Constants.PDF_FILE_EXTENSION) > -1 ? assetName : assetName + Constants.PDF_FILE_EXTENSION;

        String fileUrl = kzAssetResult.getExternalUrl() != null ? kzAssetResult.getExternalUrl() : "";
        String encodedAssetName = android.net.Uri.encode(assetName);
        String encodedAssetUrl = android.net.Uri.encode(fileUrl);

        String pdfViewerUrl = String.format(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getServerUrl() +
                ApiConstants.KNOWLEDGE_ZONE_PDF_VIEWER, encodedAssetName, encodedAssetUrl);

        return pdfViewerUrl;

    }


    public static String extractVideoIdFromUrl(String url) {

        if (TextUtils.isEmpty(url)) {
            return null;
        }

        String videoId = null;
        Pattern pattern = Pattern.compile(YOUTUBE_URL_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            videoId = matcher.group(1);
        }

        return videoId;

    }

    public static String getKzUrlFromText(String urlContentText) {

        if (URLUtil.isValidUrl(urlContentText.replace("\r\n", ""))) {
            return urlContentText;
        }

        String externalUrl = null;
        String regexString = KNOWLEDGE_ZONE_URL_PREFIX + ".*?\r\n";
        Pattern pattern = Pattern.compile(regexString, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(urlContentText);
        if (matcher.find()) {
            externalUrl = matcher.group(0);
            externalUrl = externalUrl.substring(KNOWLEDGE_ZONE_URL_PREFIX.length());
        }

        if (externalUrl != null) {
            if (externalUrl.charAt(externalUrl.length() - 1) == '*') {
                externalUrl = externalUrl.substring(0, externalUrl.length() - 1);
            }
            return externalUrl.replace("\r\n", "");
        }

        return null;
    }

}
