package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.InsightData;
import com.hp.printosmobile.data.remote.models.kz.KZAssetResult;
import com.hp.printosmobile.data.remote.models.kz.KZFavorites;
import com.hp.printosmobile.data.remote.models.kz.KZSearchBody;
import com.hp.printosmobile.data.remote.models.kz.KZSearchResult;
import com.hp.printosmobile.data.remote.models.kz.KzAutoCompleteData;
import com.hp.printosmobile.data.remote.models.kz.RecommendedContentData;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Anwar Asbah 6/20/2017
 */
public interface InsightsService {

    @GET(ApiConstants.INSIGHTS_API)
    Observable<Response<InsightData>> getTop5Data(@Query("device") List<String> devices,
                                                  @Query("from") String fromDate,
                                                  @Query("to") String toDate,
                                                  @Query("kpi") String kpi,
                                                  @Query("top") int top,
                                                  @Query("lang") String language);

    @POST(ApiConstants.KNOWLEDGE_ZONE_SEARCH)
    Observable<Response<KZSearchResult>> searchKnowledgeZone(@Body KZSearchBody kzSearchBody, @Query("bu") String bu,
                                                             @Query("mobile") boolean mobile,
                                                             @Query("lang") String language);

    @GET(ApiConstants.KNOWLEDGE_ZONE_RECOMMENDED_CONTENT)
    Observable<Response<RecommendedContentData>> getKZRecommendedContent(@Query("bu") String bu, @Query("mobile") boolean mobile, @Query("size") int size,
                                                                         @Query("lang") String language, @Query("formats") List<String> formats);

    @GET(ApiConstants.KNOWLEDGE_ZONE_SUGGESTIONS)
    Observable<Response<List<KzAutoCompleteData>>> searchSuggestions(@Query("str") String query, @Query("formats") List<String> formats, @Query("bu") String bu,
                                                                     @Query("mobile") boolean mobile,
                                                                     @Query("lang") String language);

    @GET(ApiConstants.KNOWLEDGE_ZONE_ASSET)
    Observable<Response<KZAssetResult>> getAsset(@Query("id") String assetId, @Query("bu") String bu, @Query("mobile") boolean mobile, @Query("lang") String language);

    @Streaming
    @GET
    Observable<Response<ResponseBody>> getAssetFile(@Url String fileUrl);

    @GET(ApiConstants.KNOWLEDGE_ZONE_BU)
    Observable<Response<List<String>>> getKzBu();

    @POST(ApiConstants.KNOWLEDGE_ZONE_SET_FAVORITE)
    @Headers("Content-Type: application/json")
    Observable<Response<ResponseBody>> setFavorite(@Query("bu") String bu, @Query("id") String id);

    @DELETE(ApiConstants.KNOWLEDGE_ZONE_SET_FAVORITE)
    @Headers("Content-Type: application/json")
    Observable<Response<ResponseBody>> removeFavorite(@Query("bu") String bu, @Query("id") String id);

    @PUT(ApiConstants.KNOWLEDGE_ZONE_RATE_ITEM)
    @Headers("Content-Type: application/json")
    Observable<Response<ResponseBody>> rateItem(@Query("bu") String bu, @Query("id") String id, @Query("rating") int rating);

    @GET(ApiConstants.KNOWLEDGE_ZONE_FAVORITES)
    Observable<Response<KZFavorites>> getFavorites(@Query("bu") String bu, @Query("page") int page, @Query("size") int size, @Query("mobile") boolean mobile, @Query("lang") String language, @Query("formats") List<String> supportedFormats);

    @GET
    Observable<Response<ResponseBody>> getURLContent(@Url String url);
}
