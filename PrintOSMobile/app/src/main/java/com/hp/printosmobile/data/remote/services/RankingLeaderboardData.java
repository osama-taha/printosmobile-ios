package com.hp.printosmobile.data.remote.services;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RankingLeaderboardData {

    @JsonProperty("ranks")
    private List<RankData> ranks;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("ranks")
    public List<RankData> getRanks() {
        return ranks;
    }

    @JsonProperty("ranks")
    public void setRanks(List<RankData> ranks) {
        this.ranks = ranks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class RankData {

        @JsonProperty("siteName")
        private String siteName;
        @JsonProperty("orgName")
        private String orgName;
        @JsonProperty("position")
        private int position;
        @JsonProperty("offset")
        private int offset;
        @JsonProperty("score")
        private int score;
        @JsonProperty("countryCode")
        private String countryCode;
        @JsonProperty("countryName")
        private String countryName;
        @JsonProperty("isLeaderboardEnable")
        private boolean isLeaderboardEnable;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("siteName")
        public String getSiteName() {
            return siteName;
        }

        @JsonProperty("siteName")
        public void setSiteName(String siteName) {
            this.siteName = siteName;
        }

        @JsonProperty("orgName")
        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        @JsonProperty("orgName")
        public String getOrgName() {
            return orgName;
        }

        @JsonProperty("position")
        public int getPosition() {
            return position;
        }

        @JsonProperty("position")
        public void setPosition(int position) {
            this.position = position;
        }

        @JsonProperty("countryCode")
        public String getCountryCode() {
            return countryCode;
        }

        @JsonProperty("countryCode")
        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        @JsonProperty("countryName")
        public String getCountryName() {
            return countryName;
        }

        @JsonProperty("countryName")
        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        @JsonProperty("isLeaderboardEnable")
        public void setLeaderboardEnable(boolean leaderboardEnable) {
            this.isLeaderboardEnable = leaderboardEnable;
        }

        @JsonProperty("isLeaderboardEnable")
        public boolean isLeaderboardEnable() {
            return isLeaderboardEnable;
        }

        @JsonProperty("score")
        public void setScore(int score) {
            this.score = score;
        }

        @JsonProperty("score")
        public int getScore() {
            return score;
        }

        @JsonProperty("offset")
        public void setOffset(int offset) {
            this.offset = offset;
        }

        @JsonProperty("offset")
        public int getOffset() {
            return offset;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}
