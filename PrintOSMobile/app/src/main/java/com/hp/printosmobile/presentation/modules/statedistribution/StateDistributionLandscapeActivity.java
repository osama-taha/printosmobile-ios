package com.hp.printosmobile.presentation.modules.statedistribution;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.OrientationSupportBaseActivity;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * created by Anwar Asbah 2/18/2018
 */
public class StateDistributionLandscapeActivity extends OrientationSupportBaseActivity implements SharableObject {

    public static final String TAG = StateDistributionLandscapeActivity.class.getSimpleName();
    private static final String WEB_VIEW_INTERFACE_NAME = "app";
    private static final String VIEW_MODEL_ARG = "business_unit_arg";
    private static final String DEVICE_NUMBER_ARG = "device_number_arg";
    private static final String IS_SHIFT_SUPPORT_ARG = "is_shift_support_arg";

    private static final String FABRIC_SHARE_STATE_DISTRIBUTION_LANDSCAPE = "Landscape state distribution";
    private static final String HEADER_DATE_FORMAT = "h:mm aa";
    private static final String SCREEN_SHOT_FILE_NAME = "PrintOS";

    private static final String TITLE_FORMAT = "%1$s - %2$s";

    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    @Bind(R.id.share_button)
    View shareButton;
    @Bind(R.id.sharable_content)
    View sharableContent;
    @Bind(R.id.press_name_text_view)
    TextView pressNameTextView;
    @Bind(R.id.time_description_text_view)
    TextView timeDescriptionTextView;
    @Bind(R.id.state_distribution_web_view)
    WebView stateDistributionWebView;

    private DeviceViewModel deviceViewModel;
    private StateDistributionViewModel viewModel;
    private boolean isShiftSupport;

    private StateDistributionPresenter presenter;

    public static void startActivity(Activity activity, DeviceViewModel deviceViewModel, StateDistributionViewModel viewModel,
                                     boolean isShiftSupport) {

        Intent intent = new Intent(activity, StateDistributionLandscapeActivity.class);

        Bundle bundle = new Bundle();
        if (deviceViewModel != null) {
            bundle.putSerializable(DEVICE_NUMBER_ARG, deviceViewModel);
        }
        if (viewModel != null) {
            bundle.putSerializable(VIEW_MODEL_ARG, viewModel);
        }
        bundle.putBoolean(IS_SHIFT_SUPPORT_ARG, isShiftSupport);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            if (args.containsKey(VIEW_MODEL_ARG)) {
                viewModel = (StateDistributionViewModel) args.getSerializable(VIEW_MODEL_ARG);
            }
            if (args.containsKey(IS_SHIFT_SUPPORT_ARG)) {
                isShiftSupport = args.getBoolean(IS_SHIFT_SUPPORT_ARG);
            }
            if (args.containsKey(DEVICE_NUMBER_ARG)) {
                deviceViewModel = (DeviceViewModel) args.getSerializable(DEVICE_NUMBER_ARG);
            }
        }

        displayModel(viewModel);
        Analytics.sendEvent(Analytics.LANDSCAPE_STATE_DISTRIBUTION_SHOWN_EVENT);
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.LANDSCAPE_STATE_DIS_SCREEN_LABEL);
    }

    @Override
    public boolean onPortraitOrientation() {
        super.onPortraitOrientation();
        onBackPressed();
        return true;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_state_distribution_landscape;
    }

    public void displayModel(final StateDistributionViewModel viewModel) {
        if (viewModel == null || viewModel.getAggregateStateMap() == null ||
                viewModel.getPressStates() == null || viewModel.getPressStates().isEmpty()) {
            onError(APIException.create(APIException.Kind.UNEXPECTED), TAG);
            return;
        }

        stateDistributionWebView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

        stateDistributionWebView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                stateDistributionWebView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                StateDistributionUtils.displayLandscapeView(StateDistributionLandscapeActivity.this, stateDistributionWebView, viewModel);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TargetViewManager.showTargetView(StateDistributionLandscapeActivity.this, TAG, sharableContent, null);
                    }
                }, 500);
            }
        });

        pressNameTextView.setText(String.format(TITLE_FORMAT, deviceViewModel.getName(), getString(isShiftSupport ?
                R.string.state_distribution_title_shift : R.string.state_distribution_title)));

        String headerTime = HPDateUtils.formatDate(viewModel.getPressStates().get(0).getStartDate(), HEADER_DATE_FORMAT);
        timeDescriptionTextView.setText(getString(R.string.state_distribution_header_time_stamp, headerTime));
        timeDescriptionTextView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.share_button)
    public void onShareButtonClicked() {
        if (permissionsRequested()) {
            performSharing();
        }
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0) {
                boolean allGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
                if (allGranted) {
                    performSharing();
                }
            }
        }

    }

    public void performSharing() {
        if (deviceViewModel == null) {
            return;
        }
        lockShareButton(true);

        shareButton.setVisibility(View.GONE);
        Bitmap panelBitmap = FileUtils.getScreenShot(sharableContent);
        shareButton.setVisibility(View.VISIBLE);

        if (panelBitmap != null) {
            final Uri storageUri = FileUtils.store(this, panelBitmap, SCREEN_SHOT_FILE_NAME);

            DeepLinkUtils.getShareBody(this,
                    Constants.UNIVERSAL_LINK_SCREEN_STATE_DISTRIBUTION,
                    null,
                    HomePresenter.isShiftSupport(),
                    deviceViewModel != null ? deviceViewModel.getSerialNumber() : "", false, new DeepLinkUtils.BranchIOCallback() {
                        @Override
                        public void onLinkCreated(String link) {
                            onScreenshotCreated(storageUri,
                                    getString(R.string.state_distribution_share_subject, deviceViewModel.getName()),
                                    link);
                            lockShareButton(false);
                        }
                    });
        }
    }

    public void lockShareButton(boolean lock) {
        //Concrete classes may hide the implementation of it with their custom implementation.
        if (shareButton != null) {
            shareButton.setEnabled(!lock);
            shareButton.setClickable(!lock);
        }
    }

    public void onScreenshotCreated(Uri screenshotUri, String title, String body) {
        ShareUtils.onScreenshotCreated(this, screenshotUri, title, body, this);
    }

    @Override
    public String getShareAction() {
        return FABRIC_SHARE_STATE_DISTRIBUTION_LANDSCAPE;
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void legendChanged(final String name) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(name)) {
                        Analytics.sendEvent(Analytics.STATE_DISTRIBUTION_LANDSCAPE_LEGEND_CLICKED, name);
                    }
                }
            });

        }

        @JavascriptInterface
        public void pieZoomAndScroll(final String name) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(name)) {
                        Analytics.sendEvent(Analytics.STATE_DISTRIBUTION_LANDSCAPE_ZOOM_SCROLL_CLICKED, name);
                    }
                }
            });

        }
    }
}
