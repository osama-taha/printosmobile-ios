package com.hp.printosmobile.presentation.modules.rankingpanel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.today.WebViewCustomeTouchListener;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 5/30/2018.
 */
public class RankingBreakdownPanel extends PanelView<RankBreakdownViewModel> implements SharableObject {

    public static final String TAG = RankingBreakdownPanel.class.getSimpleName();

    private static final String SHARE_ACTION = "Ranking history - %s - %s";
    private static final String WEB_VIEW_INTERFACE_NAME = "rankBreakdown";

    @Bind(R.id.panel_content)
    View panelContent;
    @Bind(R.id.empty_data_set_text_view)
    TextView emptyDataSetTextView;
    @Bind(R.id.web_view_container)
    View webViewContainer;
    @Bind(R.id.web_view)
    WebView webView;
    @Bind(R.id.tooltip_description)
    TextView tooltipDescription;
    @Bind(R.id.tooltip_week_number)
    TextView weekNumberTextView;
    @Bind(R.id.tooltip_layout)
    LinearLayout tooltipLayout;
    @Bind(R.id.tv_legend)
    TextView tvLegend;
    View panelContainer;

    private RankBreakdownPanelCallback callbacks;
    private long lastAnalyticsEventSentTime;

    public RankingBreakdownPanel(Context context, View panelContainer) {
        super(context);
        this.panelContainer = panelContainer;
    }

    public RankingBreakdownPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RankingBreakdownPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());
        initView();
    }

    private void initView() {

        webView.setOnTouchListener(new WebViewCustomeTouchListener(getContext()));

        shareButton = findViewById(R.id.share_icon);
        shareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onShareClicked();
            }
        });
    }

    @Override
    public Spannable getTitleSpannable() {
        return RankBreakdownUtils.getTitle(getContext(), getViewModel());
    }

    @Override
    public int getContentView() {
        return R.layout.rank_breakdown_panel_content;
    }

    @Override
    public void updateViewModel(final RankBreakdownViewModel viewModel) {

        setViewModel(viewModel);
        setTitle(getTitleSpannable());

        if (showEmptyCard()) {
            return;
        }

        emptyDataSetTextView.setVisibility(View.INVISIBLE);
        panelContent.setVisibility(VISIBLE);

        tooltipLayout.setVisibility(VISIBLE);


        containerView.setBackgroundResource(android.R.color.transparent);
        webViewContainer.setVisibility(VISIBLE);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        webView.getViewTreeObserver().
                addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        webView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        RankBreakdownUtils.displayGraph(PrintOSApplication.getAppContext(),
                                webView,
                                viewModel);
                    }
                });

        webView.addJavascriptInterface(new RankingBreakdownPanel.WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        sharePanel();
    }

    @Override
    public void sharePanel() {

        if (callbacks != null && getViewModel() != null && panelContainer != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            Bitmap panelBitmap = FileUtils.getScreenShot(panelContainer);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                final String title = getViewModel().getType() == RankingViewModel.RankAreaType.WORLD_WIDE ?
                        getContext().getString(R.string.share_caption_title_leaderboard_graph_worldwide)
                        : getContext().getString(R.string.share_caption_title_leaderboard_graph_region, getViewModel().getName());

                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_LEADERBOARD,
                        getViewModel().getType() == null ? "" : getViewModel().getType().getKey(),
                        null,

                        null,
                        false,
                        new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callbacks.onScreenshotCreated(Panel.RANKING, storageUri,
                                        title,
                                        link,
                                        RankingBreakdownPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addListener(RankBreakdownPanelCallback callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public String getShareAction() {
        if (getViewModel() != null && getViewModel().getType() != null) {
            String division = "";
            if (HomePresenter.getSelectedBusinessUnit() != null && HomePresenter.getSelectedBusinessUnit().getBusinessUnit() != null) {
                division = HomePresenter.getSelectedBusinessUnit().getBusinessUnit().getShortName();
            }
            return String.format(SHARE_ACTION, getViewModel().getType().name(), division).toLowerCase();
        }
        return "";
    }

    public interface RankBreakdownPanelCallback extends PanelShareCallbacks {
        void onShareButtonClicked(PanelView panelView);
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void tooltipChanged(final String ind) {

            final int index;
            try {
                index = Integer.parseInt(ind);
            } catch (Exception e) {
                return;
            }

            final long currentTime = SystemClock.elapsedRealtime();
            if (currentTime - lastAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_GRAPH_TOUCHED);
                lastAnalyticsEventSentTime = currentTime;
            }

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTooltipText(index);
                    }
                });
            }

        }
    }

    private void updateTooltipText(int index) {

        RankBreakdownViewModel model = getViewModel();
        if (model == null || model.getHistoryList() == null || index < 0 || index >= model.getHistoryList().size()) {
            return;
        }

        tooltipDescription.setText(model.getHistoryList().get(index).getFormattedTooltip(getContext()));
        weekNumberTextView.setText(model.getHistoryList().get(index).getTooltipWeekNumber(getContext()));
    }

    @Override
    protected boolean showPanelHeader() {
        return false;
    }
}


