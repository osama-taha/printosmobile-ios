package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.R;

import java.util.List;

/**
 * Created by Osama Taha on 10/12/16.
 */
public class HistogramLegendsAdapter extends RecyclerView.Adapter<HistogramLegendsAdapter.HistogramLegendsViewHolder> {

    private final List<TodayHistogramViewModel.HistogramShiftLegend> mLegends;

    public HistogramLegendsAdapter(Context context, List<TodayHistogramViewModel.HistogramShiftLegend> shiftLegends) {
        this.mLegends = shiftLegends;
    }

    @Override
    public HistogramLegendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.histogram_legend_item, parent, false);
        return new HistogramLegendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistogramLegendsViewHolder holder, int position) {

        holder.mLegendItem = mLegends.get(position);

        holder.mLegendColorView.setBackgroundResource(holder.mLegendItem.getLegendColor());
        holder.mLegendNameTextView.setText(holder.mLegendItem.getLegendName());

    }

    @Override
    public int getItemCount() {
        return mLegends == null ? 0 : mLegends.size();
    }


    public class HistogramLegendsViewHolder extends RecyclerView.ViewHolder {

        private final View mLegendColorView;
        private final TextView mLegendNameTextView;
        private TodayHistogramViewModel.HistogramShiftLegend mLegendItem;

        public HistogramLegendsViewHolder(View view) {
            super(view);
            mLegendColorView = view.findViewById(R.id.legend_color_view);
            mLegendNameTextView = view.findViewById(R.id.legend_name_text_view);
        }

    }
}
