package com.hp.printosmobile.presentation.modules.settings;

import android.content.Context;
import android.net.Uri;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 10/3/2017.
 */

public class ProfilePresenter extends Presenter<ProfileView> {

    public static final String TAG = ProfilePresenter.class.getName();

    public void getProfileImageInfo() {
        Subscription subscription = ProfileManager.getProfileImageInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Unable to get profile image info " + e.getMessage());
                        mView.onProfileImageUrlRetrieved(null);
                    }

                    @Override
                    public void onNext(String url) {
                        mView.onProfileImageUrlRetrieved(url);
                    }
                });

        addSubscriber(subscription);
    }

    public void uploadProfileImage(final Context context, Uri profileImageUri, final boolean isEdited) {
        Subscription subscription = ProfileManager.uploadProfileImage(context, profileImageUri)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                        Analytics.sendEvent(isEdited ?
                                Analytics.FAILED_TO_EDIT_PROFILE_PICTURE_ACTION :
                                Analytics.FAILED_TO_SET_PROFILE_PICTURE_ACTION);


                        HPLogger.d(TAG, "Unable to upload profile image " + e.getMessage());
                        getProfileImageInfo();
                        HPUIUtils.displayToast(context,
                                context.getString(R.string.user_profile_fail_to_upload_profile_image)
                        );
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Analytics.sendEvent(isEdited ?
                                Analytics.EDITED_PROFILE_PICTURE_ACTION :
                                Analytics.SET_PROFILE_PICTURE_ACTION);

                        getProfileImageInfo();
                    }
                });

        addSubscriber(subscription);
    }


    public void deleteProfileImage() {
        Subscription subscription = ProfileManager.deleteProfile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Analytics.sendEvent(Analytics.FAILED_TO_DELETE_PROFILE_PICTURE_ACTION);

                        HPLogger.d(TAG, "Unable to delete profile image " + e.getMessage());
                        getProfileImageInfo();
                        HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                                PrintOSApplication.getAppContext().getString(R.string.user_profile_fail_to_delete_profile_image)
                        );
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Analytics.sendEvent(Analytics.DELETE_PROFILE_PICTURE_ACTION);

                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                                .saveUserImage("");
                        ImageLoadingUtils.clearCache();
                        getProfileImageInfo();
                    }
                });

        addSubscriber(subscription);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
