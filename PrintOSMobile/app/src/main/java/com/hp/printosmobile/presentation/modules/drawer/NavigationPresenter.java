package com.hp.printosmobile.presentation.modules.drawer;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.settings.ProfileManager;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/20/16.
 */
public class NavigationPresenter extends Presenter<NavView> {

    public static final String TAG = NavigationPresenter.class.getName();

    private static List<OrganizationViewModel> organizationList;

    public void getOrganizations() {

        if (organizationList != null && !organizationList.isEmpty()) {
            if (mView != null) {
                mView.onOrganizationsRetrieved(organizationList);
            }
            return;
        }

        Subscription subscription = AAAManager.getUserOrganizations()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<OrganizationViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d("TAG", "Unable to retrieve the organizations data due to " + e.getMessage());

                        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                        if (e instanceof APIException) {
                            exception = (APIException) e;
                        }

                        setOrganizationList(null);
                        mView.onOrganizationsRetrieved(organizationList);
                        HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                                HPLocaleUtils.getSimpleErrorMsg(PrintOSApplication.getAppContext(), exception)
                        );
                    }

                    @Override
                    public void onNext(List<OrganizationViewModel> response) {
                        setOrganizationList(response);
                        Collections.sort(organizationList, OrganizationViewModel.OrganizationComparator);
                        mView.onOrganizationsRetrieved(organizationList);
                    }
                });
        addSubscriber(subscription);
    }

    public void changeContext(OrganizationViewModel viewModel) {

        Subscription subscription = AAAManager.changeContext(viewModel.getOrganizationJsonBody())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<OrganizationViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Unable to change the context due to " + e.getMessage());
                        APIException exception;
                        if (e instanceof APIException) {
                            exception = (APIException) e;
                        } else {
                            exception = APIException.create(APIException.Kind.UNEXPECTED);
                        }

                        mView.onContextChanged(false, null, exception);
                    }

                    @Override
                    public void onNext(OrganizationViewModel response) {

                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveCurrentOrganization(response);

                        mView.onContextChanged(true, response, null);
                    }
                });

        addSubscriber(subscription);
    }

    public void getProfileImageInfo() {

        Subscription subscription = ProfileManager.getProfileImageInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Unable to get profile image info " + e.getMessage());
                        mView.onProfileImageUrlRetrieved(null);
                    }

                    @Override
                    public void onNext(String url) {
                        mView.onProfileImageUrlRetrieved(url);
                    }
                });

        addSubscriber(subscription);
    }

    private static synchronized void setOrganizationList(List<OrganizationViewModel> organizationList) {
        NavigationPresenter.organizationList = organizationList;
    }

    public static void reset() {
        setOrganizationList(null);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
