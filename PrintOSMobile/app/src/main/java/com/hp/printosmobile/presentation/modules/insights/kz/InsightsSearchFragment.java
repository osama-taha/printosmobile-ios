package com.hp.printosmobile.presentation.modules.insights.kz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.VerticalSpaceItemDecoration;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.PrintOSBaseAdapter;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Observable;

/**
 * Created by Osama Taha on 3/19/18.
 */
public class InsightsSearchFragment extends BaseFragment implements InsightsSearchView, KZItemDetailsCallback {

    private static final long TRANSITION_ANIMATION_DURATION = 300;

    @Bind(R.id.appbar)
    AppBarLayout appbar;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_underline)
    View toolbarUnderline;
    @Bind(R.id.search_edit_text)
    AutoCompleteTextView searchEditText;
    @Bind(R.id.search_results_rv)
    RecyclerView searchResultsRecyclerView;
    @Bind(R.id.empty_tv)
    TextView emptyTextView;
    @Bind(R.id.search_progress_bar)
    ProgressBar searchProgressBar;
    @Bind(android.R.id.empty)
    LinearLayout emptyLayout;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;
    @Bind(R.id.main_content)
    View containerView;

    private Transition sharedElementEnterTransition;

    private Observable<CharSequence> searchTextQueryChange;
    private InsightsSearchPresenter searchPresenter;
    private SearchInsightsAdapter searchInsightsAdapter;
    private LinearLayoutManager layoutManager;

    private boolean isLoading;
    private AutoCompleteAdapter autoCompleteAdapter;
    private BusinessUnitEnum businessUnitEnum;
    private List<KZItem> kzItemsList;
    private static InsightsView callback;

    public InsightsSearchFragment() {
        // Required empty public constructor
    }


    private Transition.TransitionListener transitionListener;


    private PrintOSBaseAdapter.OnReloadClickListener onReloadClickListener = new PrintOSBaseAdapter.OnReloadClickListener() {
        @Override
        public void onReloadClick() {
            searchPresenter.getNextPage(searchInsightsAdapter.getItemCount());
        }
    };

    private PrintOSBaseAdapter.OnItemClickListener onItemClickListener = new PrintOSBaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(int position, View view) {
            openKnowledgeZoneItem(searchInsightsAdapter.getItem(position));
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (dy <= 0) {
                return;
            }

            HPUIUtils.hideSoftKeyboard(getActivity(), searchEditText);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !searchPresenter.isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= InsightsSearchPresenter.PAGE_SIZE) {
                    searchPresenter.getNextPage(searchInsightsAdapter.getItemCount());
                }
            }

        }
    };

    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        searchPresenter.onReloadButtonClicked(searchEditText.getText().toString());
    }

    @OnClick(R.id.main_content)
    public void onContainerViewClicked() {
        HPUIUtils.hideSoftKeyboard(getContext(), searchEditText);
    }

    public static InsightsSearchFragment newInstance(Bundle extras) {
        InsightsSearchFragment fragment = new InsightsSearchFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            transitionListener = new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                        final ViewPropertyAnimator viewPropertyAnimator = searchEditText.animate()
                                .alpha(1.0f).setDuration(TRANSITION_ANIMATION_DURATION);

                        viewPropertyAnimator.setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animator) {

                                if (!isAdded() || getActivity() == null) {
                                    return;
                                }

                                HPUIUtils.showKeyboard(getActivity(), searchEditText);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    sharedElementEnterTransition.removeListener(transitionListener);
                                }
                                viewPropertyAnimator.setListener(null);
                            }
                        }).start();

                    }
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            };

            sharedElementEnterTransition = getActivity().getWindow().getSharedElementEnterTransition();
            sharedElementEnterTransition.addListener(transitionListener);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        businessUnitEnum = (BusinessUnitEnum) getArguments().get(Constants.IntentExtras.BUSINESS_UNIT_EXTRA);

        setUpSearchResultsList();

        KZDefaultView.addKZItemDetailsCallback(this);
        KZVideoView.addKZItemDetailsCallback(this);
        KZDefaultDetailsFragment.addKZItemDetailsCallback(this);
        KZVideoActivity.addKZItemDetailsCallback(this);

        searchEditText.setHint(BusinessUnitViewModel.getKZSearchPlaceHolder(businessUnitEnum));

        searchPresenter = new InsightsSearchPresenter(businessUnitEnum);
        searchPresenter.attachView(this);

        searchTextQueryChange = RxTextView.textChanges(searchEditText);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    HPUIUtils.hideSoftKeyboard(getActivity(), searchEditText);
                    searchPresenter.onLoadSearch(searchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });

        searchPresenter.addOnAutoCompleteTextViewTextChangedObserver(searchTextQueryChange);

        Bundle bundle = getArguments();
        if(bundle != null && bundle.containsKey(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA)) {
            final String query = bundle.getString(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    searchEditText.setText(query);
                    HPUIUtils.hideSoftKeyboard(getActivity(), searchEditText);
                    searchPresenter.onLoadSearch(searchEditText.getText().toString());
                    searchEditText.dismissDropDown();
                }
            }, 500);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                HPUIUtils.hideSoftKeyboard(getContext(), searchEditText);
                hideLoadingView();
                searchEditText.animate()
                        .alpha(0.0f)
                        .setDuration(TRANSITION_ANIMATION_DURATION)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animator) {
                                if (getActivity() != null) {
                                    getActivity().supportFinishAfterTransition();
                                }
                            }
                        }).start();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpSearchResultsList() {

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        searchResultsRecyclerView.setLayoutManager(layoutManager);
        searchInsightsAdapter = new SearchInsightsAdapter(getActivity(), businessUnitEnum.getKzName());
        searchInsightsAdapter.setOnReloadClickListener(onReloadClickListener);
        searchResultsRecyclerView.setAdapter(searchInsightsAdapter);
        searchResultsRecyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        searchInsightsAdapter.setOnItemClickListener(onItemClickListener);
        searchResultsRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VerticalSpaceItemDecoration.VERTICAL_ITEM_SPACE));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_insights_search;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public boolean onBackPressed() {
        hideLoadingView();
        searchEditText.animate().alpha(0.0f).setDuration(TRANSITION_ANIMATION_DURATION).start();
        return super.onBackPressed();
    }

    @Override
    public void showEmptyView() {
        emptyLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyLayout.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyText(String searchQuery) {

        emptyTextView.setText(getString(R.string.insights_search_no_results_found, searchQuery));
    }

    @Override
    public void showLoadingView() {
        searchProgressBar.setVisibility(View.VISIBLE);
        toolbarUnderline.setVisibility(View.GONE);
        isLoading = true;
    }

    @Override
    public void hideLoadingView() {
        searchProgressBar.setVisibility(View.GONE);
        toolbarUnderline.setVisibility(View.VISIBLE);
        isLoading = false;
    }

    @Override
    public void addAutoCompleteResults(List<KZAutoCompleteResult> autoCompleteResults) {
        autoCompleteAdapter = new AutoCompleteAdapter(getActivity());
        searchEditText.setAdapter(autoCompleteAdapter);
        autoCompleteAdapter.update(autoCompleteResults);
    }

    @Override
    public void showErrorView() {
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void addResults(List<KZItem> kzItems) {
        searchInsightsAdapter.setSearchQuery(searchPresenter.getSearchText());
        this.kzItemsList = kzItems;
        searchInsightsAdapter.addAll(kzItemsList);
    }

    @Override
    public void clearResults() {
        searchInsightsAdapter.clearItems();
    }

    @Override
    public void hideResultsView() {
        searchResultsRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showResultsView() {
        searchResultsRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void openKnowledgeZoneItem(KZItem kzItem) {

        Analytics.sendEvent(String.format(Analytics.OPEN_INSIGHTS_ITEM, businessUnitEnum.getShortName()), KZSupportedFormat.from(kzItem.getFormat()).getValue());

        Navigator.openKZDetailsActivity(getActivity(), kzItem, businessUnitEnum);

    }

    @Override
    public void showNoResultsFooterView() {
        searchInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.NO_RESULTS);
    }

    @Override
    public void onAutoCompleteResultSelected(KZAutoCompleteResult autoCompleteResult) {

        if (autoCompleteResult.getType() == KZAutoCompleteResult.KZAutoCompleteResultType.AUTO_COMPLETE_WORD) {
            String searchQuery = HPStringUtils.replaceLastWord(searchEditText.getText().toString(), autoCompleteResult.getName());
            searchEditText.setText(searchQuery);
            HPUIUtils.showKeyboard(getActivity(), searchEditText);
        } else {
            searchEditText.setText(autoCompleteResult.getName());
            HPUIUtils.hideSoftKeyboard(getActivity(), searchEditText);
            searchPresenter.onLoadSearch(autoCompleteResult.getName());
        }

        searchEditText.dismissDropDown();
    }

    @Override
    public void showLoadingFooterView() {
        isLoading = true;
        searchInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.LOAD_MORE);
    }

    @Override
    public void hideErrorView() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void addFooterView() {
        searchInsightsAdapter.addFooterView();
    }

    @Override
    public void removeFooterView() {
        searchInsightsAdapter.removeFooterView();
        isLoading = false;
    }

    @Override
    public void showErrorFooterView() {
        searchInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.ERROR);
    }

    private void removeListeners() {
        searchInsightsAdapter.setOnItemClickListener(null);
        searchResultsRecyclerView.removeOnScrollListener(recyclerViewOnScrollListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (searchPresenter != null) {
            searchPresenter.detachView();
        }
        removeListeners();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onFavoritePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onLikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onDislikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onResume() {
        super.onResume();
        searchInsightsAdapter.notifyDataSetChanged();
    }

    private void updatePanel(KZItem kzItem) {
        if (callback != null) {
            callback.updateView(kzItem);
        }

        if (kzItemsList == null || kzItemsList.size() == 0) {
            return;
        }

        for (KZItem kzListItem : kzItemsList) {
            if (kzListItem.getId().equals(kzItem.getId())) {
                kzItemsList.set(kzItemsList.indexOf(kzListItem), kzItem);
                searchInsightsAdapter.clearItems();
                searchInsightsAdapter.addAll(kzItemsList);
                searchInsightsAdapter.notifyDataSetChanged();
            }
        }
    }


    public class AutoCompleteAdapter extends ArrayAdapter<KZAutoCompleteResult> {

        private static final String AUTO_COMPLETE_WORD_DISPLAY_FORMAT = "...%s";
        private final LayoutInflater inflater;

        public AutoCompleteAdapter(Context context) {
            super(context, 0);
            setNotifyOnChange(false);

            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.search_suggestions_row, null);
            }

            final KZAutoCompleteResult autoCompleteResult = getItem(position);

            TextView textView = convertView.findViewById(R.id.auto_complete_row_text);
            if (autoCompleteResult.getType() == KZAutoCompleteResult.KZAutoCompleteResultType.AUTO_COMPLETE_WORD) {
                textView.setText(String.format(AUTO_COMPLETE_WORD_DISPLAY_FORMAT, autoCompleteResult.getName()));
            } else {
                textView.setText(autoCompleteResult.getName());
            }

            int txtColor = ResourcesCompat.getColor(getActivity().getResources(), autoCompleteResult.getType().getTxtColorResId(), null);
            textView.setTextColor(txtColor);

            View layout = convertView.findViewById(R.id.suggestion_layout);
            int bgColor = ResourcesCompat.getColor(getActivity().getResources(), autoCompleteResult.getType().getBgColorResId(), null);
            layout.setBackgroundColor(bgColor);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (autoCompleteResult != null) {
                        onAutoCompleteResultSelected(autoCompleteResult);
                    }
                }
            });

            convertView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (event.getAction() == MotionEvent.ACTION_SCROLL) {
                        HPUIUtils.hideSoftKeyboard(getActivity(), searchEditText);
                    }

                    return false;
                }
            });

            return convertView;
        }

        /**
         * Updates adapter content with new list of search autoCompleteResults.
         *
         * @param autoCompleteResults List of search autoCompleteResults.
         */
        public void update(List<KZAutoCompleteResult> autoCompleteResults) {
            clear();
            if (autoCompleteResults != null) {
                for (KZAutoCompleteResult autoCompleteResult : autoCompleteResults) {
                    add(autoCompleteResult);
                }
            }
            notifyDataSetChanged();
        }


    }

    public static void addCallback(InsightsView insightsView) {
        callback = insightsView;
    }

    public interface InsightsView {
        void updateView(KZItem kzItem);
    }
}
