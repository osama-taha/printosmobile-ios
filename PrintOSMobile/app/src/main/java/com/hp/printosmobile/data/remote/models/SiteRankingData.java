package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Osama Taha 6/12/2018
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteRankingData {

    @JsonProperty("week")
    private Integer week;
    @JsonProperty("year")
    private Integer year;
    @JsonProperty("score")
    private Integer score;
    @JsonProperty("countryCode")
    private String countryCode;
    @JsonProperty("countryName")
    private String countryName;
    @JsonProperty("subRegion")
    private Area subRegion;
    @JsonProperty("region")
    private Area region;
    @JsonProperty("worldWide")
    private Area worldWide;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("week")
    public Integer getWeek() {
        return week;
    }

    @JsonProperty("week")
    public void setWeek(Integer week) {
        this.week = week;
    }

    @JsonProperty("year")
    public void setYear(Integer year) {
        this.year = year;
    }

    @JsonProperty("year")
    public Integer getYear() {
        return year;
    }

    @JsonProperty("score")
    public void setScore(Integer score) {
        this.score = score;
    }

    @JsonProperty("score")
    public Integer getScore() {
        return score;
    }

    @JsonProperty("subRegion")
    public Area getSubRegion() {
        return subRegion;
    }

    @JsonProperty("subRegion")
    public void setSubRegion(Area subRegion) {
        this.subRegion = subRegion;
    }

    @JsonProperty("region")
    public Area getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(Area region) {
        this.region = region;
    }

    @JsonProperty("worldWide")
    public Area getWorldWide() {
        return worldWide;
    }

    @JsonProperty("worldWide")
    public void setWorldWide(Area worldWide) {
        this.worldWide = worldWide;
    }

    @JsonProperty("countryCode")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("countryCode")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("countryName")
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @JsonProperty("countryName")
    public String getCountryName() {
        return countryName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Rankings{" +
                "subRegion=" + subRegion +
                ", region=" + region +
                ", worldWide=" + worldWide +
                '}';
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class Area {

        @JsonProperty("name")
        private String name;
        @JsonProperty("position")
        private int position;
        @JsonProperty("total")
        private int total;
        @JsonProperty("offset")
        private int offset;
        @JsonProperty("isTop")
        private boolean isTop;
        @JsonProperty("isLowerThanThreshold")
        private boolean isLowerThanThreshold;
        @JsonProperty("rankType")
        private String rankType;
        @JsonProperty("lowerTresholdValue")
        private int lowerTresholdValue;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("position")
        public int getPosition() {
            return position;
        }

        @JsonProperty("position")
        public void setPosition(int position) {
            this.position = position;
        }

        @JsonProperty("total")
        public int getTotal() {
            return total;
        }

        @JsonProperty("total")
        public void setTotal(int total) {
            this.total = total;
        }

        @JsonProperty("offset")
        public int getOffset() {
            return offset;
        }

        @JsonProperty("offset")
        public void setOffset(int offset) {
            this.offset = offset;
        }

        @JsonProperty("isTop")
        public boolean isTop() {
            return isTop;
        }

        @JsonProperty("isTop")
        public void setTop(boolean top) {
            isTop = top;
        }

        @JsonProperty("isLowerThanThreshold")
        public boolean isLowerThanThreshold() {
            return isLowerThanThreshold;
        }

        @JsonProperty("isLowerThanThreshold")
        public void setLowerThanThreshold(boolean lowerThanThreshold) {
            isLowerThanThreshold = lowerThanThreshold;
        }

        @JsonProperty("lowerTresholdValue")
        public void setLowerTresholdValue(int lowerTresholdValue) {
            this.lowerTresholdValue = lowerTresholdValue;
        }

        @JsonProperty("lowerTresholdValue")
        public int getLowerTresholdValue() {
            return lowerTresholdValue;
        }

        @JsonProperty("rankType")
        public void setRankType(String rankType) {
            this.rankType = rankType;
        }

        @JsonProperty("rankType")
        public String getRankType() {
            return rankType;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Area{" +
                    "name='" + name + '\'' +
                    ", position=" + position +
                    ", total=" + total +
                    ", offset=" + offset +
                    ", isTop=" + isTop +
                    ", isLowerThanThreshold=" + isLowerThanThreshold +
                    '}';
        }
    }

}