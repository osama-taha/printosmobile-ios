package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha
 */
public class WizardCollectCoinsFragment extends BaseFragment implements IWizardFragment {

    public static final String TAG = WizardCollectCoinsFragment.class.getSimpleName();

    private static final int COINS_EXPIRES_IN_DAYS = 10;

    @Bind(R.id.loading_indicator)
    View loadingIndicator;
    @Bind(R.id.collect_coins_get_beat_coins)
    TextView getBeatCoinsTextView;
    @Bind(R.id.finish_and_collect_coins)
    TextView collectCoins;

    private WizardCollectCoinsFragmentCallbacks callbacks;

    public static WizardCollectCoinsFragment getInstance(WizardCollectCoinsFragmentCallbacks callbacks) {

        WizardCollectCoinsFragment wizardCollectCoinsFragment = new WizardCollectCoinsFragment();
        wizardCollectCoinsFragment.callbacks = callbacks;
        Bundle args = new Bundle();
        wizardCollectCoinsFragment.setArguments(args);

        return wizardCollectCoinsFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {

        Context context = PrintOSApplication.getAppContext();
        String coins = HPLocaleUtils.getLocalizedValue(PrintOSPreferences.getInstance(context).getWizardReward());
        String text = context.getString(R.string.wizard_collect_coins_get_beat_coins, coins);

        //TODO
        int startInd = text.indexOf(coins);
        int endInd = startInd + coins.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.5f), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getBeatCoinsTextView.setText(spannableString);

    }

    @OnClick(R.id.finish_and_collect_coins)
    void onClickCollectCoins() {

        HPLogger.d(TAG, "Wizard collect coins clicked.");
        Analytics.sendEvent(Analytics.EVENT_WIZARD_COLLECT_COINS);

        showLoadingIndicator(true);
        enableCollectCoins(false);

        UserProfileManager.getInstance()
                .createCoinsClaimLink(getActivity(), PrintOSPreferences.getInstance(getContext()).getWizardReward(), COINS_EXPIRES_IN_DAYS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {

                showLoadingIndicator(false);
                enableCollectCoins(true);

            }

            @Override
            public void onError(Throwable e) {

                APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                if (e instanceof APIException) {
                    exception = (APIException) e;
                }

                HPUIUtils.displayToastException(getContext(), exception, TAG, false);

                showLoadingIndicator(false);
                enableCollectCoins(true);

            }

            @Override
            public void onNext(String claimUrl) {

                AppUtils.startApplication(getActivity(), Uri.parse(claimUrl));

                if (callbacks != null) {
                    callbacks.onCollectCoinsClicked();
                }

            }
        });
    }

    void showLoadingIndicator(boolean show) {
        HPUIUtils.setVisibility(show, loadingIndicator);
    }

    void enableCollectCoins(boolean enable) {
        collectCoins.setEnabled(enable);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.wizard_collect_coins_page;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public String getFragmentName() {
        return "Wizard_Collect_Coins";
    }

    @Override
    public void onCancel() {

    }

    interface WizardCollectCoinsFragmentCallbacks {
        void onCollectCoinsClicked();
    }

}
