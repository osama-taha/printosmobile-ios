package com.hp.printosmobile.presentation.modules.today.histogrambreakdown;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;
import com.hp.printosmobile.presentation.modules.today.HistogramGraphUtils;
import com.hp.printosmobile.presentation.modules.today.HistogramPanel;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.presentation.modules.todayhistogram.HistogramPresenterView;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramPresenter;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * created by Anwar Asbah 4/24/2018
 */
public class HistogramBreakdownFragment extends BaseFragment implements HistogramPanel.HistogramPanelCallback, HistogramPresenterView {

    private static final String TAG = HistogramBreakdownFragment.class.getName();
    private static final String HISTOGRAM_VIEW_MODEL_PARAM = "view_model_param";
    private static final String RESOLUTION_TYPE_PARAM = "resolution_type_param";
    private static final String FOCUSABLE_PANEL_PARAM = "focusable_panel_param";

    private static final int WEBVIEW_HEIGHT_DP = 400;
    private static final long SCROLL_TO_PANEL_DELAY = 200;

    private Map<TodayHistogramViewModel.DetailTypeEnum, PanelView> panelViewMap = new HashMap<>();

    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.panels_container)
    LinearLayout panelsContainerView;
    @Bind(R.id.parent_container)
    View parentContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.error_layout)
    View errorLayout;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;

    private PanelView panelRequestedSharing;
    private TodayHistogramViewModel histogramViewModel;
    private HistogramResolution resolution;
    private String focusablePanel;
    private TodayHistogramPresenter todayHistogramPresenter;

    public HistogramBreakdownFragmentCallback callback;

    public static Fragment newInstance(TodayHistogramViewModel viewModel, String focusablePanel,
                                       HistogramResolution resolution,
                                       HistogramBreakdownFragmentCallback callback) {

        Bundle bundle = new Bundle();
        if (viewModel != null) {
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, viewModel);
        }
        if (focusablePanel != null) {
            bundle.putString(FOCUSABLE_PANEL_PARAM, focusablePanel);
        }
        if (resolution != null) {
            bundle.putInt(RESOLUTION_TYPE_PARAM, resolution.ordinal());
        }

        HistogramBreakdownFragment fragment = new HistogramBreakdownFragment();
        fragment.callback = callback;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(HISTOGRAM_VIEW_MODEL_PARAM)) {
                histogramViewModel = (TodayHistogramViewModel) args.get(HISTOGRAM_VIEW_MODEL_PARAM);
            }
            if (args.containsKey(FOCUSABLE_PANEL_PARAM)) {
                focusablePanel = args.getString(FOCUSABLE_PANEL_PARAM);
            }
            if (args.containsKey(RESOLUTION_TYPE_PARAM)) {
                resolution = HistogramResolution.values()[args.getInt(RESOLUTION_TYPE_PARAM)];
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (resolution == HistogramResolution.DAILY &&
                histogramViewModel != null) {
            initPanels();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_break_down_histogram;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    public void onSelection() {
        if (histogramViewModel == null && (resolution == HistogramResolution.MONTHLY ||
                resolution == HistogramResolution.WEEKLY)) {
            if (todayHistogramPresenter == null) {
                todayHistogramPresenter = new TodayHistogramPresenter();
                todayHistogramPresenter.attachView(this);
            }

            todayHistogramPresenter.getPrintVolumeDataPerResolution(resolution);
            loadingView.setVisibility(View.VISIBLE);
        }
    }

    private void initPanels() {
        initPanel(TodayHistogramViewModel.DetailTypeEnum.MAIN);
        if (histogramViewModel.getMeasureTypeEnum() == MeasureTypeEnum.MIXED ||
                histogramViewModel.getMeasureTypeEnum() == MeasureTypeEnum.SHEETS && HistogramGraphUtils.hasValues(histogramViewModel, TodayHistogramViewModel.DetailTypeEnum.SHEETS)) {
            initPanel(TodayHistogramViewModel.DetailTypeEnum.SHEETS);
        }
        if (histogramViewModel.getMeasureTypeEnum() == MeasureTypeEnum.MIXED ||
                histogramViewModel.getMeasureTypeEnum() == MeasureTypeEnum.LENGTH) {
            if (HistogramGraphUtils.hasValues(histogramViewModel, TodayHistogramViewModel.DetailTypeEnum.SQM)) {
                initPanel(TodayHistogramViewModel.DetailTypeEnum.SQM);
            }
            if (HistogramGraphUtils.hasValues(histogramViewModel, TodayHistogramViewModel.DetailTypeEnum.LM)) {
                initPanel(TodayHistogramViewModel.DetailTypeEnum.LM);
            }
        }

        if (focusablePanel != null) {
            TodayHistogramViewModel.DetailTypeEnum panelTypeEnum = TodayHistogramViewModel
                    .DetailTypeEnum.from(focusablePanel);
            scrollFragmentToPanel(panelTypeEnum);
        }
    }

    private void initPanel(TodayHistogramViewModel.DetailTypeEnum panel) {
        PanelView panelView;
        if (!panelViewMap.containsKey(panel)) {
            LinearLayout.LayoutParams layoutParams = HPUIUtils.getPanelLayoutParams(getHostingActivity());

            panelView = new HistogramPanel(getHostingActivity());
            ((HistogramPanel) panelView).addHistogramPanelCallback(this);
            ((HistogramPanel) panelView).setBreakdownType(panel, false, true);
            ((HistogramPanel) panelView).setBreakdownResolution(resolution);
            ((HistogramPanel) panelView).showTooltip();
            ((HistogramPanel) panelView).setHistogramWebViewHeight(HPUIUtils.dpToPx(getHostingActivity(), WEBVIEW_HEIGHT_DP));
            ((HistogramPanel) panelView).updateViewModel(histogramViewModel);

            panelViewMap.put(panel, panelView);
            panelsContainerView.addView(panelView, layoutParams);
        } else {
            panelView = panelViewMap.get(panel);
            panelView.onRefresh();
            panelView.showLoading();
        }
    }

    private void scrollFragmentToPanel(final TodayHistogramViewModel.DetailTypeEnum panel) {
        if (panel == null || panelViewMap == null || !panelViewMap.containsKey(panel)) {
            return;
        }

        final PanelView panelView = panelViewMap.get(panel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
            }
        }, SCROLL_TO_PANEL_DELAY);

    }

    @Override
    public void onDestroy() {
        if (panelViewMap != null) {
            for (TodayHistogramViewModel.DetailTypeEnum panel : panelViewMap.keySet()) {
                panelViewMap.get(panel).onDestroy();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {
        panelRequestedSharing = panelView;
        if (this.panelRequestedSharing == null) {
            return;
        }

        if (callback != null) {
            callback.onShareButtonClicked(panelView);
        }
    }

    @Override
    public void onShareButtonClicked(Panel panel) {
        //do nothing
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject) {
        ShareUtils.onScreenshotCreated(getActivity(), screenshotUri, title, body, sharableObject);
    }

    @Override
    public void onHistogramViewAllClicked(TodayHistogramViewModel model) {

    }

    @Override
    public void onHistogramWebViewClicked(TodayHistogramViewModel model) {

    }

    @Override
    public Activity getHostingActivity() {
        return getActivity();
    }

    @Override
    public void setFocused(Panel panel) {

    }

    @Override
    public void onDeepLinkHandled() {

    }

    @Override
    public void updateHistogram(TodayHistogramViewModel todayHistogramViewModel) {
        if (!isAdded() || getActivity() == null) {
            return;
        }
        loadingView.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);
        histogramViewModel = todayHistogramViewModel;
        initPanels();
    }

    @Override
    public void onRequestError(APIException exception) {
        if (!isAdded() || getActivity() == null) {
            return;
        }
        errorLayout.setVisibility(View.VISIBLE);
        errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(getActivity(), exception));
        loadingView.setVisibility(View.GONE);
    }

    public View getMostFocusablePanel() {
        if (scrollView == null || panelsContainerView == null) {
            return null;
        }

        int index = HPUIUtils.getMostVisiblePanelIndex(scrollView, panelsContainerView);

        if (index >= 0 && index < panelsContainerView.getChildCount()) {
            return panelsContainerView.getChildAt(index);
        }

        return null;
    }

    @OnClick(R.id.try_again_text_view)
    public void onTryAgainClicked() {
        onSelection();
    }

    @Override
    public void onCancel() {

    }

    public interface HistogramBreakdownFragmentCallback {

        void onShareButtonClicked(PanelView panelView);

    }
}
