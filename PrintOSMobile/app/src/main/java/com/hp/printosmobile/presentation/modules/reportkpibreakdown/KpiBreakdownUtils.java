package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.kpiview.KPIValueHandleEnum;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPMathUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anwar Asbah on 1/30/2018.
 */

public class KpiBreakdownUtils {

    private static final String TAG = KpiBreakdownUtils.class.getSimpleName();
    private static final String HTML_FILE_NAME = "breakdown_blueprint.html";
    private static final String HTML_AGGREGATE_FILE_NAME = "breakdown_aggregate_blueprint.html";

    private static final String SERIES_KEY = "@TODAYSERIES@";
    private static final String XAXIS_KEY_KEY = "@XAXISVALUES@";
    private static final String LOCALE_KEY = "@LOCALE@";
    private static final String CHART_TYPE_KEY = "@CHARTTYPE@";
    private static final String VALUES_LABEL_KEY = "@VALUESLABELKEY@";
    private static final String VALUES_DIVIDER_KEY = "@VALUESDIVIDERKEY@";
    private static final String SERIES_COLOR_KEY = "@SERIESCOLOR@";
    private static final String HOVER_COLOR_KEY = "@HOVERCOLOR@";
    private static final String TOOLTIP_HEADER_KEY = "@TOOLTIPHEADERDIV@";
    private static final String EXTRA_KEY = "@EXTRA@";
    private static final String Y_AXIS_EXTRA_KEY = "@YAXISEXTRA@";
    private static final String LEGEND_EXTRA_KEY = "@LEGENDEXTRA@";
    private static final String LOCALIZED_TOTAL_UP_TIME_KEY = "@LOCALIZEDTOTALUPTIME@";
    private static final String VISIBLE_GRAPHS_KEY = "@VISIBLE_GRAPHS_KEY@";
    private static final String IS_LANDSCAPE_KEY = "@ISLANDSCAPE@";
    private static final String INTERFACE_TAG_NAME = "app";

    private static final String TODAY_DATA_ENTRY_ITEM_FORMAT = "{x: %1$s," +
            "      y: %2$s,\n" +
            "      yWithUnit: %3$s,\n" +
            "      change: %4$s, \n" +
            "      isHigher: '%5$s',\n" +
            "      tooltipdate: %6$s}";
    private static final String HEADER_TOOLTIP = "    <div id=\"tooltip\" style=\"width: 95%%;margin: 10px auto\">\n" +
            "        <div>\n" +
            "        <span id=\"tooltip_value\" style=\"font-size: 110%%\">\n" +
            "          </br>\n" +
            "        </span>\n" +
            "        <span id=\"tooltip_change\">\n" +
            "        </span>\n" +
            "        </div>\n" +
            "        <div>\n" +
            "        </div>\n" +
            "\n" +
            "        <div id=breakdown_values>%s</div>\n" +
            "        <div id=\"tooltip_date\"></br></div>\n" +
            "        <div style=\"height: 10px\"></div>\n" +
            "    </div>";
    private static final String LINE_CHART_EXTRA = "lineColor: '@SERIESCOLOR@',\n" +
            "             color: {\n" +
            "               linearGradient: {\n" +
            "                 x1: 0,\n" +
            "                 y1: 0,\n" +
            "                 x2: 0,\n" +
            "                 y2: 1\n" +
            "               },\n" +
            "                  stops: [\n" +
            "                    [0, Highcharts.Color('@SERIESCOLOR@').setOpacity(0.5).get('rgba')], //'#003865'],\n" +
            "                    [1, Highcharts.Color('#FFFFFF').setOpacity(0).get('rgba')],\n" +
            "                  ]\n" +
            "             },";
    private static final String AGGREGATE_CHART_SERIES = "{name: '%1$s',\n" +
            "    animation: true,\n" +
            "    color: '%2$s',\n" +
            "    tag: '%3$s',\n" +
            "    data: [%4$s]}";
    private static final String AGGREGATE_CHART_DATA = "{x: %1$s,\n" +
            "        y: %2$s,\n" +
            "        tooltipColor: '%3$s',\n" +
            "        kpiName: \"%4$s\",\n" +
            "        weekNumber: '%5$s',\n" +
            "        tooltipDate: \"%6$s\",\n" +
            "        tooltipTime: '%7$s',\n" +
            "        totalUptime : '%8$s'}";

    private static final String LEGEND_EXTRA = "itemWidth: legendWidth(),";
    private static final String MAX_VALUE_HTML_TAG = "max: %d,\n";
    private static final String TICK_AMOUNT_HTML_TAG = "tickAmount: %d,\n";
    private static final int DEFAULT_NUMBER_OF_TICKS = 4;

    private static final String COLUMN_CHART_EXTRA = "";
    private static final String HISTOGRAM_DATE_FORMAT = "MMM dd";

    private KpiBreakdownUtils() {
    }

    public static void displayGraph(Context context, WebView progressWebView, KpiBreakdownViewModel kpiBreakdownViewModel, boolean isLandscape) {
        if (kpiBreakdownViewModel == null) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            progressWebView.setWebContentsDebuggingEnabled(true);
        }

        try {
            if (kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_AVAILABILITY_AGGREGATE) {
                displayAggregate(context, progressWebView, kpiBreakdownViewModel, isLandscape);
                return;
            } else if (kpiBreakdownViewModel.getEventList() == null) {
                return;
            }

            String locale = PrintOSPreferences.getInstance(context).getLanguage(context.getString(R.string.default_language));
            KpiBreakdownEnum kpiBreakdownEnum = kpiBreakdownViewModel.getKpiBreakdownEnum();

            KpiBreakdownEnum.Type type = kpiBreakdownViewModel.getKpiBreakdownEnum().getType();
            if (kpiBreakdownViewModel.isOverall()) {
                type = KpiBreakdownEnum.Type.COLUMN;
            }

            String detail = FileUtils.loadAssestFile(context, HTML_FILE_NAME);
            detail = setSeriesData(context, detail, kpiBreakdownViewModel);
            detail = detail.replace(LOCALE_KEY, locale)
                    .replace(EXTRA_KEY, type == KpiBreakdownEnum.Type.COLUMN ? COLUMN_CHART_EXTRA : LINE_CHART_EXTRA)
                    .replace(CHART_TYPE_KEY, type.getHtmlTag())
                    .replace(SERIES_COLOR_KEY, kpiBreakdownEnum.getPrimaryColor())
                    .replace(HOVER_COLOR_KEY, kpiBreakdownEnum.getSecondaryColor())
                    .replace(TOOLTIP_HEADER_KEY, String.format(HEADER_TOOLTIP, ""));

            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (IOException e) {
            HPLogger.e(TAG, "error displaying graph: " + e.getMessage());
        }
    }

    private static void displayAggregate(Context context, WebView progressWebView, KpiBreakdownViewModel kpiBreakdownViewModel, boolean isLandscape) throws IOException {
        progressWebView.addJavascriptInterface(new KpiBreakdownWebViewJavaScriptInterface(), INTERFACE_TAG_NAME);

        String locale = PrintOSPreferences.getInstance(context).getLanguage(context.getString(R.string.default_language));

        String detail = FileUtils.loadAssestFile(context, HTML_AGGREGATE_FILE_NAME);
        detail = setAggregateSeriesData(context, detail, kpiBreakdownViewModel, isLandscape)
                .replace(LOCALIZED_TOTAL_UP_TIME_KEY, context.getString(R.string.reports_total_up_time))
                .replace(IS_LANDSCAPE_KEY, String.valueOf(isLandscape))
                .replace(LOCALE_KEY, locale);

        progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
    }

    private static String getYAxisExtra(int maxValue, KpiBreakdownViewModel kpiBreakdownViewModel) {
        KpiBreakdownEnum kpiBreakdownEnum = kpiBreakdownViewModel.getKpiBreakdownEnum();
        if (kpiBreakdownViewModel.isOverall()) {
            return String.format(MAX_VALUE_HTML_TAG, kpiBreakdownViewModel.getMaxScore());
        } else {
            if (kpiBreakdownEnum.getValueHandleEnum() == KPIValueHandleEnum.PERCENT) {
                return "";
            }
            return String.format(MAX_VALUE_HTML_TAG, maxValue) + String.format(TICK_AMOUNT_HTML_TAG, 4);
        }
    }

    private static String setSeriesData(Context context, String html, KpiBreakdownViewModel kpiBreakdownViewModel) {

        if (kpiBreakdownViewModel == null || html == null || kpiBreakdownViewModel.getEventList() == null) {
            return "";
        }

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

        String series = "";
        String xValues = "";
        int maxValue = 0;

        for (int i = 0; i < kpiBreakdownViewModel.getEventList().size(); i++) {
            KpiBreakdownViewModel.Event item = kpiBreakdownViewModel.getEventList().get(i);

            float dayData = item.getDayValue();
            float lastWeekData = item.getLastWeekValue();

            float percentageChange;
            if (lastWeekData == 0) {
                percentageChange = dayData == 0 ? 0 : 1;
            } else {
                percentageChange = (dayData - lastWeekData) / lastWeekData;
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(item.getDate());

            int weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
            String weekNumberString = context.getString(R.string.week_number, weekNumber);

            float value = Math.max(0, dayData);
            String unit = kpiBreakdownViewModel.getKpiBreakdownEnum().getValueHandleEnum().getUnitString(unitSystem);
            if(kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_IMPRESSIONS ||
                    kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_EPM) {
                if(kpiBreakdownViewModel.getMinimumPaperType() != null) {
                    unit = " " + kpiBreakdownViewModel.getMinimumPaperType() + unit;
                }
            }

            if (kpiBreakdownViewModel.isOverall()) {
                unit = KPIValueHandleEnum.SCORE.getUnitString(unitSystem);
            }

            series += String.format(TODAY_DATA_ENTRY_ITEM_FORMAT,
                    String.valueOf(i),
                    String.valueOf(value).replace(",", "."),
                    "\"" + HPLocaleUtils.getDecimalString(value, true, 1)+ unit + "\"",
                    String.format("%.1f", Math.abs(percentageChange) * 100).replace(",", "."),
                    percentageChange >= 0 ? true : false,
                    "'" + weekNumberString + "'");
            series += (i == kpiBreakdownViewModel.getEventList().size() - 1) ? "" : ",";

            xValues += "'" + HPDateUtils.formatDate(item.getDate(), HISTOGRAM_DATE_FORMAT) + "'";
            xValues += (i == kpiBreakdownViewModel.getEventList().size() - 1) ? "" : ",";

            maxValue = Math.max(maxValue, (int) dayData);
        }

        int creditResource = kpiBreakdownViewModel.getKpiBreakdownEnum().getCreditResource();
        int power = HPMathUtils.get1000Power(maxValue);
        String creditString = HPMathUtils.get1000PowerLabel(context, power);
        if (creditResource != -1 && !kpiBreakdownViewModel.isOverall()) {
            power = 0;
            creditString = context.getString(creditResource);
        }

        return html.replace(SERIES_KEY, series)
                .replace(VALUES_LABEL_KEY, creditString)
                .replace(VALUES_DIVIDER_KEY, String.valueOf(Math.pow(1000, Math.min(power, 3))))
                .replace(XAXIS_KEY_KEY, xValues)
                .replace(Y_AXIS_EXTRA_KEY, getYAxisExtra(maxValue, kpiBreakdownViewModel));
    }

    private static String setAggregateSeriesData(Context context, String html, KpiBreakdownViewModel kpiBreakdownViewModel,
                                                 boolean isLandscape) {

        if (kpiBreakdownViewModel == null || html == null || kpiBreakdownViewModel.getAggregateModels() == null) {
            return "";
        }

        String series = "";
        String xValues = "";

        String visibleGraphsKeys = "";

        List<KpiBreakdownViewModel> subList = kpiBreakdownViewModel.getAggregateModels();
        int seriesIndex = 0;
        int totalUptime = 0;

        for (int kpiIndex = subList.size() - 1; kpiIndex >= 0; kpiIndex--) {

            KpiBreakdownViewModel viewModel = subList.get(kpiIndex);
            KpiBreakdownEnum subKpiEnum = viewModel.getKpiBreakdownEnum();

            String kpiName = context.getString(subKpiEnum.getNameResource());

            if (subKpiEnum == KpiBreakdownEnum.INDIGO_PRINTING_TIME
                    || subKpiEnum == KpiBreakdownEnum.INDIGO_NON_PRINTING_TIME) {
                visibleGraphsKeys += "\"" +
                        (seriesIndex) + "\",";
            }

            String data = "";
            List<KpiBreakdownViewModel.Event> subKpiEvents = viewModel.getEventList();
            for (int eventIndex = 0; eventIndex < subKpiEvents.size(); eventIndex++) {
                KpiBreakdownViewModel.Event event = subKpiEvents.get(eventIndex);
                if (subKpiEnum == KpiBreakdownEnum.INDIGO_UP_TIME && eventIndex == subKpiEvents.size() - 1) {
                    totalUptime = event.getRowDayValue();
                } else {
                    if (kpiIndex == 0) {
                        xValues += "'" + HPDateUtils.formatDate(event.getDate(), HISTOGRAM_DATE_FORMAT) + "'";
                        xValues += (eventIndex == subKpiEvents.size() - 1) ? "" : ",";
                    }

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(event.getDate());
                    int weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
                    String weekNumberString = context.getString(R.string.week_number, weekNumber);

                    data += String.format(AGGREGATE_CHART_DATA,
                            String.valueOf(eventIndex),
                            String.valueOf(event.getDayValue()),
                            subKpiEnum.getSecondaryColor(),
                            kpiName,
                            weekNumberString,
                            HPLocaleUtils.getDecimalString(event.getDayValue(), true, 1),
                            HPLocaleUtils.getLocalizedTimeString(context, event.getRowDayValue(), TimeUnit.SECONDS),
                            HPLocaleUtils.getLocalizedTimeString(context, totalUptime, TimeUnit.SECONDS)
                    );

                    data += eventIndex == subKpiEvents.size() - 1 ? "" : ",";
                }
            }

            if (subKpiEnum != KpiBreakdownEnum.INDIGO_UP_TIME) {
                kpiName = context.getString(subKpiEnum.getShortNameResource());

                series += String.format(AGGREGATE_CHART_SERIES,
                        kpiName,
                        subKpiEnum.getSecondaryColor(),
                        subKpiEnum.getKey(),
                        data);
                series += kpiIndex == 0 ? "" : ",";
                seriesIndex += 1;
            }
        }

        int creditResource = kpiBreakdownViewModel.getKpiBreakdownEnum().getCreditResource();
        String creditString = "";
        if (creditResource != -1 && !kpiBreakdownViewModel.isOverall()) {
            creditString = context.getString(creditResource);
        }

        return html.replace(SERIES_KEY, series)
                .replace(VALUES_LABEL_KEY, creditString)
                .replace(XAXIS_KEY_KEY, xValues)
                .replace(Y_AXIS_EXTRA_KEY, String.format(MAX_VALUE_HTML_TAG, kpiBreakdownViewModel.getMaxScore())
                        + String.format(TICK_AMOUNT_HTML_TAG, DEFAULT_NUMBER_OF_TICKS))
                .replace(LEGEND_EXTRA_KEY, isLandscape ? "" : LEGEND_EXTRA)
                .replace(VISIBLE_GRAPHS_KEY, visibleGraphsKeys);
    }

    public static String getTitle(Context context, KpiBreakdownViewModel viewModel) {
        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

        KpiBreakdownEnum kpiBreakdownEnum = viewModel.getKpiBreakdownEnum();
        KPIValueHandleEnum valueHandleEnum = kpiBreakdownEnum.getValueHandleEnum();
        if (viewModel.isOverall()) {
            valueHandleEnum = KPIValueHandleEnum.SCORE;
        }

        String unit = valueHandleEnum.getUnitString(unitSystem);
        String graphName = context.getString(viewModel.getKpiBreakdownEnum().getNameResource());

        if (kpiBreakdownEnum == KpiBreakdownEnum.INDIGO_LM) {
            return unit;
        } else {
            return graphName;
        }
    }
}
