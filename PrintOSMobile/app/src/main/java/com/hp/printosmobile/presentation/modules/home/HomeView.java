package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.PBNotificationDataViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

/**
 * Created by Osama Taha on 5/21/16.
 */
public interface HomeView {

    void updateProductionSnapshot(ProductionViewModel productionViewModel);

    void updateLatexProductionSnapshot(ProductionViewModel productionViewModel);

    void updateTodayPanel(TodayViewModel todayViewModel, boolean isPolling);

    void updateTodayPanelWithActiveShifts(ActiveShiftsViewModel activeShiftsViewModel);

    void updateHistogram(TodayHistogramViewModel todayHistogramViewModel);

    void updateWeekPanel(WeekCollectionViewModel weekCollectionViewModel);

    void updateRankingPanel(RankingViewModel rankingViewModel);

    void updatePrintersPanel(List<DeviceViewModel> deviceViewModels);

    void updateServiceCallPanel(ServiceCallViewModel serviceCallViewModel);

    void onError(APIException exception, String... tag);

    void onPBNotificationDataRetrieved(PBNotificationDataViewModel pbNotificationDataViewModel);

}
