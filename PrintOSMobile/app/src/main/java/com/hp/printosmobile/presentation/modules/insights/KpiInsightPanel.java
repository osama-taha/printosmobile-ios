package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 6/15/2017.
 */
public class KpiInsightPanel extends PanelView<InsightsViewModel> implements InsightsChartView.InsightsChartViewCallback {

    public static final String TAG = KpiInsightPanel.class.getName();
    private static final int TITLE_MAX_NUMBER_OF_LINES = 3;

    @Bind(R.id.large_loading_view)
    View largeLoadingView;
    @Bind(R.id.insights_chart)
    InsightsChartView insightsChartView;

    private KpiInsightPanelCallback callback;
    private boolean isDateSelected = false;

    public KpiInsightPanel(Context context) {
        super(context);
    }

    public KpiInsightPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KpiInsightPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());
        setTitleNumberOfLines(TITLE_MAX_NUMBER_OF_LINES);
        insightsChartView.attachCallback(this);
    }

    public void addCallback(KpiInsightPanelCallback callback) {
        this.callback = callback;
    }

    @Override
    public Spannable getTitleSpannable() {
        if (getViewModel() == null) {
            return new SpannableStringBuilder(getContext().getString(R.string.fragment_insights_name));
        }

        return new SpannableStringBuilder(InsightsUtils.getPanelTitle(getContext(), getViewModel()));
    }

    @Override
    public int getContentView() {
        return R.layout.kpi_insight_panel;
    }

    @Override
    public void updateViewModel(InsightsViewModel viewModel) {
        hideLoading();
        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        insightsChartView.setViewModel(viewModel);
        setTitle(getTitleSpannable());
    }

    @Override
    protected boolean isValidViewModel() {
        return isDateSelected || (!(getViewModel() == null ||
                getViewModel().getInsightModels() == null
                || getViewModel().getInsightModels().isEmpty()));
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        if (insightsChartView != null) {
            insightsChartView.onRefresh();
        }
    }

    @Override
    public void onDateSelected(Date from, Date to) {
        if (callback != null && getViewModel() != null) {
            isDateSelected = true;
            callback.onDateSelected(from, to, getViewModel().getInsightKpiEnum());
        }
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    public void onShareClicked() {
        if (callback != null) {
            callback.onShareButtonClicked(getViewModel().getInsightKpiEnum() == InsightsViewModel.InsightKpiEnum.JAM ?
                    Panel.JAM_CHART : Panel.FAILURE_CHART);
        }
    }

    @Override
    public void onCorrectiveActionsButtonClicked(final InsightsViewModel.InsightModel model) {

        BaseActivity activity = null;
        if (getContext() instanceof BaseActivity) {
            activity = (BaseActivity) getContext();
        }

        if (activity == null) {
            return;
        }

        CorrectiveActionsDialog.newInstance(model,
                new CorrectiveActionsDialog.CorrectiveActionsDialogCallback() {
                    @Override
                    public void onDialogDismiss() {

                    }

                    @Override
                    public void onScreenshotCreated(InsightsViewModel.InsightModel insightModel, Uri storageUri, final SharableObject sharableObject) {

                        if (insightModel == null || sharableObject == null) {
                            return;
                        }

                        boolean isJam = insightModel.getKpi() == InsightsViewModel.InsightKpiEnum.JAM;

                        callback.onScreenshotCreated(Panel.PERFORMANCE, storageUri,
                                getContext().getString(isJam ? R.string.corrective_actions_share_title_jam : R.string.corrective_actions_share_title_failure, HPLocaleUtils.getPercentageString(getContext(), insightModel.getPercentage())),
                                getContext().getString(isJam ? R.string.corrective_actions_share_body_jam : R.string.corrective_actions_share_body_failure, HPLocaleUtils.getPercentageString(getContext(), insightModel.getPercentage()), HPLocaleUtils.getDateRangeString(getContext(), insightModel.getDateFrom(), insightModel.getDateTo()), insightModel.getLabel()),
                                new SharableObject() {
                                    @Override
                                    public String getShareAction() {
                                        return sharableObject.getShareAction();
                                    }
                                });

                    }
                }).show(activity.getSupportFragmentManager(), "");

    }

    public void onFirstCorrectiveActionsButtonClicked() {
        if (getViewModel() != null && getViewModel().getInsightModels() != null &&
                getViewModel().getInsightModels().size() > 0) {
            for (InsightsViewModel.InsightModel viewModel : getViewModel().getInsightModels()) {
                if (viewModel.hasCorrectiveActions()) {
                    viewModel.setDateFrom(getDateFrom());
                    viewModel.setDateTo(getDateTo());
                    onCorrectiveActionsButtonClicked(viewModel);
                    return;
                }
            }
        }
    }

    public void sharePanel() {

        if (callback != null) {

            boolean isJam = false;
            String action = null;
            if (getViewModel() != null) {
                if (getViewModel().getInsightKpiEnum() == InsightsViewModel.InsightKpiEnum.JAM) {
                    action = AnswersSdk.SHARE_CONTENT_TYPE_INSIGHTS_JAMS;
                    isJam = true;
                } else if (getViewModel().getInsightKpiEnum() == InsightsViewModel.InsightKpiEnum.FAILURE) {
                    action = AnswersSdk.SHARE_CONTENT_TYPE_INSIGHTS_FAILURES;
                }
            }

            lockShareButton(true);

            shareButton.setVisibility(GONE);
            final Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {

                final String finalAction = action;
                final boolean finalIsJam = isJam;
                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS,
                        getPanelTag(), null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);
                                callback.onScreenshotCreated(finalIsJam ? Panel.JAM_CHART : Panel.FAILURE_CHART, storageUri,
                                        getContext().getString(finalIsJam ? R.string.share_jam_panel_title : R.string.share_failure_panel_title),
                                        link,
                                        new SharableObject() {
                                            @Override
                                            public String getShareAction() {
                                                return finalAction;
                                            }
                                        });
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    public Date getDateFrom() {
        if (insightsChartView != null) {
            return insightsChartView.getDateFrom();
        }
        return Calendar.getInstance().getTime();
    }

    public Date getDateTo() {
        if (insightsChartView != null) {
            return insightsChartView.getDateTo();
        }
        return Calendar.getInstance().getTime();
    }

    public void resetDateFilter() {
        if (insightsChartView != null) {
            insightsChartView.resetDateFilter();
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        largeLoadingView.setVisibility(GONE);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        largeLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public String getPanelTag() {
        if (getViewModel() == null) {
            return TAG;
        }

        return getViewModel().getInsightKpiEnum() == InsightsViewModel.InsightKpiEnum.JAM ?
                Panel.JAM_CHART.getDeepLinkTag() : Panel.FAILURE_CHART.getDeepLinkTag();
    }

    @Override
    protected boolean isPanelRounded() {
        return true;
    }

    public View getViewForTargetView(TargetViewManager.TargetView targetView) {
        if (targetView == null) {
            return null;
        }

        return insightsChartView.getViewForTargetView(targetView);
    }

    public interface KpiInsightPanelCallback extends PanelShareCallbacks {
        void onDateSelected(Date from, Date to, InsightsViewModel.InsightKpiEnum kpiEnum);
    }
}
