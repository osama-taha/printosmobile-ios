package com.hp.printosmobile.presentation.modules.reports;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;

import java.io.Serializable;

/**
 * A Model holds information that is used for getting a specific report based on different Criteries.
 *
 * @Author Osama Taha
 * Created on 6/19/2015.
 */
public class ReportFilter implements Serializable {

    private ReportKpiViewModel kpi;
    private Resolution resolution;
    private Unit unit;
    private String dateFormat;
    private int offSet;
    private BusinessUnitViewModel businessUnit;

    public ReportKpiViewModel getKpi() {
        return kpi;
    }

    public void setKpi(ReportKpiViewModel kpi) {
        this.kpi = kpi;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public int getOffSet() {
        return offSet;
    }

    public void setOffset(int offSet) {
        this.offSet = offSet;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public BusinessUnitViewModel getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnitViewModel businessUnit) {
        this.businessUnit = businessUnit;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportFilter{");
        sb.append("kpi=").append(kpi);
        sb.append(", resolution=").append(resolution);
        sb.append(", unit=").append(unit);
        sb.append(", dateFormat='").append(dateFormat).append('\'');
        sb.append(", offSet=").append(offSet);
        sb.append(", businessUnit=").append(businessUnit);
        sb.append('}');
        return sb.toString();
    }

    /**
     * Represents the report x-values type.
     */
    public enum Resolution {

        DAY("Day"), WEEK("Week"), MONTH("Month"), YEAR("Year");

        private final String value;

        Resolution(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * Holds list of units to be used for filtering the chart.
     */
    public enum Unit {

        SHEETS("Sheets");
        private final String value;

        Unit(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
