package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 1/17/2017.
 */
public class NotificationsPresenter extends Presenter<NotificationsSubMenuView> {

    private static final String TAG = NotificationsPresenter.class.getName();

    private boolean wasToastShown = false;

    public void getAvailableSubscriptions() {
        HPLogger.d(TAG, "get Supported Subscriptions");
        Subscription subscription = NotificationsManager.getAvailableEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null && e instanceof APIException) {
                            mView.onFailedToGetSubscription((APIException) e);
                        }
                    }

                    @Override
                    public void onNext(List<String> events) {
                        HPLogger.d(TAG, "Supported subscriptions successfully retrieved");
                        if (mView != null) {
                            mView.setSupportedSubscriptions(events);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getUserSubscriptions() {
        HPLogger.d(TAG, "get User Subscriptions");
        Subscription subscription = NotificationsManager.getUserSubscriptions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<SubscriptionEvent>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to get the apps " + e);
                        if (mView != null && e instanceof APIException) {
                            mView.onFailedToGetSubscription((APIException) e);
                        }
                    }

                    @Override
                    public void onNext(List<SubscriptionEvent> events) {
                        HPLogger.d(TAG, "Subscriptions successfully retrieved");
                        if (mView != null) {
                            mView.setSubscriptions(events);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void subscribeEvent(final SubscriptionEvent subscriptionEvent) {

        if (subscriptionEvent == null || subscriptionEvent.getNotificationEventEnum() == null) {
            return;
        }

        final String eventDescName = subscriptionEvent.getNotificationEventEnum().getKey();

        HPLogger.d(TAG, "subscribing to event: " + eventDescName);

        if(subscriptionEvent.isBrowser() || subscriptionEvent.isEmail()) {
            //turn on mobile only
            setSubscriptionDestination(subscriptionEvent.getUserSubscriptionID());
            return;
        }

        Subscription subscription = NotificationsManager.subscribeEvent(eventDescName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SubscriptionEvent>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to subscribe an event description: " + eventDescName + " due to " + e);
                    }

                    @Override
                    public void onNext(SubscriptionEvent event) {
                        HPLogger.d(TAG, "successfully subscribed to even: " + eventDescName);
                        if (mView != null) {
                            mView.onSubscribed(event);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void setSubscriptionDestination(final String subscriptionID) {
        HPLogger.d(TAG, "setting subscription destination with id: " + subscriptionID);

        Subscription subscription = NotificationsManager.setSubscriptionDestination(subscriptionID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (!wasToastShown) {
                            wasToastShown = true;
                            HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                                    PrintOSApplication.getAppContext().getString(R.string.msg_failed_to_save_notifications_settings)
                            );
                        }
                    }

                    @Override
                    public void onNext(ResponseBody events) {
                        HPLogger.d(TAG, "successfully set subscription destination for id: " + subscriptionID);
                    }
                });
        addSubscriber(subscription);
    }

    public void unsubscribeToEvent(final SubscriptionEvent subscriptionEvent) {

        if (subscriptionEvent == null || subscriptionEvent.getUserSubscriptionID() == null) {
            return;
        }

        HPLogger.d(TAG, "un-subscribed to event with userID "
                + subscriptionEvent.getUserSubscriptionID());


        if(subscriptionEvent.isBrowser() || subscriptionEvent.isEmail()) {
            //turn off mobile only
            setSubscriptionDestination(subscriptionEvent.getUserSubscriptionID());
            return;
        }

        Subscription subscription = NotificationsManager.unsubscribeEvent(subscriptionEvent.getUserSubscriptionID())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to unsubscribe an event with userID "
                                + subscriptionEvent.getUserSubscriptionID()
                                + " due to " + e);

                        if (!wasToastShown) {
                            wasToastShown = true;
                            HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                                    PrintOSApplication.getAppContext().getString(R.string.msg_failed_to_save_notifications_settings)
                            );
                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        HPLogger.d(TAG, "Successfully un-subscribed to event with userID "
                                + subscriptionEvent.getUserSubscriptionID());
                    }
                });
        addSubscriber(subscription);
    }

    public void setWasToastShown(boolean wasToastShown) {
        this.wasToastShown = wasToastShown;
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}