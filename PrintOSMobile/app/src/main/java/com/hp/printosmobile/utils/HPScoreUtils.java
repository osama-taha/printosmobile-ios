package com.hp.printosmobile.utils;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.BusinessUnitThresholds;

/**
 * Created by Anwar Asbah on 5/22/2016.
 */
public class HPScoreUtils {

    private HPScoreUtils() {
    }

    public static int getProgressColor(Context context, BusinessUnitEnum businessUnitEnum, double score, double target, boolean isTrack) {

        BusinessUnitThresholds thresholds = PrintOSPreferences.getInstance(context).getThresholds(businessUnitEnum);

        double percentage;
        if (target == 0) {
            percentage = score == 0 ? 100 : score;
        } else {
            percentage = score / target;
        }

        if (percentage <= ((float) thresholds.getFailPercent() / 100f)) {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_red : R.color.bar_red, null);
        } else if (percentage <= ((float) thresholds.getAveragePercent() / 100f)) {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_orange : R.color.bar_orange, null);
        } else {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_green : R.color.bar_green, null);
        }
    }
}
