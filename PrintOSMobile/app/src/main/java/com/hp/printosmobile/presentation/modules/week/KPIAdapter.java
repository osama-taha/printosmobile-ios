package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.kpiview.KPIView;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anwar Asbah on 5/15/2016.
 */
public class KPIAdapter extends RecyclerView.Adapter<KPIAdapter.KpiViewHolder> {

    private Context context;
    private List<KPIViewModel> kpiList;
    private BusinessUnitEnum businessUnitEnum;
    private int maxKpiScoreTextViewWidth;
    private KPICallbacks listener;
    private SynchronizedClickListener synchronizedClickListener;

    public KPIAdapter(List<KPIViewModel> kpiList, Context context, KPICallbacks listener,
                      BusinessUnitEnum businessUnitEnum) {
        this.kpiList = kpiList;
        this.context = context;
        this.listener = listener;
        this.maxKpiScoreTextViewWidth = getMaxKpiScoreTextViewWidth();
        this.businessUnitEnum = businessUnitEnum;
        this.synchronizedClickListener = new SynchronizedClickListener();

    }

    private int getMaxKpiScoreTextViewWidth() {
        int maxWidth = 0;
        KPIView view = new KPIView(context);
        for (KPIViewModel model : kpiList) {
            view.setViewModel(model);
            maxWidth = Math.max(maxWidth, view.getKpiScoreTextViewWidth());
        }
        return maxWidth + HPUIUtils.dpToPx(context, 3); //must be bigger than the exact actual size
    }

    @Override
    public KpiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new KpiViewHolder(new KPIView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(final KpiViewHolder holder, final int position) {
        holder.kpiItem.setViewModel(kpiList.get(position));
        holder.kpiItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (synchronizedClickListener != null) {
                    synchronizedClickListener.onKpiClicked(position);
                }
            }
        });

        if (PrintOSPreferences.getInstance(context).isKpiExplanationEnabled() &&
                (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER || businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS)) {
            holder.kpiItem.addOnScoreClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (synchronizedClickListener != null) {
                                synchronizedClickListener.onScoreClicked(position);
                            }
                        }
                    });
        }

        //Hide SeparatorView for last KPI.
        holder.kpiItem.hideSeparatorView(position == kpiList.size() - 1);

    }

    @Override
    public int getItemCount() {
        if (kpiList != null) {
            return kpiList.size();
        }
        return 0;
    }

    public class KpiViewHolder extends RecyclerView.ViewHolder {
        KPIView kpiItem;

        public KpiViewHolder(KPIView itemView) {
            super(itemView);
            kpiItem = itemView;
            kpiItem.setKpiScoreTextViewWidth(maxKpiScoreTextViewWidth);
        }
    }

    public interface KPICallbacks {
        void onKpiSelected(String s);

        void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position);
    }

    public class SynchronizedClickListener {
        private static final long DELAY = 1000;
        boolean enabled = true;

        public void onKpiClicked(int position) {
            if (listener != null && enabled) {
                tempDisabling();
                listener.onKpiSelected(kpiList.get(position).getName());
            }
        }

        public void onScoreClicked(int position) {
            if (listener != null && enabled) {
                tempDisabling();
                listener.showKpiExplanationDialog((ArrayList<KPIViewModel>) kpiList, position);
            }
        }

        private void tempDisabling() {
            enabled = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    enabled = true;
                }
            }, DELAY);
        }

    }
}