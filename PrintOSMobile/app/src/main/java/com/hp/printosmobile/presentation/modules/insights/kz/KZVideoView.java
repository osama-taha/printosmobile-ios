package com.hp.printosmobile.presentation.modules.insights.kz;

import android.app.Activity;
import android.content.Context;
import android.text.Spannable;
import android.text.TextUtils;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZFooter;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;

public class KZVideoView extends KZBaseCardView {

    public KZVideoView(Context context) {
        super(context);
    }

    public KZVideoView(Activity context, String selectedBu) {
        super(context);
        this.selectedBu = selectedBu;
        this.activity = context;
    }

    @Override
    public int getContentView() {
        return R.layout.kz_item_video_card;
    }

    @Override
    public void updateViewModel(KZItem viewModel) {
        setViewModel(viewModel);
        setUpCard(viewModel);
    }

    @Override
    public void bindViews() {

    }

}
