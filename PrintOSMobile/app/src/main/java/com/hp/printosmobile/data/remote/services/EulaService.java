package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.AcceptEulaRequestBody;
import com.hp.printosmobile.data.remote.models.AcceptPrivacyVersionRequestBody;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Retrofit REST interface definition for EULA
 *
 * @author Anwar Asbah
 */
public interface EulaService {

    /**
     * get EULA content
     */
    @GET(ApiConstants.EULA_URL)
    @Headers("Accept: text/html")
    Observable<Response<ResponseBody>> getEULA(@Path("language") String language);

    @PUT(ApiConstants.EULA_ACCEPT_URL)
    Observable<Response<ResponseBody>> acceptEula(@Body AcceptEulaRequestBody eulaInput);

    @PUT(ApiConstants.PRIVACY_ACCEPT_URL)
    Observable<Response<ResponseBody>> acceptPrivacy(@Body AcceptPrivacyVersionRequestBody privacyVersion);
}
