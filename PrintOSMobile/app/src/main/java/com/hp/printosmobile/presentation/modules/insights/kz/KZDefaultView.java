package com.hp.printosmobile.presentation.modules.insights.kz;

import android.content.Context;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;

/**
 * Created by Osama Taha
 */
public class KZDefaultView extends KZBaseCardView {

    public KZDefaultView(Context context) {
        super(context);
    }

    public KZDefaultView(Context context, String selectedBu) {
        super(context);
        this.selectedBu = selectedBu;
    }


    @Override
    public int getContentView() {
        return R.layout.kz_item_card;
    }

    @Override
    public void updateViewModel(KZItem viewModel) {
        setViewModel(viewModel);
        setUpCard(viewModel);
    }

    @Override
    public void bindViews() {

    }

}
