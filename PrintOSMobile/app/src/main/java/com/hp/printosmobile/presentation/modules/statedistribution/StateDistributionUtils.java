package com.hp.printosmobile.presentation.modules.statedistribution;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anwar Asbah on 2/18/2018.
 */

public class StateDistributionUtils {

    private static final String TAG = StateDistributionUtils.class.getSimpleName();
    private static final String HTML_PORTRAIT_FILE_NAME = "pie_chart.html";
    private static final String HTML_LANDSCAPE_FILE_NAME = "state_diagram.html";

    private static final String DATE_STRING_FORMAT = "MMM d, yyyy";
    private static final String DATE_TIME_LANDSCAPE_FORMAT = "h:mm aa";

    private static final String SERIES_KEY = "@SERIES@";
    private static final String MAX_VALUE_KEY = "@MAXYAXIS@";
    private static final String START_DATE_KEY = "@STARTDATE@";
    private static final String LOCALE_KEY = "@LOCALE@";

    private static final String SERIES_ITEM = "{name: '%1$s', y: %2$s,color: '%3$s',tooltipData: '%4$s',tag: '%5$s'},";
    private static final String TOOLTIP_FORMAT = "<div>" +
            "<span style=\"color:%1$s; font-weight: bold \">" +
            "%2$s<br></div>" +
            "<hr><div>%3$s<br><b>%4$s</div>";
    private static final String SERIES_CONTENT = "{name: 'SPACER',showInLegend: false,data: [%s],color: '#FFFFFF'},";
    private static final String TOOLTIP_DATA_FORMAT = "<div style=\" font-size: 1.4em\">" +
            "<span style=\"color:%1$s; font-weight: bold\">" +
            "%2$s</span> %3$s<br></div><hr>" +
            "<div style=\" font-size: 1.3em\">%4$s <b>%5$s</b></div>" +
            "<div style=\" font-size: 1.3em\">%6$s <b>%7$s</b></div>" +
            "<hr><div style=\" font-size: 1em\">%8$s</div>";

    private static final String SERIES_ITEM_CONTENT = "{name: '%1$s', className: '%2$s',linkedTo: '%3$s',data: [{x: 0,y: %4$s, tooltipdata: '%5$s'}],color: '%6$s',tag:'%7$s'},";
    private static final String LEGEND_FORMAT = "{name: '%1$s', className: '%2$s',id: '%3$s',data: [%4$s],color: '%5$s',tag:'%6$s'},";
    private static final String TIME_SINCE_1970 = "1970-01-01 00:00:00";
    private static final String STATE_PREFIX = "series_state_";
    private static final String LEGEND_LANDSCAPE_TAG_FORMAT = "%1$s <b>(%2$s%%)</b>";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static void displayPortraitGraph(Context context, WebView progressWebView, StateDistributionViewModel stateDistributionViewModel,
                                            TextView disconnectedTextView) {
        if (stateDistributionViewModel == null) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            progressWebView.setWebContentsDebuggingEnabled(true);
        }

        try {

            String locale = PrintOSPreferences.getInstance(context).getLanguage(context.getString(R.string.default_language));

            String detail = FileUtils.loadAssestFile(context, HTML_PORTRAIT_FILE_NAME);
            if (disconnectedTextView != null) {
                detail = setPortraitSeriesData(context, detail, stateDistributionViewModel, disconnectedTextView);
                if (detail != null) {
                    detail = detail.replace(LOCALE_KEY, locale);
                }
            }

            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (IOException e) {
            HPLogger.e(TAG, "error displaying portrait state distribution graph: " + e.getMessage());
        }
    }

    private static String setPortraitSeriesData(Context context, String html, StateDistributionViewModel stateDistributionViewModel,
                                                TextView disconnectedTextView) {

        if (stateDistributionViewModel == null || html == null || stateDistributionViewModel.getAggregateStateMap() == null
                || stateDistributionViewModel.getPressStates() == null || stateDistributionViewModel.getPressStates().isEmpty()) {
            return "";
        }

        StateDistributionViewModel.PressState firstState = stateDistributionViewModel.getPressStates().get(0);
        StateDistributionViewModel.PressState lastState = stateDistributionViewModel.getPressStates().get(
                stateDistributionViewModel.getPressStates().size() - 1);

        if (firstState == null || lastState == null) {
            return null;
        }

        long total = firstState != lastState
                ? (firstState.getStartDate().getTime() - lastState.getStartDate().getTime())
                : (firstState.getEndDate().getTime() - lastState.getStartDate().getTime());
        total = Math.abs(total);

        Map<DeviceState, Float> aggregateMap = stateDistributionViewModel.getAggregateStateMap();

        String seriesData = "";
        for (DeviceState state : aggregateMap.keySet()) {

            float yValue = total * aggregateMap.get(state) / 100.0f;

            if (state == DeviceState.DISCONNECTED) {
                if ((int) yValue != 0) {
                    disconnectedTextView.setText(context.getString(R.string.state_distribution_excluding_disconnected_time,
                            HPLocaleUtils.getLocalizedTimeString(context, (int) (yValue / 1000f), TimeUnit.SECONDS)));
                }
                continue;
            }

            String tooltipData = String.format(TOOLTIP_FORMAT,
                    state.getStateDistributionColor(),
                    context.getString(state.getDisplayNameResourceId()),
                    context.getString(R.string.state_distribution_tooltip_total_time_spent),
                    HPLocaleUtils.getLocalizedSecondsString(context, (int) (yValue / 1000f)));

            String item = String.format(SERIES_ITEM,
                    context.getString(state.getDisplayNameResourceId()),
                    yValue,
                    state.getStateDistributionColor(),
                    tooltipData,
                    state.getState());

            seriesData += item;

        }

        return html.replace(SERIES_KEY, seriesData);
    }

    public static void displayLandscapeView(Context context, WebView progressWebView, StateDistributionViewModel stateDistributionViewModel) {
        if (stateDistributionViewModel == null || stateDistributionViewModel.getPressStates() == null ||
                stateDistributionViewModel.getPressStates().isEmpty()) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            progressWebView.setWebContentsDebuggingEnabled(true);
        }

        try {
            String detail = FileUtils.loadAssestFile(context, HTML_LANDSCAPE_FILE_NAME);

            String seriesContent = "";
            long total;

            List<StateDistributionViewModel.PressState> statesList = stateDistributionViewModel.getPressStates();
            Date startOfTime = HPDateUtils.parseDate(TIME_SINCE_1970, DATE_FORMAT);
            Date firstStamp = statesList.get(0).getStartDate();
            total = firstStamp.getTime() - startOfTime.getTime();
            String StartDateString = String.valueOf(total);
            seriesContent += String.format(SERIES_CONTENT, StartDateString);

            for (int i = 0; i < statesList.size(); i++) {
                StateDistributionViewModel.PressState currentPressState = statesList.get(i);
                Date endTimeStamp = currentPressState.getEndDate();
                Date startTimeStamp = currentPressState.getStartDate();
                long time = endTimeStamp.getTime() - startTimeStamp.getTime();
                total += time;

                DeviceState deviceState = currentPressState.getDeviceState();
                String stateKey = deviceState.getState();
                String stateName = context.getString(deviceState.getDisplayNameResourceId());

                String tooltipData = String.format(TOOLTIP_DATA_FORMAT,
                        deviceState.getStateDistributionColor(),
                        stateName,
                        HPDateUtils.formatDate(startTimeStamp, DATE_STRING_FORMAT),
                        context.getString(R.string.state_distribution_start_time_tag),
                        HPDateUtils.formatDate(startTimeStamp, DATE_TIME_LANDSCAPE_FORMAT),
                        context.getString(R.string.state_distribution_end_time_tag),
                        HPDateUtils.formatDate(endTimeStamp, DATE_TIME_LANDSCAPE_FORMAT),
                        context.getString(R.string.site_time_is_displayed));

                String seriesItem = String.format(SERIES_ITEM_CONTENT,
                        stateName,
                        STATE_PREFIX + deviceState.getState(),
                        stateName,
                        String.valueOf(time),
                        tooltipData,
                        deviceState.getStateDistributionColor(),
                        stateKey);

                seriesContent = seriesItem + seriesContent;//reversed order

                //this is a must for the legend to show right:
                if (i == statesList.size() - 1) {
                    for (DeviceState key : stateDistributionViewModel.getAggregateStateMap().keySet()) {
                        seriesContent = getFakeObjForLegend(context, key, stateDistributionViewModel.getAggregateStateMap().get(key)) + seriesContent;
                    }
                }
            }
            detail = detail.replace(MAX_VALUE_KEY, String.valueOf(total))
                    .replace(START_DATE_KEY, String.valueOf(firstStamp.getTime() - startOfTime.getTime()))
                    .replace(SERIES_KEY, seriesContent);


            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (Exception e) {
            HPLogger.e(TAG, "error displaying landscape state distribution graph:" + e.toString());
        }
    }

    public static boolean isPressDisconnected(StateDistributionViewModel viewModel) {
        float total = 0;
        for (DeviceState state : viewModel.getAggregateStateMap().keySet()) {
            if (state != DeviceState.DISCONNECTED) {
                Float val = viewModel.getAggregateStateMap().get(state);
                total += val == null ? 0 : val;
            }
        }
        return total == 0;
    }

    public static String getNoDataMsg(StateDistributionViewModel viewModel) {

        String timeString = "-";

        if (viewModel != null && viewModel.getAggregateStateMap() != null &&
                !viewModel.getAggregateStateMap().isEmpty() &&
                viewModel.getAggregateStateMap().containsKey(DeviceState.DISCONNECTED)) {

            StateDistributionViewModel.PressState firstState = viewModel.getPressStates().get(0);
            StateDistributionViewModel.PressState lastState = viewModel.getPressStates().get(
                    viewModel.getPressStates().size() - 1);

            if (firstState != null && lastState != null) {
                long total = firstState != lastState
                        ? (firstState.getStartDate().getTime() - lastState.getStartDate().getTime())
                        : (firstState.getEndDate().getTime() - lastState.getStartDate().getTime());
                total = Math.abs(total);

                float yValue = total * viewModel.getAggregateStateMap().get(DeviceState.DISCONNECTED) / 100.0f;

                timeString = HPLocaleUtils.getLocalizedTimeString(PrintOSApplication.getAppContext(),
                        (int) (yValue / 1000f), TimeUnit.SECONDS);
            }
        }

        return PrintOSApplication.getAppContext().getString(HomePresenter.isShiftSupport() ?
                        R.string.state_distribution_shift_no_data_msg
                        : R.string.state_distribution_no_data_msg,
                timeString);
    }

    private static String getFakeObjForLegend(Context context, DeviceState state, float percent) {
        return String.format(LEGEND_FORMAT,
                String.format(LEGEND_LANDSCAPE_TAG_FORMAT, context.getString(state.getDisplayNameResourceId()), HPLocaleUtils.getDecimalString(percent, true, 1)),
                STATE_PREFIX + state.getState(),
                context.getString(state.getDisplayNameResourceId()),
                String.valueOf(0),
                state.getStateDistributionColor(),
                state.getState());
    }
}
