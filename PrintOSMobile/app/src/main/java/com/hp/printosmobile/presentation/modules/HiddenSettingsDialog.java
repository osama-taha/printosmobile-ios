package com.hp.printosmobile.presentation.modules;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Minerva on 3/14/2018.
 */

public class HiddenSettingsDialog extends DialogFragment {

    public static final String TAG = HiddenSettingsDialog.class.getSimpleName();

    private HiddenSettingsDialogCallback callback;


    @Bind(R.id.switch_location)
    TextView switchLocation;

    public static com.hp.printosmobile.presentation.modules.HiddenSettingsDialog getInstance(HiddenSettingsDialogCallback callback) {
        HiddenSettingsDialog hiddenSettingsDialog = new HiddenSettingsDialog();
        hiddenSettingsDialog.callback = callback;
        return hiddenSettingsDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.dialog_hidden_settings, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isSelectServerEnabled()) {
            HPUIUtils.setVisibilityForViews(true, switchLocation);
            switchLocation.setText(PrintOSApplication.getAppContext().getString(R.string.switch_location));
        }

        setCancelable(true);
    }

    @OnClick(R.id.switch_location)
    public void onSwitchLocationButtonClicked() {
        HPLogger.d(TAG, "switch location button clicked");

        dismissAllowingStateLoss();
        if (callback != null) {
            callback.onSwitchLocationClicked();
        }
    }

    @OnClick(R.id.ignore_flags)
    public void onIgnoreFlagsButtonClicked() {
        HPLogger.d(TAG, "ignore flags button clicked");

        dismissAllowingStateLoss();
        if (callback != null) {
            callback.onIgnoreFlagsClicked();
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
    }

    public interface HiddenSettingsDialogCallback {
        void onIgnoreFlagsClicked();

        void onSwitchLocationClicked();
    }
}
