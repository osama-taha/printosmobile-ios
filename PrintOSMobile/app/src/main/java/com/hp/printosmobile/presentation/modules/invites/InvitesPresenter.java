package com.hp.printosmobile.presentation.modules.invites;

import android.content.Context;
import android.text.TextUtils;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.InvitesListData;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/20/2017.
 */
public class InvitesPresenter extends Presenter<InvitesView> {

    private static final String TAG = InvitesPresenter.class.getName();

    public void getInvitesList() {
        Subscription subscription = InvitesDataManager.getInvites()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<InvitesViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Failed to retrieve contact list " + e.getMessage());
                        InviteUtils.setInvitesViewModel(null);
                    }

                    @Override
                    public void onNext(InvitesViewModel invitesViewModel) {
                        InviteUtils.setInvitesViewModel(invitesViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    public void getInviteUserRoles() {

        Subscription subscription = InvitesDataManager.getInviteUserRoles()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<InviteUserRoleViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Failed to get Invite user roles " + e.getMessage());
                        if (mView != null) {
                            mView.errorRetrievingUserRoles();
                        }
                    }

                    @Override
                    public void onNext(List<InviteUserRoleViewModel> inviteUserRoleViewModels) {
                        if (mView != null) {
                            mView.onUserRolesRetrieved(inviteUserRoleViewModels);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void inviteContact(final InviteContactViewModel contact, final InvitesListFragment.InvitesType type,
                              final boolean resend, final boolean isAll) {

        final PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        InviteUserRoleViewModel model = preferences.getSelectedInviteUserRole();

        if (model == null && !resend) {
            Subscription subscription = InvitesDataManager.getInviteUserRoles()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<List<InviteUserRoleViewModel>>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            HPLogger.d(TAG, "Error invited contact: failure retrieving user roles");
                            APIException apiException = APIException.create(APIException.Kind.UNEXPECTED);
                            if (e instanceof APIException) {
                                apiException = (APIException) e;
                            }
                            InviteUtils.errorInvitingContact(contact, apiException, resend);
                        }

                        @Override
                        public void onNext(List<InviteUserRoleViewModel> inviteUserRoleViewModels) {
                            InviteUserRoleViewModel selectedModel = preferences.getSelectedInviteUserRole();
                            if (selectedModel == null) {
                                HPLogger.d(TAG, "Failed to invited contact: user roles missing");
                                InviteUtils.errorInvitingContact(contact, APIException.create(APIException.Kind.UNEXPECTED), resend);
                            } else {
                                inviteContact(contact, type, selectedModel, resend, isAll);
                            }
                        }
                    });
            addSubscriber(subscription);
        } else {
            inviteContact(contact, type, model, resend, isAll);
        }
    }

    private void inviteContact(final InviteContactViewModel contact, final InvitesListFragment.InvitesType invitesType,
                               InviteUserRoleViewModel model, final boolean resend, final boolean isAll) {
        Subscription subscription = InvitesDataManager.invite(contact, model, resend)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<InvitesListData.Invite>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Failed to invited contact: " + e.getMessage());
                        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                        if (e instanceof APIException) {
                            exception = (APIException) e;
                        }
                        InviteUtils.errorInvitingContact(contact, exception, resend);
                    }

                    @Override
                    public void onNext(InvitesListData.Invite invite) {
                        if (invite == null || invite.getId() == null) {
                            InviteUtils.errorInvitingContact(contact, APIException.create(APIException.Kind.UNEXPECTED), resend);
                            return;
                        }

                        contact.setInviteId(invite.getId());
                        contact.setInviteStatus(InviteContactViewModel.InviteStatus.from(invite.getStatus()));

                        contact.setRevokeEnabled(false);
                        contact.setResendEnabled(false);
                        contact.setLanguage(invite.getLanguageCode());
                        contact.setRoleId(invite.getRoleId());
                        contact.setRoleType(invite.getRoleType());
                        if (invite.getLinks() != null) {
                            contact.setRevokeEnabled(invite.getLinks().getRevoke() != null && invite.getLinks().getRevoke().getHref() != null);
                            contact.setResendEnabled(invite.getLinks().getResend() != null && invite.getLinks().getResend().getHref() != null);
                        }

                        InviteUtils.successfullyInvitedContact(contact, invitesType, resend, isAll);
                        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesCoinEnabled()) {
                            sendInviteUserID(invite.getId());
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void sendInviteUserID(final String id) {

        if (TextUtils.isEmpty(id) || PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .isLatexOnly()) {
            return;
        }

        Subscription subscription = InvitesDataManager.sendInviteUserID(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "sendInviteUserID " + id + " error: " + e.getMessage());
                    }

                    @Override
                    public void onNext(String s) {
                        HPLogger.d(TAG, "sendInviteUserID success " + id);
                    }
                });
        addSubscriber(subscription);
    }

    public void revokeContact(final InviteContactViewModel inviteContactViewModel) {

        if (inviteContactViewModel == null || inviteContactViewModel.getInviteId() == null) {
            APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
            InviteUtils.errorRevokingInvite(inviteContactViewModel, exception);
            return;
        }
        Subscription subscription = InvitesDataManager.revokeInvite(inviteContactViewModel.getInviteId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                        if (e instanceof APIException) {
                            exception = (APIException) e;
                        }
                        InviteUtils.errorRevokingInvite(inviteContactViewModel, exception);
                    }

                    @Override
                    public void onNext(String s) {
                        InviteUtils.successfullyRevokedInvite(inviteContactViewModel);
                    }
                });

        addSubscriber(subscription);
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}