package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPEditText;

import java.util.Map;

import butterknife.Bind;

/**
 * Created by Osama
 */
public class UserAddressInfoFragment extends BaseFragment implements IWizardFragment {

    public static final String TAG = UserAddressInfoFragment.class.getSimpleName();
    private static final String KEY_DATA = "extra";

    private static final String CANADA_COUNTRY_CODE = "CA";
    private static final String USA_COUNTRY_CODE = "US";
    private static final String CANADA_POSTAL_CODE_REGEX = "^(?i)[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]( )?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$";
    private static final String USA_POSTAL_CODE_REGEX = "^[0-9]{5}(-[0-9]{4})?$";
    private static final String CANADA_REGION_REGEX = "^(?i)AB|BC|MB|N[BLSTU]|ON|PE|QC|SK|YT$";
    private static final String USA_REGION_REGEX = "^(?i)A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]$";
    private static final String ADDRESS_REGEX = "^[\\p{L}\\p{N}\\p{Z}\\s\\p{P}\\p{S}-]+$";
    private static final int ADDRESS_MAX_LENGTH = 100;

    @Bind(R.id.user_address_info_address1)
    HPEditText address1EditText;
    @Bind(R.id.user_address_info_address2)
    HPEditText address2EditText;
    @Bind(R.id.user_address_info_region)
    HPEditText regionEditText;
    @Bind(R.id.user_address_info_city)
    HPEditText cityEditText;
    @Bind(R.id.user_address_info_postal_code)
    HPEditText postalCodeEditText;
    @Bind(R.id.countries_spinner)
    ContactHpTextField countriesSpinner;

    private WizardViewModel wizardViewModel;
    private Map<String, String> countriesMap;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

    }

    public static UserAddressInfoFragment getInstance(WizardViewModel wizardViewModel) {

        UserAddressInfoFragment userAddressInfoFragment = new UserAddressInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, wizardViewModel);
        userAddressInfoFragment.setArguments(args);

        return userAddressInfoFragment;
    }

    private void initView() {

        wizardViewModel = (WizardViewModel) getArguments().getSerializable(KEY_DATA);

        address1EditText.setText(wizardViewModel.getAddress1());
        address2EditText.setText(wizardViewModel.getAddress2());
        regionEditText.setText(wizardViewModel.getRegion());
        cityEditText.setText(wizardViewModel.getCity());
        postalCodeEditText.setText(wizardViewModel.getPostalCode());

        countriesMap = UserProfileManager.getInstance().getCountriesMap();

        countriesSpinner.setAutoCompleteList(countriesMap);
        countriesSpinner.setTextFieldRightDrawable(ContextCompat.getDrawable(getContext(), com.hp.printosmobilelib.ui.R.drawable.cancel_light));
        countriesSpinner.getAutoCompleteTextView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String text = countriesSpinner.getText();
                    if (countriesMap == null || !countriesMap.containsKey(text)) {
                        countriesSpinner.getAutoCompleteTextView().setText("");
                    }
                }
            }
        });

        if (!TextUtils.isEmpty(wizardViewModel.getCountry())) {
            String country = countriesMap != null && countriesMap.containsKey(wizardViewModel.getCountry())
                    ? countriesMap.get(wizardViewModel.getCountry()) : "";
            countriesSpinner.getAutoCompleteTextView().setText(country);
        }

        ((NestedScrollView) getView()).getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
            }

        });

        countriesSpinner.getAutoCompleteTextView().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    private void hideKeyboard() {
        countriesSpinner.getAutoCompleteTextView().clearFocus();
        HPUIUtils.hideSoftKeyboard(getActivity(), address1EditText, address2EditText, regionEditText,
                postalCodeEditText, cityEditText, countriesSpinner.getAutoCompleteTextView());

    }

    @Override
    public boolean isValid() {

        hideKeyboard();

        Context context = PrintOSApplication.getAppContext();

        String country = countriesSpinner.getText();

        if (TextUtils.isEmpty(country)) {
            HPUIUtils.displayToast(context, context.getString(R.string.error_message_country_field_required));
            return false;
        }

        String address1 = address1EditText.getText().toString();
        if (!TextUtils.isEmpty(address1) && !isValidAddress(address1)) {
            HPUIUtils.displayToast(context, getString(R.string.address_wizard_invalid_address1));
            address1EditText.requestFocus();
            return false;
        }

        String address2 = address2EditText.getText().toString();
        if (!TextUtils.isEmpty(address2) && !isValidAddress(address2)) {
            HPUIUtils.displayToast(context, getString(R.string.address_wizard_invalid_address2));
            address2EditText.requestFocus();
            return false;
        }

        String region = regionEditText.getText().toString();
        if (!TextUtils.isEmpty(region) && !isValidRegionForCountry(country, region)) {
            HPUIUtils.displayToast(context, getString(R.string.address_wizard_invalid_region));
            regionEditText.requestFocus();
            return false;
        }

        String postalCode = postalCodeEditText.getText().toString();
        if (!TextUtils.isEmpty(postalCode) && !isValidPostalCode(country, postalCode)) {
            HPUIUtils.displayToast(context, getString(R.string.address_wizard_invalid_postal_code));
            postalCodeEditText.requestFocus();
            return false;
        }

        if (wizardViewModel != null) {
            wizardViewModel.setAddress1(address1EditText.getText().toString());
            wizardViewModel.setAddress2(address2EditText.getText().toString());
            wizardViewModel.setCity(cityEditText.getText().toString());
            wizardViewModel.setRegion(regionEditText.getText().toString());
            wizardViewModel.setPostalCode(postalCodeEditText.getText().toString());
            wizardViewModel.setCountry(country);
        }

        return true;
    }

    private static boolean isValidPostalCode(String countryCode, String code) {

        if (countryCode == null) {
            return false;
        }

        if (countryCode.equalsIgnoreCase(USA_COUNTRY_CODE)) {
            return code.matches(USA_POSTAL_CODE_REGEX);
        } else if (countryCode.equalsIgnoreCase(CANADA_COUNTRY_CODE)) {
            return code.matches(CANADA_POSTAL_CODE_REGEX);
        }

        return code.length() <= ADDRESS_MAX_LENGTH;
    }


    private static boolean isValidRegionForCountry(String countryCode, String region) {
        if (countryCode == null) {
            return false;
        }

        if (countryCode.equalsIgnoreCase(USA_COUNTRY_CODE)) {
            return region.matches(USA_REGION_REGEX);
        } else if (countryCode.equalsIgnoreCase(CANADA_COUNTRY_CODE)) {
            return region.matches(CANADA_REGION_REGEX);
        }

        return region.length() <= ADDRESS_MAX_LENGTH;
    }

    private static boolean isValidAddress(String address) {
        return address.matches(ADDRESS_REGEX) && address.length() <= ADDRESS_MAX_LENGTH;
    }

    @Override
    public String getFragmentName() {
        return "Wizard_Address_info";
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.wizard_address_info_page;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public void onCancel() {

    }
}
