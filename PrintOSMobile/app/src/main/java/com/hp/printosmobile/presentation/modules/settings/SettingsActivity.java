package com.hp.printosmobile.presentation.modules.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardStatus;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.about.AboutActivity;
import com.hp.printosmobile.presentation.modules.chinapopup.SwitchProductionServerDialog;
import com.hp.printosmobile.presentation.modules.invites.InviteUtils;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingDataManager;
import com.hp.printosmobile.presentation.modules.settings.wizard.UserProfileManager;
import com.hp.printosmobile.presentation.modules.settings.wizard.WizardFragment;
import com.hp.printosmobile.presentation.modules.settings.wizard.WizardViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SettingsActivity extends BaseSettingsActivity implements SwitchProductionServerDialog.SwitchProductionServerDialogCallbacks, LanguageFragment.LanguageFragmentCallback,
        NotificationsFragment.NotificationFragmentCallback, UnitSystemFragment.UnitSystemFragmentCallback,
        WizardFragment.WizardFragmentCallbacks {

    private static final String TAG = SettingsActivity.class.getName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;
    @Bind(R.id.notifications_button)
    View notificationSettingsView;
    @Bind(R.id.notification_settings_title)
    TextView notificationSettingsTitle;
    @Bind(R.id.unit_system_button)
    View unitSystemView;
    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.edit_my_details_button)
    View editDetailsButton;
    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.intercept_layout)
    View interceptLayout;
    @Bind(R.id.unit_system_separator)
    ImageView unitSystemSeparator;
    @Bind(R.id.notifications_separator)
    ImageView notificationsSeparator;
    @Bind(R.id.select_server)
    View selectServer;
    @Bind(R.id.switch_server_separator)
    View switchServerSeparator;
    @Bind(R.id.ranking_leaderboard_settings_layout)
    View rankingLeaderboardSettingsContainer;
    @Bind(R.id.ranking_leaderboard_settings_separator)
    View rankingLeaderboardSeparator;
    @Bind(R.id.ranking_leaderboard_loading)
    View rankingLeaderboardLoadingView;
    @Bind(R.id.ranking_leaderboard_toggle)
    SwitchButton rankingLeaderboardToggle;

    HPFragment subMenuFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_SETTINGS_EVENT);
        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_SETTINGS);

        initView();
        initPresenter(true);
    }

    @Override
    protected void initPresenter(boolean loadInfo) {
        super.initPresenter(loadInfo);
    }

    private void initView() {

        toolbarDisplayName.setText(getString(R.string.settings_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            HPUIUtils.setVisibility(false, notificationSettingsView, unitSystemView, editDetailsButton, notificationsSeparator, unitSystemSeparator, rankingLeaderboardSettingsContainer);
        } else {

            if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isSelectServerEnabled() && PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).wasSwitchServerDialogOpened()) {
                HPUIUtils.setVisibilityForViews(true, selectServer, switchServerSeparator);
            }

            if (!PrintOSApplication.isGooglePlayServicesEnabled()) {
                notificationSettingsTitle.setEnabled(false);
            }

            if (!PrintOSPreferences.getInstance(this).wizardEnabled()) {
                editDetailsButton.setVisibility(View.GONE);
            }

            HPUIUtils.setVisibility(PrintOSPreferences.getInstance(this).supportV2(), unitSystemView, unitSystemSeparator);
        }

        if (PrintOSPreferences.getInstance().isRankingLeaderboardEnabled() && PrintOSPreferences.getInstance().getHasIndigoDivision()) {

            PermissionsManager.getInstance()
                    .getPermissionsData(false)
                    .subscribe(new Subscriber<Response<PermissionsData>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            showLeaderboardToggle();
                            toggleLeaderboard(false);
                            rankingLeaderboardSettingsContainer.setVisibility(View.GONE);
                            rankingLeaderboardSeparator.setVisibility(View.GONE);
                        }

                        @Override
                        public void onNext(Response<PermissionsData> permissionsDataResponse) {

                            if (PermissionsManager.getInstance().hasPermission(PermissionsManager.Permission.UPDATE)) {

                                showLeaderboardToggle();

                                if (permissionsDataResponse.isSuccessful()) {
                                    toggleLeaderboard(PermissionsManager.getInstance().getRankingLeaderboardStatus() != RankingLeaderboardStatus.DISABLED);
                                } else {
                                    toggleLeaderboard(false);
                                }

                                rankingLeaderboardSettingsContainer.setVisibility(View.VISIBLE);
                                rankingLeaderboardSeparator.setVisibility(View.VISIBLE);

                            } else {
                                rankingLeaderboardSettingsContainer.setVisibility(View.GONE);
                                rankingLeaderboardSeparator.setVisibility(View.GONE);

                            }

                        }
                    });

            rankingLeaderboardToggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    rankingLeaderboardLoadingView.setVisibility(View.VISIBLE);
                    rankingLeaderboardToggle.setVisibility(View.INVISIBLE);

                    if (rankingLeaderboardToggle.isChecked()) {

                        rankingLeaderboardToggle.setChecked(false);

                        restartApp(Constants.IntentExtras.ENABLE_RANKING_LEADERBOARD_REQUESTED, true);

                    } else {

                        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_FEATURE_DISABLED);

                        RankingDataManager.disableRankingLeaderboard()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Subscriber<Response<Void>>() {
                                    @Override
                                    public void onCompleted() {
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showLeaderboardToggle();
                                        toggleLeaderboard(true);
                                        displayErrorUpdateLeaderboardStatusMessage();
                                    }

                                    @Override
                                    public void onNext(Response<Void> voidResponse) {
                                        showLeaderboardToggle();
                                        PermissionsManager.getInstance().clear();

                                        if (!voidResponse.isSuccessful()){
                                            displayErrorUpdateLeaderboardStatusMessage();
                                        }
                                    }
                                });
                    }
                }
            });

        }

    }

    private void displayErrorUpdateLeaderboardStatusMessage() {
        HPUIUtils.displayToast(SettingsActivity.this, SettingsActivity.this.getString(R.string.ranking_leaderboard_failed_disable_feature));
    }

    private void toggleLeaderboard(boolean on) {
        rankingLeaderboardToggle.setChecked(on);
    }

    private void showLeaderboardToggle() {
        rankingLeaderboardToggle.setVisibility(View.VISIBLE);
        rankingLeaderboardLoadingView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    protected void updateProfileImageWithImageUrl(String url) {
        ImageLoadingUtils.setLoadedImageFromUrl(this, profileImage, url, R.drawable.user_profile, null, true);
        if (subMenuFragment != null && subMenuFragment instanceof WizardFragment) {
            ((WizardFragment) subMenuFragment).updateProfilePhotoWithUrl(url);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.languages_button)
    public void onLanguagesSelected() {
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.LANGUAGE_SCREEN_LABEL);

        openSubMenu(LanguageFragment.getNewInstance(this, LanguageFragment.LanguageSpecs.MAIN));
    }

    @OnClick(R.id.unit_system_button)
    public void onUnitSystemSelected() {
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.UNIT_SYSTEM_SCREEN_LABEL);

        openSubMenu(UnitSystemFragment.getNewInstance(this));
    }

    @OnClick(R.id.edit_my_details_button)
    public void onEditDetailsSelected() {

        HPLogger.d(TAG, "edit my details clicked");

        showLoadingView();

        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_WIZARD_FRAGMENT);
        Analytics.sendEvent(Analytics.EVENT_WIZARD_ENTER_SCREEN_FROM_EDIT_DETAILS);

        if (PrintOSPreferences.getInstance(this).isHPIDFeatureEnabled()) {

            UserProfileManager.getInstance().getEditMyDetailsUrl(new UserProfileManager.FetchHPIDEditMyDetailsUrlCallback() {
                @Override
                public void onSuccess(String hpidEditDetailsUrl) {
                    HPLogger.d(TAG, "Success getting edit my details url");
                    hideLoadingView();
                    AppUtils.startApplication(PrintOSApplication.getAppContext(), Uri.parse(hpidEditDetailsUrl));
                }

                @Override
                public void onFailure() {
                    HPLogger.d(TAG, "failed to get edit my details url");
                    hideLoadingView();
                    HPUIUtils.displayToast(PrintOSApplication.getAppContext(), getString(R.string.failed_to_edit_my_details));
                }
            });

        } else {

            UserProfileManager.getInstance().getProfileData().subscribe(new Subscriber<Boolean>() {
                @Override
                public void onCompleted() {
                    hideLoadingView();
                }

                @Override
                public void onError(Throwable e) {

                    hideLoadingView();

                    if (e instanceof APIException) {
                        HPUIUtils.displayToastException(SettingsActivity.this, (APIException) e, TAG, false);
                    } else {
                        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                        HPUIUtils.displayToastException(SettingsActivity.this, exception, TAG, false);
                    }
                }

                @Override
                public void onNext(Boolean isSuccessful) {

                    if (isSuccessful) {
                        openSubMenu(WizardFragment.getInstance(false));
                    } else {
                        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
                        HPUIUtils.displayToastException(SettingsActivity.this, exception, TAG, false);
                    }
                }
            });
        }

    }

    private void hideLoadingView() {
        HPUIUtils.setVisibility(false, loadingView);
        HPUIUtils.setVisibility(false, interceptLayout);
    }

    private void showLoadingView() {
        HPUIUtils.setVisibility(true, loadingView);
        HPUIUtils.setVisibility(true, interceptLayout);
    }

    @OnClick(R.id.notifications_button)
    public void onNotificationsSelected() {

        if (PrintOSApplication.isGooglePlayServicesEnabled()) {
            openSubMenu(NotificationsFragment.getNewInstance(this));

            Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.NOTIFICATION_SETTINGS_SCREEN_LABEL);
        } else {
            HPUIUtils.displayToast(this, getString(R.string.google_play_services_not_installed));
        }

    }

    private void openSubMenu(HPFragment fragment) {

        if (fragment == null) {
            return;
        }

        HPLogger.d(TAG, "open sub menu " + fragment.getClass().getName());
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();

        subMenuFragment = fragment;

        trans.replace(R.id.sub_menu_container, subMenuFragment);
        trans.commit();

        toolbarDisplayName.setText(getString(subMenuFragment.getToolbarDisplayName()));
    }

    @Override
    public void onBackPressed() {

        if (subMenuFragment != null) {

            if (subMenuFragment instanceof WizardFragment && subMenuFragment.onBackPressed()) {
                return;
            }

            removeSubMenu();
            AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_SETTINGS);
            return;
        }

        super.onBackPressed();
    }

    private void removeSubMenu() {
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.remove(subMenuFragment);
        trans.commit();
        subMenuFragment = null;
        toolbarDisplayName.setText(getString(R.string.settings_title));
    }

    @Override
    public void onLanguageChanged(String selectedLanguageKey) {

        Analytics.sendEvent(Analytics.SETTINGS_CHANGE_LANGUAGE_ACTION, selectedLanguageKey);

        PrintOSPreferences.getInstance(this).resetInviteUserRoleCache();

        PrintOSPreferences.getInstance(this).setLanguage(selectedLanguageKey);
        HPLocaleUtils.updateLanguage(this, selectedLanguageKey);
        HPLocaleUtils.configureAppLanguage(PrintOSApplication.getInstance(), Locale.getDefault());

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
        PrintOSAppWidgetProvider.updateAllWidgets(SettingsActivity.this);

        HPLogger.i(TAG, String.format("user changed language to: %s",
                PrintOSPreferences.getInstance(PrintOSApplication.getInstance()).getLanguageCode()));

        IntercomSdk.getInstance(this).login();

        InviteUtils.setInvitesViewModel(null);
        requestSignOut(false);
    }

    @Override
    public void closeNotificationsFragment() {
        onBackPressed();
    }

    @Override
    public void onUnitSystemChanged() {

        PrintOSAppWidgetProvider.updateAllWidgets(this);

        requestSignOut(false);
    }

    private void restartApp(String intentExtra, boolean signOut) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(intentExtra, signOut);
        setResult(Activity.RESULT_OK, returnIntent);
        this.finish();
    }

    void requestSignOut(boolean signOut) {
        restartApp(Constants.IntentExtras.SIGN_OUT_REQUESTED, signOut);
    }

    @OnClick(R.id.profile_click_area)
    public void onProfilePhotoClicked() {

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            HPUIUtils.displayToast(this, getString(R.string.option_not_available_demo_mode));
        } else {
            super.onUserProfilePhotoClicked();
        }
    }

    @OnClick(R.id.about_button)
    public void onAboutButtonClicked() {

        Analytics.sendEvent(Analytics.ABOUT_CLICKED_ACTION);
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.ABOUT_SCREEN_LABEL);
    }

    @OnClick(R.id.select_server)
    public void onSelectServerClicked() {
        DialogManager.displayChinaLoggingDialog(this, false);

    }

    @OnClick(R.id.sign_out_button)
    public void onSignOutClicked() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(getString(R.string.logout_confirmation_msg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.logout_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        HPLogger.d(TAG, "Logout selected.");
                        requestSignOut(true);

                    }
                })
                .setNegativeButton(getString(R.string.logout_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onDismissButtonClicked() {

    }

    @Override
    public void onWizardStepsCompleted(WizardViewModel wizardViewModel) {
        onBackPressed();
    }

    @Override
    public void onWizardFragmentAttached() {
        initPresenter(true);
    }

    @Override
    public void onSwitchServer() {
        requestSignOut(false);
    }

    @Override
    public void onDismissSwitchServerPopup() {

    }
}
