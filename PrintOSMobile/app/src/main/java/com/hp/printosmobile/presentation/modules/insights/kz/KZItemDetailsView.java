package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Osama on 3/20/18.
 */

public interface KZItemDetailsView extends MVPView {

    void showLoading();
    void hideLoading();
    void showErrorView();
    void hideErrorView();
    void onFinishedGettingAssetData(Object result);
}
