package com.hp.printosmobile.presentation.modules.contacthp.shared;

import android.net.Uri;

/**
 * Created by Anwar Asbah on 10/11/2016.
 */
public interface ImageFetchListener {
    void onImageFetched(Uri uri);
}
