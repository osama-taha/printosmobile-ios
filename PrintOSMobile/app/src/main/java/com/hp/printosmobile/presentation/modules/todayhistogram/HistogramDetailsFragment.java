package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.today.HistogramGraphUtils;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.SpannableStringUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import butterknife.Bind;

/**
 * create by Anwar Asbah 9/27/2016
 */
public class HistogramDetailsFragment extends HPFragment {

    private static final String ARG_HISTOGRAM_VIEW_MODEL = "ARG_HISTOGRAM_VIEW_MODEL";
    private static final String ARG_BREAKDOWN = "ARG_BREAKDOWN";
    private static final String ARG_TIME_RESOLUTION = "ARG_TIME_RESOLUTION";
    private static final String WEB_VIEW_INTERFACE_NAME = "app";

    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.histogram_pager)
    ViewPager histogramPager;
    @Bind(R.id.histogram_web_view)
    WebView histogramWebView;
    @Bind(R.id.all_1000_values)
    View all100TextView;
    @Bind(R.id.tooltip_title)
    TextView tooltipTitle;
    @Bind(R.id.tooltip_date)
    TextView tooltipDate;
    @Bind(R.id.tooltip_breakdown)
    TextView tooltipBreakdown;
    @Bind(R.id.tooltip_layout)
    LinearLayout tooltipLayout;
    @Bind(R.id.tv_legend)
    TextView tvLegend;

    private TodayHistogramViewModel viewModel;
    private HistogramDetailsAdapter histogramAdapter;
    private TodayHistogramViewModel.DetailTypeEnum typeEnum = null;
    private HistogramResolution histogramResolution;
    private Context context;

    public static HistogramDetailsFragment newInstance(TodayHistogramViewModel viewModel, TodayHistogramViewModel.DetailTypeEnum typeEnum, String resolution) {
        HistogramDetailsFragment fragment = new HistogramDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_HISTOGRAM_VIEW_MODEL, viewModel);
        if (typeEnum != null) {
            args.putInt(ARG_BREAKDOWN, typeEnum.ordinal());
        }
        if(resolution != null) {
            args.putString(ARG_TIME_RESOLUTION, resolution);
        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            viewModel = (TodayHistogramViewModel) bundle.getSerializable(ARG_HISTOGRAM_VIEW_MODEL);
            context = PrintOSApplication.getAppContext();

            if (bundle.containsKey(ARG_BREAKDOWN)) {
                typeEnum = TodayHistogramViewModel.DetailTypeEnum.values()[bundle.getInt(ARG_BREAKDOWN)];
            }

            if(bundle.containsKey(ARG_TIME_RESOLUTION)) {
                histogramResolution = HistogramResolution.from(bundle.getString(ARG_TIME_RESOLUTION));
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_VIEW_7DAY_DETAILED_EVENT);

        displayModel();
    }

    private void displayModel() {
        boolean isGraphEnabled = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .isHistogramGraphEnabled();

        histogramWebView.setVisibility(View.GONE);
        histogramWebView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

        histogramPager.setVisibility(View.GONE);
        all100TextView.setVisibility(View.GONE);

        if (isGraphEnabled || typeEnum != null) {
            typeEnum = typeEnum == null ? TodayHistogramViewModel.DetailTypeEnum.MAIN : typeEnum;
            titleTextView.setText(HistogramGraphUtils.getHistogramTitle(PrintOSApplication.getAppContext(), viewModel, typeEnum, histogramResolution, false, false));

            HistogramGraphUtils.displayGraph(PrintOSApplication.getAppContext(), histogramWebView, viewModel,
                    typeEnum == null ? TodayHistogramViewModel.DetailTypeEnum.MAIN : typeEnum, histogramResolution,
                    viewModel != null && viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS
                            && typeEnum == TodayHistogramViewModel.DetailTypeEnum.MAIN, true);
            histogramWebView.setVisibility(View.VISIBLE);
            all100TextView.setVisibility(View.GONE);
            tooltipLayout.setVisibility(View.VISIBLE);
        } else {
            int numberOfDays = getResources().getInteger(R.integer.today_panel_histogram_span) + 1;
            titleTextView.setText(getString(viewModel.isShiftSupport() ? R.string.histogram_shifts_title
                    : R.string.histogram_view_title, numberOfDays));

            histogramAdapter = new HistogramDetailsAdapter(getContext(), viewModel, numberOfDays);
            histogramPager.setAdapter(histogramAdapter);
            if (histogramAdapter.getCount() > 0) {
                histogramPager.setCurrentItem(histogramAdapter.getCount() - 1);
            }
            histogramPager.setVisibility(View.VISIBLE);
            all100TextView.setVisibility(viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? View.VISIBLE :
                    View.GONE);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.histogram_details_view;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.histogram_view_title;
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void tooltipChanged(final String tooltipDate, final String value, final String change, final String isChangeHigher, final String breakdownString) {

            if (getContext() instanceof Activity && isAdded()) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTooltipText(tooltipDate, value, change, isChangeHigher, breakdownString);
                    }
                });
            }

        }

        @JavascriptInterface
        public void graphLegend(final String legend) {

            if (getContext() instanceof Activity && isAdded()) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Context appContext = PrintOSApplication.getAppContext();

                        //spannable for graph legend
                        if (!TextUtils.isEmpty(legend)) {
                            SpannableStringUtils spannableLegend = new SpannableStringUtils(legend);
                            spannableLegend.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(appContext,
                                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) appContext.getResources().getDimension(R.dimen.legend_font), ResourcesCompat.getColor(appContext.getResources(), R.color.c62, null), 0, legend.length());

                            tvLegend.setVisibility(View.VISIBLE);
                            tvLegend.setText(spannableLegend.getSpannableString());
                        }
                    }
                });
            }
        }
    }

    private void updateTooltipText(String tooltipDate, String value, String change, String isHigher, String breakdownString) {

        if (getActivity() != null && isAdded()) {

            Context appContext = PrintOSApplication.getAppContext();

            //spannable for tooltip title
            String arrow = String.valueOf(Boolean.parseBoolean(isHigher) ? Html.fromHtml(appContext.getResources().getString(R.string.tooltip_change_higher)) : Html.fromHtml(appContext.getResources().getString(R.string.tooltip_change_lower)));
            String changeWithArrow = arrow + String.valueOf(Math.abs(Float.parseFloat(change))) + "%";
            int color = Boolean.parseBoolean(isHigher) ? R.color.bar_green : R.color.bar_red;

            SpannableStringUtils spannableValue = new SpannableStringUtils(value);
            spannableValue.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(appContext,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) appContext.getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(appContext.getResources(), R.color.tooltip_title_color, null), 0, value.length());
            SpannableString title = spannableValue.getSpannableString();

            SpannableStringUtils spannableChange = new SpannableStringUtils(changeWithArrow);
            spannableChange.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(appContext,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) appContext.getResources().getDimension(R.dimen.tooltip_change_font), ResourcesCompat.getColor(appContext.getResources(), color, null), 0, changeWithArrow.length());

            tooltipTitle.setText(TextUtils.concat(title, " ", spannableChange.getSpannableString()));

            //spannable for tooltip date
            SpannableStringUtils spannableDate = new SpannableStringUtils(tooltipDate);
            spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(appContext,
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) appContext.getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(appContext.getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length());

            this.tooltipDate.setText(spannableDate.getSpannableString());

            //spannable for tooltip breakdown
            if (!TextUtils.isEmpty(breakdownString)) {

                SpannableStringUtils spannableBreakdown = new SpannableStringUtils(breakdownString);
                spannableBreakdown.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(appContext,
                        TypefaceManager.HPTypeface.HP_REGULAR)), (int) appContext.getResources().getDimension(R.dimen.tooltip_breakdown_font), ResourcesCompat.getColor(appContext.getResources(), R.color.tooltip_title_color, null), 0, breakdownString.length());

                tooltipBreakdown.setText(spannableBreakdown.getSpannableString());
            } else {
                tooltipBreakdown.setVisibility(View.GONE);
            }
        }
    }
}