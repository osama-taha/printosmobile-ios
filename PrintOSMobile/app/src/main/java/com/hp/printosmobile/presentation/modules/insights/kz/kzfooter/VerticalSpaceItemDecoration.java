package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Minerva on 5/30/2018.
 */
public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    public static final int VERTICAL_ITEM_SPACE = 30;

    private final int verticalSpaceHeight;

    public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
        this.verticalSpaceHeight = verticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = verticalSpaceHeight;
    }
}