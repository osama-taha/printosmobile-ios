package com.hp.printosmobile.presentation.modules.rankingpanel;

import android.text.TextUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.cache.ApiCacheUtils;
import com.hp.printosmobile.data.remote.models.RankBreakdownData;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardBody;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardStatus;
import com.hp.printosmobile.data.remote.services.RankingLeaderboardData;
import com.hp.printosmobile.data.remote.services.RankingService;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel.RankAreaType;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * created by Anwar Asbah 5/30/2018
 */
public class RankingDataManager {

    private static final String TAG = RankingDataManager.class.getSimpleName();

    private static final String RANK_BREAKDOWN_CACHE_KEY = "rank_cache";
    private static final String RANK_BREAKDOWN_CACHE_ID_KEY = "rank_cache_id";
    private static final String RANK_BREAKDOWN_CACHE_ID_FORMAT = "rank_cache_%s_%s";

    public static final int WEEK_OFFSET = 16;
    private static final String COUNTRY_FLAG_URL_FORMAT = "https://www.countryflags.io/%s/shiny/64.png";
    private static final int TOP_LIST = 5;

    public static Observable<Response<Void>> requestRankingLeaderboardPermission() {

        return PrintOSApplication.getApiServicesProvider().getRankingService()
                .requestLeaderboardPermission();
    }

    public static Observable<Response<Void>> disableRankingLeaderboard() {

        RankingLeaderboardBody body = new RankingLeaderboardBody();
        body.setRankingLeaderboardStatus(RankingLeaderboardStatus.DISABLED);

        return PrintOSApplication.getApiServicesProvider().getRankingService().disableLeaderboard(body);
    }

    public static Observable<List<RankBreakdownViewModel>> getBreakdownRankData(String selectedSite) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RankingService rankingService = serviceProvider.getRankingService();

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        String lang = preferences.getLanguage();

        String cacheID = String.format(RANK_BREAKDOWN_CACHE_ID_FORMAT, selectedSite, lang);
        if (ApiCacheUtils.didCacheIDChange(RANK_BREAKDOWN_CACHE_ID_KEY, cacheID)) {
            ApiCacheUtils.clearApiCache(RANK_BREAKDOWN_CACHE_KEY);
        }

        return ApiCacheUtils.getCacheAndUpdate(rankingService.getRankingBreakdownData(selectedSite, WEEK_OFFSET, lang),
                RANK_BREAKDOWN_CACHE_KEY,
                new TypeReference<RankBreakdownData>() {
                }).map(new Func1<Response<RankBreakdownData>, List<RankBreakdownViewModel>>() {
            @Override
            public List<RankBreakdownViewModel> call(Response<RankBreakdownData> response) {
                if (response != null && response.isSuccessful() && response.body() != null) {
                    return parseData(response.body());
                }
                return null;
            }
        });
    }

    private static List<RankBreakdownViewModel> parseData(RankBreakdownData rankBreakdownData) {

        if (rankBreakdownData.getRankDataList() == null) {
            return null;
        }

        List<RankBreakdownViewModel> viewModels = new ArrayList<>();
        for (RankBreakdownData.RankData rankData : rankBreakdownData.getRankDataList()) {
            RankAreaType rankAreaType = RankAreaType.from(rankData.getType());
            if (rankAreaType == RankAreaType.UNKNOWN || rankData.getHistoryList() == null ||
                    rankData.getHistoryList().size() == 0) {
                continue;
            }
            RankBreakdownViewModel model = new RankBreakdownViewModel();

            model.setName(rankData.getName() == null ? "" : rankData.getName());
            model.setType(rankAreaType);

            List<RankBreakdownViewModel.RankHistoryItem> events = new ArrayList<>();

            boolean hasData = false;
            for (int i = 0; i < rankData.getHistoryList().size(); i++) {
                RankBreakdownData.History item = rankData.getHistoryList().get(i);

                RankBreakdownViewModel.RankHistoryItem event = new RankBreakdownViewModel.RankHistoryItem();

                int weekNumber = item.getWeek();
                int year = item.getYear();
                Integer offset = item.getOffset();

                Date date = HPDateUtils.parseDate("01-01-" + year, "dd-MM-yyyy");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.WEEK_OF_YEAR, weekNumber - 1);
                date = calendar.getTime();
                event.setDate(date);

                int rankValue;
                RankBreakdownViewModel.RankValueTypeEnum typeEnum;

                if (item.isLowerThanThreshold() && !item.isLowerThanTotalThreshold()) {
                    hasData = true;
                    rankValue = item.getLowerThirdValue();
                    typeEnum = RankBreakdownViewModel.RankValueTypeEnum.RANK_VALUE_LOWER_THIRD;
                } else {
                    if (item.getPosition() == null) {
                        rankValue = -1;
                        typeEnum = RankBreakdownViewModel.RankValueTypeEnum.RANK_VALUE_NO_DATA;
                    } else {
                        if (!item.isLowerThanTotalThreshold()) {
                            rankValue = item.getPosition();
                            hasData = true;
                            if (offset == null) {
                                typeEnum = RankBreakdownViewModel.RankValueTypeEnum.RANK_VALUE_NO_OFFSET;
                            } else {
                                typeEnum = RankBreakdownViewModel.RankValueTypeEnum.RANK_VALUE_WITH_OFFSET;
                            }
                        } else {
                            rankValue = -1;
                            typeEnum = RankBreakdownViewModel.RankValueTypeEnum.RANK_VALUE_NO_DATA;
                        }
                    }
                }

                if (rankValue == -1 && !hasData) {
                    continue;
                }

                event.setRankValue(rankValue);
                event.setRankValueTypeEnum(typeEnum);

                event.setTotal(item.getTotal());
                event.setWeek(item.getWeek());
                event.setYear(item.getYear());
                event.setName(item.getName());
                event.setLowerThanThreshold(item.isLowerThanThreshold());
                event.setLowerThanTotalThreshold(item.isLowerThanTotalThreshold());
                event.setOffset(item.getOffset() == null ? 0 : item.getOffset());
                event.setLowerThirdValue(item.getLowerThirdValue());
                event.setTop(item.isTop());

                events.add(event);
            }

            //clearing null trailing points
            List<RankBreakdownViewModel.RankHistoryItem> eventsClearedTrailing = new ArrayList<>();
            boolean canSkip = true;
            for (int index = events.size() - 1; index >= 0; index--) {
                if (events.get(index).getRankValue() == -1 && canSkip) {
                    continue;
                } else {
                    eventsClearedTrailing.add(0, events.get(index));
                }

                canSkip = canSkip && events.get(index).getRankValue() == -1;
            }
            events = eventsClearedTrailing;

            //handling null values
            for (int index = 0; index < events.size(); index++) {
                RankBreakdownViewModel.RankHistoryItem historyItem = events.get(index);
                if (historyItem.getRankValue() != -1) {
                    continue;
                }

                RankBreakdownViewModel.RankHistoryItem itemPrev = index - 1 >= 0 ? events.get(index - 1) : null;
                RankBreakdownViewModel.RankHistoryItem itemNext = null;
                for (int nextIndex = index; nextIndex < events.size(); nextIndex++) {
                    if (events.get(nextIndex).getRankValue() != -1) {
                        itemNext = events.get(nextIndex);
                        break;
                    }
                }
                if (itemPrev == null) {
                    if (itemNext == null) {
                        historyItem.setRankValue(0);
                    } else {
                        historyItem.setRankValue(itemNext.getRankValue());
                    }
                } else {
                    if (itemNext == null) {
                        historyItem.setRankValue(itemPrev.getRankValue());
                    } else {
                        historyItem.setRankValue((itemNext.getRankValue() + itemPrev.getRankValue() + 1) / 2);
                        //+1 for rounding up
                    }
                }
            }

            model.setHistoryList(events);

            if (hasData) {
                viewModels.add(model);
            }
        }

        Collections.sort(viewModels, RankBreakdownViewModel.RANK_BREAKDOWN_COMPARATOR);
        return viewModels;
    }

    public static Observable<List<RankingViewModel.SiteRankViewModel>> getRankingLeaderboardList(String siteId, final RankingViewModel.SiteRankViewModel siteRankViewModel, String bu, int rankFrom, int rankTo) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RankingService rankingService = serviceProvider.getRankingService();

        return rankingService.getRankingLeaderboardData(siteId, bu, siteRankViewModel.getYear(), siteRankViewModel.getWeek(), siteRankViewModel.getRankAreaType().getKey(), rankFrom, rankTo)
                .map(new Func1<Response<RankingLeaderboardData>, List<RankingViewModel.SiteRankViewModel>>() {
                    @Override
                    public List<RankingViewModel.SiteRankViewModel> call(Response<RankingLeaderboardData> rankingLeaderboardDataResponse) {
                        if (rankingLeaderboardDataResponse.isSuccessful()) {
                            return parseLeaderboardData(siteRankViewModel, rankingLeaderboardDataResponse.body());
                        }
                        return null;
                    }
                });
    }

    private static List<RankingViewModel.SiteRankViewModel> parseLeaderboardData(RankingViewModel.SiteRankViewModel siteRankViewModel, RankingLeaderboardData rankingLeaderboardData) {

        if (rankingLeaderboardData == null) {
            return null;
        }

        String currentOrgName = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSelectedOrganization().getOrganizationName();

        List<RankingViewModel.SiteRankViewModel> ranks = new ArrayList<>();

        for (RankingLeaderboardData.RankData rankData : rankingLeaderboardData.getRanks()) {
            RankingViewModel.SiteRankViewModel leaderboardSiteRankViewModel = new RankingViewModel.SiteRankViewModel();

            if (rankData.isLeaderboardEnable()) {
                String siteName = TextUtils.isEmpty(rankData.getSiteName()) ? "" : rankData.getSiteName();
                String orgName = TextUtils.isEmpty(rankData.getOrgName()) ? "" : rankData.getOrgName();
                String name = getOrgSiteName(orgName, siteName);
                leaderboardSiteRankViewModel.setName(name);
            } else {
                leaderboardSiteRankViewModel.setName(PrintOSApplication.getAppContext().getString(R.string.ranking_leaderboard_incognito_site));
            }

            int offset = rankData.getOffset();

            leaderboardSiteRankViewModel.setActualRank(rankData.getPosition());
            leaderboardSiteRankViewModel.setTrend(getTrend(offset));
            if (rankData.getCountryCode() != null) {
                leaderboardSiteRankViewModel.setCountryFlagUrl(getCountryFlagUrl(rankData.getCountryCode()));
            }
            leaderboardSiteRankViewModel.setIncognito(!rankData.isLeaderboardEnable());
            leaderboardSiteRankViewModel.setScore(rankData.getScore());

            leaderboardSiteRankViewModel.setMySiteRank(rankData.getPosition() == siteRankViewModel.getActualRank()
                    && currentOrgName.equalsIgnoreCase(rankData.getOrgName()));
            leaderboardSiteRankViewModel.setShowLowerThirdLabel(rankData.getPosition() >= siteRankViewModel.getLowerThirdValue());
            leaderboardSiteRankViewModel.setOffset(rankData.getOffset());

            leaderboardSiteRankViewModel.setIsInTop(rankData.getPosition() <= TOP_LIST);
            if (leaderboardSiteRankViewModel.isMySiteRank()) {
                leaderboardSiteRankViewModel.setRankBreakdownViewModel(siteRankViewModel.getRankBreakdownViewModel());
            }
            ranks.add(leaderboardSiteRankViewModel);
        }

        if (siteRankViewModel.getActualRank() > siteRankViewModel.getLowerThirdValue()) {
            siteRankViewModel.setMySiteRank(true);
            siteRankViewModel.setIsInTop(siteRankViewModel.getActualRank() <= TOP_LIST);

            BusinessUnitViewModel selectedBu = HomePresenter.getSelectedBusinessUnit();

            String orgName = TextUtils.isEmpty(currentOrgName) ? "" : currentOrgName;

            String currentSite = selectedBu != null && selectedBu.getFiltersViewModel() != null
                    && selectedBu.getFiltersViewModel().getSelectedSite() != null ?
                                    selectedBu.getFiltersViewModel().getSelectedSite().getName() : "";

            currentSite = TextUtils.isEmpty(currentOrgName) ? "" : currentSite;

            siteRankViewModel.setName(getOrgSiteName(orgName, currentSite));

            ranks.add(siteRankViewModel);
        }

        return ranks;
    }

    private static String getOrgSiteName(String orgName, String siteName) {

        if (siteName.equalsIgnoreCase(orgName)) {
            return orgName;
        } else {
            if (!siteName.isEmpty()) {
                return String.format("%s - %s", orgName, siteName);
            } else {
                return orgName;
            }
        }
    }

    private static RankingViewModel.Trend getTrend(int offset) {
        RankingViewModel.Trend trend;
        if (offset > 0) {
            trend = RankingViewModel.Trend.UP;
        } else if (offset < 0) {
            trend = RankingViewModel.Trend.DOWN;
        } else {
            trend = RankingViewModel.Trend.EQUALS;
        }
        return trend;
    }

    public static String getCountryFlagUrl(String countryCode) {
        if (TextUtils.isEmpty(countryCode)) {
            return "";
        }
        return String.format(COUNTRY_FLAG_URL_FORMAT, countryCode);
    }

}