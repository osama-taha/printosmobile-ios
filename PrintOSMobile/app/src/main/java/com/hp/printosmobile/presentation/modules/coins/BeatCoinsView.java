package com.hp.printosmobile.presentation.modules.coins;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Osama Taha
 */
public interface BeatCoinsView extends MVPView {

    void onClaimCoinsSucceeded();
    void onClaimCoinsFailed();
    void onStoreAccountNotFound();
    void onCreateAccountSelected();
    void onCancelCreateAccountSelected();
}
