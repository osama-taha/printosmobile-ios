package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;

/**
 * Created by Osama Taha
 */
public interface RankingLeaderboardView extends MVPView {

    void showLoading();

    void hideLoading();

    void onRequestLeaderboardPermissionCompleted();

    void onRequestLeaderboardPermissionFailed();

    void onGettingSiteRankingViewModelCompleted(RankingViewModel rankingViewModel, boolean isGroupSelected);

    void onGettingSiteRankingViewModelFailed();
}
