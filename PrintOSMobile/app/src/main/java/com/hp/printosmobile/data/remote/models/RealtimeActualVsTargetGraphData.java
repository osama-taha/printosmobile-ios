package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah on 1/15/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RealtimeActualVsTargetGraphData {

    @JsonProperty("last30PvData")
    private List<HistogramItem> last30PvData;
    @JsonProperty("impressionsType")
    private String impressionsType;
    @JsonProperty("measureType")
    private String measureType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("last30PvData")
    public List<HistogramItem> getLast30PvData() {
        return last30PvData;
    }

    @JsonProperty("last30PvData")
    public void setLast30PvData(List<HistogramItem> last30PvData) {
        this.last30PvData = last30PvData;
    }

    @JsonProperty("measureType")
    public String getMeasureType() {
        return measureType;
    }

    @JsonProperty("measureType")
    public void setMeasureType(String measureType) {
        this.measureType = measureType;
    }

    @JsonProperty("impressionsType")
    public String getImpressionsType() {
        return impressionsType;
    }

    @JsonProperty("impressionsType")
    public void setImpressionsType(String impressionsType) {
        this.impressionsType = impressionsType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class HistogramItem {
        @JsonProperty("lastWeekPvData")
        private DayItem lastWeekPvData;
        @JsonProperty("pvData")
        private DayItem pvData;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("lastWeekPvData")
        public DayItem getLastWeekPvData() {
            return lastWeekPvData;
        }

        @JsonProperty("lastWeekPvData")
        public void setLastWeekPvData(DayItem lastWeekPvData) {
            this.lastWeekPvData = lastWeekPvData;
        }

        @JsonProperty("pvData")
        public DayItem getPvData() {
            return pvData;
        }

        @JsonProperty("pvData")
        public void setPvData(DayItem pvData) {
            this.pvData = pvData;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    public static class DayItem {
        @JsonProperty("date")
        private String date;
        @JsonProperty("value")
        private Double value;
        @JsonProperty("sheets")
        private Double sheets;
        @JsonProperty("meters")
        private Double meters;
        @JsonProperty("sqm")
        private Double sqm;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("date")
        public String getDate() {
            return date;
        }

        @JsonProperty("date")
        public void setDate(String date) {
            this.date = date;
        }

        @JsonProperty("value")
        public Double getValue() {
            return value;
        }

        @JsonProperty("value")
        public void setValue(Double value) {
            this.value = value;
        }

        @JsonProperty("sheets")
        public Double getSheets() {
            return sheets;
        }

        @JsonProperty("sheets")
        public void setSheets(Double sheets) {
            this.sheets = sheets;
        }

        @JsonProperty("meters")
        public Double getMeters() {
            return meters;
        }

        @JsonProperty("meters")
        public void setMeters(Double meters) {
            this.meters = meters;
        }

        @JsonProperty("sqm")
        public Double getSqm() {
            return sqm;
        }

        @JsonProperty("sqm")
        public void setSqm(Double sqm) {
            this.sqm = sqm;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}
