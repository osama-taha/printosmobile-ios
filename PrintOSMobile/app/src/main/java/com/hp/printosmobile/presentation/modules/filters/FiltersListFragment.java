package com.hp.printosmobile.presentation.modules.filters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class FiltersListFragment extends HPFragment implements FilterItemsAdapter.FilterItemsAdapterCallbacks {

    private static final String ARG_FILTER_ITEMS = "FILTER_ITEMS";
    private static final String ARG_HAS_SEARCH = "HAS_SEARCH";
    private static final int MIN_NUMBER_OF_FILTERS_FOR_SEARCHING_DEF_VALUE = 5;

    @Bind(R.id.fragment_filter_content)
    View filterFragmentContentView;
    @Bind(R.id.filter_layout_view)
    View filterLayoutView;
    @Bind(R.id.filter_input_field)
    HPEditText filterInputField;
    @Bind(R.id.items_recycler_view)
    RecyclerView itemsRecyclerView;
    @Bind(R.id.list_empty)
    TextView emptyListTextView;

    private List<FilterItem> mItems;

    private FiltersListFragmentCallbacks mListener;
    private FilterItemsAdapter adapter;
    private boolean hasSearchBar;

    public static FiltersListFragment newInstance(List<FilterItem> items, boolean hasSearchBar) {
        FiltersListFragment fragment = new FiltersListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FILTER_ITEMS, new ArrayList<>(items));
        args.putBoolean(ARG_HAS_SEARCH, hasSearchBar);
        fragment.setArguments(args);
        return fragment;
    }

    public FiltersListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hasSearchBar = false;
        if (getArguments() != null) {
            mItems = (ArrayList<FilterItem>) getArguments().getSerializable(ARG_FILTER_ITEMS);
            hasSearchBar = getArguments().getBoolean(ARG_HAS_SEARCH);
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    private void initView() {

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        itemsRecyclerView.setLayoutManager(manager);

        adapter = new FilterItemsAdapter(getActivity(), mItems, this);

        itemsRecyclerView.setAdapter(adapter);
        itemsRecyclerView.setHasFixedSize(true);

        filterFragmentContentView.setVisibility(!mItems.isEmpty() ? View.VISIBLE : View.INVISIBLE);
        emptyListTextView.setVisibility(!mItems.isEmpty() ? View.INVISIBLE : View.VISIBLE);
        filterLayoutView.setVisibility(shouldShowBar(mItems) ? View.VISIBLE : View.GONE);

        filterInputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        itemsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx != 0 || dy != 0) {
                    HPUIUtils.hideSoftKeyboard(getActivity(), filterInputField);
                }
            }
        });

        filterInputField.forceHideClearButton(true);

        itemsRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard();
                return false;
            }
        });
    }

    private boolean shouldShowBar(List<FilterItem> items) {
        return hasSearchBar && items != null && items.size() > (PrintOSPreferences.getInstance(getActivity())
                .getFilterSearchBoxDeviceCount(MIN_NUMBER_OF_FILTERS_FOR_SEARCHING_DEF_VALUE) + 1); //+1 to account for all
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_filters_list;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof FiltersListFragmentCallbacks) {
            mListener = (FiltersListFragmentCallbacks) getParentFragment();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FiltersFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(boolean selected, FilterItem filterItem) {

        mListener.onItemSelected(selected, filterItem);
        HPUIUtils.hideSoftKeyboard(getActivity(), filterInputField);
    }

    public void updateView(List<FilterItem> items) {
        if (adapter != null && items != null) {

            filterInputField.setText("");

            this.mItems = items;
            adapter.addItems(items);

            RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
            itemsRecyclerView.setLayoutManager(manager);

            itemsRecyclerView.setVisibility(!mItems.isEmpty() ? View.VISIBLE : View.INVISIBLE);
            emptyListTextView.setVisibility(!mItems.isEmpty() ? View.INVISIBLE : View.VISIBLE);
            filterLayoutView.setVisibility(shouldShowBar(items) ? View.VISIBLE : View.GONE);

            emptyListTextView.setText(PrintOSApplication.getAppContext().getString(R.string.filter_no_groups));
        }
    }

    public void manageSelection(FilterItem filterItem, boolean select) {
        if (adapter != null) {
            if (filterItem instanceof SiteViewModel) {
                adapter.clearSelections();
                adapter.selectItem(filterItem, true);
            } else if (filterItem instanceof GroupViewModel) {
                adapter.clearSelections();
                adapter.selectItem(filterItem, select);
            } else {
                adapter.selectItem(filterItem, select);
            }
        }
    }

    public List<FilterItem> getItems() {
        return mItems;
    }

    public boolean isEmpty() {
        return mItems == null || mItems.isEmpty();
    }

    public List<FilterItem> getSelectedItems() {
        if (adapter == null) {
            return new ArrayList<>();
        }
        return adapter.getSelectedItems();
    }

    public void selectItems(List<FilterItem> items) {
        if (adapter != null) {
            adapter.selectRange(items);
        }
    }

    public void clearSelection() {
        adapter.clearSelections();
    }

    public void hideKeyboard() {
        HPUIUtils.hideSoftKeyboard(getActivity(), filterInputField);
    }

    public void clearSearch() {
        if (filterInputField != null) {
            filterInputField.setText("");
        }
    }

    public interface FiltersListFragmentCallbacks {
        void onItemSelected(boolean selected, FilterItem filterItem);
    }

}
