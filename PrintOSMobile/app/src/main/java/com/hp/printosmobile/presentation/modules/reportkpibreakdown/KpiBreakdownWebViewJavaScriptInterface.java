package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.text.TextUtils;
import android.webkit.JavascriptInterface;

import com.hp.printosmobile.Analytics;

/**
 * Created by anwar asbah on 2/25/2018.
 */

public class KpiBreakdownWebViewJavaScriptInterface {

    @JavascriptInterface
    public void legendClicked(String name) {
        if(!TextUtils.isEmpty(name)) {
            Analytics.sendEvent(Analytics.EVENT_AVAILABILITY_LEGEND_CLICK, name.toLowerCase());
        }
    }
}
