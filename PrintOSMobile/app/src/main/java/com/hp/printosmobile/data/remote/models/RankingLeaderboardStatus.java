package com.hp.printosmobile.data.remote.models;

public enum RankingLeaderboardStatus {
    DISABLED, ENABLED, REQUESTED;
}