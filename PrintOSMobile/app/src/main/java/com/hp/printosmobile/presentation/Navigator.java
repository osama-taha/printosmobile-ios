package com.hp.printosmobile.presentation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;

import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.modules.insights.kz.KZItemDetailsActivity;
import com.hp.printosmobile.presentation.modules.insights.kz.KZSupportedFormat;
import com.hp.printosmobile.presentation.modules.insights.kz.KZVideoActivity;
import com.hp.printosmobile.presentation.modules.login.LoginActivity;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.maintenance.MaintenanceActivity;
import com.hp.printosmobile.presentation.modules.settings.wizard.WizardActivity;
import com.hp.printosmobile.presentation.modules.userimagepopup.AddUserImageActivity;

/**
 * A class responsible to manage the navigation between the activities.
 * <p/>
 * Created by Osama Taha on 4/10/16.
 */
public class Navigator {

    private Navigator() {
    }

    public static void openMainActivity(Context context, Bundle bundle) {

        Intent intent = new Intent(context, MainActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    public static void openLoginActivity(Context context) {

        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);

    }

    public static void openMaintenanceActivity(Context context) {

        Intent intent = new Intent(context, MaintenanceActivity.class);
        context.startActivity(intent);

    }

    public static void openWizardActivity(Context context) {

        Intent intent = new Intent(context, WizardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public static void openAddUserImageActivity(Context context) {

        Intent intent = new Intent(context, AddUserImageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }


    public static void openKZDetailsActivity(Activity activity, KZItem kzItem, BusinessUnitEnum businessUnitEnum) {

        if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.MP4) {
            ActivityCompat.startActivity(activity, KZVideoActivity.createIntent(activity, kzItem, businessUnitEnum), null);
        } else {
            ActivityCompat.startActivity(activity, KZItemDetailsActivity.createIntent(activity, kzItem, businessUnitEnum), null);
        }
    }
}
