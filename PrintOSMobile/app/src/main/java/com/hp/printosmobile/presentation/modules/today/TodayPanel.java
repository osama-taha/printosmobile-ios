package com.hp.printosmobile.presentation.modules.today;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory.PersonalAdvisorObserver;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.tooltip.MaintenanceFanTooltip;
import com.hp.printosmobile.presentation.modules.tooltip.MessageTooltip;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltip;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.HPProgressArc;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 5/15/2016.
 */
public class TodayPanel extends PanelView<TodayViewModel> implements SharableObject, TodayDevicesAdapter.TodayDeviceAdapterCallback, PersonalAdvisorObserver {

    public static final String TAG = TodayPanel.class.getSimpleName();

    public static final String TAG_ACTUAL_VS_TARGET = "ACTUAL_VS_TARGET";
    private static final long FADE_DURATION = 500;
    private static final int MIN_ITEMS_TO_SHOW_VIEW_ALL_DEVICES_BUTTON = 2;
    private static final String PERCENTAGE_STRING_FORMAT = "%s%%";
    private static final String TITLE_WITH_IMPRESSION = "%1$s (%2$s)";
    private static final String WEB_VIEW_INTERFACE_NAME = "app";

    @Bind(R.id.today_panel_content)
    View todayPanelContent;
    @Bind(R.id.recycler_view_printers)
    RecyclerView printersRecyclerView;
    @Bind(R.id.info_tooltip_button)
    ImageButton infoTooltipButton;
    @Bind(R.id.progress_arc)
    HPProgressArc progressArc;
    @Bind(R.id.imp_value_text)
    TextView impressionValueText;
    @Bind(R.id.goal_value_text)
    TextView goalValueText;
    @Bind(R.id.impressions_title_text_view)
    TextView impressionTitleTextView;
    @Bind(R.id.progress_arc_container)
    View progressArcContainer;
    @Bind(R.id.last_update_time_text)
    TextView lastUpdateTimeText;
    @Bind(R.id.last_update_time_value_text)
    TextView lastUpdateTimeValueText;
    @Bind(R.id.progress_graph_container)
    View progressGraphContainer;
    @Bind(R.id.progress_web_view)
    WebView progressWebView;
    @Bind(R.id.info_tooltip_button_today_graph)
    View infoTooltipButtonTodayGraph;
    @Bind(R.id.view_separator)
    View separatorView;
    @Bind(R.id.button_view_all)
    TextView buttonViewAll;
    @Bind(R.id.today_mode_buttons)
    RadioGroup shiftsDaysPicker;
    @Bind(R.id.chart_footer_today)
    TextView chartFooterTodayTextView;
    @Bind(R.id.chart_footer_today_legend_icon)
    View chartFooterTodayIcon;
    @Bind(R.id.not_graph_error_msg)
    View noGraphErrorMsg;
    @Bind(R.id.title_so_far)
    TextView titleSoFar;
    @Bind(R.id.progress_container)
    View progressContainer;
    @Bind(R.id.chart_description_text_view)
    TextView titleDescriptionTextView;

    private TodayPanelCallbacks callbacks;
    private ActiveShiftsViewModel activeShiftsViewModel;
    private ShiftViewModel selectedShift;
    private ImageView shiftsDaysArrowImage;
    private MessageTooltip tooltip;
    private PreferencesData.UnitSystem unitSystem;
    private TodayDevicesAdapter todayDevicesAdapter;
    private long lastAnalyticsEventSentTime;

    public TodayPanel(Context context) {
        super(context);
    }

    public TodayPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TodayPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());
        TodayPanel.this.unitSystem = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
        initView();
        initHeaderIcons();
        PersonalAdvisorFactory.getInstance().addObserver(TodayPanel.this);
    }

    private void initHeaderIcons() {
        View headerLeftView = inflate(getContext(), R.layout.today_panel_header_left_view, getHeaderLeftView());
        shiftsDaysArrowImage = headerLeftView.findViewById(R.id.today_panel_arrow);

        shiftsDaysArrowImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                closeShiftsDaysPicker(shiftsDaysPicker.getVisibility() != GONE);
            }
        });
    }

    private void initView() {
        todayPanelContent.setVisibility(GONE);
        progressWebView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        progressWebView.setLongClickable(false);
        progressWebView.setOnTouchListener(new WebViewCustomeTouchListener(getContext()));
        progressWebView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);
    }

    public void onFilterSelected() {
        selectedShift = null;
        onRefresh();
    }

    @Override
    public Spannable getTitleSpannable() {

        SpannableStringBuilder builder;

        if (getViewModel() == null || getViewModel().getDeviceViewModels() == null) {
            builder = new SpannableStringBuilder(getContext().getString(R.string.today_panel_title_press_view));
        } else {


            int selectedDevicesCount = getViewModel().getDeviceViewModels().size();
            int siteDevicesCount = getViewModel().getSiteViewModel().getDevices().size();

            String devicesCountTitle = String.valueOf(siteDevicesCount);

            if (selectedDevicesCount != siteDevicesCount) {
                devicesCountTitle = String.format("%d/%d", selectedDevicesCount, siteDevicesCount);
            }

            builder = new SpannableStringBuilder(String.format(
                    getContext().getString(R.string.today_panel_panel_title_press_view_with_extra), devicesCountTitle));
        }

        if (selectedShift != null && selectedShift.getShiftType() == ShiftViewModel.ShiftType.CURRENT) {
            builder = new SpannableStringBuilder(getContext().getString(R.string.today_panel_title_current_shift));
        }

        return builder;

    }

    @Override
    public int getContentView() {
        return R.layout.today_panel_content;
    }

    @Override
    public void updateViewModel(TodayViewModel viewModel) {

        setViewModel(viewModel);
        updatePersonalAdvisorObserver();
        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callbacks, true);
            return;
        }
        setTitle(getTitleSpannable());

        hideShiftsDaysArrow(true);
        closeShiftsDaysPicker(true);
        buildView();

        if (callbacks != null) {
            callbacks.setHasRTDevices(false);
        }

        BusinessUnitEnum bu = getViewModel() == null || getViewModel().getBusinessUnitViewModel() == null ?
                BusinessUnitEnum.UNKNOWN : getViewModel().getBusinessUnitViewModel().getBusinessUnit();
        boolean hasRTDevices = false;
        if (viewModel != null && viewModel.getDeviceViewModels() != null) {
            for (DeviceViewModel deviceViewModel : viewModel.getDeviceViewModels()) {
                if (deviceViewModel != null && deviceViewModel.isRTSupported()) {
                    hideShiftsDaysArrow(!(activeShiftsViewModel != null && activeShiftsViewModel.getShiftViewModels() != null && !activeShiftsViewModel.getShiftViewModels().isEmpty()));
                    if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isTodayGraphSupported(bu)) {
                        showRealtimeGraph();
                    }
                    if (callbacks != null) {
                        callbacks.setHasRTDevices(true);
                        hasRTDevices = true;
                    }
                    break;
                }
            }
        }

        if (!hasRTDevices) {
            setShareButtonVisibility(false);
        }

        progressContainer.setVisibility(hasRTDevices ? VISIBLE : GONE);
        todayPanelContent.setVisibility(VISIBLE);

        DeepLinkUtils.respondToIntentForPanel(this, callbacks, false);
    }

    @Override
    public boolean showEmptyCard() {
        boolean returnValue = super.showEmptyCard();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                TodayViewModel todayViewModel = getViewModel();
                if (todayViewModel != null) {
                    BusinessUnitViewModel businessUnitViewModel = todayViewModel.getBusinessUnitViewModel();
                    if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {
                        setShareButtonVisibility(false);
                    }
                }
            }
        });

        return returnValue;
    }

    private void showRealtimeGraph() {
        TodayViewModel.TodayVsLastWeekViewModel viewModel = getViewModel() == null ? null
                : getViewModel().getTodayVsLastWeekViewModel();
        boolean hasGraphData = viewModel != null && viewModel.getTodaySeries() != null && viewModel.getLastWeekSeries() != null;
        progressGraphContainer.setVisibility(hasGraphData ? VISIBLE : GONE);
        noGraphErrorMsg.setVisibility(hasGraphData ? GONE : VISIBLE);
    }

    private void buildView() {

        if (getViewModel() == null) {
            return;
        }

        BusinessUnitEnum bu = getViewModel() == null || getViewModel().getBusinessUnitViewModel() == null ?
                BusinessUnitEnum.UNKNOWN : getViewModel().getBusinessUnitViewModel().getBusinessUnit();
        boolean isGaugeEnabled = !PrintOSPreferences.getInstance(getContext()).isTodayGraphSupported(bu);
        if (isGaugeEnabled) {
            lastUpdateTimeText.setText(getContext().getString(R.string.device_detail_last_update, ""));
            lastUpdateTimeValueText.setText(HPDateUtils.getCurrentTime(getContext()));
            buildTodayScoreView(getViewModel().getPrintVolumeValue(),
                    getViewModel().getPrintVolumeIntraDailyTargetValue(),
                    getViewModel().getPrintVolumeShiftTargetValue());
        } else {
            progressWebView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    progressWebView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    TodayGraphUtils.displayGraph(getContext(), progressWebView, chartFooterTodayTextView, chartFooterTodayIcon, selectedShift == null ?
                            ShiftViewModel.ShiftType.TODAY : selectedShift.getShiftType(), getViewModel());

                    if (getViewModel() != null && getContext() != null) {
                        int weightedAv = TodayGraphUtils.getWeightedAverage();
                        int printVolume = (int) getViewModel().getPrintVolumeValue();
                        boolean isLower = printVolume < weightedAv;

                        float percentage;
                        if (weightedAv != 0) {
                            percentage = (float) (printVolume - weightedAv) / (float) weightedAv * 100f;
                        } else {
                            percentage = printVolume == 0 ? 0 : 100;
                        }
                        //fix rounding issue
                        percentage = (float) (int) (percentage * 10) / 10f;

                        String percentageString = String.format(PERCENTAGE_STRING_FORMAT,
                                HPLocaleUtils.getDecimalString(Math.abs(percentage), true, 1));
                        String description = getContext().getString(isLower ? R.string.today_chart_lower_than_last_week :
                                R.string.today_chart_higher_than_last_week, percentageString);
                        SpannableString spannableString = new SpannableString(description);

                        int color;
                        if (Math.abs(percentage) <= 5) {
                            color = R.color.c204;
                        } else if (percentage < 0) {
                            color = R.color.bar_red;
                        } else {
                            color = R.color.bar_green;
                        }

                        addColorSpanForTooltipString(spannableString, description, percentageString, color);

                        titleDescriptionTextView.setText(spannableString);
                    }
                }
            });

            printersRecyclerView.setHasFixedSize(true);
            printersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            printersRecyclerView.setNestedScrollingEnabled(false);

            //tooltip is hidden until further notice.
            infoTooltipButton.setVisibility(View.GONE);
            infoTooltipButtonTodayGraph.setVisibility(GONE);

            boolean isShift = selectedShift != null && selectedShift.getShiftType() != ShiftViewModel.ShiftType.TODAY;
            BusinessUnitEnum businessUnitEnum = getViewModel().getBusinessUnitViewModel().getBusinessUnit();
            int titleResourceId;
            if (!isShift) {
                if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                    titleResourceId = R.string.today_chart_today_imp;
                } else {
                    titleResourceId = unitSystem == PreferencesData.UnitSystem.Metric ? R.string.today_chart_today_sqm :
                            R.string.today_chart_today_ft_square;
                }
            } else {
                if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                    titleResourceId = R.string.today_chart_this_shift_imp;
                } else {
                    titleResourceId = unitSystem == PreferencesData.UnitSystem.Metric ? R.string.today_chart_this_shift_sqm :
                            R.string.today_chart_this_shift_square;
                }
            }

            String printVolume = HPLocaleUtils.getLocalizedValue((int) getViewModel().getPrintVolumeValue());
            String title = getContext().getString(titleResourceId, printVolume);

            if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS && getViewModel().getImpressionType() != null) {
                title = String.format(TITLE_WITH_IMPRESSION, title, getViewModel().getImpressionType());
            }

            int startIndex = title.indexOf(printVolume);
            int endIndex = title.indexOf(printVolume) + printVolume.length();
            Spannable spannable = new SpannableString(title);
            spannable.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.c62, null)),
                    startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(getContext(),
                    TypefaceManager.HPTypeface.HP_BOLD)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            titleSoFar.setText(spannable);
        }

        progressArcContainer.setVisibility(isGaugeEnabled ? VISIBLE : GONE);
        progressGraphContainer.setVisibility(GONE); //visibility updated later if has RT devices
        noGraphErrorMsg.setVisibility(GONE); //visibility updated later if has RT devices

        buildPrintersList();

        OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInfoTooltip(view);
            }
        };

        infoTooltipButton.setOnClickListener(onClickListener);
        infoTooltipButtonTodayGraph.setOnClickListener(onClickListener);

        progressArc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                openIntraDailyTargetToolTip(event.getX(), event.getY());
                return false;
            }
        });

        printersRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                printersRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (callbacks != null) {
                    callbacks.onTodayViewInitialized();
                }
            }
        });

        hideViewAllButton(getViewModel().getSiteViewModel().getDevices().size() < MIN_ITEMS_TO_SHOW_VIEW_ALL_DEVICES_BUTTON);
    }

    private void openIntraDailyTargetToolTip(float x, float y) {

        if (tooltip == null) {
            tooltip = new MessageTooltip(getContext());
        }

        Context context = getContext();
        TodayViewModel todayViewModel = getViewModel();

        HPProgressArc.ProgressArcClickEvent event = progressArc.isThumbClicked(x, y);
        if (event != null) {
            String intraDailyTargetValue;
            if (todayViewModel.getBusinessUnitViewModel().getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                intraDailyTargetValue = context.getString(R.string.imp_value, HPLocaleUtils.getLocalizedValue(
                        (int) todayViewModel.getPrintVolumeIntraDailyTargetValue()
                ));
            } else {
                intraDailyTargetValue = context.getString(Unit.SQM.getUnitTextResourceId(unitSystem, Unit.UnitStyle.VALUE),
                        HPLocaleUtils.getLocalizedValue((int) todayViewModel.getPrintVolumeIntraDailyTargetValue()));
            }

            String tooltipMsg = context.getString(R.string.tooltip_intra_daily_target, intraDailyTargetValue);

            tooltip.updateView(HPStringUtils.setSpannableRegexBold(tooltipMsg, intraDailyTargetValue));
            tooltip.showToolTip(progressArc, event.getLocationX(), event.getLocationY(), event.getGravity());
        }
    }

    private void buildPrintersList() {
        TodayViewModel todayViewModel = getViewModel();
        Context context = getContext();
        if (todayViewModel.getDeviceViewModels() == null) {
            return;
        }

        BusinessUnitEnum businessUnitEnum = todayViewModel.getBusinessUnitViewModel().getBusinessUnit();
        List<DeviceViewModel> devices = todayViewModel.getDeviceViewModels();

        int numOfPrinters = devices.size();
        int maxNumOfPrintersToShow = businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER ? numOfPrinters
                : TodayViewModel.NUMBER_OF_DEVICES;
        int printersSize = numOfPrinters > maxNumOfPrintersToShow ? maxNumOfPrintersToShow : numOfPrinters;

        List<DeviceViewModel> deviceViewModels = devices.subList(0, printersSize);

        todayDevicesAdapter = new TodayDevicesAdapter(context, businessUnitEnum, deviceViewModels, this);
        printersRecyclerView.setAdapter(todayDevicesAdapter);
        printersRecyclerView.addItemDecoration(new DividerItemDecoration(context, 1, printersSize, false));

        todayDevicesAdapter.notifyDataSetChanged();
    }

    private void buildTodayScoreView(double score, double target, double max) {

        Context context = getContext();

        BusinessUnitEnum businessUnitEnum = getViewModel().getBusinessUnitViewModel().getBusinessUnit();

        int progressColor = HPScoreUtils.getProgressColor(context, businessUnitEnum, score, target, false);
        int trackColor = HPScoreUtils.getProgressColor(context, businessUnitEnum, score, target, true);


        if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {

            impressionTitleTextView.setText(context.getString(R.string.impressions));
            impressionValueText.setText(HPLocaleUtils.getLocalizedValue((int) score));
            goalValueText.setText(HPLocaleUtils.getLocalizedValue((int) max));

        } else {

            String squareMeters = context.getString(Unit.SQM.getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));

            impressionTitleTextView.setText(squareMeters);
            impressionValueText.setText(HPLocaleUtils.getLocalizedValue((int) score));
            goalValueText.setText(HPLocaleUtils.getLocalizedValue((int) max));

        }

        impressionValueText.setTextColor(progressColor);

        final int[] sweepColors = {progressColor, progressColor};
        final float[] sweepPosition = {0.5f, 1f};
        progressArc.setProgressSweepGradient(sweepColors, sweepPosition);
        progressArc.setRoundedEdges(false);
        progressArc.setTrackColor(trackColor);
        progressArc.requestLayout();

        double percentageFill;
        if (max == 0) {
            if (score == 0) {
                percentageFill = 0;
            } else {
                percentageFill = 1;
            }
        } else {
            percentageFill = score / max;
        }

        percentageFill = Math.max(0, percentageFill);

        double percentageTarget = max == 0 ? 0 : target / max;
        percentageTarget = Math.max(0, percentageTarget);

        progressArc.setThumb((int) (percentageTarget * 100) == 0 ? null : ResourcesCompat.getDrawable(context.getResources(), R.drawable.circle, null));
        progressArc.setTarget((int) (percentageTarget * 100));
        progressArc.setProgress((int) (percentageFill * 100));

        progressArcContainer.setVisibility(View.VISIBLE);

    }

    @Override
    public void onMaintenanceIconClicked(View view, DeviceViewModel model) {

        MaintenanceFanTooltip maintenanceFanTooltip = new MaintenanceFanTooltip(getContext());
        maintenanceFanTooltip.updateView(model.getMaintenanceState());
        maintenanceFanTooltip.showToolTip(view);
    }

    @Override
    public void onStateDistributionClicked(DeviceViewModel model) {
        if (callbacks != null) {
            callbacks.onStateDistributionClicked(model);
        }
    }

    private void showInfoTooltip(View button) {

        if (getContext() == null || button == null) {
            return;
        }

        BusinessUnitEnum bu = getViewModel() == null || getViewModel().getBusinessUnitViewModel() == null ?
                BusinessUnitEnum.UNKNOWN : getViewModel().getBusinessUnitViewModel().getBusinessUnit();
        TodayInfoTooltip todayInfoTooltip = new TodayInfoTooltip(getContext());
        todayInfoTooltip.updateView(getViewModel().getInfoTooltipViewModel());
        if (!PrintOSPreferences.getInstance(getContext()).isTodayGraphSupported(bu)) {
            todayInfoTooltip.showToolTipToLeftSide(button, 2 * getContext().getResources().getDimension(R.dimen.panel_side_margin), -button.getPaddingBottom() / 2f);
        } else {
            todayInfoTooltip.showToolTipToLeft(button, -button.getPaddingBottom() / 2f);
        }
    }

    public ActiveShiftsViewModel getActiveShiftsViewModel() {
        return this.activeShiftsViewModel;
    }

    public void updateActiveShifts(ActiveShiftsViewModel activeShiftsViewModel) {

        this.activeShiftsViewModel = activeShiftsViewModel;

        if (activeShiftsViewModel.getShiftViewModels() == null || activeShiftsViewModel.getShiftViewModels().isEmpty()) {
            hideShiftsDaysArrow(true);
            return;
        }

        selectedShift = null;
        for (ShiftViewModel shiftViewModel : activeShiftsViewModel.getShiftViewModels()) {
            if (shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT &&
                    HomePresenter.isShiftSupport()) {
                selectedShift = shiftViewModel;
            }
        }

        shiftsDaysPicker.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < activeShiftsViewModel.getShiftViewModels().size(); i++) {
            final ShiftViewModel shiftViewModel = activeShiftsViewModel.getShiftViewModels().get(i);

            String shiftName = getContext().getString(shiftViewModel.getShiftType().getDisplayNameResource());
            String selectShiftName = selectedShift == null ?
                    getContext().getString(R.string.day_default_shift_display_name)
                    : getContext().getString(selectedShift.getShiftType().getDisplayNameResource());

            View shiftModeView = inflater.inflate(R.layout.today_panel_content_radio, null);
            RadioButton shiftModeButton = shiftModeView.findViewById(R.id.radio_button);
            shiftModeButton.setId(i);
            shiftModeButton.setText(shiftName);
            shiftModeButton.setChecked(shiftName.equals(selectShiftName));

            shiftModeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeShiftsDaysPicker(true);
                }
            });
            shiftModeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                ShiftViewModel model = shiftViewModel;

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    onRefresh();

                    if (isChecked) {
                        selectedShift = model;
                        if (callbacks != null) {
                            callbacks.onShiftSelected(selectedShift);
                        }
                        showLoading();
                    }
                }
            });

            shiftsDaysPicker.addView(shiftModeButton);
        }
    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null
                || getViewModel().getDeviceViewModels() == null
                || getViewModel().getDeviceViewModels().isEmpty());
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    private void hideShiftsDaysArrow(boolean hide) {
        shiftsDaysArrowImage.setVisibility(hide ? GONE : VISIBLE);
    }

    private void hideViewAllButton(boolean hide) {
        HPUIUtils.setVisibility(!hide, separatorView, buttonViewAll);
    }

    private void closeShiftsDaysPicker(boolean hide) {

        if (hide) {
            //Fade out animation
            shiftsDaysPicker.animate().alpha(0.0f).setDuration(FADE_DURATION);
            shiftsDaysPicker.setVisibility(GONE);
            //Arrow direction is DOWN (original state).
            shiftsDaysArrowImage.setRotation(0);
        } else {
            //Fade in animation
            shiftsDaysPicker.animate().alpha(1.0f).setDuration(FADE_DURATION);
            shiftsDaysPicker.setVisibility(VISIBLE);
            //Arrow direction is UP.
            shiftsDaysArrowImage.setRotation(-180);
        }

    }

    @Override
    public void showLoading() {
        super.showLoading();
        shiftsDaysPicker.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        shiftsDaysPicker.setEnabled(true);
    }

    public void addTodayPanelCallbacks(TodayPanelCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @OnClick(R.id.button_view_all)
    void onViewAllButtonClicked() {
        if (callbacks != null) {
            callbacks.onTodayPanelViewAllClicked();
        }
    }

    @Override
    public void onDeviceClicked(DeviceViewModel model) {
        if (callbacks != null) {
            callbacks.onDeviceClicked(model);
        }
    }

    @Override
    public void onHasAdviceIconClicked(DeviceViewModel model) {
        if (callbacks != null) {
            callbacks.onHasAdviceIconClicked(model);
        }
    }

    @Override
    public synchronized void updatePersonalAdvisorObserver() {
        HPLogger.d(TAG, "updating Personal Advisor Observer");
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                TodayViewModel todayViewModel = getViewModel();
                if (todayViewModel != null) {
                    List<DeviceViewModel> deviceViewModels = todayViewModel.getDeviceViewModels();
                    PersonalAdvisorFactory factory = PersonalAdvisorFactory.getInstance();
                    if (deviceViewModels != null) {
                        for (DeviceViewModel model : deviceViewModels) {
                            model.setPersonalAdvisorViewModel(factory.getDeviceAdvices(model.getSerialNumber()));
                        }
                    }

                    if (callbacks != null) {
                        Activity hostingActivity = callbacks.getHostingActivity();
                        if (hostingActivity != null) {
                            Integer screen = DeepLinkUtils.getIntExtra(hostingActivity, Constants.UNIVERSAL_LINK_SCREEN_KEY);
                            String extra = DeepLinkUtils.getStringExtra(hostingActivity, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);

                            if (screen != null && screen == Constants.UNIVERSAL_LINK_SCREEN_PAPOPUP) {
                                callbacks.onDeepLinkHandled();
                                if (extra != null) {
                                    if (deviceViewModels != null) {
                                        for (DeviceViewModel model : deviceViewModels) {
                                            if (model != null && model.getSerialNumber() != null &&
                                                    model.getSerialNumber().equals(extra)) {
                                                callbacks.onHasAdviceIconClicked(model);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (todayDevicesAdapter != null) {
                    todayDevicesAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        //Do Nothing
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalAdvisorFactory.getInstance().removeObserver(this);
        if (progressWebView != null) {
            progressWebView.destroy();
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        setTitle(getTitleSpannable());
        if (callbacks != null) {
            callbacks.resetHistogramData();
        }
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callbacks != null) {
            callbacks.onShareButtonClicked(Panel.TODAY);
        }
    }

    @Override
    public void sharePanel() {
        if (callbacks != null) {
            lockShareButton(true);

            printersRecyclerView.setVisibility(GONE);
            progressContainer.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.panel_view_bg_color, null));
            Bitmap panelBitmap = FileUtils.getScreenShot(progressContainer);
            progressContainer.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));
            printersRecyclerView.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                String title = "";
                TodayViewModel todayViewModel = getViewModel();
                if (todayViewModel != null && todayViewModel.getTodayVsLastWeekViewModel() != null) {
                    boolean isToday = selectedShift == null || selectedShift.getShiftType() == ShiftViewModel.ShiftType.TODAY;
                    float weightedAverage = todayViewModel.getTodayVsLastWeekViewModel().getWeightedAverage();
                    float printVolumeValue = (float) todayViewModel.getPrintVolumeValue();

                    float changePercentage;
                    if (weightedAverage != 0) {
                        changePercentage = (printVolumeValue - weightedAverage) / weightedAverage * 100;
                    } else if (printVolumeValue == 0) {
                        changePercentage = 0;
                    } else {
                        changePercentage = 100;
                    }

                    PreferencesData.UnitSystem unitSystemObj = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
                    BusinessUnitEnum businessUnitEnum = todayViewModel.getBusinessUnitViewModel() != null ?
                            todayViewModel.getBusinessUnitViewModel().getBusinessUnit() : BusinessUnitEnum.INDIGO_PRESS;
                    String unit = getContext().getString(businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS ?
                            R.string.impressions : Unit.SQM.getUnitTextResourceId(unitSystemObj, Unit.UnitStyle.DEFAULT));

                    int titleStringId;
                    if (isToday) {
                        if (changePercentage >= 0) {
                            titleStringId = R.string.share_today_panel_title_higher;
                        } else {
                            titleStringId = R.string.share_today_panel_title_lower;
                        }
                    } else {
                        if (changePercentage >= 0) {
                            titleStringId = R.string.share_today_panel_title_higher_shift;
                        } else {
                            titleStringId = R.string.share_today_panel_title_lower_shift;
                        }
                    }

                    title = getContext().getString(titleStringId,
                            HPLocaleUtils.getDecimalString(printVolumeValue, true, 2), unit, HPLocaleUtils.getDecimalString(Math.abs(changePercentage), true, 2));
                }

                final String finalTitle = title;
                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(),
                        HomePresenter.isShiftSupport(), null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callbacks.onScreenshotCreated(Panel.TODAY, storageUri,
                                        finalTitle,
                                        link,
                                        TodayPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_PRODUCTION_TODAY;
    }

    @Override
    public String getPanelTag() {
        return Panel.TODAY.getDeepLinkTag();
    }

    public void openStateDistribution(String pressID) {
        if (getViewModel() != null && getViewModel().getDeviceViewModels() != null && callbacks != null) {
            for (DeviceViewModel viewModel : getViewModel().getDeviceViewModels()) {
                if (viewModel != null && viewModel.getSerialNumber() != null && ((pressID != null &&
                        pressID.equals(viewModel.getSerialNumber())) || pressID == null)) {
                    callbacks.onStateDistributionClicked(viewModel);
                    return;
                }
            }
        }
    }

    public interface TodayPanelCallbacks extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {

        void onTodayPanelViewAllClicked();

        void onDeviceClicked(DeviceViewModel model);

        void onHasAdviceIconClicked(DeviceViewModel model);

        void onHasAdviceIconClicked(PersonalAdvisorViewModel model);

        void onShiftSelected(ShiftViewModel shiftViewModel);

        void onTodayViewInitialized();

        void setHasRTDevices(boolean hasRTDevices);

        void resetHistogramData();

        void onStateDistributionClicked(DeviceViewModel model);
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void initGraph(String value) {
            if (getContext() instanceof Activity) {
                PrintOSApplication.logStartup();
            }
        }

        @JavascriptInterface
        public void tooltipChanged(final boolean higherThanListWeek, final String change, final String time, final String todayYValue, final String lastWeekYValue) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (getViewModel() != null && getViewModel().getBusinessUnitViewModel() != null) {
                            updateTooltipWithTodayVsLastWeekData(getViewModel().getBusinessUnitViewModel().getBusinessUnit(),
                                    higherThanListWeek, change, time, todayYValue, lastWeekYValue);
                        }

                    }
                });
            }

        }
    }

    private void updateTooltipWithTodayVsLastWeekData(BusinessUnitEnum businessUnitEnum, boolean higherThanListWeek, String change, String time, String todayYValue, String lastWeekYValue) {

        String unit = getContext().getString(getPrintUnitForBusinessUnit(businessUnitEnum));

        if (!TextUtils.isEmpty(todayYValue)) {

            //Update the title label.

            String title = String.format("%1$s - %2$s %3$s", time, todayYValue, unit);
            if (!TextUtils.isEmpty(getViewModel().getImpressionType())) {
                title = String.format(TITLE_WITH_IMPRESSION, title, getViewModel().getImpressionType());
            }

            SpannableString spannable = new SpannableString(title);
            addColorSpanForTooltipString(spannable, title, todayYValue, R.color.c62);

            titleSoFar.setText(spannable);

            //Update the description label.

            int color = higherThanListWeek ? R.color.bar_green : R.color.bar_red;
            String description = getContext().getString(higherThanListWeek ? R.string.today_chart_higher_than_last_week_value
                    : R.string.today_chart_lower_than_last_week_value, change, lastWeekYValue);
            spannable = new SpannableString(description);
            addColorSpanForTooltipString(spannable, description, change, color);
            addColorSpanForTooltipString(spannable, description, lastWeekYValue, R.color.c62);

            titleDescriptionTextView.setText(spannable);

        } else {

            //Update the title label.

            String title = getContext().getString(R.string.today_chart_last_week_at_this_time, time);
            SpannableString spannable = new SpannableString(title);
            titleSoFar.setText(spannable);

            //Update the description label.

            String description = String.format("%1$s %2$s", lastWeekYValue, unit);

            if (!TextUtils.isEmpty(getViewModel().getImpressionType())) {
                description = String.format("%1$s (%2$s)", description, getViewModel().getImpressionType());
            }

            spannable = new SpannableString(description);
            addColorSpanForTooltipString(spannable, description, lastWeekYValue, R.color.c62);

            titleDescriptionTextView.setText(spannable);

        }

        final long currentTime = SystemClock.elapsedRealtime();
        if (currentTime - lastAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
            Analytics.sendEvent(Analytics.EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH, !TextUtils.isEmpty(todayYValue) ? Analytics.EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH_LABEL_TODAY : Analytics.EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH_LABEL_LAST_WEEK);
            lastAnalyticsEventSentTime = currentTime;
        }
    }

    private void addColorSpanForTooltipString(SpannableString spannable, String string, String value, int color) {

        int startIndex = string.lastIndexOf(value);
        int endIndex = string.lastIndexOf(value) + value.length();
        spannable.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), color, null)),
                startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(getContext(),
                TypefaceManager.HPTypeface.HP_BOLD)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    private int getPrintUnitForBusinessUnit(BusinessUnitEnum businessUnitEnum) {

        if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
            return R.string.kpi_explanation_bar_tag_print_volume_indigo;
        } else {
            return unitSystem == PreferencesData.UnitSystem.Metric ? R.string.kpi_explanation_bar_tag_print_volume_latex_sqm :
                    R.string.kpi_explanation_bar_tag_print_volume_latex_ft;
        }
    }

    public void searchDevicesBySerialNumber(String serialNumber) {
        if (getViewModel() != null && getViewModel().getDeviceViewModels() != null) {
            for (DeviceViewModel deviceViewModel : getViewModel().getDeviceViewModels()) {
                if (deviceViewModel != null && !TextUtils.isEmpty(deviceViewModel.getSerialNumber()) && deviceViewModel.getSerialNumber().equals(serialNumber) && deviceViewModel.isRTSupported()) {
                    onDeviceClicked(deviceViewModel);
                }
            }
        }
    }
}