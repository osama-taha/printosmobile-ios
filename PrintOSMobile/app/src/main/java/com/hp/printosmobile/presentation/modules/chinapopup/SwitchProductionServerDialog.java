package com.hp.printosmobile.presentation.modules.chinapopup;

import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Minerva Halteh on 14/2/2018.
 */
public class SwitchProductionServerDialog extends DialogFragment {

    public static final String TAG = SwitchProductionServerDialog.class.getName();
    private static String currentServerUrl;
    private static String recommendedServerUrl;
    private static boolean shouldResetSessionCount;
    private SwitchProductionServerDialogCallbacks callback;

    @Bind(R.id.current_server_text_view)
    TextView currentTextView;

    @Bind(R.id.recommended_server_text_view)
    TextView recommendedTextView;

    @Bind(R.id.button_use_server)
    TextView buttonUseServer;

    @Bind(R.id.keep_using_server_text_view)
    TextView keepUsingServerTextView;

    public static SwitchProductionServerDialog getInstance(SwitchProductionServerDialogCallbacks callback, String serverUrl, boolean resetSessionCount) {
        SwitchProductionServerDialog switchProductionServerDialog = new SwitchProductionServerDialog();

        switchProductionServerDialog.callback = callback;
        currentServerUrl = serverUrl;
        shouldResetSessionCount = resetSessionCount;
        if (currentServerUrl.equals(PrintOSApplication.getAppContext().getString(R.string.production_server))) {
            recommendedServerUrl = PrintOSApplication.getAppContext().getString(R.string.china_production_server);
        } else {
            recommendedServerUrl = PrintOSApplication.getAppContext().getString(R.string.production_server);
        }
        return switchProductionServerDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.ChinaLoggingPopupStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.dialog_switch_server, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (shouldResetSessionCount) {
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setSessionCount(0);
        }
        initView();
        setCancelable(false);
    }

    private void initView() {

        boolean isUsingProductionServer = currentServerUrl.equals(getString(R.string.production_server));

        SpannableString spannable;
        String keepUsingServerString;
        currentTextView.setText(getString(R.string.switch_prod_server_popup_title, getString(isUsingProductionServer ? R.string.production_server_display_name : R.string.china_production_server_display_name)));
        recommendedTextView.setText(getString(R.string.switch_prod_server_popup_subtitle, (getString(isUsingProductionServer ? R.string.china_production_server_display_name : R.string.production_server_display_name))));
        buttonUseServer.setText(getString(R.string.switch_prod_server_popup_button_use_server, getString(isUsingProductionServer ? R.string.china_production_server_display_name : R.string.production_server_display_name)));

        keepUsingServerString = getString(R.string.switch_prod_server_popup_text_use_server, getString(isUsingProductionServer ? R.string.production_server_display_name : R.string.china_production_server_display_name)).toLowerCase();
        spannable = new SpannableString(keepUsingServerString);
        spannable.setSpan(new UnderlineSpan(), 0, keepUsingServerString.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        keepUsingServerTextView.setText(spannable);
    }

    @OnClick(R.id.button_use_server)
    public void onSwitchServerButtonClicked() {
        HPLogger.d(TAG, "switch to recommended server button clicked");

        PrintOSApplication.initServicesProvider(PrintOSApplication.getAppContext(), recommendedServerUrl);
        PrintOSApplication.initSDKs(recommendedServerUrl);
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setServerUrl(PrintOSApplication.getAppContext(), recommendedServerUrl);

        dismissAllowingStateLoss();

        if (callback != null) {
            callback.onSwitchServer();
        }
    }

    @OnClick(R.id.keep_using_server_text_view)
    public void onKeepUsingServerButtonClicked() {
        HPLogger.d(TAG, "keep using server button clicked");

        dismissAllowingStateLoss();
        if(callback != null){
            callback.onDismissSwitchServerPopup();
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
    }

    public interface SwitchProductionServerDialogCallbacks {
        void onSwitchServer();
        void onDismissSwitchServerPopup();
    }
}
