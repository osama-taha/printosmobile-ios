package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoData implements Serializable {

    private static final String utf_8 = "utf-8";

    @JsonProperty("customerName")
    private String customerName;
    @JsonProperty("euid")
    private String euid;
    @JsonProperty("highLevelCustomerName")
    private String highLevelCustomerName;
    @JsonProperty("ownership")
    private String ownership;
    @JsonProperty("productLine")
    private String productLine;
    @JsonProperty("region")
    private String region;
    @JsonProperty("series")
    private String series;
    @JsonProperty("service_area")
    private String serviceArea;
    @JsonProperty("subRegion")
    private String subRegion;
    @JsonProperty("country")
    private String country;
    @JsonProperty("email")
    private String email;
    @JsonProperty("corporateCode")
    private String corporateCode;
    @JsonProperty("firstname")
    private String firstname;
    @JsonProperty("lastname")
    private String lastname;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("customerName")
    public String getCustomerName() {
        return customerName;
    }

    @JsonProperty("customerName")
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @JsonProperty("euid")
    public String getEuid() {
        return euid;
    }

    @JsonProperty("euid")
    public void setEuid(String euid) {
        this.euid = euid;
    }

    @JsonProperty("highLevelCustomerName")
    public String getHighLevelCustomerName() {
        return highLevelCustomerName;
    }

    @JsonProperty("highLevelCustomerName")
    public void setHighLevelCustomerName(String highLevelCustomerName) {
        this.highLevelCustomerName = highLevelCustomerName;
    }

    @JsonProperty("ownership")
    public String getOwnership() {
        return ownership;
    }

    @JsonProperty("ownership")
    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    @JsonProperty("productLine")
    public String getProductLine() {
        return productLine;
    }

    @JsonProperty("productLine")
    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("series")
    public String getSeries() {
        return series;
    }

    @JsonProperty("series")
    public void setSeries(String series) {
        this.series = series;
    }

    @JsonProperty("service_area")
    public String getServiceArea() {
        return serviceArea;
    }

    @JsonProperty("service_area")
    public void setServiceArea(String serviceArea) {
        this.serviceArea = serviceArea;
    }

    @JsonProperty("subRegion")
    public String getSubRegion() {
        return subRegion;
    }

    @JsonProperty("subRegion")
    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("corporateCode")
    public String getCorporateCode() {
        return corporateCode;
    }

    @JsonProperty("corporateCode")
    public void setCorporateCode(String corporateCode) {
        this.corporateCode = corporateCode;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return firstname;
    }

    @JsonProperty("firstname")
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return lastname;
    }

    @JsonProperty("lastname")
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getQueryString() throws UnsupportedEncodingException {
        String queryString = ";" +
                (region == null ? "" : "&region=" + URLEncoder.encode(region, utf_8)) +
                (subRegion == null ? "" : "&subregion=" + URLEncoder.encode(subRegion, utf_8)) +
                (country == null ? "" : "&country=" + URLEncoder.encode(country, utf_8)) +
                (serviceArea == null ? "" : "&service_area=" + URLEncoder.encode(serviceArea, utf_8)) +
                (ownership == null ? "" : "&ownership=" + URLEncoder.encode(ownership, utf_8)) +
                (highLevelCustomerName == null ? "" : "&high_level_customer_name=" + URLEncoder.encode(highLevelCustomerName, utf_8)) +
                (corporateCode == null ? "" : "&corporate_code=" + URLEncoder.encode(corporateCode, utf_8)) +
                (series == null ? "" : "&series=" + URLEncoder.encode(series, "utf-8")) +
                (productLine == null ? "" : "&product_line=" + URLEncoder.encode(productLine, utf_8)) +
                (email == null ? "" : "&customer_emails=" + URLEncoder.encode(email, utf_8)) +
                (euid == null ? "" : "&end_user_id=" + URLEncoder.encode(euid, utf_8));
        queryString = queryString.replace(";&", "");
        queryString = queryString.replace(";", "");
        return queryString;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserInfoData{");
        sb.append("customerName='").append(customerName).append('\'');
        sb.append(", euid='").append(euid).append('\'');
        sb.append(", highLevelCustomerName='").append(highLevelCustomerName).append('\'');
        sb.append(", ownership='").append(ownership).append('\'');
        sb.append(", productLine='").append(productLine).append('\'');
        sb.append(", region='").append(region).append('\'');
        sb.append(", series='").append(series).append('\'');
        sb.append(", serviceArea='").append(serviceArea).append('\'');
        sb.append(", subRegion='").append(subRegion).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", corporateCode='").append(corporateCode).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    public boolean hasNullValue() {
        return customerName == null ||
                euid == null ||
                highLevelCustomerName == null ||
                ownership == null ||
                productLine == null ||
                region == null ||
                series == null ||
                serviceArea == null ||
                subRegion == null ||
                country == null ||
                email == null ||
                corporateCode == null;
    }
}
