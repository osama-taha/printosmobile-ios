package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardBody;
import com.hp.printosmobile.data.remote.models.RankBreakdownData;
import com.hp.printosmobile.data.remote.models.SiteRankingData;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Anwar Asbah 5/30/2018
 */
public interface RankingService {

    @GET(ApiConstants.RANK_BREAKDOWN_API)
    Observable<Response<RankBreakdownData>> getRankingBreakdownData(@Query("siteId") String siteId, @Query("offset") int offset, @Query("lang") String lang);

    @GET(ApiConstants.SITE_RANKING_API)
    Observable<Response<SiteRankingData>> getSiteRankingData(@Query("siteId") String siteId, @Query("bu") String bu, @Query("lang") String lang);

    @GET(ApiConstants.RANKING_LEADERBOARD_API)
    Observable<Response<RankingLeaderboardData>> getRankingLeaderboardData(@Query("siteId") String siteId,
                                                                    @Query("bu") String bu,
                                                                    @Query("year") int year,
                                                                    @Query("week") int week,
                                                                    @Query("rankType") String rankType,
                                                                    @Query("fromRank") int fromRank,
                                                                    @Query("toRank") int toRank);


    @PUT(ApiConstants.ORGANIZATION_SETTINGS_API)
    Observable<Response<Void>> disableLeaderboard(@Body RankingLeaderboardBody body);

    @GET(ApiConstants.REQUEST_LEADERBOARD_PERMISSION_API)
    Observable<Response<Void>> requestLeaderboardPermission();

}
