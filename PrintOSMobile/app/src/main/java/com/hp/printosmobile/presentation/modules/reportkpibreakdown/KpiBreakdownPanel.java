package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.today.WebViewCustomeTouchListener;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.utils.SpannableStringUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 1/30/2018.
 */
public class KpiBreakdownPanel extends PanelView<KpiBreakdownViewModel> implements SharableObject {

    public static final String TAG = KpiBreakdownPanel.class.getSimpleName();
    public static final String OVER_ALL_PERFORMANCE_TAG = "overall performance";

    private static final String SHARE_ACTION = "share kpi breakdown %1$S %2$S";
    private static final String WEB_VIEW_INTERFACE_NAME = "kpi";

    @Bind(R.id.panel_content)
    View panelContent;
    @Bind(R.id.empty_data_set_text_view)
    TextView emptyDataSetTextView;
    @Bind(R.id.web_view_container)
    View webViewContainer;
    @Bind(R.id.web_view)
    WebView webView;
    @Bind(R.id.tooltip_title)
    TextView tooltipTitle;
    @Bind(R.id.tooltip_date)
    TextView tooltipDate;
    @Bind(R.id.tooltip_layout)
    LinearLayout tooltipLayout;
    @Bind(R.id.tv_legend)
    TextView tvLegend;

    private KpiBreakdownPanelCallback callbacks;
    private long lastAnalyticsEventSentTime;
    private long lastAvailabilityAnalyticsEventSentTime;

    public KpiBreakdownPanel(Context context) {
        super(context);
    }

    public KpiBreakdownPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KpiBreakdownPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());
        initView();
    }

    private void initView() {
        webView.setOnTouchListener(new WebViewCustomeTouchListener(getContext()));
    }

    @Override
    public Spannable getTitleSpannable() {
        if (getViewModel() == null || getViewModel().getKpiBreakdownEnum() == null) {
            return new SpannableString("");
        }

        return new SpannableString(KpiBreakdownUtils.getTitle(getContext(), getViewModel()));
    }

    @Override
    public int getContentView() {
        return R.layout.kpi_breakdown_panel_content;
    }

    @Override
    public void updateViewModel(final KpiBreakdownViewModel viewModel) {

        setViewModel(viewModel);
        setTitle(getTitleSpannable());

        if (showEmptyCard()) {
            return;
        }

        emptyDataSetTextView.setVisibility(View.INVISIBLE);
        panelContent.setVisibility(VISIBLE);

        tooltipLayout.setVisibility(VISIBLE);

        webViewContainer.setVisibility(VISIBLE);
        webView.getViewTreeObserver().
                addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        webView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        KpiBreakdownUtils.displayGraph(PrintOSApplication.getAppContext(),
                                webView,
                                viewModel, false);
                    }
                });

        webView.addJavascriptInterface(new KpiBreakdownPanel.WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callbacks != null) {
            callbacks.onShareButtonClicked(this);
        }
    }

    @Override
    public void sharePanel() {
        if (callbacks != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                final String title = getContext().getString(R.string.share_caption_title_kpi_breakdown,
                        getTitleSpannable().toString(), KpiBreakdownActivity.NUMBER_OF_WEEKS);

                boolean isOverall = getViewModel() != null && getViewModel().isOverall();
                KpiBreakdownEnum kpiBreakdownEnum = getViewModel() == null ||
                        getViewModel().getKpiBreakdownEnum() == null ? null :
                        getViewModel().getKpiBreakdownEnum();

                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_KPI_BREAKDOWN,
                        kpiBreakdownEnum == null ? null : kpiBreakdownEnum.getDeepLinkTag().toLowerCase(),
                        null,
                        isOverall ? OVER_ALL_PERFORMANCE_TAG : kpiBreakdownEnum == null ? null :
                                kpiBreakdownEnum.getParentKpi().getKey().toLowerCase(), false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callbacks.onScreenshotCreated(Panel.HISTOGRAM, storageUri,
                                        title,
                                        link,
                                        KpiBreakdownPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addKpiBreakdownPanelCallback(KpiBreakdownPanelCallback callbacks) {
        this.callbacks = callbacks;
    }


    @Override
    public String getShareAction() {
        if (getViewModel() != null && getViewModel().getKpiBreakdownEnum() != null) {
            return String.format(SHARE_ACTION, getViewModel().getKpiBreakdownEnum().getKey()
                            .replace(KpiBreakdownDataManager.SUB_TAG, ""),
                    getViewModel().getKpiBreakdownEnum().getBusinessUnitEnum().getShortName()).toLowerCase();
        }
        return "";
    }

    public interface KpiBreakdownPanelCallback extends PanelShareCallbacks {
        void onShareButtonClicked(PanelView panelView);
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void tooltipChanged(final String tooltipDate, final String value, final String change, final String isChangeHigher) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTooltipText(tooltipDate, value, change, isChangeHigher);
                        final long currentTime = SystemClock.elapsedRealtime();
                        if (getViewModel() != null && getViewModel().getKpiBreakdownEnum() != null && currentTime - lastAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                            Analytics.sendEvent(String.format(Analytics.KPI_TOOLTIP_GRAPH_IS_CLICKED, getViewModel().getKpiBreakdownEnum().getKey()
                                    .replace(KpiBreakdownDataManager.SUB_TAG, ""), getViewModel().getKpiBreakdownEnum().getBusinessUnitEnum().getShortName()).toLowerCase());
                            lastAnalyticsEventSentTime = currentTime;
                        }
                    }
                });
            }

        }

        @JavascriptInterface
        public void availabilityTooltipChanged(final String tooltipColor, final String kpiName, final String weekNumber, final String tooltipDate, final String tooltipTime, final String reset) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateAvailabilityTooltipText(tooltipDate, tooltipColor, tooltipTime, weekNumber, kpiName, reset);
                        final long currentTime = SystemClock.elapsedRealtime();
                        if (getViewModel() != null && getViewModel().getKpiBreakdownEnum() != null && currentTime - lastAvailabilityAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                            Analytics.sendEvent(String.format(Analytics.KPI_TOOLTIP_GRAPH_IS_CLICKED, getViewModel().getKpiBreakdownEnum().getKey()

                                    .replace(KpiBreakdownDataManager.SUB_TAG, ""), getViewModel().getKpiBreakdownEnum().getBusinessUnitEnum().getShortName()).toLowerCase());

                            lastAvailabilityAnalyticsEventSentTime = currentTime;
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void graphLegend(final String legend) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Context context = PrintOSApplication.getAppContext();
                        //spannable for graph legend
                        if (!TextUtils.isEmpty(legend)) {
                            SpannableStringUtils spannableLegend = new SpannableStringUtils(legend);
                            spannableLegend.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) context.getResources().getDimension(R.dimen.legend_font), ResourcesCompat.getColor(context.getResources(), R.color.c62, null), 0, legend.length());

                            tvLegend.setVisibility(View.VISIBLE);
                            tvLegend.setText(spannableLegend.getSpannableString());
                        }
                    }
                });
            }
        }

    }

    private void updateTooltipText(String tooltipDate, String value, String change, String isHigher) {

        Context context = PrintOSApplication.getAppContext();

        change = HPLocaleUtils.getDecimalString(Math.abs(Float.parseFloat(change)), true, 1);

        String arrow = String.valueOf(Boolean.parseBoolean(isHigher) ? Html.fromHtml(context.getResources().getString(R.string.tooltip_change_higher)) : Html.fromHtml(context.getResources().getString(R.string.tooltip_change_lower)));
        String changeWithArrow = arrow + change + "%";
        int color = Boolean.parseBoolean(isHigher) ? R.color.bar_green : R.color.bar_red;

        SpannableStringUtils spannableValue = new SpannableStringUtils(value);
        spannableValue.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_BOLD)), (int) context.getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), 0, value.length());
        SpannableString spannableValueString = spannableValue.getSpannableString();

        SpannableStringUtils spannableChange = new SpannableStringUtils(changeWithArrow);
        spannableChange.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_BOLD)), (int) context.getResources().getDimension(R.dimen.tooltip_change_font), ResourcesCompat.getColor(context.getResources(), color, null), 0, changeWithArrow.length());

        tooltipTitle.setText(TextUtils.concat(spannableValueString, " ", spannableChange.getSpannableString()));

        //spannable for tooltip date
        SpannableStringUtils spannableDate = new SpannableStringUtils(tooltipDate);
        spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_REGULAR)), (int) context.getResources().getDimension(R.dimen.tooltip_title_date_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length());

        this.tooltipDate.setText(spannableDate.getSpannableString());
    }

    private void updateAvailabilityTooltipText(String tooltipDate, String tooltipColor, String tooltipTime, String weekNumber, String kpiName, String reset) {

        SpannableStringUtils spannableDate;
        SpannableStringUtils spannableKpiName;
        String availabilityTitle;

        Context context = PrintOSApplication.getAppContext();

        if (Boolean.valueOf(reset)) {

            String totalUpTime = context.getResources().getString(R.string.reports_total_up_time);
            availabilityTitle = getResources().getString(R.string.reports_total_up_time) + " - " + weekNumber;

            //spannable for tooltip date
            spannableDate = new SpannableStringUtils(tooltipTime);
            spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) context.getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), 0, tooltipTime.length());

            //spannable for kpi name
            spannableKpiName = new SpannableStringUtils(availabilityTitle);
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.c62, null), 0, totalUpTime.length());
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), availabilityTitle.lastIndexOf(" - "), availabilityTitle.length());
        } else {

            //spannable for tooltip date
            spannableDate = new SpannableStringUtils(tooltipDate.concat("%  ") + "(" + tooltipTime + ")");
            spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) context.getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length() + 1);

            //spannable for kpi name
            availabilityTitle = kpiName + " - " + weekNumber;

            spannableKpiName = new SpannableStringUtils(availabilityTitle);
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) context.getResources().getDimension(R.dimen.availability_tooltip_title_font), Color.parseColor(tooltipColor), 0, kpiName.length());
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) context.getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(context.getResources(), R.color.tooltip_title_color, null), availabilityTitle.lastIndexOf(" - "), availabilityTitle.length());
        }

        tooltipTitle.setText(spannableKpiName.getSpannableString());
        this.tooltipDate.setText(spannableDate.getSpannableString());

    }

}


