package com.hp.printosmobile.presentation.modules.today;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.OrientationSupportBaseActivity;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment;
import com.hp.printosmobile.presentation.modules.todayhistogram.HistogramDetailsActivity;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.AnalyticsUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

/**
 * created by Anwar Asbah 1/17/2018
 */
public class HistogramBreakdownActivity extends OrientationSupportBaseActivity implements
        HistogramBreakDownRootFragment.HistogramBreakdownRootFragmentCallback {

    private static final String TAG = HistogramBreakdownActivity.class.getName();

    public static final String HISTOGRAM_VIEW_MODEL_PARAM = "view_model_param";
    public static final String FOCUSABLE_PANEL_PARAM = "focusable_panel_param";
    public static final String RESOLUTION_PARAM = "resolution_param";

    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    private Map<TodayHistogramViewModel.DetailTypeEnum, PanelView> panelViewMap = new HashMap<>();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarTitle;
    @Bind(R.id.fragment_container)
    FrameLayout fragmentContainer;

    private PanelView panelRequestedSharing;
    private TodayHistogramViewModel histogramViewModel = null;
    private String focusablePanel = null;
    private String resolution = null;

    private HistogramBreakDownRootFragment histogramBreakDownRootFragment;

    public static void startActivity(Activity activity, TodayHistogramViewModel viewModel, String focusablePanel,
                                     String resolution) {
        Intent intent = new Intent(activity, HistogramBreakdownActivity.class);

        Bundle bundle = new Bundle();
        if (viewModel != null) {
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, viewModel);
        }
        if (focusablePanel != null) {
            bundle.putString(FOCUSABLE_PANEL_PARAM, focusablePanel);
        }
        if (resolution != null) {
            bundle.putString(RESOLUTION_PARAM, resolution);
        }
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnalyticsUtils.sendEvents(this, Analytics.HISTOGRAM_BREAK_DOWN_SHOW_EVENT);
        HPLogger.d(TAG, "onCreate");

        Bundle args = getIntent().getExtras();
        if (args != null) {
            if (args.containsKey(HISTOGRAM_VIEW_MODEL_PARAM)) {
                histogramViewModel = (TodayHistogramViewModel) args.get(HISTOGRAM_VIEW_MODEL_PARAM);
            }
            if (args.containsKey(FOCUSABLE_PANEL_PARAM)) {
                focusablePanel = args.getString(FOCUSABLE_PANEL_PARAM);
            }
            if (args.containsKey(RESOLUTION_PARAM)) {
                resolution = args.getString(RESOLUTION_PARAM);
            }
        }

        histogramBreakDownRootFragment = HistogramBreakDownRootFragment.newInstance(histogramViewModel, focusablePanel, resolution, this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, histogramBreakDownRootFragment).commit();

        toolbarTitle.setText(getString(R.string.histogram_view_days_title_with_resolution));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (Exception e) {
            HPLogger.e(TAG, "landscape histogram on back pressed error ", e);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_histogram_break_down;
    }

    @Override
    public void onDestroy() {
        if (panelViewMap != null) {
            for (TodayHistogramViewModel.DetailTypeEnum panel : panelViewMap.keySet()) {
                panelViewMap.get(panel).onDestroy();
            }
        }
        super.onDestroy();
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0) {
                boolean allGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
                if (allGranted) {
                    performSharing(panelRequestedSharing);
                }
            }
        }
    }

    public void performSharing(PanelView panelView) {
        if (panelView != null) {
            panelView.sharePanel();
        }
    }

    @Override
    public boolean onLandscapeOrientation() {

        if (histogramBreakDownRootFragment != null) {
            View panel = histogramBreakDownRootFragment.getMostFocusablePanel();

            if (panel instanceof HistogramPanel) {
                HistogramDetailsActivity.startActivity(this, ((HistogramPanel) panel).getViewModel(),
                        ((HistogramPanel) panel).getBreakdownType(),
                        ((HistogramPanel) panel).getHistogramResolution().name());
                return true;
            }
        }

        return false;
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {
        if (panelView != null) {
            panelRequestedSharing = panelView;

            if (permissionsRequested()) {
                performSharing(panelRequestedSharing);
            }
        }
    }
}
