package com.hp.printosmobile.presentation.modules.reports;

import android.graphics.Typeface;

import com.github.mikephil.charting.components.XAxis;

/**
 * Represents a base configuration class for different kinds of charts.
 * The concrete configuration classes will extend this in order to have the common behavior.
 *
 * @Author : Osama Taha
 * Created on 6/25/2015.
 */
public class ChartConfiguration {
    /**
     * Chart name.
     */
    private String name;
    /**
     * Chart description
     */
    private String description;
    /**
     * Text to be displayed when the chart is empty.
     */
    private String noDataTextDescription;
    /**
     * flag that indicates if pinch-zoom is enabled. if true, both x and y axis
     * can be scaled with 2 fingers, if false, x and y axis can be scaled
     * separately
     */
    private boolean pinchZoomEnabled = true;
    /**
     * if true, dragging is enabled for the chart
     */
    private boolean dragEnabled = true;
    /**
     * Set this to false to disable all gestures and touches on the chart,
     */
    private boolean touchEnabled = true;
    /**
     * flag that indicates if the scaling is enabled
     */
    private boolean scaleXEnabled = true;
    private boolean scaleYEnabled = true;
    /**
     * value highlighting is enabled which means that values can
     * be highlighted programmatically or by touch gesture.
     */
    private boolean highlightEnabled = false;
    /**
     * flag that indicates if highlighting per dragging over a fully zoomed out
     * chart is enabled
     */
    private boolean highlightPerDragEnabled = true;
    /**
     * flag that indicates if double tap zoom is enabled or not
     */
    private boolean doubleTapToZoomEnabled;
    /**
     * The background color of the chart.
     */
    private int backgroundColor;
    private int animationDuration = 1000;
    private float leftAxisMaxValue;
    private boolean drawGridColorEnabled;
    private float yAxisTextSize = 10;
    /**
     * Flag that indicates if x-axis is enabled or not.
     */
    private boolean xAxisEnabled;
    /**
     * The position of the x-labels relative to the chart.
     */
    private XAxis.XAxisPosition xAxisPosition;
    /**
     * The text size of the x-axis labels.
     */
    private float xAxisTextSize = 11;
    /**
     * The typeface used for the x-axis labels.
     */
    private Typeface xAxisTypeFace;
    /**
     * The text color to use for the x-axis labels.
     */
    private int xAxisTextColor;
    /**
     * The space that should be left out (in characters) between the x-axis
     * labels.
     */
    private int spaceBetweenLabels;
    /**
     * Flag that indicates if right-y axis is enabled or not.
     */
    private boolean yLeftAxisEnabled;
    /**
     * Flag that indicates if left-y axis is enabled or not.
     */
    private boolean yRightAxisEnabled;
    /**
     * The typeface used for the y-axis labels.
     */
    private Typeface yAxisTypeFace;
    /**
     * The text color to use for the y-axis labels.
     */
    private int yAxisTextColor;
    /**
     * flag indicating if the grid lines for the axis should be drawn.
     */
    private boolean drawGridLinesEnabled;
    /**
     * flag indicating if to show the data-set values.
     */
    private boolean drawValues;
    /**
     * Flag that indicates if the legend is enabled or not.
     */
    private boolean legendEnabled;
    private int xAxisLineColor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoDataTextDescription() {
        return noDataTextDescription;
    }

    public void setNoDataTextDescription(String noDataTextDescription) {
        this.noDataTextDescription = noDataTextDescription;
    }

    public boolean isPinchZoomEnabled() {
        return pinchZoomEnabled;
    }

    public void setPinchZoomEnabled(boolean pinchZoomEnabled) {
        this.pinchZoomEnabled = pinchZoomEnabled;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(int animationDuration) {
        this.animationDuration = animationDuration;
    }

    public float getLeftAxisMaxValue() {
        return leftAxisMaxValue;
    }

    public void setLeftAxisMaxValue(float leftAxisMaxValue) {
        this.leftAxisMaxValue = leftAxisMaxValue;
    }

    public boolean isDrawGridColorEnabled() {
        return drawGridColorEnabled;
    }

    public void setDrawGridColorEnabled(boolean drawGridColorEnabled) {
        this.drawGridColorEnabled = drawGridColorEnabled;
    }

    public boolean isDragEnabled() {
        return dragEnabled;
    }

    public void setDragEnabled(boolean dragEnabled) {
        this.dragEnabled = dragEnabled;
    }

    public boolean isTouchEnabled() {
        return touchEnabled;
    }

    public void setTouchEnabled(boolean touchEnabled) {
        this.touchEnabled = touchEnabled;
    }

    public boolean isScaleXEnabled() {
        return scaleXEnabled;
    }

    public void setScaleXEnabled(boolean scaleXEnabled) {
        this.scaleXEnabled = scaleXEnabled;
    }

    public boolean isScaleYEnabled() {
        return scaleYEnabled;
    }

    public void setScaleYEnabled(boolean scaleYEnabled) {
        this.scaleYEnabled = scaleYEnabled;
    }

    public boolean isHighlightEnabled() {
        return highlightEnabled;
    }

    public void setHighlightEnabled(boolean highlightEnabled) {
        this.highlightEnabled = highlightEnabled;
    }

    public boolean isHighlightPerDragEnabled() {
        return highlightPerDragEnabled;
    }

    public void setHighlightPerDragEnabled(boolean highlightPerDragEnabled) {
        this.highlightPerDragEnabled = highlightPerDragEnabled;
    }

    public boolean isDoubleTapToZoomEnabled() {
        return doubleTapToZoomEnabled;
    }

    public void setDoubleTapToZoomEnabled(boolean doubleTapToZoomEnabled) {
        this.doubleTapToZoomEnabled = doubleTapToZoomEnabled;
    }

    public int getSpaceBetweenLabels() {
        return spaceBetweenLabels;
    }

    public void setSpaceBetweenLabels(int spaceBetweenLabels) {
        this.spaceBetweenLabels = spaceBetweenLabels;
    }

    public XAxis.XAxisPosition getxAxisPosition() {
        return xAxisPosition;
    }

    public void setxAxisPosition(XAxis.XAxisPosition xAxisPosition) {
        this.xAxisPosition = xAxisPosition;
    }

    public boolean isxAxisEnabled() {
        return xAxisEnabled;
    }

    public void setxAxisEnabled(boolean xAxisEnabled) {
        this.xAxisEnabled = xAxisEnabled;
    }

    public boolean isyLeftAxisEnabled() {
        return yLeftAxisEnabled;
    }

    public void setyLeftAxisEnabled(boolean yLeftAxisEnabled) {
        this.yLeftAxisEnabled = yLeftAxisEnabled;
    }

    public boolean isyRightAxisEnabled() {
        return yRightAxisEnabled;
    }

    public void setyRightAxisEnabled(boolean yRightAxisEnabled) {
        this.yRightAxisEnabled = yRightAxisEnabled;
    }

    public Typeface getxAxisTypeFace() {
        return xAxisTypeFace;
    }

    public void setxAxisTypeFace(Typeface xAxisTypeFace) {
        this.xAxisTypeFace = xAxisTypeFace;
    }

    public Typeface getyAxisTypeFace() {
        return yAxisTypeFace;
    }

    public void setyAxisTypeFace(Typeface yAxisTypeFace) {
        this.yAxisTypeFace = yAxisTypeFace;
    }

    public float getxAxisTextSize() {
        return xAxisTextSize;
    }

    public void setxAxisTextSize(float xAxisTextSize) {
        this.xAxisTextSize = xAxisTextSize;
    }

    public float getyAxisTextSize() {
        return yAxisTextSize;
    }

    public void setyAxisTextSize(float yAxisTextSize) {
        this.yAxisTextSize = yAxisTextSize;
    }

    public int getxAxisTextColor() {
        return xAxisTextColor;
    }

    public void setxAxisTextColor(int xAxisTextColor) {
        this.xAxisTextColor = xAxisTextColor;
    }

    public int getyAxisTextColor() {
        return yAxisTextColor;
    }

    public void setyAxisTextColor(int yAxisTextColor) {
        this.yAxisTextColor = yAxisTextColor;
    }

    public boolean isDrawGridLinesEnabled() {
        return drawGridLinesEnabled;
    }

    public void setDrawGridLinesEnabled(boolean drawGridLinesEnabled) {
        this.drawGridLinesEnabled = drawGridLinesEnabled;
    }

    public boolean isDrawValues() {
        return drawValues;
    }

    public void setDrawValues(boolean drawValues) {
        this.drawValues = drawValues;
    }

    public boolean isLegendEnabled() {
        return legendEnabled;
    }

    public void setLegendEnabled(boolean legendEnabled) {
        this.legendEnabled = legendEnabled;
    }

    public int getxAxisLineColor() {
        return xAxisLineColor;
    }

    public void setxAxisLineColor(int xAxisLineColor) {
        this.xAxisLineColor = xAxisLineColor;
    }
}
