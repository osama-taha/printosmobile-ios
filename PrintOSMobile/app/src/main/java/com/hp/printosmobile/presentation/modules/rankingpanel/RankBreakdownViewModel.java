package com.hp.printosmobile.presentation.modules.rankingpanel;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel.RankAreaType;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 5/30/2018.
 */
public class RankBreakdownViewModel implements Serializable, Comparable<RankBreakdownViewModel> {

    private static final String OFFSET_INCREASE_FORMAT = "%s %s";
    private static final String OFFSET_DECREASE_FORMAT = "%s %s";
    private static final String OFFSET_EQUAL_FORMAT = "=";

    public enum RankValueTypeEnum {

        RANK_VALUE_NO_DATA,
        RANK_VALUE_LOWER_THIRD,
        RANK_VALUE_NO_OFFSET,
        RANK_VALUE_WITH_OFFSET;

        public Spannable getFormattedTooltip(Context context, RankHistoryItem model) {

            int rankValue = model.getRankValue();
            int total = model.getTotal();
            int offset = model.getOffset();

            switch (this) {
                case RANK_VALUE_LOWER_THIRD:
                    return new SpannableString(context.getString(R.string.ranking_breakdown_graph_tooltip_lower_third));
                case RANK_VALUE_NO_OFFSET:
                    return getSpannable(context, rankValue, total, offset, false);
                case RANK_VALUE_WITH_OFFSET:
                    return getSpannable(context, rankValue, total, offset, true);
                default:
                    return new SpannableString(context.getString(R.string.ranking_breakdown_graph_tooltip_no_data));
            }
        }

        public String getWeekNumber(Context context, RankHistoryItem model) {
            return context.getString(R.string.week_number, model.getWeek());
        }

        private Spannable getSpannable(Context context, int rankValue, int total, int offset, boolean showOffset) {

            String rank = HPLocaleUtils.getLocalizedValue(rankValue);
            String outOfLabel = HPLocaleUtils.getLocalizedValue(total);

            String tooltipString;
            String offsetString = null;
            int offsetColorId = 0;

            if (showOffset) {
                if (offset == 0) {
                    offsetString = OFFSET_EQUAL_FORMAT;
                    offsetColorId = R.color.c103b;
                } else if (offset < 0) {
                    String arrow = String.valueOf(Html.fromHtml(context.getResources().getString(R.string.tooltip_change_lower)));
                    offsetString = String.format(OFFSET_DECREASE_FORMAT, arrow,
                            HPLocaleUtils.getLocalizedValue(Math.abs(offset)));
                    offsetColorId = R.color.kpi_score_state_average_progress_color;
                } else {
                    String arrow = String.valueOf(Html.fromHtml(context.getResources().getString(R.string.tooltip_change_higher)));
                    offsetString = String.format(OFFSET_INCREASE_FORMAT, arrow,
                            HPLocaleUtils.getLocalizedValue(offset));
                    offsetColorId = R.color.kpi_score_state_great_progress_color;
                }

                tooltipString = context.getString(R.string.ranking_breakdown_graph_tooltip,
                        rank,
                        outOfLabel,
                        offsetString);
            } else {
                tooltipString = context.getString(R.string.ranking_breakdown_graph_tooltip_without_offset,
                        rank,
                        outOfLabel);
            }

            SpannableString tooltipSpan = new SpannableString(tooltipString);

            //Rank position's span.
            int startInd = tooltipString.indexOf(rank);
            int endInd = startInd + rank.length();

            int color = ResourcesCompat.getColor(context.getResources(), R.color.c2, null);
            tooltipSpan.setSpan(new ForegroundColorSpan(color), startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tooltipSpan.setSpan(new RelativeSizeSpan(1.5f), startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            Typeface hpRegular = TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_REGULAR);
            tooltipSpan.setSpan(new CustomTypefaceSpan("", hpRegular), startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            if (showOffset) {
                //Rank offset's span.
                startInd = tooltipString.indexOf(offsetString);
                endInd = startInd + offsetString.length();

                color = ResourcesCompat.getColor(context.getResources(), offsetColorId, null);
                tooltipSpan.setSpan(new ForegroundColorSpan(color), startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }

            return tooltipSpan;
        }
    }

    private RankAreaType type;
    private String name;
    private List<RankHistoryItem> historyList;

    public static final Comparator<RankBreakdownViewModel> RANK_BREAKDOWN_COMPARATOR = new Comparator<RankBreakdownViewModel>() {
        @Override
        public int compare(RankBreakdownViewModel eL, RankBreakdownViewModel eR) {
            if (eL == null) {
                if (eR == null) {
                    return 0;
                }
                return 1;
            }
            if (eR == null) {
                return -1;
            }
            return eL.compareTo(eR);
        }
    };

    public RankAreaType getType() {
        return type;
    }

    public void setType(RankAreaType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RankHistoryItem> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<RankHistoryItem> historyList) {
        this.historyList = historyList;
    }

    @Override
    public int compareTo(@NonNull RankBreakdownViewModel model) {
        RankAreaType typeL = getType();
        RankAreaType typeR = model.getType();

        return typeL.ordinal() - typeR.ordinal();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(type, name, historyList);
        }
        return 0;
    }

    public static class RankHistoryItem implements Comparable<RankHistoryItem>, Serializable {

        private int rankValue;
        private RankValueTypeEnum valueTypeEnum;
        private Date date;
        private boolean isLowerThanThreshold;
        private boolean isLowerThanTotalThreshold;
        private boolean isTop;
        private int lowerThirdValue;
        private String name;
        private int offset;
        private int total;
        private int week;
        private int year;

        public int getRankValue() {
            return rankValue;
        }

        public void setRankValue(int rankValue) {
            this.rankValue = rankValue;
        }

        public Spannable getFormattedTooltip(Context context) {
            return valueTypeEnum.getFormattedTooltip(context, this);
        }

        public String getTooltipWeekNumber(Context context) {
            return valueTypeEnum.getWeekNumber(context, this);
        }

        public void setRankValueTypeEnum(RankValueTypeEnum typeEnum) {
            this.valueTypeEnum = typeEnum;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public boolean isLowerThanThreshold() {
            return isLowerThanThreshold;
        }

        public void setLowerThanThreshold(boolean lowerThanThreshold) {
            isLowerThanThreshold = lowerThanThreshold;
        }

        public boolean isLowerThanTotalThreshold() {
            return isLowerThanTotalThreshold;
        }

        public void setLowerThanTotalThreshold(boolean lowerThanTotalThreshold) {
            isLowerThanTotalThreshold = lowerThanTotalThreshold;
        }

        public boolean isTop() {
            return isTop;
        }

        public void setTop(boolean top) {
            isTop = top;
        }

        public int getLowerThirdValue() {
            return lowerThirdValue;
        }

        public void setLowerThirdValue(int lowerThirdValue) {
            this.lowerThirdValue = lowerThirdValue;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getWeek() {
            return week;
        }

        public void setWeek(int week) {
            this.week = week;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        @Override
        public int compareTo(@NonNull RankHistoryItem item) {
            int yearL = getYear();
            int weekNumberL = getWeek();
            int yearR = item.getYear();
            int weekNumberR = item.getWeek();

            if (yearL == yearR) {
                return weekNumberL - weekNumberR;
            }

            return yearL - yearR;
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(rankValue, valueTypeEnum, date, isLowerThanThreshold, isLowerThanTotalThreshold, isTop, lowerThirdValue, name, offset, total, week, year);
            }
            return 0;
        }

    }
}
