package com.hp.printosmobile.presentation.modules.settings.wizard;

/**
 * Created by Osama
 */
public interface IWizardFragment {

    boolean isValid();

    String getFragmentName();

}
