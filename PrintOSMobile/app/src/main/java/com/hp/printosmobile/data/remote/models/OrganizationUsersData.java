package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama on 9/27/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationUsersData {

    @JsonProperty("total")
    private Integer total;
    @JsonProperty("limit")
    private Integer limit;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("users")
    private List<OrganizationUser> users = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * @return The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return The limit
     */
    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit The limit
     */
    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return The offset
     */
    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    /**
     * @param offset The offset
     */
    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     * @return The contexts
     */
    @JsonProperty("users")
    public List<OrganizationUser> getUsers() {
        return users;
    }

    /**
     * @param users The users
     */
    @JsonProperty("users")
    public void setContexts(List<OrganizationUser> users) {
        this.users = users;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class OrganizationUser {

        @JsonProperty("userId")
        private String userId;
        @JsonProperty("firstName")
        private String firstName;
        @JsonProperty("lastName")
        private String lastName;
        @JsonProperty("displayName")
        private String displayName;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        /**
         * @return The userId
         */
        @JsonProperty("userId")
        public String getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        @JsonProperty("userId")
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        @JsonProperty("firstName")
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        @JsonProperty("firstName")
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonProperty("lastName")
        public String getLastName() {
            return lastName;
        }

        @JsonProperty("lastName")
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @JsonProperty("displayName")
        public String getDisplayName() {
            return displayName;
        }

        @JsonProperty("displayName")
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}
