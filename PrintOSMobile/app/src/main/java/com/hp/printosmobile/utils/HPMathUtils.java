package com.hp.printosmobile.utils;

import android.content.Context;

import com.hp.printosmobile.R;

/**
 * Created by anwar asbah on 1/25/2018.
 */

public class HPMathUtils {

    private HPMathUtils() {
    }

    public static int get1000Power(int maxValue) {
        return maxValue == 0 ? 0 : ((int) Math.log10(maxValue) / 3);
    }

    public static String get1000PowerLabel(Context context, int power) {
        String powerLabel;
        switch (power) {
            case 0:
                powerLabel = "";
                break;
            case 1:
                powerLabel = context.getString(R.string.thousands);
                break;
            case 2:
                powerLabel = context.getString(R.string.millions);
                break;
            default:
                powerLabel = context.getString(R.string.billions);
                break;
        }
        return power == 0 ? "" : context.getString(R.string.all_values_in, powerLabel);
    }
}
