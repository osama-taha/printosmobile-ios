package com.hp.printosmobile.presentation.modules.productionsnapshot.indigo;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class IndigoProductionPanel extends PanelView<ProductionViewModel> implements SharableObject {

    public static final String TAG = IndigoProductionPanel.class.getSimpleName();

    @Bind(R.id.production_snapshot_content)
    View productionSnapshotContent;
    @Bind(R.id.print_queue_jobs_in_queue_text_view)
    TextView jobsInQueueValue;
    @Bind(R.id.text_completed_jobs_value)
    TextView completedJobsValue;

    IndigoProductionPanelCallback callback;

    public IndigoProductionPanel(Context context) {
        super(context);
    }

    public IndigoProductionPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IndigoProductionPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public Spannable getTitleSpannable() {
        return new SpannableStringBuilder(getContext().getString(R.string.production_panel_title));
    }

    @Override
    public int getContentView() {
        return R.layout.production_snapshot_content_indigo;
    }

    @Override
    public void updateViewModel(ProductionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callback, true);
            return;
        }

        jobsInQueueValue.setText(getLocalizedValue(viewModel.getJobsInQueue()));
        completedJobsValue.setText(getLocalizedValue(viewModel.getJobsCompleted()));

        DeepLinkUtils.respondToIntentForPanel(this, callback, false);
    }

    @Override
    public void bindViews() {

    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addCallback(IndigoProductionPanelCallback callback) {
        this.callback = callback;
    }

    @OnClick(R.id.job_in_queue_container)
    public void onJobsInQueueClicked() {
        if (callback != null) {
            callback.onJobsInQueueClicked(DeviceViewModel.JobType.QUEUED);
        }
    }

    @OnClick(R.id.job_completed_container)
    public void onJobsCompletedClicked() {
        if (callback != null && PrintOSPreferences.getInstance(getContext()).isCompletedJobsFeatureEnabled()
                && getViewModel() != null && getViewModel().isHasCompletedJobs()) {
            callback.onJobsInQueueClicked(DeviceViewModel.JobType.COMPLETED);
        }
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callback != null) {
            callback.onShareButtonClicked(Panel.INDIGO_PRODUCTION);
        }
    }

    @Override
    public void sharePanel() {
        if (callback != null && productionSnapshotContent != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            final Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(), null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);
                                callback.onScreenshotCreated(Panel.INDIGO_PRODUCTION, storageUri,
                                        getContext().getString(R.string.share_jobs_today_title),
                                        link,
                                        IndigoProductionPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    private String getLocalizedValue(int value) {
        return HPLocaleUtils.getLocalizedValue(value);
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_JOBS_TODAY;
    }

    @Override
    public String getPanelTag() {
        return Panel.INDIGO_PRODUCTION.getDeepLinkTag();
    }

    public interface IndigoProductionPanelCallback extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {
        void onJobsInQueueClicked(DeviceViewModel.JobType jobType);
    }
}
