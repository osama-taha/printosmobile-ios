package com.hp.printosmobile.presentation.modules.shared;

/**
 * Created by Anwar Asbah on 2/1/2018.
 */
public enum MeasureTypeEnum {
    MIXED("mixed", "Impressions"),
    LENGTH("length", "LM"),
    SHEETS("sheet", "Sheets"),
    IMPRESSIONS("impressions", "Impressions");

    String key;
    String requestUnitSystem;

    MeasureTypeEnum(String key, String requestUnitSystem) {
        this.key = key;
        this.requestUnitSystem = requestUnitSystem;
    }

    public String getRequestUnitSystem() {
        return requestUnitSystem;
    }

    public static MeasureTypeEnum from(String key) {
        if (key == null) {
            return MIXED;
        }

        for (MeasureTypeEnum measureTypeEnum : MeasureTypeEnum.values()) {
            if (key.equalsIgnoreCase(measureTypeEnum.key)) {
                return measureTypeEnum;
            }
        }

        return MIXED;
    }
}
