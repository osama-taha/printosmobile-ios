package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha
 */
public class WizardDialog extends DialogFragment {

    public static final String TAG = WizardDialog.class.getSimpleName();

    @Bind(R.id.wizard_dialog_message)
    TextView descriptionTextView;

    @Bind(R.id.wizard_dialog_dismiss)
    TextView dismissButton;

    private WizardDialogCallbacks callback;

    public static WizardDialog getInstance(WizardDialogCallbacks callback) {
        WizardDialog nameDialog = new WizardDialog();
        Bundle args = new Bundle();
        nameDialog.setArguments(args);
        nameDialog.callback = callback;
        return nameDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.PopupStyle);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.wizard_dialog, container, false);
        ButterKnife.bind(this, rootView);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_WIZARD_POPUP);
        Analytics.sendEvent(Analytics.EVENT_WIZARD_POPUP_SHOWN);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setCancelable(false);

        initView();

    }

    private void initView() {

        String coins = HPLocaleUtils.getLocalizedValue(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getWizardReward());

        String text = getString(R.string.wizard_dialog_get_coins_message, coins);

        //TODO
        int startInd = text.indexOf(coins);
        int endInd = startInd + coins.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.5f), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        descriptionTextView.setText(spannableString);

        String nextTimeText = getString(R.string.wizard_popup_dismiss);
        SpannableString spannable = new SpannableString(nextTimeText);
        spannable.setSpan(new UnderlineSpan(), 0, nextTimeText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        dismissButton.setText(spannable);

    }

    @OnClick(R.id.wizard_dialog_proceed)
    public void onSendButtonClicked() {

        HPLogger.d(TAG, "Wizard dialog - Send button clicked.");
        Analytics.sendEvent(Analytics.EVENT_WIZARD_POPUP_CLICK_START);

        if (callback != null) {
            callback.onSendButtonClicked(getActivity());
        }

        dismissAllowingStateLoss();
    }

    @OnClick(R.id.wizard_dialog_dismiss)
    public void onDismissClicked() {

        HPLogger.d(TAG, "Wizard dialog - dismiss button clicked.");
        Analytics.sendEvent(Analytics.EVENT_WIZARD_POPUP_CLICK_NEXT_TIME);

        dismissAllowingStateLoss();
    }


    @Override
    public void dismissAllowingStateLoss() {
        try {
            super.dismissAllowingStateLoss();
            HPLogger.d(TAG, "dismissAllowingStateLoss.");

            if (callback != null) {
                callback.onNextTimeButtonClicked();
            }
        } catch (Exception e) {

        }
    }

    public interface WizardDialogCallbacks {

        void onSendButtonClicked(Activity activity);

        void onNextTimeButtonClicked();

    }
}
