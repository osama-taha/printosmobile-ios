package com.hp.printosmobile.presentation.modules.statedistribution;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 2/18/2018.
 */
public interface StateDistributionView extends MVPView {

    void onPressStateDistributionRetrieved(StateDistributionViewModel viewModel);

}
