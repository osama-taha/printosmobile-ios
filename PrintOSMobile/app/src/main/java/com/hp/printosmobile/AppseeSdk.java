package com.hp.printosmobile;

import android.content.Context;
import android.text.TextUtils;

import com.appsee.Appsee;
import com.appsee.AppseeListener;
import com.appsee.AppseeScreenDetectedInfo;
import com.appsee.AppseeSessionEndedInfo;
import com.appsee.AppseeSessionEndingInfo;
import com.appsee.AppseeSessionStartedInfo;
import com.appsee.AppseeSessionStartingInfo;
import com.crashlytics.android.Crashlytics;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Arrays;
import java.util.List;

/**
 * Created by OsamaTaha on 3/7/17.
 */

public class AppseeSdk implements AppseeListener {

    private static final String TAG = AppseeSdk.class.getSimpleName();

    public static final String SCREEN_LANGUAGES = "Languages";
    public static final String SCREEN_UNIT_SYSTEM = "Unit System";
    public static final String SCREEN_NPS = "NPS";
    public static final String SCREEN_SEND_INVITES = "Send Invites";
    public static final String HPID_POPUP = "HPID_POPUP";
    public static final String SCREEN_BEAT_COINS = "Beat coins";
    public static final String SCREEN_NOTIFICATION_SETTINGS = "NotificationSettings";
    public static final String SCREEN_SETTINGS = "Settings";
    public static final String SCREEN_NOTIFICATIONS_LIST = "NotificationsList";
    public static final String SCREEN_HOME = "Home";
    public static final String SCREEN_DEVICES = "Devices";
    public static final String SCREEN_INSIGHTS = "Insights";
    public static final String SCREEN_RANKING_LEADERBOARD = "RankingLeaderboard";
    public static final String SCREEN_INDIGO_DEVICE_DETAILS = "IndigoDeviceDetails";
    public static final String SCREEN_LATEX_DEVICE_DETAILS = "LatexDeviceDetails";
    public static final String SCREEN_REPORTS = "Reports";
    public static final String JOBS = "Jobs";
    public static final String SCREEN_NAME_PERSONAL_ADVISOR_POPUP = "PersonalAdvisorPopup";
    public static final String SCREEN_WIZARD_FRAGMENT = "WizardFragment";
    public static final String SCREEN_FILTER = "Filter";
    public static final String SCREEN_INVITES = "Invites to PrintOS";
    public static final String SCREEN_WIZARD_POPUP = "Wizard popup";
    public static final String SCREEN_ADD_USER_IMAGE_POPUP = "Add user image popup";


    private static final List<String> IGNORED_COMPONENTS = Arrays.asList("Dialog");


    public static final String EVENT_INVITE_VIA_SUGGESTED_ACTION = "PRINTOS_INVITE_VIA_SUGGESTED";
    public static final String EVENT_INVITE_VIA_CONTACT_ACTION = "PRINTOS_INVITE_VIA_CONTACT";
    public static final String EVENT_INVITE_VIA_EMAIL_ACTION = "PRINTOS_INVITE_VIA_EMAIL";
    public static final String EVENT_INVITE_USER_REWARD_ACTION = "PRINTOS_USER_GOT_REWARD";

    private static AppseeSdk sharedInstance;
    private final Context context;

    public static AppseeSdk getInstance(Context context) {
        if (sharedInstance == null) {

            HPLogger.d(TAG, "Init Appsee sdk.");

            sharedInstance = new AppseeSdk(context);
            Appsee.addAppseeListener(sharedInstance);
        }
        return sharedInstance;
    }

    private AppseeSdk(Context context) {
        this.context = context;
    }

    public void init() {
        Appsee.start(BuildConfig.DEBUG ? context.getString(R.string.appsee_test_account_api_key)
                : context.getString(R.string.appsee_production_account_api_key));
        setUserId();
    }

    public void setUserId() {

        UserViewModel userViewModel = PrintOSPreferences.getInstance(context).getUserInfo();
        if (!TextUtils.isEmpty(userViewModel.getUserId())) {
            Appsee.setUserId(PrintOSPreferences.getInstance(context).getUserInfo().getUserId());
        } else {
            Appsee.setUserId("");
        }
    }

    public void startScreen(String screenName) {
        Appsee.startScreen(screenName);
    }

    @Override
    public void onAppseeSessionStarting(AppseeSessionStartingInfo sessionStartingInfo) {

    }

    @Override
    public void onAppseeSessionStarted(AppseeSessionStartedInfo sessionStartedInfo) {
        // Crashlytics (session-level integration)
        String crashlyticsAppseeId = Appsee.generate3rdPartyId("Crashlytics", false);
        Crashlytics.getInstance().core.setString("AppseeSessionUrl",
                "https://dashboard.appsee.com/3rdparty/crashlytics/" + crashlyticsAppseeId);
    }

    @Override
    public void onAppseeSessionEnding(AppseeSessionEndingInfo sessionEndingInfo) {

    }

    @Override
    public void onAppseeSessionEnded(AppseeSessionEndedInfo sessionEndedInfo) {

    }

    @Override
    public void onAppseeScreenDetected(AppseeScreenDetectedInfo screenDetectedInfo) {

        // Ignore some screen elements like dialog from being detected automatically.
        if (IGNORED_COMPONENTS.contains(screenDetectedInfo.getScreenName())) {
            screenDetectedInfo.setScreenName(null);
        }

    }

    public void sendEvent(String eventName, String value) {
        String event = String.format("%s_%s", eventName, value);
        sendEvent(event);
    }

    public void sendEvent(String eventName) {
        Appsee.addEvent(eventName);
    }

}
