package com.hp.printosmobile;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.webkit.WebView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.core.communications.remote.ApiConfig;
import com.hp.printosmobilelib.core.logging.HPLogConfig;
import com.hp.printosmobilelib.core.logging.HPLogEvent;
import com.hp.printosmobilelib.core.logging.HPLogListener;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.NetworkUtils;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;

/**
 * An Entry point for PrintOS app.
 *
 * @author Osama Taha
 */
public class PrintOSApplication extends MultiDexApplication {

    private static final String TAG = PrintOSApplication.class.getSimpleName();
    private static ApiServicesProvider apiServicesProvider;
    private static ApiServicesProvider apiServicesProviderWithAuthenticator;
    private static Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnits;
    private static Context appContext;
    private static Application instance;
    private static long startTimeMilliSeconds;
    private static boolean fromBackground;

    public static Application getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        new WebView(this).destroy();

        setAppContextAndInstance(this);

        initServicesProvider(this, null);

        PrintOSPreferences.getInstance(this).saveUserInfo();

        initLogger();
        initFabric();

        HPLocaleUtils.configureAppLanguage(this, Locale.getDefault());

        initSDKs();

        checkGooglePlayServices();

        HPLogger.w(TAG, "Launch PrintOS application..");
        HPLogger.i(TAG, AppUtils.getDeviceInfo(this));

        String locale = PrintOSPreferences.getInstance(appContext).getLanguageCode();
        HPLogger.i(TAG, String.format("app is now open. locale used: %s",
                PrintOSPreferences.getInstance(appContext).getLanguageCode()));
        Analytics.sendEvent(Analytics.LOCAL_EVENT, locale);
    }

    private static synchronized void setAppContextAndInstance(PrintOSApplication printOSApplication) {
        appContext = printOSApplication;
        instance = printOSApplication;
    }

    private void initFabric() {

        //Init answers sdk regardless the build type.
        if (BuildConfig.DEBUG) {

            Fabric.with(this, new Answers());

        } else {

            Fabric.with(this, new CrashlyticsCore(), new Crashlytics());
            Fabric.with(this, new Answers());

            UserViewModel userViewModel = PrintOSPreferences.getInstance(this).getUserInfo();
            String userId = userViewModel.getUserId();

            if (userId != null && !userId.equals("")) {

                String userIdentifier = userId;
                String serverName = PrintOSPreferences.getInstance(this).getServerName();
                if (serverName != null && !serverName.equals(getString(R.string.default_server_item))) {
                    userIdentifier = String.format("%s-%s", serverName, userId);
                }

                HPLogger.d(TAG, "init Crashlytics with Identifier " + userIdentifier);

                Crashlytics.setUserIdentifier(userIdentifier);
            }
        }

    }

    private void checkGooglePlayServices() {

        int status = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(this);

        HPLogger.d(TAG, String.format("%s = %s", Analytics.GOOGLE_PLAY_SERVICES_STATUS_ACTION,
                AppUtils.getGooglePlayServicesAvailabilityStatusMessage(status)));

    }

    public static void initServicesProvider(Context context, String updatedUrl) {

        String serverUrl = updatedUrl != null ? updatedUrl : PrintOSPreferences.getInstance(context).getServerUrl();

        apiServicesProvider = new ApiServicesProvider(context, new ApiConfig(serverUrl));
        apiServicesProviderWithAuthenticator = new ApiServicesProvider(context, new ApiConfig(serverUrl), false);

        HPLogConfig.getInstance(context).setDispatchService(apiServicesProvider.getLogService());

    }

    public static void initSDKs() {

        Application application = PrintOSApplication.getInstance();
        String serverUrl = PrintOSPreferences.getInstance(application).getServerUrl();
        initSDKs(serverUrl);
    }

    public static void initSDKs(String serverUrl) {

        Application application = PrintOSApplication.getInstance();

        Analytics.TrackerName trackerName = Analytics.TrackerName.TESTING_TRACKER;
        IntercomSdk.IntercomProject intercomProject = IntercomSdk.IntercomProject.TESTING;

        if (serverUrl != null) {

            String stgServer = application.getString(R.string.staging_server);
            String productionServer = application.getString(R.string.production_server);

            if (serverUrl.equals(stgServer)) {
                trackerName = Analytics.TrackerName.STG_TRACKER;
            } else if (serverUrl.equals(productionServer)) {
                trackerName = Analytics.TrackerName.RELEASE_TRACKER;
                intercomProject = IntercomSdk.IntercomProject.PRODUCTION;
            }
        }

        Analytics.init(trackerName, application);

        IntercomSdk.getInstance(application).init(intercomProject);
        IntercomSdk.getInstance(application).login();

        AppseeSdk.getInstance(application).init();
        AppseeSdk.getInstance(application).setUserId();

        // Initialize Branch automatic session tracking
        Branch.getAutoInstance(application);

    }

    public static ApiServicesProvider getApiServicesProvider() {
        return apiServicesProvider;
    }

    public static ApiServicesProvider getApiServicesProviderWithAuthenticator() {
        return apiServicesProviderWithAuthenticator;
    }

    public static void setBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsList) {
        businessUnits = businessUnitsList;
    }

    public static Map<BusinessUnitEnum, BusinessUnitViewModel> getBusinessUnits() {
        return businessUnits;
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static boolean isGooglePlayServicesEnabled() {
        return true;
    }

    private void initLogger() {

        HPLogConfig.getInstance(this)
                .enabled(true)
                .setLogToFileEnabled(true)
                // wait for this many log events before flushing ...
                .batchSize(30)
                //Max logs to keep before login.
                .setPreloginMaxSize(100)
                //Max failed logs to keep.
                .setFailedLogsMaxSize(200)
                .setAppVersion(BuildConfig.VERSION_NAME)
                .setDispatchService(apiServicesProviderWithAuthenticator.getLogService())
                .logListener(new HPLogListener() {
                    public void flushLogs(final List<HPLogEvent> logEvents) {
                        HPLogger.logD(TAG, "send logs to server " + logEvents.size());
                    }
                });

        String logFilePath = Environment.getExternalStorageDirectory().toString()
                + File.separator;
        String fileName = "printos_log.txt";
        String fileNameFormat = "printos_log%i.txt";
        HPLogger.getInstance(this).init(logFilePath, fileName, fileNameFormat);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        HPLocaleUtils.configureAppLanguage(this, newConfig.locale);
        new WebView(this).destroy();
    }

    public static void setStartTimeMilliSeconds(boolean fromBackground) {
        setStartTimeMilliSeconds(System.currentTimeMillis(), fromBackground);
    }

    public static void setStartTimeMilliSeconds(long startTimeMilliSeconds, boolean fromBackground) {
        PrintOSApplication.startTimeMilliSeconds = startTimeMilliSeconds;
        PrintOSApplication.fromBackground = fromBackground;
    }

    public static long getStartTimeMilliSeconds() {
        return startTimeMilliSeconds;
    }

    public static void logStartup() {
        //Log app startup if needed.
        if (getStartTimeMilliSeconds() > 0) {

            String nslookupResult = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLastSavedLocation();
            String networkType = NetworkUtils.getNetworkType(PrintOSApplication.getAppContext());

            long currentTimeMillis = System.currentTimeMillis();
            String label = fromBackground ? "back from background" : "startup time";
            HPLogger.i(TAG, String.format("%1$s: %2$s ms , nslookup: %3$s , network type: %4$s", label,
                    String.valueOf(currentTimeMillis - getStartTimeMilliSeconds()), nslookupResult, networkType));

            setStartTimeMilliSeconds(0, fromBackground);
        }

    }

    public static void resetStartupTime() {
        startTimeMilliSeconds = System.currentTimeMillis();
    }


}
