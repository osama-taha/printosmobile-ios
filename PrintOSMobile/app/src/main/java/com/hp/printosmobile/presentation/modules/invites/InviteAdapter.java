package com.hp.printosmobile.presentation.modules.invites;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class InviteAdapter extends RecyclerView.Adapter<InviteAdapter.ContactViewHolder>
        implements Filterable {

    private static final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};

    Context context;
    List<InviteContactViewModel> originalContactList;
    List<InviteContactViewModel> filteredContactList;
    private Filter filter;
    private InvitesListFragment.InvitesType type;

    public InviteAdapter(Context context, List<InviteContactViewModel> contentList, InvitesListFragment.InvitesType type) {
        originalContactList = contentList == null ? new ArrayList<InviteContactViewModel>() : contentList;
        getFilter().filter("");

        this.originalContactList = contentList;
        this.context = context;
        this.type = type;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invites_contact_item, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        final InviteContactViewModel contact = filteredContactList.get(i);

        String name = contact.getName();
        name = TextUtils.isEmpty(name) ? contact.getEmail() : name;
        name = TextUtils.isEmpty(name) ? "" : name;
        contactViewHolder.contactName.setText(name);
        if (contact.getPhoto() != null) {
            contactViewHolder.contactImage.setImageBitmap(contact.getPhoto());
        } else {
            contactViewHolder.contactInitials.setText(contact.getInitials());
            contactViewHolder.contactInitials.setBackground(getColorDrawable(contact.getColor()));
        }

        boolean hasName = !TextUtils.isEmpty(contact.getName());
        if (hasName) {
            contactViewHolder.contactEmail.setText(contact.getEmail());
        }
        contactViewHolder.contactEmail.setVisibility(hasName ? View.VISIBLE : View.GONE);

        contactViewHolder.contactImage.setVisibility(contact.getPhoto() == null ? View.GONE : View.VISIBLE);
        contactViewHolder.contactInitials.setVisibility(contact.getPhoto() == null ? View.VISIBLE : View.GONE);

        contactViewHolder.inviteContactButton.setText(contact.getInviteStatus().label);
        contactViewHolder.inviteContactButton.setTextColor(ResourcesCompat.getColor(context.getResources(),
                contact.getInviteStatus().textColor, null));
        contactViewHolder.inviteButtonLayout.setBackground(ResourcesCompat.getDrawable(context.getResources(),
                contact.getInviteStatus().drawable, null));
        contactViewHolder.inviteButtonLeftImage.setVisibility(View.GONE);
        if (contact.getInviteStatus().drawableLeft != -1) {
            contactViewHolder.inviteButtonLeftImage.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),
                    contact.getInviteStatus().drawableLeft, null));
            contactViewHolder.inviteButtonLeftImage.setVisibility(View.VISIBLE);
        }

        contactViewHolder.inviteButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (contact.getInviteStatus()) {
                    case TO_BE_INVITED:
                        InviteUtils.inviteContact(contact, type, false, false);
                        break;
                    case INVITED:
                        if (context instanceof BaseActivity) {
                            InviteUtils.onInvitedButtonClicked(context, ((BaseActivity) context)
                                    .getSupportFragmentManager(), contact, type);
                        }
                        break;
                    case ON_BOARD:
                        Analytics.sendEvent(Analytics.INVITE_CLICK_SHARE_APP);

                        AppUtils.sendInvites(context, AnswersSdk.INVITE_METHOD_INVITES_SCREEN);
                        break;
                    default:
                        break;
                }
            }
        });

        boolean isLoading = contact.getInviteStatus() == InviteContactViewModel.InviteStatus.LOADING;
        contactViewHolder.spinner.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        contactViewHolder.inviteContactButton.setVisibility(isLoading ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return filteredContactList == null ? 0 : filteredContactList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String filterString = constraint.toString().toLowerCase();

                    FilterResults results = new FilterResults();

                    final List<InviteContactViewModel> list = originalContactList;

                    int count = list.size();
                    final ArrayList<InviteContactViewModel> filteredList = new ArrayList<>();

                    String filterableString;

                    for (int i = 0; i < count; i++) {
                        filterableString = list.get(i).getName() + list.get(i).getEmail();
                        if (filterableString.toLowerCase().contains(filterString)) {
                            filteredList.add(list.get(i));
                        }
                    }

                    results.values = filteredList;
                    results.count = filteredList.size();

                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    filteredContactList = (List<InviteContactViewModel>) results.values;
                    notifyDataSetChanged();
                }
            };
        }
        return filter;
    }

    public void setContactList(List<InviteContactViewModel> inviteContactViewModels) {
        setContactList(inviteContactViewModels, "");
    }

    public void setContactList(List<InviteContactViewModel> inviteContactViewModels, String filer) {
        this.originalContactList = inviteContactViewModels;
        getFilter().filter(filer);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.invite_button_left_image)
        ImageView inviteButtonLeftImage;
        @Bind(R.id.invite_contact_button_layout)
        View inviteButtonLayout;
        @Bind(R.id.contact_name)
        TextView contactName;
        @Bind(R.id.contact_image)
        ImageView contactImage;
        @Bind(R.id.contact_initials)
        TextView contactInitials;
        @Bind(R.id.invite_contact_button)
        TextView inviteContactButton;
        @Bind(R.id.contact_email)
        TextView contactEmail;
        @Bind(R.id.spinner)
        View spinner;

        ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private Drawable getColorDrawable(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(CORNER_RADII);
        shape.setColor(color);
        return shape;
    }

}