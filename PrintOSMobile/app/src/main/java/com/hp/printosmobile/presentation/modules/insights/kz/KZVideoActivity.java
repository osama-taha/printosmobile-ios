package com.hp.printosmobile.presentation.modules.insights.kz;

import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.halilibo.bettervideoplayer.subtitle.CaptionsView;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.insights.InsightsUtils;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZFooter;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import butterknife.Bind;
import butterknife.OnClick;

public class KZVideoActivity extends BaseActivity implements KZItemDetailsView {

    @Bind(R.id.app_bar_layout)
    View appTopBar;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.bottom_bar_view)
    View bottomBarView;
    @Bind(R.id.bvp)
    BetterVideoPlayer videoPlayer;
    @Bind(R.id.youtube_player_view)
    YouTubePlayerView youTubePlayerView;
    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;
    @Bind(R.id.error_text_view)
    TextView errorMessageTextView;
    @Bind(R.id.retry_btn)
    Button retryButton;
    @Bind(R.id.kz_footer)
    KZFooter kzFooter;

    private KZItemDetailsPresenter presenter;
    private BusinessUnitEnum businessUnitEnum;
    private KZItem kzItem;
    private boolean isLandscape;
    private static KZItemDetailsCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        kzItem = (KZItem) getIntent().getSerializableExtra(Constants.IntentExtras.KZ_ITEM_EXTRA);
        businessUnitEnum = (BusinessUnitEnum) getIntent().getSerializableExtra(Constants.IntentExtras.BUSINESS_UNIT_EXTRA);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, KZSupportedFormat.MP4.getCardBackgroundColorResId()));
        }

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(KZSupportedFormat.MP4.getCardBackgroundColorResId())));
            actionBar.setTitle(kzItem.getName());
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            toolbar.getNavigationIcon().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        }

        if ((Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) && kzItem.getProvider() == KZItem.Provider.YOUTUBE) {
            hideLoading();
            showYoutubePlayerNotSupportedView();
        } else {
            getVideoAsset();
        }

        setKZFooter(kzItem);
    }

    private void setKZFooter(final KZItem kzItem) {
        kzFooter.setKZProps(kzItem.getId(), businessUnitEnum.getKzName(), kzItem.getName());
        kzFooter.setIsFavorite(kzItem.getUserAssetPref().getFavorite() == null ? false : kzItem.getUserAssetPref().getFavorite());
        if (kzItem.getUserAssetPref() != null && kzItem.getUserAssetPref().getRating() != null && kzItem.getUserAssetPref().getRating() != 0)
            kzFooter.setIsLiked(kzItem.getUserAssetPref().getRating());
        kzFooter.setEventListener(new KZFooter.UserInteractionsEventListener() {
            @Override
            public void onFavoritePressed(boolean isFavorite) {
                kzItem.getUserAssetPref().setFavorite(isFavorite);
                if (callback != null) {
                    callback.onFavoritePressed(kzItem);
                }
            }

            @Override
            public void onLikePressed(int rating) {
                kzItem.getUserAssetPref().setRating(rating);
                if (callback != null) {
                    callback.onLikePressed(kzItem);
                }
            }

            @Override
            public void onDislikePressed(int rating) {
                kzItem.getUserAssetPref().setRating(rating);
                if (callback != null) {
                    callback.onDislikePressed(kzItem);
                }
            }

            @Override
            public void onSharePressed(String name, String link) {
                ShareUtils.onScreenshotCreated(KZVideoActivity.this, null,
                        KZVideoActivity.this.getString(R.string.corrective_actions_share_insights_kz_item, name),
                        link,
                        new SharableObject() {
                            @Override
                            public String getShareAction() {
                                return AnswersSdk.SHARE_CONTENT_TYPE_KNOWLEDGE_ZONE;
                            }
                        });
            }
        });
    }

    public static void addKZItemDetailsCallback(KZItemDetailsCallback kzItemDetailsCallback) {
        callback = kzItemDetailsCallback;
    }

    private void showYoutubePlayerNotSupportedView() {
        showErrorView();
        errorMessageTextView.setText(getString(R.string.kz_error_message_youtube_player_not_supported));
        retryButton.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kzvideo;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoPlayer != null) {
            videoPlayer.pause();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        HPUIUtils.setVisibility(!isLandscape, appTopBar, bottomBarView, kzFooter);

        if (isLandscape) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            videoPlayer.showToolbar();
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            videoPlayer.hideToolbar();
            youTubePlayerView.exitFullScreen();
        }


    }

    private void initVideoPlayer() {

        videoPlayer.setAutoPlay(true);

        videoPlayer.getToolbar().setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        videoPlayer.getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotateScreen(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });

        videoPlayer.hideToolbar();
        videoPlayer.getToolbar().setTitle(kzItem.getName());
        videoPlayer.enableSwipeGestures();
        videoPlayer.toggleControls();

    }


    private void initYouTubePlayerView(final String url) {

        getLifecycle().addObserver(youTubePlayerView);

        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(final YouTubePlayer youTubePlayer) {

                youTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        loadVideo(youTubePlayer, url);
                    }
                });

                addFullScreenListenerToPlayer();
            }


        }, true);

    }

    private void loadVideo(YouTubePlayer youTubePlayer, String url) {

        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
            youTubePlayer.loadVideo(InsightsUtils.extractVideoIdFromUrl(url), 0);
        } else {
            youTubePlayer.cueVideo(InsightsUtils.extractVideoIdFromUrl(url), 0);
        }

        youTubePlayerView.getPlayerUIController().setVideoTitle(kzItem.getName());
    }

    private void addFullScreenListenerToPlayer() {

        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }

            @Override
            public void onYouTubePlayerExitFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        });

    }

    private void rotateScreen(int requestedOrientation) {
        setRequestedOrientation(requestedOrientation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                youTubePlayerView.exitFullScreen();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        }, 1000);
    }

    private void getVideoAsset() {

        if (kzItem != null && TextUtils.isEmpty(kzItem.getExternalUrl())) {
            presenter = new KZItemDetailsPresenter();
            presenter.attachView(this);
            presenter.getAssetData(kzItem, businessUnitEnum);
        } else {
            hideLoading();
            if (kzItem.getProvider() == KZItem.Provider.YOUTUBE) {

                youTubePlayerView.setVisibility(View.VISIBLE);
                initYouTubePlayerView(kzItem.getExternalUrl());

            } else {

                videoPlayer.setVisibility(View.VISIBLE);

                initVideoPlayer();

                videoPlayer.setSource(Uri.parse(kzItem.getExternalUrl()));

                if (!TextUtils.isEmpty(kzItem.getSubtitleUrl())) {
                    videoPlayer.setCaptions(Uri.parse(kzItem.getSubtitleUrl()), CaptionsView.CMime.WEBVTT);
                }
            }
        }
    }

    public static Intent createIntent(Context context, KZItem kzItem, BusinessUnitEnum businessUnitEnum) {
        Intent intent = new Intent(context, KZVideoActivity.class);
        intent.putExtra(Constants.IntentExtras.KZ_ITEM_EXTRA, kzItem);
        intent.putExtra(Constants.IntentExtras.BUSINESS_UNIT_EXTRA, businessUnitEnum);
        return intent;
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }


    @Override
    public void showErrorView() {
        videoPlayer.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishedGettingAssetData(Object result) {

        KZAssetModel asset = (KZAssetModel) result;

        if (asset.getKZItem().getProvider() == KZItem.Provider.YOUTUBE) {

            youTubePlayerView.setVisibility(View.VISIBLE);
            initYouTubePlayerView(asset.getExternalUrl());

        } else {

            videoPlayer.setVisibility(View.VISIBLE);

            initVideoPlayer();

            videoPlayer.setSource(Uri.parse(asset.getExternalUrl()));

            if (!TextUtils.isEmpty(asset.getSubtitleUrl())) {
                videoPlayer.setCaptions(Uri.parse(asset.getSubtitleUrl()), CaptionsView.CMime.WEBVTT);
            }
        }

    }

    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        presenter.getAssetData(kzItem, businessUnitEnum);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (isLandscape) {
            rotateScreen(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }
    }

}
