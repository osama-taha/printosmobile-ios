package com.hp.printosmobile.presentation.modules.invites;

import android.text.TextUtils;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.InviteContactInputData;
import com.hp.printosmobile.data.remote.models.InviteSendUserIDData;
import com.hp.printosmobile.data.remote.models.InviteUserRoleData;
import com.hp.printosmobile.data.remote.models.InvitesListData;
import com.hp.printosmobile.data.remote.services.InvitesService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.intercom.okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 11/6/17.
 */
public class InvitesDataManager {

    private static final int INVITES_ROLE_CACHING_TIME = 60 * 60 * 1000;
    private static final String INVITATION_TYPE = "USER_IN_ORG";
    private static final String NAME_SEPERATOR = " ";

    private InvitesDataManager() {
        //private constructor
    }

    public static Observable<InvitesViewModel> getInvites() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InvitesService invitesService = serviceProvider.getInvitesService();

        return invitesService.getInvitesList().map(new Func1<Response<InvitesListData>, InvitesViewModel>() {
            @Override
            public InvitesViewModel call(Response<InvitesListData> invitesListDataResponse) {
                if (invitesListDataResponse != null && invitesListDataResponse.isSuccessful()) {
                    return parseInvitesData(invitesListDataResponse.body());
                }
                return null;
            }
        });
    }

    private static InvitesViewModel parseInvitesData(InvitesListData invitesListData) {

        if (invitesListData == null) {
            return null;
        }

        InvitesViewModel invitesViewModel = new InvitesViewModel();
        invitesViewModel.setCanInvite(invitesListData.getLinks() != null &&
                invitesListData.getLinks().getNewLinks() != null &&
                invitesListData.getLinks().getNewLinks().getHref() != null);

        List<String> addEmails = new ArrayList<>(); //to avoid duplicate historical records
        List<InvitesViewModel.InvitedContact> invitedContacts = new ArrayList<>();
        List<InvitesListData.Invite> invites = invitesListData.getInviteList();
        if (invites != null) {
            for (int i = 0; i < invites.size(); i++) { //to make sure going through records by order.
                InvitesListData.Invite invite = invites.get(i);
                if (invite != null && invite.getEmail() != null && !addEmails.contains(invite.getEmail().toLowerCase())) {
                    InviteContactViewModel.InviteStatus inviteStatus = InviteContactViewModel.InviteStatus.from(invite.getStatus());
                    if (inviteStatus != InviteContactViewModel.InviteStatus.UNKNOWN) {
                        InvitesViewModel.InvitedContact invitedContact = new InvitesViewModel.InvitedContact();
                        invitedContact.setInviteEmail(invite.getEmail());
                        invitedContact.setInviteID(invite.getId());
                        invitedContact.setInviteStatus(inviteStatus);
                        invitedContact.setLangaugeCode(invite.getLanguageCode());
                        invitedContact.setRoleID(invite.getRoleId());
                        invitedContact.setRoleType(invite.getRoleType());

                        String firstName = TextUtils.isEmpty(invite.getFirstName()) ? "" : invite.getFirstName();
                        String lastName = TextUtils.isEmpty(invite.getLastName()) ? "" : invite.getLastName();
                        String name = !TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName) ?
                                firstName + " " + lastName : firstName + lastName;
                        invitedContact.setName(name);

                        if (invite.getLinks() != null) {
                            invitedContact.setRevokeEnabled(invite.getLinks().getRevoke() != null && invite.getLinks().getRevoke().getHref() != null);
                            invitedContact.setResendEnabled(invite.getLinks().getResend() != null && invite.getLinks().getResend().getHref() != null);
                        }

                        invitedContacts.add(invitedContact);
                        addEmails.add(invite.getEmail().toLowerCase());
                    }
                }
            }
        }
        invitesViewModel.setInvitedContacts(invitedContacts);

        return invitesViewModel;
    }

    public static Observable<List<InviteUserRoleViewModel>> getInviteUserRoles() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InvitesService invitesService = serviceProvider.getInvitesService();

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        final InviteUserRoleData data = preferences.getInviteUserRoleCache();
        Long lastCachingTime = preferences.getInviteUserRoleCachingTime();

        if (System.currentTimeMillis() - lastCachingTime > INVITES_ROLE_CACHING_TIME || data == null) {
            return invitesService.getInviteUserRoles().map(new Func1<Response<InviteUserRoleData>, List<InviteUserRoleViewModel>>() {
                @Override
                public List<InviteUserRoleViewModel> call(Response<InviteUserRoleData> inviteUserRoleDataResponse) {
                    if (inviteUserRoleDataResponse != null && inviteUserRoleDataResponse.isSuccessful()) {
                        return parseInvitesUserRoleData(inviteUserRoleDataResponse.body());
                    }
                    return null;
                }
            });
        }

        return Observable.fromCallable(new Callable<List<InviteUserRoleViewModel>>() {
            @Override
            public List<InviteUserRoleViewModel> call() {
                return parseInvitesUserRoleData(data);
            }
        });
    }

    private static List<InviteUserRoleViewModel> parseInvitesUserRoleData(InviteUserRoleData inviteUserRoleData) {

        if (inviteUserRoleData == null) {
            return null;
        }

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        preferences.saveInviteUserRoleCache(inviteUserRoleData);
        preferences.setInviteUserRoleCachingTime(System.currentTimeMillis());

        InviteUserRoleViewModel selectedRole = preferences.getSelectedInviteUserRole();
        boolean hasRole = selectedRole != null && !selectedRole.getId().isEmpty() && !selectedRole.getName().isEmpty();

        List<InviteUserRoleViewModel> inviteUserRoleViewModels = new ArrayList<>();
        List<InviteUserRoleData.InvitationOrgType> invitationOrgTypes = inviteUserRoleData.getUserRoles();
        if (invitationOrgTypes != null) {
            for (InviteUserRoleData.InvitationOrgType type : invitationOrgTypes) {
                if (type != null && type.getInvitationType() != null && type.getInvitationType().equalsIgnoreCase(INVITATION_TYPE)) {
                    List<InviteUserRoleData.UserRole> roles = type.getUserRoles();
                    if (roles != null) {
                        for (int i = 0; i < roles.size(); i++) {
                            InviteUserRoleData.UserRole role = roles.get(i);
                            if (role != null) {
                                InviteUserRoleViewModel model = new InviteUserRoleViewModel();
                                model.setName(role.getName());
                                model.setId(role.getId());
                                model.setOrgType(role.getOrganizationType());
                                inviteUserRoleViewModels.add(model);

                                //default role is first returned one
                                if (!hasRole && i == 0) {
                                    selectedRole = model;
                                }

                                if (!hasRole && role.getName() != null && role.getName().equalsIgnoreCase(UserRoleFragment.DEFAULT_ROLE_TYPE)) {
                                    selectedRole = model;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (selectedRole != null) {
            preferences.setSelectedInviteUserRole(selectedRole);
        }
        return inviteUserRoleViewModels;
    }

    public static Observable<InvitesListData.Invite> invite(InviteContactViewModel contact, InviteUserRoleViewModel userRole,
                                                            boolean resend) {
        String language = resend ? contact.getLanguage() : PrintOSPreferences.getInstance(
                PrintOSApplication.getAppContext()).getInvitesLanguage();

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InvitesService invitesService = serviceProvider.getInvitesService();

        String firstName = "_";
        String lastName = "";
        if (contact.getName() != null) {
            String[] nameSplit = contact.getName().split(NAME_SEPERATOR);
            firstName = nameSplit[0];
            firstName = firstName.matches(Constants.NAME_ALLOWED_CHARS_REGEX_FORMAT) ? firstName : "";
            int lastInd = nameSplit.length - 1;
            if (lastInd > 0) {
                lastName = nameSplit[lastInd];
                lastName = lastName.matches(Constants.NAME_ALLOWED_CHARS_REGEX_FORMAT) ? lastName : "";
            }
        }

        InviteContactInputData data = new InviteContactInputData();
        data.setEmail(contact.getEmail());
        data.setFirstName(firstName);
        data.setInvitationType(INVITATION_TYPE);
        data.setLanguageCode(language);
        data.setLastName(lastName);
        data.setOrganizationType(resend ? contact.getRoleType() : userRole.getOrgType());
        data.setRoleId(resend ? contact.getRoleId() : userRole.getId());

        Observable<Response<InvitesListData.Invite>> observable;
        if (resend) {
            data.setId(contact.getInviteId());
            observable = invitesService.resendInvite(data);
        } else {
            observable = invitesService.inviteContact(data);
        }

        return observable.map(new Func1<Response<InvitesListData.Invite>, InvitesListData.Invite>() {
            @Override
            public InvitesListData.Invite call(Response<InvitesListData.Invite> inviteResponse) {
                if (inviteResponse != null && inviteResponse.isSuccessful()) {
                    return inviteResponse.body();
                }
                return null;
            }
        });
    }

    public static Observable<String> sendInviteUserID(String id) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InvitesService invitesService = serviceProvider.getInvitesService();

        InviteSendUserIDData data = new InviteSendUserIDData();
        data.setInvitationId(id);

        return invitesService.sendInviteUserID(data).map(new Func1<Response<ResponseBody>, String>() {
            @Override
            public String call(Response<ResponseBody> responseBodyResponse) {
                if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                    return "";
                }
                return null;
            }
        });
    }

    public static Observable<String> revokeInvite(String id) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InvitesService invitesService = serviceProvider.getInvitesService();

        return invitesService.revokeInvite(id).map(new Func1<Response<ResponseBody>, String>() {
            @Override
            public String call(Response<ResponseBody> responseBodyResponse) {
                if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                    return "";
                }
                return null;
            }
        });
    }

}