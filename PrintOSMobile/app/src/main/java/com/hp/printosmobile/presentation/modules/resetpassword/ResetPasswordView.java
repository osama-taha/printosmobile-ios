package com.hp.printosmobile.presentation.modules.resetpassword;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobilelib.core.communications.remote.APIException;

/**
 * @author Anwar Asbah
 */
public interface ResetPasswordView extends MVPView {

    void showLoading();

    void hideLoading();

    void onResetPasswordSuccess();

    void onResetPasswordError(APIException error);

    void setViewEnabled(boolean isEnabled);
}