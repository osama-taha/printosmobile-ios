package com.hp.printosmobile.data.remote.models;

public enum AccountType {

    HP, CHANNEL, PSP, NOT_SET;

    public static AccountType from(String type) {

        if (type != null) {
            type = type.toUpperCase();
            if (type.contains("HP")) {
                return HP;
            } else if (type.contains("PSP")) {
                return PSP;
            } else if (type.contains("CHANNEL")) {
                return CHANNEL;
            }
        }

        return NOT_SET;
    }
}