package com.hp.printosmobile.presentation.modules.about;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.HiddenSettingsDialog;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

import butterknife.Bind;
import butterknife.OnClick;

public class AboutActivity extends BaseActivity implements HiddenSettingsDialog.HiddenSettingsDialogCallback {

    private static final CharSequence DEVICE_TOKEN = "Device Token";
    private static final String TAG = AboutActivity.class.getName();

    @Bind(R.id.tvAboutAppVersion)
    TextView appVersionNameTextView;
    @Bind(R.id.tvAboutAppName)
    TextView appNameTextView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;
    @Bind(R.id.device_token_text_view)
    TextView deviceTokenTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_ABOUT_EVENT);

        initView();

    }

    private Toast toastView;

    private void initView() {

        appNameTextView.setText(R.string.printos_app_name);
        appVersionNameTextView.setText(getString(R.string.about_version, BuildConfig.VERSION_NAME));

        //support hidden settings
        appVersionNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                versionNotifyLabelClick();
            }
        });

        String token = DeviceUtils.getDeviceIdMD5(this);
        if (token != null) {
            deviceTokenTextView.setText(getString(R.string.about_device_token, DeviceUtils.getDeviceIdMD5(this)));
        }
        deviceTokenTextView.setVisibility(token == null ? View.GONE : View.VISIBLE);


        toolbarDisplayName.setText(getString(R.string.about_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    long lastClickTime = 0;
    int clicksCounter = 0;
    final static int TRIGGER_END_OF_COUNT = 7;
    final static long RESET_COUNTER_TIMEOUT = 1500;

    private void versionNotifyLabelClick() {
        HPLogger.d(TAG, "user clicked the about version label. current count = " + clicksCounter);
        long time = System.currentTimeMillis();
        if (time - lastClickTime > RESET_COUNTER_TIMEOUT) {
            lastClickTime = time;
            clicksCounter = 1;
        } else {
            lastClickTime = time;
            clicksCounter++;


            if (clicksCounter == TRIGGER_END_OF_COUNT - 1)
                HPUIUtils.displayToastAndCancelCurrent(AboutActivity.this, toastView, getString(R.string.ignore_flags_one_more), Toast.LENGTH_SHORT);
            else if (clicksCounter == TRIGGER_END_OF_COUNT - 2)
                HPUIUtils.displayToastAndCancelCurrent(AboutActivity.this, toastView, getString(R.string.ignore_flags_two_more), Toast.LENGTH_SHORT);
            else if (clicksCounter == TRIGGER_END_OF_COUNT - 3)
                HPUIUtils.displayToastAndCancelCurrent(AboutActivity.this, toastView, getString(R.string.ignore_flags_three_more), Toast.LENGTH_SHORT);
            else if (clicksCounter == TRIGGER_END_OF_COUNT) {
                DialogManager.displayHiddenSettingsPopup(this);
                clicksCounter = 0;
            }
        }
    }


    private void reloadApp(Context ctx) {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.device_token_text_view)
    public void onDeviceTokenTextViewClicked() {

        String token = DeviceUtils.getDeviceIdMD5(this);
        if (token == null) {
            return;
        }

        HPLogger.d(TAG, "user clicked token - <" + token + ">");

        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        final android.content.ClipData clipData = android.content.ClipData
                .newPlainText(DEVICE_TOKEN, token);
        clipboardManager.setPrimaryClip(clipData);

        HPUIUtils.displayToast(this, getString(R.string.about_screen_device_token_copy_msg));
    }

    @OnClick(R.id.privacy_button)
    public void openPrivacy() {
        Analytics.sendEvent(Analytics.OPEN_SOURCE_CLICKED_ACTION);

        Intent intent = new Intent(this, EulaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, false);
        intent.putExtras(bundle);
        startActivity(intent);

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.EULA_SCREEN_LABEL);
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }

    private void ignoreFlagsClick() {

        HPUIUtils.displayToastAndCancelCurrent(AboutActivity.this, toastView,
                !PrintOSPreferences.getInstance(getApplicationContext()).shouldIgnoreFlags() ?
                        getString(R.string.ignore_flags_ignoring_flags) :
                        getString(R.string.ignore_flags_considering_flags), Toast.LENGTH_SHORT);

        // PrintOSPreferences.getInstance(getApplicationContext()).setVersionCheckingTime(System.currentTimeMillis());
        PrintOSPreferences.getInstance(getApplicationContext()).setShouldIgnoreFlags(!PrintOSPreferences.getInstance(getApplicationContext()).shouldIgnoreFlags());
        HPLogger.d(TAG, "use clicked hidden setting to open all flags (Ignoreflags). current state = " + PrintOSPreferences.getInstance(getApplicationContext()).shouldIgnoreFlags());
        HPLogger.d(TAG, "Reloading app with new Ignoreflags status");
        reloadApp(getApplicationContext());
    }

    @Override
    public void onIgnoreFlagsClicked() {
        ignoreFlagsClick();
    }

    @Override
    public void onSwitchLocationClicked() {

        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setFakeLocation("fake");
        reloadApp(getApplicationContext());

    }

}
