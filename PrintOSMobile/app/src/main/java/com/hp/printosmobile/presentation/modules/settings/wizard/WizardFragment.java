package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.HPTextView;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * Created by Osama
 */
public class WizardFragment extends BaseFragment implements WizardCollectCoinsFragment.WizardCollectCoinsFragmentCallbacks, UserPersonalInfoFragment.UserPersonalInfoFragmentsCallbacks {

    private static final String TAG = WizardFragment.class.getSimpleName();
    private static final String KEY_COLLECT_COINS = "KEY_COLLECT_COINS_ENABLED";

    @Bind(R.id.viewpager)
    HPViewPager viewPager;
    @Bind(R.id.bottom_layout)
    View bottomLayout;
    @Bind(R.id.page_sentence_text_view)
    HPTextView bottomTextView;
    @Bind(R.id.wizard_next_page)
    HPTextView nextButton;
    @Bind(R.id.loading_indicator)
    ProgressBar loadingIndicator;

    private PagerAdapter adapter;
    private WizardFragmentCallbacks callbacks;
    private WizardViewModel wizardViewModel;
    private boolean collectCoinsEnabled = true;
    private boolean stepsCompleted;

    public WizardFragment() {
        // Required empty public constructor
    }

    public static WizardFragment getInstance(boolean collectCoinsEnabled) {

        WizardFragment wizardFragment = new WizardFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_COLLECT_COINS, collectCoinsEnabled);
        wizardFragment.setArguments(args);

        return wizardFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_WIZARD_FRAGMENT);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof WizardFragmentCallbacks) {
            callbacks = (WizardFragmentCallbacks) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement WizardFragmentCallbacks.");

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    private void initView() {

        if (getArguments() != null && getArguments().containsKey(KEY_COLLECT_COINS)) {
            collectCoinsEnabled = getArguments().getBoolean(KEY_COLLECT_COINS);
        }

        wizardViewModel = new WizardViewModel();

        UserData.User userData = UserProfileManager.getInstance().getUserData();
        PreferencesData preferencesData = UserProfileManager.getInstance().getPreferencesData();

        if (userData == null) {
            return;
        }

        wizardViewModel.setFirstName(userData.getFirstName());
        wizardViewModel.setLastName(userData.getLastName());
        wizardViewModel.setPhoneNumber(userData.getPrimaryPhone());

        if (userData.getAddress() != null) {
            wizardViewModel.setAddress1(userData.getAddress().getAddressLine1());
            wizardViewModel.setAddress2(userData.getAddress().getAddressLine2());
            wizardViewModel.setRegion(userData.getAddress().getRegion());
            wizardViewModel.setCity(userData.getAddress().getLocality());
            wizardViewModel.setCountry(userData.getAddress().getCountry());
            wizardViewModel.setPostalCode(userData.getAddress().getPostalCode());
        }

        if (preferencesData != null && preferencesData.getGeneral() != null) {
            wizardViewModel.setTimeZone(preferencesData.getGeneral().getTimeZone());
            wizardViewModel.setUnitSystem(preferencesData.getGeneral().getUnitSystem());
        }

        adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(UserPersonalInfoFragment.getInstance(wizardViewModel, this));
        adapter.addFragment(UserAdditionalInfoFragment.getInstance(wizardViewModel));
        adapter.addFragment(UserAddressInfoFragment.getInstance(wizardViewModel));

        if (collectCoinsEnabled) {
            adapter.addFragment(WizardCollectCoinsFragment.getInstance(this));
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getFragments().size());
        viewPager.setScrollingEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                onWizardPageSelected(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        onWizardPageSelected(viewPager.getCurrentItem());

        if (callbacks != null) {
            callbacks.onWizardFragmentAttached();
        }

        enableNextButton(false);
        showLoadingIndicator(true);
        enableNextButton(true);
        showLoadingIndicator(false);


        Fragment firstFragment = adapter.getCount() > 0 ? adapter.getFragments().get(0) : null;

        if (firstFragment != null && firstFragment instanceof IWizardFragment) {
            sendViewWizardEvent((IWizardFragment) firstFragment);
        }

    }

    private void onWizardPageSelected(int position) {

        Fragment fragment = adapter.getItem(position);

        if (fragment == null) {
            return;
        }

        AppseeSdk.getInstance(getActivity()).startScreen(fragment.getClass().getSimpleName());

        HPUIUtils.setVisibility(!(fragment instanceof WizardCollectCoinsFragment), bottomLayout);

        if (!collectCoinsEnabled) {

            bottomTextView.setText("");

            if (fragment instanceof UserAddressInfoFragment) {
                nextButton.setText(R.string.wizard_done);
            } else {
                nextButton.setText(R.string.wizard_next_page_text);
            }
            return;

        } else {

            HPUIUtils.setVisibility(!(fragment instanceof WizardCollectCoinsFragment), bottomLayout);

        }

        switch (position) {
            case 0:
                bottomTextView.setText(R.string.wizard_page_sentence_message_1);
                break;
            case 1:
                bottomTextView.setText(R.string.wizard_page_sentence_message_2);
                break;
            case 2:
                bottomTextView.setText(R.string.wizard_page_sentence_message_3);
                break;
            default:
                bottomTextView.setText("");
        }

    }

    @OnClick(R.id.wizard_next_page)
    void onNextButtonClicked() {

        Fragment fragment = adapter == null ? null : adapter.getItem(viewPager.getCurrentItem());

        if (fragment != null && fragment instanceof IWizardFragment && ((IWizardFragment) fragment).isValid()) {

            int currentPageIndex = viewPager.getCurrentItem();
            boolean isLast = currentPageIndex == adapter.getCount() - 1;

            int nextPageIndex = viewPager.getCurrentItem() + 1;
            nextPageIndex = nextPageIndex < adapter.getCount() ? nextPageIndex : -1;

            Fragment nextFragment;
            if (nextPageIndex > -1) {
                nextFragment = adapter.getItem(nextPageIndex);
                if (nextFragment instanceof WizardCollectCoinsFragment) {
                    sendData(false);
                } else {

                    viewPager.setCurrentItem(nextPageIndex);

                    sendViewWizardEvent((IWizardFragment) nextFragment);
                }


                sendWizardClickNextEvent(fragment);

            }

            if (isLast) {
                sendData(true);
            }

        }

    }

    private void sendData(final boolean isLastPage) {

        HPLogger.d(TAG, "wizard finish clicked.");
        Analytics.sendEvent(Analytics.EVENT_WIZARD_FINISH);

        showLoadingIndicator(true);
        enableNextButton(false);

        UserProfileManager.getInstance().sendWizardResults(wizardViewModel, new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                UserProfileManager.getInstance().showErrorSavingPreferences();
                showLoadingIndicator(false);
                enableNextButton(true);
            }

            @Override
            public void onNext(Boolean success) {

                showLoadingIndicator(false);
                enableNextButton(true);

                if (!success) {
                    UserProfileManager.getInstance().showErrorSavingPreferences();
                } else {

                    if (isLastPage) {
                        stepsCompleted = true;
                        if (callbacks != null) {
                            callbacks.onWizardStepsCompleted(wizardViewModel);
                        }
                    } else if (viewPager != null) {

                        Fragment fragment = viewPager.getCurrentItem() + 1 >= adapter.getCount() ? null : adapter.getItem(viewPager.getCurrentItem() + 1);
                        if (fragment != null) {
                            sendViewWizardEvent((IWizardFragment) fragment);
                        }

                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);

                    }
                }

            }
        });

        Log.d(TAG, "send data " + wizardViewModel);
    }

    private void sendViewWizardEvent(IWizardFragment fragment) {

        Analytics.sendEvent(Analytics.EVENT_WIZARD_VIEW_PAGE, fragment.getFragmentName());
        HPLogger.d(TAG, "wizard view " + fragment.getFragmentName());

    }

    private void sendWizardClickBackEvent(Fragment fragment) {

        Analytics.sendEvent(Analytics.EVENT_WIZARD_CLICK_BACK, ((IWizardFragment) fragment).getFragmentName());
        HPLogger.d(TAG, "wizard click back " + ((IWizardFragment) fragment).getFragmentName());

    }

    private void sendWizardClickNextEvent(Fragment fragment) {

        Analytics.sendEvent(Analytics.EVENT_WIZARD_CLICK_NEXT, ((IWizardFragment) fragment).getFragmentName());
        HPLogger.d(TAG, "wizard click next " + ((IWizardFragment) fragment).getFragmentName());

    }

    void showLoadingIndicator(boolean show) {
        HPUIUtils.setVisibility(show, loadingIndicator);
    }

    void enableNextButton(boolean enable) {
        nextButton.setEnabled(enable);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_wizard;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.wizard_fragment_title;
    }

    @Override
    public boolean onBackPressed() {

        if (loadingIndicator.getVisibility() == View.VISIBLE) {
            return true;
        }

        if (adapter == null || viewPager == null || stepsCompleted) {
            return false;
        }

        if (viewPager.getCurrentItem() >= 0) {

            if (viewPager.getCurrentItem() == 0) {

                if (collectCoinsEnabled) {
                    HPLogger.d(TAG, "abort wizard.");
                    Analytics.sendEvent(Analytics.EVENT_WIZARD_ABORT);
                }

                return false;
            }

            Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
            if (fragment instanceof IWizardFragment) {
                sendWizardClickBackEvent(fragment);
            }

            Fragment prevFragment = viewPager.getCurrentItem() - 1 >= adapter.getCount() ? null : adapter.getItem(viewPager.getCurrentItem() - 1);

            if (prevFragment != null && prevFragment instanceof IWizardFragment) {
                sendViewWizardEvent((IWizardFragment) prevFragment);
            }

            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);


            return true;

        } else if (callbacks != null) {
            callbacks.onDismissButtonClicked();
        }

        return false;
    }

    @Override
    public void onCollectCoinsClicked() {
        if (callbacks != null) {
            callbacks.onWizardStepsCompleted(wizardViewModel);
        }
    }

    @Override
    public void onUserProfilePhotoClicked() {
        if (callbacks != null) {
            callbacks.onUserProfilePhotoClicked();
        }
    }

    public void updateProfilePhotoWithUrl(String url) {

        if (adapter != null && !adapter.getFragments().isEmpty()) {

            UserPersonalInfoFragment personalInfoFragment = (UserPersonalInfoFragment) adapter.getFragments().get(0);
            if (personalInfoFragment != null) {
                personalInfoFragment.updateProfileImage(url);
            }
        }

    }

    @Override
    public void onCancel() {

    }

    private static class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> fragments = new ArrayList<>();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment) {
            fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragments.set(position, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public List<Fragment> getFragments() {
            return fragments;
        }

    }

    public interface WizardFragmentCallbacks {

        void onDismissButtonClicked();

        void onWizardStepsCompleted(WizardViewModel wizardViewModel);

        void onUserProfilePhotoClicked();

        void onWizardFragmentAttached();
    }

}
