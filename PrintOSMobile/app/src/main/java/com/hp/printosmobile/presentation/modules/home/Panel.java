package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.dailyspotlight.SpotlightPanel;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel;
import com.hp.printosmobile.presentation.modules.insights.KpiInsightPanel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionPanel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.today.HistogramPanel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;

/**
 * Created by Osama Taha on 5/18/16.
 */
public enum Panel {

    LATEX_PRODUCTION(1, LatexProductionPanel.TAG, "LatexJobsPanel"),
    HISTOGRAM(2, HistogramPanel.TAG, "DailyPrintVolumePanel"),
    TODAY(3, TodayPanel.TAG, "ProductionPanel"),
    PRINTERS(4, PrinterSnapshotPanel.TAG, ""),
    DAILY_SPOTLIGHT(5, SpotlightPanel.TAG, "DailySpotLightPanel"),
    RANKING(6, RankingPanel.TAG, "RankingPanel"),
    PERFORMANCE(7, WeekPanel.TAG, "WeekPanel"),
    PERSONAL_ADVISOR(8, PersonalAdvisorPanel.TAG, ""),
    INDIGO_PRODUCTION(9, IndigoProductionPanel.TAG, "IndigoJobsPanel"),
    PANEL_SERVICE_CALL(10, ServiceCallPanel.TAG, "ServiceCallsPanel"),
    JAM_CHART(11, InsightsViewModel.InsightKpiEnum.JAM.getTag(), "JamsChartPanel"),
    FAILURE_CHART(12, InsightsViewModel.InsightKpiEnum.FAILURE.getTag(), "FailureChartPanel"),
    UNKNOWN(-1, "", "");

    private int panelOrder;
    private String panelClass;
    private String deepLinkTag;

    Panel(int panelOrder, String panelClass, String deepLinkTag) {
        this.panelOrder = panelOrder;
        this.panelClass = panelClass;
        this.deepLinkTag = deepLinkTag;
    }

    public int getPanelOrder() {
        return panelOrder;
    }

    public String getPanelTag() {
        return panelClass;
    }

    public String getDeepLinkTag() {
        return deepLinkTag;
    }

    public static Panel from(String panelTag) {
        for (Panel panel : Panel.values()) {
            if (panel.getPanelTag().equals(panelTag))
                return panel;
        }
        return Panel.UNKNOWN;
    }

    public static Panel fromDeepLinkTag(String panelTag) {
        for (Panel panel : Panel.values()) {
            if (panel.getDeepLinkTag().equals(panelTag))
                return panel;
        }
        return Panel.UNKNOWN;
    }
}
