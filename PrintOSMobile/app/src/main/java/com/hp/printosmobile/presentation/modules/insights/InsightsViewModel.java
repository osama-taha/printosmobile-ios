package com.hp.printosmobile.presentation.modules.insights;

import android.os.Build;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.InsightData;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 6/14/2017.
 */

public class InsightsViewModel {

    public static final Comparator<InsightModel> COMPARATOR = new Comparator<InsightModel>() {
        @Override
        public int compare(InsightModel lhs, InsightModel rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null) return 1;
            if (rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };

    public enum InsightKpiEnum {
        JAM("Jams", R.string.insights_top_jams, R.drawable.insights_jams, R.drawable.corrective_actions_jams),
        FAILURE("Failures", R.string.insights_top_failure, R.drawable.insights_failures, R.drawable.corrective_actions_failures);

        private final String tag;
        private final int titleStringId;
        private final int drawableId;
        private final int correctiveActionDrawableId;

        InsightKpiEnum(String tag, int titleStringId, int drawableId, int correctiveActionDrawableId) {
            this.tag = tag;
            this.titleStringId = titleStringId;
            this.drawableId = drawableId;
            this.correctiveActionDrawableId = correctiveActionDrawableId;
        }

        public String getTag() {
            return tag;
        }

        public int getDrawableId() {
            return drawableId;
        }

        public int getTitleStringId() {
            return titleStringId;
        }

        public int getCorrectiveActionDrawableId() {
            return correctiveActionDrawableId;
        }
    }

    private InsightKpiEnum insightKpiEnum;
    private List<InsightModel> insightModels;
    private int totalDeviceCount;
    private int selectedDeviceCount;

    public List<InsightModel> getInsightModels() {
        return insightModels;
    }

    public void setInsightModels(List<InsightModel> insightModels) {
        this.insightModels = insightModels;
    }

    public InsightKpiEnum getInsightKpiEnum() {
        return insightKpiEnum;
    }

    public void setInsightKpiEnum(InsightKpiEnum insightKpiEnum) {
        this.insightKpiEnum = insightKpiEnum;
    }

    public int getTotalDeviceCount() {
        return totalDeviceCount;
    }

    public void setTotalDeviceCount(int totalDeviceCount) {
        this.totalDeviceCount = totalDeviceCount;
    }

    public int getSelectedDeviceCount() {
        return selectedDeviceCount;
    }

    public void setSelectedDeviceCount(int selectedDeviceCount) {
        this.selectedDeviceCount = selectedDeviceCount;
    }

    public static class InsightModel implements Comparable<InsightModel>, Serializable {

        private int percentage;
        private String label;
        private int occurrences;
        private int color;
        private int insightCursorDrawableID;
        private List<InsightData.Insight.Hint> correctiveActions;
        private boolean hasCorrectiveActions;
        private InsightKpiEnum kpi;
        private Date dateFrom;
        private Date dateTo;

        public int getPercentage() {
            return percentage;
        }

        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int colorResourceId) {
            this.color = colorResourceId;
        }

        public int getOccurrences() {
            return occurrences;
        }

        public void setOccurrences(int occurrences) {
            this.occurrences = occurrences;
        }

        public int getInsightCursorDrawableID() {
            return insightCursorDrawableID;
        }

        public void setInsightCursorDrawableID(int insightCursorDrawableID) {
            this.insightCursorDrawableID = insightCursorDrawableID;
        }

        public void setCorrectiveActions(List<InsightData.Insight.Hint> hints) {
            this.correctiveActions = hints;
        }

        public List<InsightData.Insight.Hint> getCorrectiveActions() {
            return correctiveActions;
        }


        public void setHasCorrectiveActions(boolean hasCorrectiveActions) {
            this.hasCorrectiveActions = hasCorrectiveActions;
        }

        public boolean hasCorrectiveActions() {
            return hasCorrectiveActions;
        }

        public void setKpi(InsightKpiEnum kpi) {
            this.kpi = kpi;
        }

        public InsightKpiEnum getKpi() {
            return kpi;
        }

        public void setDateFrom(Date dateFrom) {
            this.dateFrom = dateFrom;
        }

        public Date getDateFrom() {
            return dateFrom;
        }

        public void setDateTo(Date dateTo) {
            this.dateTo = dateTo;
        }

        public Date getDateTo() {
            return dateTo;
        }

        @Override
        public int compareTo(InsightModel insightModel) {
            if (insightModel == null) {
                return 1;
            }

            if (insightModel.getPercentage() == getPercentage()) {
                return 0;
            } else if (insightModel.getPercentage() < getPercentage()) {
                return -1;
            } else {
                return 1;
            }
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(percentage, label, occurrences, color, insightCursorDrawableID, correctiveActions, hasCorrectiveActions, kpi, dateFrom, dateTo);
            }
            return 0;
        }
    }
}
