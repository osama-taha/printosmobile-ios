package com.hp.printosmobile.data.remote.models.kz;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 3/19/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KZAssetResult {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("ratingResponse")
    private RatingResponse ratingResponse;
    @JsonProperty("subtitleEntity")
    private List<SubtitleEntity> subtitleEntity;
    @JsonProperty("external_url")
    private String externalUrl;
    @JsonProperty("format")
    private String format;
    @JsonProperty("language")
    private String language;
    @JsonProperty("modificationDate")
    private String modificationDate;
    @JsonProperty("provider")
    private KZItem.Provider provider;
    @JsonProperty("highlight")
    private String highlight;
    @JsonProperty("userAssetPref")
    private UserAssetPref userAssetPref;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("ratingResponse")
    public RatingResponse getRatingResponse() {
        return ratingResponse;
    }

    @JsonProperty("ratingResponse")
    public void setRatingResponse(RatingResponse ratingResponse) {
        this.ratingResponse = ratingResponse;
    }

    @JsonProperty("subtitleEntity")
    public List<SubtitleEntity> getSubtitleEntity() {
        return subtitleEntity;
    }

    @JsonProperty("subtitleEntity")
    public void setSubtitleEntity(List<SubtitleEntity> subtitleEntity) {
        this.subtitleEntity = subtitleEntity;
    }

    @JsonProperty("external_url")
    public String getExternalUrl() {
        return externalUrl;
    }

    @JsonProperty("external_url")
    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    @JsonProperty("userAssetPref")
    public UserAssetPref getUserAssetPref() {
        return userAssetPref;
    }

    @JsonProperty("userAssetPref")
    public void setUserAssetPref(UserAssetPref userAssetPref) {
        this.userAssetPref = userAssetPref;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("format")
    public String getFormat() {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(String format) {
        this.format = format;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("modificationDate")
    public String getModificationDate() {
        return modificationDate;
    }

    @JsonProperty("modificationDate")
    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    @JsonProperty("provider")
    public KZItem.Provider getProvider() {
        return provider;
    }

    @JsonProperty("provider")
    public void setProvider(KZItem.Provider provider) {
        this.provider = provider;
    }

    @JsonProperty("highlight")
    public String getHighlight() {
        return highlight;
    }

    @JsonProperty("highlight")
    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubtitleEntity {

        @JsonProperty("id")
        private Integer id;
        @JsonProperty("storageServiceUUID")
        private String storageServiceUUID;
        @JsonProperty("name")
        private String name;
        @JsonProperty("languageEntity")
        private LanguageEntity languageEntity;
        @JsonProperty("preSignedUrl")
        private String preSignedUrl;
        @JsonProperty("bu")
        private String bu;
        @JsonProperty("assetId")
        private Integer assetId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("id")
        public Integer getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(Integer id) {
            this.id = id;
        }

        @JsonProperty("storageServiceUUID")
        public String getStorageServiceUUID() {
            return storageServiceUUID;
        }

        @JsonProperty("storageServiceUUID")
        public void setStorageServiceUUID(String storageServiceUUID) {
            this.storageServiceUUID = storageServiceUUID;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("languageEntity")
        public LanguageEntity getLanguageEntity() {
            return languageEntity;
        }

        @JsonProperty("languageEntity")
        public void setLanguageEntity(LanguageEntity languageEntity) {
            this.languageEntity = languageEntity;
        }

        @JsonProperty("preSignedUrl")
        public String getPreSignedUrl() {
            return preSignedUrl;
        }

        @JsonProperty("preSignedUrl")
        public void setPreSignedUrl(String preSignedUrl) {
            this.preSignedUrl = preSignedUrl;
        }

        @JsonProperty("bu")
        public String getBu() {
            return bu;
        }

        @JsonProperty("bu")
        public void setBu(String bu) {
            this.bu = bu;
        }

        @JsonProperty("assetId")
        public Integer getAssetId() {
            return assetId;
        }

        @JsonProperty("assetId")
        public void setAssetId(Integer assetId) {
            this.assetId = assetId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class LanguageEntity {

        @JsonProperty("id")
        private Integer id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("langAnsi")
        private String langAnsi;
        @JsonProperty("langTwoLetterAnsi")
        private String langTwoLetterAnsi;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("id")
        public Integer getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(Integer id) {
            this.id = id;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("langAnsi")
        public String getLangAnsi() {
            return langAnsi;
        }

        @JsonProperty("langAnsi")
        public void setLangAnsi(String langAnsi) {
            this.langAnsi = langAnsi;
        }

        @JsonProperty("langTwoLetterAnsi")
        public String getLangTwoLetterAnsi() {
            return langTwoLetterAnsi;
        }

        @JsonProperty("langTwoLetterAnsi")
        public void setLangTwoLetterAnsi(String langTwoLetterAnsi) {
            this.langTwoLetterAnsi = langTwoLetterAnsi;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}
