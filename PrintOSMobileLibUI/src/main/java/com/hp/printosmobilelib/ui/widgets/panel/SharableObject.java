package com.hp.printosmobilelib.ui.widgets.panel;

/**
 * Created by Anwar Asbah on 11/12/2017.
 */
public interface SharableObject {
    String getShareAction();
}
